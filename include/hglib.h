// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

// include util headers
#include "hglib/utils.h"

// include vertex headers
#include "hglib/vertex.h"

// include hyperedge headers
#include "hglib/hyperedge.h"

// include hypergraph headers
#include "hglib/hypergraphs.h"

// include filtered_vector headers
#include "filtered_vector.h"

// include algorithms
#include "hglib/algorithms.h"

// include exceptions
#include "hglib/exceptions.h"

/**
 * \defgroup hglibScope Typedefs, enums, variables in the hglib namespace
 */
