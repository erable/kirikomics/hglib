// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_DIRECTEDVERTEX_H
#define HGLIB_DIRECTEDVERTEX_H


#include "hglib/vertex/VertexBase.h"

#include <memory>
#include <vector>

#include "hglib/utils/IsParentOf.h"
#include "hglib/utils/types.h"
#include "hglib/hyperedge/directed/DirectedHyperedgeBase.h"

namespace hglib {

template <typename DirectedVertex_t>
class DirectedHyperedgeBase;

/**
 * \brief Basic class of a directed vertex
 *
 * \details
 * This is a base class of directed vertices. It contains two containers, one
 * for incoming and another one for outgoing hyperedges. Furthermore,
 * attributes that are specific to a type of directed vertex are stored in an
 * object of the nested class SpecificAttributes (class without attributes in
 * the class DirectedVertex). This architecture allows to add any kind of
 * directed vertex to a directed hypergraph via the function addVertex(). One
 * can use a custom class of a directed vertex in a directed hypergraph under
 * the condition that this class is derived from DirectedVertex and that it
 * contains a nested class SpecificAttributes.
 *
 * \tparam Hyperarc_t Type that is used to store incoming and outgoing directed
 * hyperedges (of type Hyperarc_t).
 */
template <typename Hyperarc_t>
class DirectedVertex : public VertexBase {
  template <template <typename> typename Vertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class Hypergraph;
  template <template <typename> typename Vertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class DirectedHypergraph;
  template <template <typename> typename Vertex_t, typename Hyperedge_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class DirectedSubHypergraph;

 public:
  // Assert that Hyperarc_t is derived from DirectedHyperedgeBase
  static_assert(is_parent_of<DirectedHyperedgeBase, Hyperarc_t>::value,
                "Template parameter of DirectedVertex must be a class derived "
                        "from DirectedHyperedgeBase");
  /// Alias to make the template parameter publicily available
  using HyperarcType = Hyperarc_t;

 protected:
  /// DirectedVertex constructor
  DirectedVertex(VertexIdType id, std::string name,
                 const SpecificAttributes& specificAttributes);
  /// DirectedVertex constructor
  DirectedVertex(VertexIdType id,
                 const SpecificAttributes& specificAttributes);
  /// DirectedVertex copy constructor
  DirectedVertex(const DirectedVertex& other);
  /// Destructor
  ~DirectedVertex() {}
  /// Add an outgoing directed hyperedge
  void addOutHyperarc(Hyperarc_t* hyperarc);
  /// Add an incoming directed hyperedge
  void addInHyperarc(Hyperarc_t* hyperarc);
  /// Remove an outgoing directed hyperedge
  void removeOutHyperarc(const Hyperarc_t& hyperarcToBeDeleted);
  /// Remove an incoming directed hyperedge
  void removeInHyperarc(const Hyperarc_t& hyperarcToBeDeleted);
  /// Get arguments needed to create this DirectedVertex
  ArgumentsToCreateVertex<
          DirectedVertex<Hyperarc_t>> argumentsToCreateVertex() const;
  /// Compare function
  bool compare(const std::string& name, const SpecificAttributes& args) const;

 protected:
  /// Incoming directed hyperedges
  std::shared_ptr<GraphElementContainer<Hyperarc_t*>> inHyperarcs_;
  /// Outgoing directed hyperedges
  std::shared_ptr<GraphElementContainer<Hyperarc_t*>> outHyperarcs_;
};

}  // namespace hglib

#include "DirectedVertex.hpp"

#endif  // HGLIB_DIRECTEDVERTEX_H
