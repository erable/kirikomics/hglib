// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "ArgumentsToCreateVertex.h"

namespace hglib {

/**
 * \brief ArgumentsToCreateVertex constructor
 *
 * \param name Name of the vertex.
 * \param specificAttributes Attributes that are specific to the type of vertex.
 */
template<typename Vertex_t>
ArgumentsToCreateVertex<Vertex_t>::ArgumentsToCreateVertex(
        const std::string& name,
        const typename Vertex_t::SpecificAttributes& specificAttributes) :
        name_(name),
        specificAttributes_(specificAttributes) {}


/**
 * \brief Get the name of the vertex
 *
 * \return Name of the vertex.
 */
template<typename Vertex_t>
const std::string& ArgumentsToCreateVertex<Vertex_t>::
name() const {
  return name_;
}

/**
 * \brief Get specific attributes
 *
 * \return Attributes specific to the vertex type.
 */
template<typename Vertex_t>
const typename Vertex_t::SpecificAttributes&
ArgumentsToCreateVertex<Vertex_t>::
specificAttributes() const {
  return specificAttributes_;
}


/**
 * \brief Set name of the vertex
 *
 * \param name Name of the vertex.
 */
template<typename Vertex_t>
void ArgumentsToCreateVertex<Vertex_t>::
setName(const std::string& name) {
  name_ = name;
}


/**
 * \brief Set attributes that are specific to the vertex type (Vertex_t)
 *
 * \param args Attributes that are specific to the vertex type.
 */
template<typename Vertex_t>
void ArgumentsToCreateVertex<Vertex_t>::
setSpecificArguments(const typename Vertex_t::SpecificAttributes& args) {
  specificAttributes_ = args;
}
}  // namespace hglib
