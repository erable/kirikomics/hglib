// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "UndirectedVertex.h"

#include <memory>

#include "filtered_vector.h"

namespace hglib {

/**
 * \brief UndirectedVertex constructor
 *
 * \param id Identifier of the vertex.
 * \param name Name of the vertex.
 * \param specificAttributes Specific arguments of UndirectedVertex.
 */
template <typename Hyperedge_t>
UndirectedVertex<Hyperedge_t>::
UndirectedVertex(VertexIdType id, std::string name,
                 const SpecificAttributes& specificAttributes) :
        VertexBase(id, name) {
  hyperedges_ = create_filtered_vector<Hyperedge_t*>();
  specificAttributes_ =
      std::make_unique<SpecificAttributes>(specificAttributes);
}


/**
 * \brief UndirectedVertex constructor
 *
 * \details
 * This constructor creates a vertex with the name 'udv_' appended by the given
 * identifier.
 *
 * \param id Identifier of the vertex.
 * \param specificAttributes Specific arguments of UndirectedVertex.
 */
template <typename Hyperedge_t>
UndirectedVertex<Hyperedge_t>::
UndirectedVertex(VertexIdType id,
                 const SpecificAttributes& specificAttributes) :
        UndirectedVertex(id,
                         "udv_" + std::to_string(id),
                         specificAttributes) {}


/**
 * \brief UndirectedVertex copy constructor
 *
 * \param other The to be copied UndirectedVertex.
 */
template <typename Hyperedge_t>
UndirectedVertex<Hyperedge_t>::
UndirectedVertex(const UndirectedVertex& other) : VertexBase(other) {
  hyperedges_ = create_filtered_vector<Hyperedge_t*>();
  specificAttributes_ =
      std::make_unique<SpecificAttributes>(*(other.specificAttributes_));
}


/**
 * \brief Add an undirected hyperedge
 *
 * \param hyperedge Pointer to an undirected hyperedge.
 *
 * \par Complexity
 * Constant (amortized time, reallocation may happen).\n
 * If a reallocation happens, the reallocation is itself up to linear in the
 * entire size of the container.
 */
template <typename Hyperedge_t>
void UndirectedVertex<Hyperedge_t>::
addHyperedge(Hyperedge_t* hyperedge) {
  hyperedges_->push_back(hyperedge);
}


/**
 * \brief Remove an undirected hyperedge
 *
 * \param hyperedgeToBeDeleted Const reference to an undirected hyperedge.
 *
 * \par Complexity
 * Linear
 */
template <typename Hyperedge_t>
void UndirectedVertex<Hyperedge_t>::
removeHyperedge(const Hyperedge_t& hyperedgeToBeDeleted) {
  // We suppose that there is at most one occurrence of the given hyperedge
  // in the hyperedges.
  auto newEnd = std::remove(hyperedges_->data(),
                            hyperedges_->data() + hyperedges_->size(),
                            &hyperedgeToBeDeleted);
  // new end is different from the old one
  if (newEnd != (hyperedges_->data() + hyperedges_->size())) {
    hyperedges_->resize(hyperedges_->size() - 1);
  }
}


/**
 * \brief Get arguments needed to create this UndirectedVertex
 *
 * \details
 * This function returns an instance of ArgumentsToCreateVertex that
 * provides all arguments (name, specific attributes) needed to add this
 * undirected vertex to a hypergraph.
 *
 * \return ArgumentsToCreateVertex<UndirectedVertex<Hyperedge_t>> Arguments to
 * add an undirected vertex to a hypergraph via the function addVertex().
 */
template <typename Hyperedge_t>
ArgumentsToCreateVertex<UndirectedVertex<Hyperedge_t>> UndirectedVertex<
        Hyperedge_t>::
argumentsToCreateVertex() const {
  return ArgumentsToCreateVertex<UndirectedVertex<Hyperedge_t>>(
          name_, *specificAttributes_);
}


/**
 * \brief Compare function
 *
 * \details
 * Compare the given name with the vertex name. The second parameter is ignored
 * in the DirectedVertex class because the nested class SpecificAttributes is
 * empty and thus there is nothing to compare. However, other classes modeling
 * directed vertices may contain attributes in this nested class. The function
 * is called independent of the vertex type in the Hypergraph as
 * vertex.compare(someName, someSpecArgs);
 *
 * \param name String to compare with the vertex name.
 * \param SpecificAttributes Specific arguments of DirectedVertex.
 *
 * \return true if the given name is identical to this vertex name, false
 * otherwise.
 */
template <typename Hyperedge_t>
bool UndirectedVertex<Hyperedge_t>::
compare(const std::string& name, const SpecificAttributes&) const {
  return (name.compare(name_) == 0);
}
}  // namespace hglib
