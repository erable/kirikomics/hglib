/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

namespace hglib {
/**
 * \brief VertexBase constructor
 *
 * \param id Identifier of the vertex.
 * \param name Name of the vertex.
 */
VertexBase::VertexBase(VertexIdType id, std::string name) :
    id_(id), name_(name) {}


/**
 * \brief VertexBase constructor
 *
 * \details
 * This constructor creates a VertexBase with a default name 'v_' appended by
 * the given identifier.
 *
 * \param id Identifier of the vertex.
 */
VertexBase::VertexBase(VertexIdType id) : id_(id),
                                          name_("v_" + std::to_string(id)) {}


/**
 * \brief VertexBase copy constructor
 *
 * \param other The to be copied VertexBase.
 */
VertexBase::VertexBase(const VertexBase& other) : id_(other.id_),
                                                  name_(other.name_) {}

/**
 * \brief Get the identifier of the vertex
 *
 * \return Identifer of the vertex.
 */
VertexIdType VertexBase::id() const {
  return id_;
}

/**
 * \brief Get the name of the vertex
 *
 * \return Name of the vertex.
 */
std::string VertexBase::name() const {
  return name_;
}


/**
 * \brief Print the vertex
 *
 * \details
 * Prints the name of the vertex.
 *
 * \param out std::ostream
 */
void VertexBase::print(std::ostream& out) const {
  out << "\"" << name_ << "\"";
}

/**
 * \brief Assign an identifier to the vertex
 *
 * \param id Identifier to be assigned.
 */
void VertexBase::
setId(const VertexIdType& id) {
  id_ = id;
}

/// Operator<<
inline std::ostream& operator<< (std::ostream& out,
                                 const VertexBase& vertex) {
  vertex.print(out);
  return out;
}

}  // namespace hglib
