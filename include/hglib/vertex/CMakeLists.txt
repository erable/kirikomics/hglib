
# Append files in this directory to header files list
list(APPEND ${PROJECT_NAME}_HEADER_FILES
  ${CMAKE_CURRENT_LIST_DIR}/ArgumentsToCreateVertex.h
  ${CMAKE_CURRENT_LIST_DIR}/ArgumentsToCreateVertex.hpp
  ${CMAKE_CURRENT_LIST_DIR}/VertexBase.h
  ${CMAKE_CURRENT_LIST_DIR}/VertexBase.hpp)

include(${CMAKE_CURRENT_LIST_DIR}/directed/CMakeLists.txt)
include(${CMAKE_CURRENT_LIST_DIR}/undirected/CMakeLists.txt)
