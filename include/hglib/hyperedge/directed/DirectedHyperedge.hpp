/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <memory>

namespace hglib {

/**
 * \brief DirectedHyperedge constructor
 *
 * \param id Identifier of the DirectedHyperedge.
 * \param tails Shared_ptr to the container of pointers to tail vertices.
 * \param heads Shared_ptr to the container of pointers to head vertices.
 * \param specificAttributes Const reference to SpecificAttributes.
 */
DirectedHyperedge::DirectedHyperedge(
    int id,
    std::shared_ptr<GraphElementContainer<Vertex_t*>> tails,
    std::shared_ptr<GraphElementContainer<Vertex_t*>> heads,
    std::unique_ptr<SpecificAttributes>&& specificAttributes) :
    DirectedHyperedgeBase(id, tails, heads, std::move(specificAttributes)) {
}


/**
 * \brief DirectedHyperedge copy constructor
 *
 * \param other To be copied DirectedHyperedge.
 */
DirectedHyperedge::DirectedHyperedge(const DirectedHyperedge& other) :
    DirectedHyperedgeBase(other) {}


/**
 * \brief Get arguments to create this DirectedHyperedge
 *
 * \details
 * This function returns an instance of ArgumentsToCreateDirectedHyperedge that
 * provides all arguments (tails and head Ids, specific attributes) to add this
 * directed hyperedge to a hypergraph.
 *
 * \par Example
 * \parblock
 * \code
 *
 * // Get arguments to create directed hyperedge with Id hyperarcId
 * auto args = g.argumentsToCreateHyperarc(hyperarcId);
 * // Add directed hyperedge to another hypergraph g2. It is assumed that the
 * // tail and head vertices are present in the hypergraph g2.
 * const auto& hyperparc2 = g2.addHyperarc(args.tailAndHeadIds(),
 *                                         args.specificAttributes());
 * \endcode
 * \endparblock
 *
 * \return ArgumentsToCreateDirectedHyperedge\<DirectedHyperedge\> Arguments to
 * add a directed hyperedge to a hypergraph via the function addHyperarc().
 */
ArgumentsToCreateDirectedHyperedge<DirectedHyperedge> DirectedHyperedge::
argumentsToCreateHyperarc() const {
  std::vector<hglib::VertexIdType> tails, heads;
  for (const auto vertex : *(tails_.get())) {
    tails.push_back(vertex->id());
  }
  for (const auto vertex : *(heads_.get())) {
    heads.push_back(vertex->id());
  }
  return ArgumentsToCreateDirectedHyperedge<DirectedHyperedge>(
      std::make_pair(tails, heads),
      specificAttributes(*this));
}

/**
 * \brief Compare function
 *
 * \details
 * Compare if the Ids of the tail and head vertices
 * are identical to the attributes of this DirectedHyperedge.
 *
 * \param tails Pointers to tail vertices.
 * \param heads Pointers to head vertices.
 * \param SpecificAttributes Specific attributes of the hyperarc type (none
 * for DirectedHyperedge).
 *
 * \return True if this DirectedHyperedge has the same tail and head vertex
 * Ids, false otherwise.
 *
 * \par Complexity
 * Linear in the number of tail/head vertices of this and the given pointers to
 * vertices.
 */
bool DirectedHyperedge::
compare(const std::vector<const Vertex_t*>& tails,
        const std::vector<const Vertex_t*>& heads,
        const SpecificAttributes&) const {
  return DirectedHyperedgeBase<Vertex_t >::compare(tails, heads);
}


/**
 * \brief Print the DirectedHyperedge
 *
 * \details
 * Print the directed hyperedge in the following format:
 * Tail_1 + ... + Tail_n -> Head_1 + ... + Head_m
 *
 * \param out std::ostream
 */
void DirectedHyperedge::
print(std::ostream& out) const {
  DirectedHyperedgeBase<Vertex_t>::printTailsAndHeads(out);
}

}  // namespace hglib
