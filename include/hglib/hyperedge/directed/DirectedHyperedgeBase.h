// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_DIRECTEDHYPEREDGEBASE_H
#define HGLIB_DIRECTEDHYPEREDGEBASE_H

#include "hglib/hyperedge/HyperedgeBase.h"

#include <memory>
#include <vector>
#include <tuple>
#include <utility>
#include <string>

#include "hglib/vertex/directed/DirectedVertex.h"
#include "hglib/utils/types.h"

namespace hglib {

template <typename Hyperarc_t>
class DirectedVertex;

/**
 * \brief Base template class of a directed hyperedge
 *
 * \details
 * This base template class of a directed hyperedge contains attributes that are
 * common to all types of directed hyperedges, that is an identifier, and a
 * container of tail and head vertices. We use the term directed hyperedge
 * instead of hyperarc to make the distinction between a directed and an
 * undirected hyperedge easier to grasp for people that do not work in the field
 * of graph theory.\n
 * A directed hyperedge relates a set of tail vertices with a set of head
 * vertices.\n
 * The template parameter specifies the type of vertices in the directed
 * hyperedge.\n
 * This class is an abstract class. To create a directed hypergraph with some
 * type of directed hyperedge, one needs to use a derived class of
 * DirectedHyperedgeBase, \em e.g. DirectedHyperedge or NamedDirectedHyperedge.
 * One may derive a class \em A from DirectedHyperedgeBase to model a custom
 * directed hyperedge. The class \em A must derive from
 * DirectedHyperedgeBase<T>, where \em T is a directed vertex type (\em e.g.
 * DirectedVertex) which itself is  a class template. The template parameter
 * of the directed vertex type is the type of directed hyperedge,
 * \em e.g. \em A. Thus, the class \em A may derive from
 * DirectedHyperedgeBase\<DirectedVertex\<A\>\>. Any other directed vertex type
 * class (must be derived from DirectedVertex) can be used in this
 * derivation. This kind of curiously recurring template pattern (CRTP) is used
 * to achieve static polymorphism.\n\n
 * Editing a directed hyperedge, \em e.g. adding/removing a tail/head vertex is
 * done via the directed hypergraph.
 *
 *
 * \tparam DirectedVertex_t Type of a directed vertex class template.
 */
template <typename DirectedVertex_t>
class DirectedHyperedgeBase : public HyperedgeBase {
  template <template <typename> typename Vertex_t, typename Hyperarc_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class DirectedHypergraph;
  template <template <typename> typename Vertex_t, typename Hyperarc_t,
          typename VertexProperty, typename EdgeProperty,
          typename HypergraphProperty>
  friend class DirectedSubHypergraph;

 protected:
  /// DirectedHyperedgeBase constructor
  DirectedHyperedgeBase(HyperedgeIdType id,
               std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> tails,
               std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> heads,
               std::unique_ptr<SpecificAttributes>&& specificAttributes);
  /// DirectedHyperedgeBase copy constructor
  DirectedHyperedgeBase(const DirectedHyperedgeBase& hyperarc);

 public:
  /// Destructor
  virtual ~DirectedHyperedgeBase() = default;

 public:
  /// Print the directed hyperedge
  void print(std::ostream& out) const;

 protected:
  /// Remove a tail vertex from the directed hyperedge
  void removeTailVertex(const DirectedVertex_t& vertexToBeDeleted);
  /// Remove a head vertex from the directed hyperedge
  void removeHeadVertex(const DirectedVertex_t& vertexToBeDeleted);
  /// Compare tails and heads
  bool compare(const std::vector<const DirectedVertex_t*>& tails,
               const std::vector<const DirectedVertex_t*>& heads) const;
  /// Print tail and head vertex names of the directed hyperedge
  void printTailsAndHeads(std::ostream& out) const;
  /// Add a tail vertex
  void addTailVertex(DirectedVertex_t* vertex);
  /// Add a head vertex
  void addHeadVertex(DirectedVertex_t* vertex);

 protected:
  /// Tail vertices of the hyperarc
  std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> tails_;
  /// Head vertices of the hyperarc
  std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> heads_;
};

template <typename DirectedVertex_t>
std::ostream& operator<< (
        std::ostream& out,
        const DirectedHyperedgeBase<DirectedVertex_t>& hyperarc) {
  hyperarc.print(out);
  return out;
}

}  // namespace hglib

#include "DirectedHyperedgeBase.hpp"
#endif  // HGLIB_DIRECTEDHYPEREDGEBASE_H
