// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_ARGUMENTSTOCREATEDIRECTEDHYPEREDGE_H
#define HGLIB_ARGUMENTSTOCREATEDIRECTEDHYPEREDGE_H

#include <vector>
#include <utility>

#include "hglib/utils/types.h"

namespace hglib {

/**
 * \brief ArgumentsToCreateDirectedHyperedge class
 *
 * \details
 * A wrapper class that holds the arguments needed to create a directed
 * hyperedge of the template parameter type. This includes the tail and head
 * vertex Ids, and attributes that are specific to the directed hyperedge type.
 *
 * \tparam Hyperarc_t Type of directed hyperedge.
 */
template<typename Hyperarc_t>
class ArgumentsToCreateDirectedHyperedge {
  friend Hyperarc_t;

 public:
  /// Alias for data structure to provide tail and head ids
  using TailAndHeadIds =
  std::pair<std::vector<hglib::VertexIdType>, std::vector<hglib::VertexIdType>>;

 protected:
  /// ArgumentsToCreateDirectedHyperedge constructor
  ArgumentsToCreateDirectedHyperedge(
          const TailAndHeadIds& tailAndHeadIds,
          const typename Hyperarc_t::SpecificAttributes& specificAttributes);

 public:
  /// Get identifiers of tail and head vertices
  const TailAndHeadIds& tailAndHeadIds() const;
  /// Get specific attributes
  const typename Hyperarc_t::SpecificAttributes& specificAttributes() const;
  /// Set identifiers of tail and head vertices
  void setTailsAndHeadIds(const TailAndHeadIds&);
  /// Set attributes that are specific to the directed hyperedge type
  void setSpecificArguments(const typename Hyperarc_t::SpecificAttributes&);

 protected:
  /// Identifiers of the tail and head vertices
  TailAndHeadIds tailAndHeadIds_;
  /// Specific attributes for the directed hyperedge type
  typename Hyperarc_t::SpecificAttributes specificAttributes_;
};
}  // namespace hglib

#include "ArgumentsToCreateDirectedHyperedge.hpp"
#endif  // HGLIB_ARGUMENTSTOCREATEDIRECTEDHYPEREDGE_H
