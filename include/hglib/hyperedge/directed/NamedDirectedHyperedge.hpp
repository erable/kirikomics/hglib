/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <memory>

namespace hglib {

/**
 * \brief NamedDirectedHyperedge constructor
 *
 * \param id Identifier of the DirectedHyperedge.
 * \param tails Shared_ptr to the container of pointers to tail vertices.
 * \param heads Shared_ptr to the container of pointers to head vertices.
 * \param specificAttributes Const reference to SpecificAttributes (contains
 * the name).
 */
NamedDirectedHyperedge::
NamedDirectedHyperedge(
    int id,
    std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> tails,
    std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> heads,
    std::unique_ptr<SpecificAttributes>&& specificAttributes) :
    DirectedHyperedgeBase(id, tails, heads, std::move(specificAttributes)) {
}


/**
 * \brief NamedDirectedHyperedge copy constructor
 *
 * \param other To be copied NamedDirectedHyperedge.
 */
NamedDirectedHyperedge::
NamedDirectedHyperedge(const NamedDirectedHyperedge& other) :
    DirectedHyperedgeBase(other) {}


/**
 * \brief Get arguments to create this NamedDirectedHyperedge
 *
 * \details
 * This function returns an instance of ArgumentsToCreateDirectedHyperedge that
 * provides all arguments (tails and head Ids, specific attributes) to add this
 * named directed hyperedge to a hypergraph.
 *
 * \par Example
 * \parblock
 * \code
 * // Hypergraphs
 * hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g, g2;
 * // Add vertices and directed hyperedges to hypergraphs
 * // ...
 *
 * // Get arguments to create named directed hyperedge with Id hyperarcId
 * auto args = g.argumentsToCreateHyperarc(hyperarcId);
 * // Add named directed hyperedge to another hypergraph g2. It is assumed that
 * // the tail and head vertices are present in the hypergraph g2.
 * const auto& hyperparc2 = g2.addHyperarc(args.tailAndHeadIds(),
 *                                         args.specificAttributes());
 * \endcode
 * \endparblock
 *
 * \return ArgumentsToCreateDirectedHyperedge\<NamedDirectedHyperedge\>
 * Arguments to add a named directed hyperedge to a hypergraph via the function
 * addHyperarc().
 *
 * \par Complexity
 * Linear in the number of tail and head vertices.
 */
ArgumentsToCreateDirectedHyperedge<NamedDirectedHyperedge>
NamedDirectedHyperedge::
argumentsToCreateHyperarc() const {
  std::vector<hglib::VertexIdType> tails, heads;
  for (const auto vertex : *(tails_.get())) {
    tails.push_back(vertex->id());
  }
  for (const auto vertex : *(heads_.get())) {
    heads.push_back(vertex->id());
  }
  return ArgumentsToCreateDirectedHyperedge<NamedDirectedHyperedge>(
      std::make_pair(tails, heads), specificAttributes(*this));
}


/**
 * \brief Print the NamedDirectedHyperedge
 *
 * \details
 * Prints the named directed hyperedge in the following format:
 * Hyperedge_name: Tail_1 + ... + Tail_n -> Head_1 + ... + Head_m
 *
 * \param out std::ostream
 */
void NamedDirectedHyperedge::
print(std::ostream& out) const {
  out << name() << ": ";
  DirectedHyperedgeBase<DirectedVertex_t>::printTailsAndHeads(out);
}


/**
 * \brief Compare function
 *
 * \details
 * Compare if the provided name, and the Ids of the tail and head vertices
 * are identical to the attributes of this NamedDirectedHyperedge.
 *
 * \param tails Pointers to tail vertices.
 * \param heads Pointers to head vertices.
 * \param specificAttributes SpecificAttributes Specific attributes of the
 * hyperarc type (name for NamedDirectedHyperedge).
 *
 * \return True if this NamedDirectedHyperedge has the same name, and tail and
 * head vertex Ids, false otherwise.
 *
 * \par Complexity
 * Linear in the number of tail/head vertices of this and the given pointers to
 * vertices.
 */
bool NamedDirectedHyperedge::
compare(const std::vector<const DirectedVertex_t*>& tails,
        const std::vector<const DirectedVertex_t*>& heads,
        const SpecificAttributes& specificAttributes) const {
  return specificAttributes.name_.compare(name()) == 0 and
         DirectedHyperedgeBase<DirectedVertex_t>::compare(tails, heads);
}

}  // namespace hglib
