// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_DIRECTEDHYPEREDGE_H
#define HGLIB_DIRECTEDHYPEREDGE_H

#include "DirectedHyperedgeBase.h"

#include <memory>
#include <tuple>
#include <utility>
#include <vector>
#include <string>

#include "ArgumentsToCreateDirectedHyperedge.h"
#include "hglib/vertex/directed/DirectedVertex.h"
#include "hglib/utils/types.h"

namespace hglib {

/**
 * \brief Directed hyperedge class
 *
 * \details
 * This is a concrete class of a directed hyperedge.\n
 * All concrete classes derived from DirectedHyperedgeBase are final to
 * prohibit further derivation. The template parameter of the derived
 * DirectedHyperedgeBase is fixed to DirectedVertex\<Concrete_class\>, \em e.g.
 * DirectedVertex\<\em DirectedHyperedge\>. The template parameters of a
 * directed hypergraph are then DirectedVertex\<DirectedHyperedge\> and
 * DirectedHyperedge for the vertex and directed hyperedge type.
 */
class DirectedHyperedge final :
    public DirectedHyperedgeBase<DirectedVertex<DirectedHyperedge>> {
  template <template <typename> typename DirectedVertex_t,
            typename Hyperarc_t,
            typename VertexProperty,
            typename EdgeProperty,
            typename HypergraphProperty>
  friend class DirectedHypergraph;

  template <template <typename> typename DirectedVertex_t,
            typename Hyperarc_t,
            typename VertexProperty,
            typename EdgeProperty,
            typename HypergraphProperty>
  friend class DirectedHypergraphInterface;

  template <template <typename> typename Vertex_t,
            typename Hyperarc_t,
            typename VertexProperty,
            typename EdgeProperty,
            typename HypergraphProperty>
  friend class DirectedSubHypergraph;

  /// Alias for the vertex type
  using Vertex_t = DirectedVertex<DirectedHyperedge>;

 public:
  /// SpecificAttributes nested class
  class SpecificAttributes;

  /* **************************************************************************
   * **************************************************************************
   *                              Constructors
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// DirectedHyperedge constructor
  inline DirectedHyperedge(
          int id,
          std::shared_ptr<GraphElementContainer<Vertex_t*>> tails,
          std::shared_ptr<GraphElementContainer<Vertex_t*>> heads,
          std::unique_ptr<SpecificAttributes>&& specificAttributes);
  /// DirectedHyperedge copy constructor
  inline DirectedHyperedge(const DirectedHyperedge& other);

 public:
  /// Destructor
  virtual ~DirectedHyperedge() = default;

  /* **************************************************************************
   * **************************************************************************
   *                        Public member functions
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Get arguments to create this DirectedHyperedge
  inline ArgumentsToCreateDirectedHyperedge<DirectedHyperedge>
  argumentsToCreateHyperarc() const;
  /// Print the DirectedHyperedge
  inline void print(std::ostream& out) const;

  /* **************************************************************************
   * **************************************************************************
   *                        Protected member functions
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Compare function
  inline bool compare(const std::vector<const Vertex_t*>& tails,
                      const std::vector<const Vertex_t*>& heads,
                      const SpecificAttributes& specificAttributes) const;

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   ***************************************************************************/
 protected:
};

/**
 * \brief SpecificAttributes nested class
 *
 * \details
 * The nested class SpecificAttributes allows classes derived from
 * HyperedgeBase to specify additional attributes in a packed and
 * unified way.
 */
class DirectedHyperedge::SpecificAttributes :
    public DirectedHyperedgeBase::SpecificAttributes {
 public:
  /// Make a copy of the object
  std::unique_ptr<HyperedgeBase::SpecificAttributes> Clone() override {
    return std::make_unique<SpecificAttributes>();
  }
};

}  // namespace hglib

#include "hglib/hyperedge/directed/DirectedHyperedge.hpp"

#endif  // HGLIB_DIRECTEDHYPEREDGE_H
