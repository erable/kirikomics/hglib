// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "DirectedHyperedgeBase.h"

#include <algorithm>
#include <memory>
#include <vector>
#include <set>

#include "filtered_vector.h"

namespace hglib {

/**
 * \brief DirectedHyperedgeBase constructor
 *
 * \details
 * A protected constructor to create the base of a directed hyperedge. The
 * friend classes DirectedHypergraph and DirectedSubHypergraph allow the
 * creation of a DirectedHyperedgeBase.
 *
 * \param id Identifier of the directed hyperedge.
 * \param tails Shared_ptr to a container of pointers to tail vertices.
 * \param heads Shared_ptr to a container of pointers to head vertices.
 */
template <typename DirectedVertex_t>
DirectedHyperedgeBase<DirectedVertex_t>::
DirectedHyperedgeBase(
    HyperedgeIdType id,
    std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> tails,
    std::shared_ptr<GraphElementContainer<DirectedVertex_t*>> heads,
    std::unique_ptr<SpecificAttributes>&& specificAttributes) :
        HyperedgeBase(id, std::move(specificAttributes)),
        tails_(tails), heads_(heads) {}


/**
 * \brief DirectedHyperedgeBase copy constructor
 *
 * \details
 * The copy constructor copies only the identifier of the given other
 * DirectedHyperedgeBase. The (pointers to) tail and head vertices are not
 * copied here, and must be added via addTailVertex() and addHeadVertex().
 *
 * \param other The to be copied DirectedHyperedgeBase.
 */
template <typename DirectedVertex_t>
DirectedHyperedgeBase<DirectedVertex_t>::
DirectedHyperedgeBase(const DirectedHyperedgeBase& other) :
    HyperedgeBase(other) {
  tails_ = create_filtered_vector<DirectedVertex_t*>();
  heads_ = create_filtered_vector<DirectedVertex_t*>();
}


/**
 * \brief Print the directed hyperedge
 *
 * \details
 * This function calls the print() method of the derived class, \em e.g.
 * DirectedHyperedge or NamedDirectedHyperedge.
 *
 * \param out std::ostream
 */
template <typename DirectedVertex_t>
void DirectedHyperedgeBase<DirectedVertex_t>::
print(std::ostream& out) const {
  static_cast<typename DirectedVertex_t::HyperarcType const&>(*this).print(
          out);
}

/* ****************************************************************************
 * ****************************************************************************
 *                        Protected methods
 * ****************************************************************************
 */

/**
 * \brief Remove a tail vertex from the directed hyperedge
 *
 * \param vertexToBeDeleted Const reference of the to be deleted vertex
 *
 * \par Complexity
 * Linear
 */
template <typename DirectedVertex_t>
void DirectedHyperedgeBase<DirectedVertex_t>::
removeTailVertex(const DirectedVertex_t& vertexToBeDeleted) {
  auto newEnd = std::remove(tails_->data(),
                            tails_->data() + tails_->size(),
                            &vertexToBeDeleted);
  // new end is different from
  if (newEnd != (tails_->data() + tails_->size())) {
    tails_->resize(tails_->size() - 1);
  }
}


/**
 * \brief Remove a head vertex from the directed hyperedge
 *
 * \param vertexToBeDeleted Const reference of the to be deleted vertex
 *
 * \par Complexity
 * Linear
 */
template <typename DirectedVertex_t>
void DirectedHyperedgeBase<DirectedVertex_t>::
removeHeadVertex(const DirectedVertex_t& vertexToBeDeleted) {
  // We suppose that there is at most one occurrence of the given vertex
  // in the heads.
  auto newEnd = std::remove(heads_->data(),
                            heads_->data() + heads_->size(),
                            &vertexToBeDeleted);
  // new end is different from
  if (newEnd != (heads_->data() + heads_->size())) {
    heads_->resize(heads_->size() - 1);
  }
}

/**
 * \brief Compare tails and heads
 *
 * \details
 * This function compares whether this DirectedHyperedgeBase has the same tail
 * and head vertex Ids as the given tail and head vertices.
 *
 * \param tails Pointers to tail vertices.
 * \param heads Pointers to head vertices.
 *
 * \return True if this DirectedHyperedgeBase has the same tail and head vertex
 * Ids as the given pointers to tail/head vertices, false otherwise.
 *
 * \par Complexity
 * Linear in the number of tail/head vertices of this and the given pointers to
 * vertices.
 */
template <typename DirectedVertex_t>
bool DirectedHyperedgeBase<DirectedVertex_t>::
compare(const std::vector<const DirectedVertex_t*>& tails,
        const std::vector<const DirectedVertex_t*>& heads) const {
  // Compare first the number of tail and head vertices (names).
  // If different, return false.
  // Compare the names of the tail vertices by adding the provided tail vertex
  // names to a set, and then removing the tail vertex names of this hyperarc.
  // If the set is empty this hyperarc has the same tail vertex names as the
  // provided ones. Otherwise return false. Repeat the procedure with the
  // names of the head vertices. Return true if the set is empty.
  if (tails.size() != tails_->size() or heads.size() != heads_->size()) {
    return false;
  }
  // Compare names of tail vertices
  std::set<VertexIdType> vertexIds;
  for (auto& tail : tails) {
    vertexIds.insert(tail->id());
  }
  for (const auto& tail : *(tails_.get())) {
    vertexIds.erase(tail->id());
  }
  if (not vertexIds.empty()) {
    return false;
  }
  // Compare names of head vertices
  for (auto& head : heads) {
    vertexIds.insert(head->id());
  }
  for (const auto& head : *(heads_.get())) {
    vertexIds.erase(head->id());
  }
  return vertexIds.empty();
}


/**
 * \brief Print tail and head vertex names of the directed hyperedge
 *
 * \details
 * Prints tail and head vertices of the directed hyperedge in the following
 * format: Tail_1 + ... + Tail_n -> Head_1 + ... + Head_m
 *
 * \param out std::ostream
 *
 * \par Complexity
 * Linear in the number of tails and heads.
 */
template <typename DirectedVertex_t>
void DirectedHyperedgeBase<DirectedVertex_t>::
printTailsAndHeads(std::ostream& out) const {
  // print tails
  out << "{";
  const char *padding = "";
  for (const auto &tail : *(tails_.get())) {
    out << padding << *tail;
    padding = ", ";
  }
  out << "} -> {";
  // print heads
  padding = "";
  for (const auto &head : *(heads_.get())) {
    out << padding << *head;
    padding = ", ";
  }
  out << "}";
}


/**
 * \brief Add a tail vertex
 *
 * \details
 * Add a pointer to a vertex to the tail vertices.
 *
 * \param vertexPtr Pointer to a directed vertex of type DirectedVertex_t.
 *
 * \par Complexity
 * Constant (amortized time, reallocation may happen).\n
 * If a reallocation happens, the reallocation is itself up to linear in the
 * entire size of the container.
 */
template <typename DirectedVertex_t>
void DirectedHyperedgeBase<DirectedVertex_t>::
addTailVertex(DirectedVertex_t* vertexPtr) {
  tails_->push_back(vertexPtr);
}


/**
 * \brief Add a head vertex
 *
 * \details
 * Add a pointer to a vertex to the head vertices.
 *
 * \param vertexPtr Pointer to a directed vertex of type DirectedVertex_t.
 *
 * \par Complexity
 * Constant (amortized time, reallocation may happen).\n
 * If a reallocation happens, the reallocation is itself up to linear in the
 * entire size of the container.
 */
template <typename DirectedVertex_t>
void DirectedHyperedgeBase<DirectedVertex_t>::
addHeadVertex(DirectedVertex_t* vertex) {
  heads_->push_back(vertex);
}
}  // namespace hglib
