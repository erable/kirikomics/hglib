// Copyright (C) 2018  INRIA
//
// Treerecs is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// Treerecs is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#ifndef HGLIB_HYPEREDGEBASE_H
#define HGLIB_HYPEREDGEBASE_H

#include "hglib/utils/types.h"

namespace hglib {

/// Get the SpecificAttributes of hyperedge in its specific type
template <typename Hyperedge_t>
const typename Hyperedge_t::SpecificAttributes&
specificAttributes(const Hyperedge_t& hyperedge);

/**
 * \brief Base class for all hyperedges
 */
class HyperedgeBase {
 public:
  /// SpecificAttributes nested class
  class SpecificAttributes;

  /// Constructor
  inline HyperedgeBase(
      HyperedgeIdType id,
      std::unique_ptr<SpecificAttributes>&& specificAttributes);
  /// Copy constructor
  inline HyperedgeBase(const HyperedgeBase& other);
  /// Destructor
  virtual ~HyperedgeBase() = default;

  /// Get the identifier
  const auto id() const { return id_; }
  /// Get the specific attributes
  const auto& specific_attributes() const { return *specific_attributes_; }

 protected:
  /// Assign new identifier
  void setId(const HyperedgeIdType& id) { id_ = id; }

  /// Unique identifier
  HyperedgeIdType id_;
  /// Specific attributes
  std::unique_ptr<SpecificAttributes> specific_attributes_;
};

/**
 * \brief SpecificAttributes nested class
 *
 * \details
 * The nested class SpecificAttributes allows classes derived from
 * HyperedgeBase to specify additional attributes in a packed and
 * unified way.
 */
class HyperedgeBase::SpecificAttributes {
 public:
  SpecificAttributes() = default;
  SpecificAttributes(const SpecificAttributes&) = default;
  SpecificAttributes(SpecificAttributes&&) = default;
  SpecificAttributes& operator=(const SpecificAttributes&) = default;
  SpecificAttributes& operator=(SpecificAttributes&&) = default;
  virtual ~SpecificAttributes() = default;

  /**
   * \brief Make a copy of the object
   *
   * This is declared pure virtual to prevent developers of derived classes
   * from forgetting to override it.
   * Concrete classes with an empty SpecificAttributes class can use the
   * following:
   *   return std::make_unique<SpecificAttributes>();
   *
   * \return the fresh copy
   */
  virtual std::unique_ptr<SpecificAttributes> Clone() = 0;
};

/**
 * Get the SpecificAttributes of hyperedge in its specific type
 *
 * \tparam Hyperedge_t enclosing class for SpecificAttributes
 * \param hyperedge hyperedge which specific attributes are to be returned
 * \return hyperedge's specific attributes casted to
 * const Hyperedge_t::SpecificAttributes
 */
template <typename Hyperedge_t>
const typename Hyperedge_t::SpecificAttributes&
specificAttributes(const Hyperedge_t& hyperedge) {
  return static_cast<const typename Hyperedge_t::SpecificAttributes&>(
      hyperedge.specific_attributes());
}

/// HyperedgeBase constructor
HyperedgeBase::HyperedgeBase(HyperedgeIdType id,
std::unique_ptr<SpecificAttributes>&& specificAttributes) :
id_(id), specific_attributes_(std::move(specificAttributes)) {}

/// HyperedgeBase copy constructor
HyperedgeBase::HyperedgeBase(const HyperedgeBase& other) : id_(other.id_) {
  specific_attributes_ =
      std::move(other.specific_attributes_->Clone());
}

}  // namespace hglib

#endif  // HGLIB_HYPEREDGEBASE_H
