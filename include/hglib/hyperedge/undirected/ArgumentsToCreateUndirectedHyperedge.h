// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_ARGUMENTSTOCREATEHYPEREDGE_H
#define HGLIB_ARGUMENTSTOCREATEHYPEREDGE_H

#include <vector>

#include "hglib/utils/types.h"

namespace hglib {

/**
 * \brief ArgumentsToCreateUndirectedHyperedge class
 *
 * \details
 * A wrapper class that holds the arguments needed to create an undirected
 * hyperedge of the template parameter type. This includes the vertex Ids, and
 * attributes that are specific to the undirected hyperedge type.
 *
 * \tparam Hyperedge_t Type of undirected hyperedge.
 */
template<typename Hyperedge_t>
class ArgumentsToCreateUndirectedHyperedge {
  friend Hyperedge_t;

 public:
  /// Alias for the data structure to provide vertex Ids
  using VerticesIds = std::vector<hglib::VertexIdType>;

 protected:
  /// ArgumentsToCreateUndirectedHyperedge constructor
  ArgumentsToCreateUndirectedHyperedge(
          const VerticesIds& verticesIds,
          const typename Hyperedge_t::SpecificAttributes& specificAttributes);

 public:
  /// Get identifiers of vertices
  const VerticesIds& verticesIds() const;
  /// Get specific arguments
  const typename Hyperedge_t::SpecificAttributes& specificAttributes() const;
  /// Set identifiers of vertices
  void setVerticesIds(const VerticesIds&);
  /// Set attributes that are specific to the undirected hyperedge type
  void setSpecificArguments(const typename Hyperedge_t::SpecificAttributes&);

 protected:
  /// Identifiers of the vertices
  VerticesIds verticesIds_;
  /// Specific attributes for the undirected hyperedge type
  typename Hyperedge_t::SpecificAttributes specificAttributes_;
};
}  // namespace hglib

#include "ArgumentsToCreateUndirectedHyperedge.hpp"

#endif  // HGLIB_ARGUMENTSTOCREATEHYPEREDGE_H
