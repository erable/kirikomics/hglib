// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "ArgumentsToCreateUndirectedHyperedge.h"

namespace hglib {

/**
 * \brief ArgumentsToCreateUndirectedHyperedge constructor
 *
 * \param verticesIds Identifiers of the vertices.
 * \param specificAttributes Attributes that are specific to the type of
 * undirected hyperedge.
 */
template<typename Hyperedge_t>
ArgumentsToCreateUndirectedHyperedge<Hyperedge_t>::
ArgumentsToCreateUndirectedHyperedge(
        const VerticesIds& verticesIds,
        const typename Hyperedge_t::SpecificAttributes& specificAttributes) :
        verticesIds_(verticesIds),
        specificAttributes_(specificAttributes) {}


/**
 * \brief Get identifiers of vertices
 *
 * \return VerticesIds Identifiers of vertices.
 */
template<typename Hyperedge_t>
const typename ArgumentsToCreateUndirectedHyperedge<Hyperedge_t>::VerticesIds&
ArgumentsToCreateUndirectedHyperedge<Hyperedge_t>::
verticesIds() const {
  return verticesIds_;
}


/**
 * \brief Get specific attributes
 *
 * \return Hyperedge_t::SpecificAttributes Attributes that are specific for the
 * class template parameter Hyperedge_t.
 */
template<typename Hyperedge_t>
const typename Hyperedge_t::SpecificAttributes&
ArgumentsToCreateUndirectedHyperedge<Hyperedge_t>::
specificAttributes() const {
  return specificAttributes_;
}


/**
 * \brief Set identifiers of vertices
 *
 * @param verticesIds Identifiers of vertices (type VerticesIds)
 */
template<typename Hyperedge_t>
void ArgumentsToCreateUndirectedHyperedge<Hyperedge_t>::
setVerticesIds(const typename ArgumentsToCreateUndirectedHyperedge<
        Hyperedge_t>::VerticesIds& verticesIds) {
  verticesIds_ = verticesIds;
}


/**
 * \brief Set attributes that are specific to the undirected hyperedge type
 *
 * \param args Attributes that are specific to the undirected hyperedge type.
 */
template<typename Hyperedge_t>
void ArgumentsToCreateUndirectedHyperedge<Hyperedge_t>::
setSpecificArguments(const typename Hyperedge_t::SpecificAttributes& args) {
  specificAttributes_ = args;
}
}  // namespace hglib
