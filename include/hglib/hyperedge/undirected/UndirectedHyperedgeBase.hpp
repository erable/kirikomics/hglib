// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "UndirectedHyperedgeBase.h"

#include <algorithm>
#include <vector>
#include <set>

#include "filtered_vector.h"

namespace hglib {

/**
 * \brief UndirectedHyperedgeBase constructor
 *
 * \details
 * A protected constructor to create the base of an undirected hyperedge. The
 * friend classes UndirectedHypergraph and UndirectedSubHypergraph allow the
 * creation of a UndirectedHyperedgeBase.
 *
 * @param id Identifier of the undirected hyperedge.
 * @param vertices Shared_ptr to a container of pointers to implied vertices.
 */
template <typename UndirectedVertex_t>
UndirectedHyperedgeBase<UndirectedVertex_t>::
UndirectedHyperedgeBase(
    HyperedgeIdType id,
    std::shared_ptr<GraphElementContainer<UndirectedVertex_t*>> vertices,
    std::unique_ptr<SpecificAttributes>&& specificAttributes) :
        HyperedgeBase(id, std::move(specificAttributes)),
        vertices_(vertices) {}


/**
 * \brief UndirectedHyperedgeBase copy constructor
 *
 * \details
 * The copy constructor copies only the identifier of the given other
 * UndirectedHyperedgeBase. The (pointers to) vertices are not
 * copied here, and must be added via addVertex().
 *
 * \param other The to be copied UndirectedHyperedgeBase.
 */
template <typename UndirectedVertex_t>
UndirectedHyperedgeBase<UndirectedVertex_t>::
UndirectedHyperedgeBase(const UndirectedHyperedgeBase& other) :
    HyperedgeBase(other) {
  vertices_ = create_filtered_vector<UndirectedVertex_t*>();
}


/**
 * \brief Print the undirected hyperedge
 *
 * \details
 * This function calls the print() method of the derived class, \em e.g.
 * UndirectedHyperedge or NamedUndirectedHyperedge.
 *
 * \param out std::ostream
 */
template <typename UndirectedVertex_t>
void UndirectedHyperedgeBase<UndirectedVertex_t>::
print(std::ostream& out) const {
  static_cast<typename UndirectedVertex_t::HyperedgeType const&>(
          *this).print(out);
}

/* ****************************************************************************
 * ****************************************************************************
 *                        Protected methods
 * ****************************************************************************
 */
/**
 * \brief Add a vertex
 *
 * \details
 * Add a pointer to a vertex to the vertices.
 *
 * \param vertexPtr Pointer to an undirected vertex of type UndirectedVertex_t.
 *
 * \par Complexity
 * Constant (amortized time, reallocation may happen).\n
 * If a reallocation happens, the reallocation is itself up to linear in the
 * entire size of the container.
 */
template <typename UndirectedVertex_t>
void UndirectedHyperedgeBase<UndirectedVertex_t>::
addVertex(UndirectedVertex_t* vertexPtr) {
  vertices_->push_back(vertexPtr);
}


/**
 * \brief Remove a vertex from the undirected hyperedge
 *
 * \param vertexToBeDeleted Const reference of the to be deleted vertex
 *
 * \par Complexity
 * Linear
 */
template <typename UndirectedVertex_t>
void UndirectedHyperedgeBase<UndirectedVertex_t>::
removeVertex(const UndirectedVertex_t& vertexToBeDeleted) {
  // We suppose that there is at most one occurrence of the given vertex
  // in the vertex list.
  auto newEnd = std::remove(vertices_->data(),
                            vertices_->data() + vertices_->size(),
                            &vertexToBeDeleted);
  // new end is different from the old one
  if (newEnd != (vertices_->data() + vertices_->size())) {
    vertices_->resize(vertices_->size() - 1);
  }
}


/**
 * \brief Compare method
 *
 * \details
 * This function compares whether this UndirectedHyperedgeBase has the same
 * vertex identifiers as the given vertices.
 *
 * \param vertices Pointers to vertices.
 *
 * \return True if this UndirectedHyperedgeBase has the same vertex Ids as the
 * given pointers to vertices, false otherwise.
 *
 * \par Complexity
 * Linear in the number of vertices of this and the given pointers to
 * vertices.
 */
template <typename UndirectedVertex_t>
bool UndirectedHyperedgeBase<UndirectedVertex_t>::
compare(const std::vector<const UndirectedVertex_t*>& vertices) const {
  if (vertices_->size() != vertices.size()) {
    return false;
  }
  // Compare names of vertices
  std::set<VertexIdType> vertexIds;
  for (auto& vertex : vertices) {
    vertexIds.insert(vertex->id());
  }
  for (const auto& vertex : *(vertices_.get())) {
    vertexIds.erase(vertex->id());
  }
  return vertexIds.empty();
}

/**
 * \brief Prints vertices of the undirected hyperedge
 *
 * \details
 * Prints vertices of the undirected hyperedge in a comma separated list. The
 * way a vertex is appended to the ostream depends on the implementation of the
 * print() method of the type of undirected vertex (template parameter
 * UndirectedVertex_t), \em e.g. the print() method in UndirectedVertex appends
 * the name of the vertex.
 *
 * \param out std::ostream
 *
 * \par Complexity
 * Linear in the number of vertices.
 */
template <typename UndirectedVertex_t>
void UndirectedHyperedgeBase<UndirectedVertex_t>::
printVertices(std::ostream& out) const {
  // print vertices
  out << '{';
  const char *padding = "";
  for (const auto &vertex : *(vertices_.get())) {
    out << padding << *vertex;
    padding = ", ";
  }
  out << '}';
}
}  // namespace hglib
