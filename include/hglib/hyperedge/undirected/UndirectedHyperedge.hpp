/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

#include <memory>

namespace hglib {
/**
 * \brief UndirectedHyperedge constructor
 *
 * \param id Identifier of the DirectedHyperedge.
 * \param vertices Shared_ptr to the container of pointers to vertices.
 * \param specificAttributes Const reference to SpecificAttributes.
 */
UndirectedHyperedge::
UndirectedHyperedge(int id,
                    std::shared_ptr<GraphElementContainer<Vertex_t*>> vertices,
                    std::unique_ptr<SpecificAttributes>&& specificAttributes) :
    UndirectedHyperedgeBase(id, vertices, std::move(specificAttributes)) {}


/**
 * \brief UndirectedHyperedge copy constructor
 *
 * \param other To be copied UndirectedHyperedge.
 */
UndirectedHyperedge::
UndirectedHyperedge(const UndirectedHyperedge& other) :
    UndirectedHyperedgeBase(other) {}


/**
 * \brief Get arguments to create this UndirectedHyperedge
 *
 * \details
 * This function returns an instance of ArgumentsToCreateUndirectedHyperedge
 * that provides all arguments (vertices Ids, specific attributes) to add this
 * undirected hyperedge to a hypergraph.
 *
 * \par Example
 * \parblock
 * \code
 *
 * // Get arguments to create an undirected hyperedge with Id hyperedgeId
 * auto args = g.argumentsToCreateHyperedge(hyperedgeId);
 * // Add undirected hyperedge to another hypergraph g2. It is assumed that the
 * // vertices are present in the hypergraph g2.
 * const auto& hyperpedge2 = g2.addHyperedge(args.verticesIds(),
 *                                           args.specificAttributes());
 * \endcode
 * \endparblock
 *
 * \return ArgumentsToCreateUndirectedHyperedge\<UndirectedHyperedge\>
 * Arguments to add an undirected hyperedge to a hypergraph via the function
 * addHyperedge().
 */
ArgumentsToCreateUndirectedHyperedge<UndirectedHyperedge>
UndirectedHyperedge::
argumentsToCreateHyperedge() const {
  std::vector<hglib::VertexIdType> vertices;
  for (const auto vertex : *(vertices_.get())) {
    vertices.push_back(vertex->id());
  }
  return ArgumentsToCreateUndirectedHyperedge<UndirectedHyperedge>(
      vertices,
      specificAttributes(*this));
}


/**
 * \brief Compare function
 *
 * \details
 * Compare if the Ids of the vertices
 * are identical to the attributes of this UndirectedHyperedge.
 *
 * \param vertices Pointers to vertices.
 * \param SpecificAttributes Specific attributes of the hyperedge type (none
 * for UndirectedHyperedge).
 *
 * \return True if this UndirectedHyperedge has the same vertex Ids, false
 * otherwise.
 *
 * \par Complexity
 * Linear in the number of vertices of this and the given pointers to
 * vertices.
 */
bool UndirectedHyperedge::
compare(const std::vector<const Vertex_t*>& vertices,
        const SpecificAttributes&) const {
  return UndirectedHyperedgeBase<Vertex_t>::compare(vertices);
}


/**
 * \brief Print the undirected hyperedge
 *
 * \details
 * Calls the function printVertices() of the base class UndirectedHyperedgeBase.
 *
 * \param out std::ostream
 */
void UndirectedHyperedge::
print(std::ostream& out) const {
  UndirectedHyperedgeBase<Vertex_t>::printVertices(out);
}

}  // namespace hglib
