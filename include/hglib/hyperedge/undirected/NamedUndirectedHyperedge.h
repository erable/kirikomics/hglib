// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_NAMEDUNDIRECTEDHYPEREDGE_H
#define HGLIB_NAMEDUNDIRECTEDHYPEREDGE_H

#include "UndirectedHyperedgeBase.h"

#include <memory>

#include "ArgumentsToCreateUndirectedHyperedge.h"
#include "hglib/utils/types.h"
#include "hglib/vertex/undirected/UndirectedVertex.h"

namespace hglib {
/**
 * \brief Named undirected hyperedge class
 *
 * \details
 * This is a concrete class of an undirected hyperedge.\n
 * All concrete classes derived from UndirectedHyperedgeBase are final to
 * prohibit further derivation. The template parameter of the derived
 * UndirectedHyperedgeBase is fixed to UndirectedVertex\<Concrete_class\>,
 * \em e.g. UndirectedVertex\<\em NamedUndirectedHyperedge\>. The template
 * parameters of an undirected hypergraph are then
 * UndirectedVertex\<NamedUndirectedHyperedge\> and
 * NamedUndirectedHyperedge for the vertex and undirected hyperedge type.
 */
class NamedUndirectedHyperedge final :
        public UndirectedHyperedgeBase<
                UndirectedVertex<NamedUndirectedHyperedge>> {
  template <template <typename> typename DirectedVertex_t,
            typename Hyperedge_t,
            typename VertexProperty,
            typename EdgeProperty,
            typename HypergraphProperty>
  friend class UndirectedHypergraph;

  template <template <typename> typename DirectedVertex_t,
            typename Hyperedge_t,
            typename VertexProperty,
            typename EdgeProperty,
            typename HypergraphProperty>
  friend class UndirectedHypergraphInterface;

  template <template <typename> typename Vertex_t,
            typename Hyperedge_t,
            typename VertexProperty,
            typename EdgeProperty,
            typename HypergraphProperty>
  friend class UndirectedSubHypergraph;

  /// Alias for the vertex type
  using Vertex_t = UndirectedVertex<NamedUndirectedHyperedge>;

  /* **************************************************************************
   * **************************************************************************
   *                               Nested class
   * **************************************************************************
   ***************************************************************************/
 public:
  /// SpecificAttributes nested class
  class SpecificAttributes;

  /* **************************************************************************
   * **************************************************************************
   *                              Constructors
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// NamedUndirectedHyperedge constructor
  inline NamedUndirectedHyperedge(
      int id,
      std::shared_ptr<GraphElementContainer<Vertex_t*>> vertices,
      std::unique_ptr<HyperedgeBase::SpecificAttributes>&& specificAttributes);
  /// NamedUndirectedHyperedge copy constructor
  inline NamedUndirectedHyperedge(const NamedUndirectedHyperedge& other);

 public:
  /// Destructor
  virtual ~NamedUndirectedHyperedge() = default;

  /* **************************************************************************
   * **************************************************************************
   *                        Public member functions
   * **************************************************************************
   ***************************************************************************/
 public:
  /// Get the name of the undirected hyperedge
  inline std::string name() const;
  /// Print the named undirected hyperedge
  inline void print(std::ostream& out) const;
  /// Get arguments to create this NamedUndirectedHyperedge
  inline ArgumentsToCreateUndirectedHyperedge<NamedUndirectedHyperedge>
  argumentsToCreateHyperedge() const;

  /* **************************************************************************
   * **************************************************************************
   *                        Protected member functions
   * **************************************************************************
   ***************************************************************************/
 protected:
  /// Compare function
  inline bool compare(const std::vector<const Vertex_t*>& vertices,
                      const SpecificAttributes& specificAttributes) const;

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   ***************************************************************************/
 protected:
};

/**
 * \brief SpecificAttributes nested class
 *
 * \details
 * The nested class SpecificAttributes allows classes derived from
 * HyperedgeBase to specify additional attributes in a packed and
 * unified way.
 */
class NamedUndirectedHyperedge::SpecificAttributes :
    public UndirectedHyperedgeBase::SpecificAttributes {
 public:
  /// SpecificAttributes constructors
  SpecificAttributes() = delete;
  SpecificAttributes(const std::string& name) : name_(name) {}
  SpecificAttributes(const SpecificAttributes&) = default;
  SpecificAttributes(SpecificAttributes&&) = default;
  SpecificAttributes& operator=(const SpecificAttributes&) = default;
  SpecificAttributes& operator=(SpecificAttributes&&) = default;
  virtual ~SpecificAttributes() = default;

  /// Make a copy of the object
  std::unique_ptr<HyperedgeBase::SpecificAttributes> Clone() override {
    return std::make_unique<SpecificAttributes>(*this);
  };
  /// Name of the undirected hyperedge
  std::string name_;
};

}  // namespace hglib

#include "hglib/hyperedge/undirected/NamedUndirectedHyperedge.hpp"

#endif  // HGLIB_NAMEDUNDIRECTEDHYPEREDGE_H
