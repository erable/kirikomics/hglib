// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_HYPERGRAPHS_H
#define HGLIB_HYPERGRAPHS_H

// general hypergraph
#include "hglib/hypergraph/HypergraphInterface.h"
#include "hglib/hypergraph/Hypergraph.h"

// directed hypergraph
#include "hglib/hypergraph/directed/DirectedHypergraphInterface.h"
#include "hglib/hypergraph/directed/DirectedHypergraph.h"
#include "hglib/hypergraph/directed/DirectedSubHypergraph.h"

// undirected hypergraph
#include "hglib/hypergraph/undirected/UndirectedHypergraphInterface.h"
#include "hglib/hypergraph/undirected/UndirectedHypergraph.h"
#include "hglib/hypergraph/undirected/UndirectedSubHypergraph.h"

// Observer pattern related
#include "hglib/utils/observation/ObservableEvent.h"
#include "hglib/utils/observation/Observer.h"
#include "hglib/utils/observation/Observable.h"


#endif  // HGLIB_HYPERGRAPHS_H
