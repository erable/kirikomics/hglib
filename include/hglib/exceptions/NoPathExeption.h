// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_NOPATHEXEPTION_H
#define HGLIB_NOPATHEXEPTION_H

namespace hglib {
/**
 * \brief Exception class
 *
 * \details
 * Exception class to signal that there is no path between a set of sources and
 * a set of targets.
 */
class no_path_exception : public std::exception {
 public:
  /// no_path_exception constructor
  explicit no_path_exception() {}

  /// Get string identifying exception
  virtual const char* what() const throw() {
    return "No path exists.\n";
  }
};
}  // namespace hglib
#endif  // HGLIB_NOPATHEXEPTION_H
