// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_DIRECTEDHYPERGRAPHINTERFACE_H
#define HGLIB_DIRECTEDHYPERGRAPHINTERFACE_H

#include "filtered_vector.h"

#include "hglib/hypergraph/HypergraphInterface.h"
#include "hglib/utils/observation/Observable.h"
#include "hglib/hyperedge/directed/ArgumentsToCreateDirectedHyperedge.h"
#include "hglib/utils/types.h"

namespace hglib {

/**
 * \brief This abstract class defines the interface of a directed hypergraph.
 *
 * \details
 * This interface is currently implemented by DirectedHypergraph and
 * DirectedSubHypergraph.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_type,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
class DirectedHypergraphInterface : virtual public HypergraphInterface<
        VertexTemplate_t, Hyperarc_type, VertexProperty, HyperarcProperty,
        HypergraphProperty> {
 public:
  /// Alias for the directed vertex type
  using Vertex_t = typename HypergraphInterface<VertexTemplate_t,
          Hyperarc_type, VertexProperty, HyperarcProperty,
          HypergraphProperty>::Vertex_t;
  /// Alias for Hyperarc type
  using Hyperarc_t = Hyperarc_type;
  /// Alias for Hyperarc property type
  using HyperarcProperty_t = HyperarcProperty;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = typename HypergraphInterface<VertexTemplate_t,
          Hyperarc_type, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexContainer;
  /// Alias for conditional range of a container of Hyperarc_t ptr
  using HyperarcContainer = GraphElementContainer<Hyperarc_t*>;
  /// Alias for iterator over a container of vertices
  using vertex_iterator = typename HypergraphInterface<VertexTemplate_t,
          Hyperarc_type, VertexProperty, HyperarcProperty,
          HypergraphProperty>::vertex_iterator;
  /// Alias for iterator over a container of hyperarcs
  using hyperarc_iterator = typename GraphElementContainer<
          Hyperarc_t*>::const_iterator;
  /// \brief Alias for the data container that stores the names of tails and
  /// heads of a hyperarc
  using TailAndHeadNames = std::pair<std::vector<std::string>,
                                     std::vector<std::string>>;
  /// \brief Alias for the data container that stores the ids of tails and
  /// heads of a hyperarc
  using TailAndHeadIds = std::pair<std::vector<VertexIdType>,
                                   std::vector<VertexIdType>>;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = typename HypergraphInterface<VertexTemplate_t,
          Hyperarc_type, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr = typename HypergraphInterface<
          VertexTemplate_t, Hyperarc_type, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexPropertyContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<Hyperarc_t*>
  using HyperarcContainerPtr =
      std::shared_ptr<GraphElementContainer<Hyperarc_t*>>;
  /// Alias for unique_ptr of GraphElementContainer<HyperarcProperty*>
  using HyperarcPropertyContainerPtr =
      std::shared_ptr<GraphElementContainer<HyperarcProperty*>>;

  /* **************************************************************************
   * **************************************************************************
   *                    Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Destructor
  virtual ~DirectedHypergraphInterface() = default;

  /* **************************************************************************
   * **************************************************************************
   *                    General
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if multi-hyperarcs are allowed in this hypergraph
  virtual bool allowMultiHyperarcs() const = 0;

  /* **************************************************************************
  * **************************************************************************
  *                        Hyperarc access
  * **************************************************************************
  * *************************************************************************/
 public:
  /// Number of hyperarcs in the hypergraph
  virtual size_t nbHyperarcs() const = 0;
  /// Size of the hyperarc container
  virtual size_t hyperarcContainerSize() const = 0;

  /// Add a hyperarc
  virtual const Hyperarc_t* addHyperarc(
          decltype(hglib::NAME),
          const TailAndHeadNames& tails_heads,
          const typename Hyperarc_t::SpecificAttributes& attributes = {}) = 0;

  /// Add a hyperarc
  virtual const Hyperarc_t* addHyperarc(
          const TailAndHeadIds& tails_heads,
          const typename Hyperarc_t::SpecificAttributes& attributes = {}) = 0;

  /// Remove a hyperarc with the given id from the hypergraph
  virtual void removeHyperarc(const HyperedgeIdType& hyperarcId) = 0;

  /// Search hyperarc list for an hyperarc with the provided id
  virtual const Hyperarc_t* hyperarcById(
          const HyperedgeIdType & hyperarcId) const = 0;

  /// Const hyperarc iterator pointing to begin of the hyperarcs
  virtual hyperarc_iterator hyperarcsBegin() const = 0;
  /// Const hyperarc iterator pointing to end of the hyperarcs
  virtual hyperarc_iterator hyperarcsEnd() const = 0;
  /// Begin/End iterator pair of the hyperarcs of the directed hypergraph
  virtual std::pair<hyperarc_iterator,
          hyperarc_iterator> hyperarcsBeginEnd() const = 0;
  /// Return a reference to the container of hyperarcs.
  virtual const HyperarcContainer& hyperarcs() const = 0;

  // Tails
  /// Return number of tail vertices in the hyperarc with given id.
  virtual size_t nbTailVertices(const HyperedgeIdType & hyperarcId) const = 0;
  /// Check if the hyperarc with the given id has tail vertices
  virtual bool hasTailVertices(const HyperedgeIdType & hyperarcId) const = 0;
  /// Const vertex iterator pointing to the begin of the tail vertices
  virtual vertex_iterator tailsBegin(
          const HyperedgeIdType & hyperarcId) const = 0;
  /// Const vertex iterator pointing to the end of the tail vertices
  virtual vertex_iterator tailsEnd(
          const HyperedgeIdType & hyperarcId) const = 0;
  /// Pair of vertex iterators pointing to the begin/end of the tail vertices
  virtual std::pair<vertex_iterator, vertex_iterator> tailsBeginEnd(
          const HyperedgeIdType & hyperarcId) const = 0;
  /// Get shared_ptr of container of the tail vertices of given hyperarc id
  virtual std::shared_ptr<const GraphElementContainer<Vertex_t*>> tails(
          const HyperedgeIdType& hyperarcId) const = 0;

  // Heads
  /// Return number of head vertices in the hyperarc with the given id
  virtual size_t nbHeadVertices(const HyperedgeIdType & hyperarcId) const = 0;
  /// Check if the hyperarc with the given id has head vertices
  virtual bool hasHeadVertices(const HyperedgeIdType & hyperarcId) const = 0;
  /// Const vertex iterator pointing to the begin of the head vertices
  virtual vertex_iterator headsBegin(
          const HyperedgeIdType & hyperarcId) const = 0;
  /// Const vertex iterator pointing to the end of the head vertices
  virtual vertex_iterator headsEnd(
          const HyperedgeIdType & hyperarcId) const = 0;
  /// Pair of vertex iterators pointing to the begin/end of the head vertices
  virtual std::pair<vertex_iterator, vertex_iterator> headsBeginEnd(
          const HyperedgeIdType & hyperarcId) const = 0;
  /// Get shared_ptr of container of the head vertices of given hyperarc id
  virtual std::shared_ptr<const GraphElementContainer<Vertex_t*>> heads(
          const HyperedgeIdType& hyperarcId) const = 0;

  /* **************************************************************************
   * **************************************************************************
   *                       DirectedVertex access
   * **************************************************************************
   * *************************************************************************/
 public:
  // incoming hyperarcs
  /// Return number of incoming hyperarcs of the vertex with the given id
  virtual size_t inDegree(const VertexIdType& vertexId) const = 0;
  /// Check if the vertex with the given id has incoming hyperarcs
  virtual bool hasInHyperarcs(const VertexIdType& vertexId) const = 0;
  /// Const hyperarc iterator pointing to the begin of the in-hyperarcs
  virtual hyperarc_iterator inHyperarcsBegin(
          const VertexIdType& vertexId) const = 0;
  /// Const hyperarc iterator pointing to the end of the incoming hyperarcs
  virtual hyperarc_iterator inHyperarcsEnd(
          const VertexIdType& vertexId) const = 0;
  /// Pair of hyperarc iterators pointing to the begin/end of the in-arcs
  virtual std::pair<hyperarc_iterator, hyperarc_iterator> inHyperarcsBeginEnd(
          const VertexIdType& vertexId) const = 0;
  /// Get shared_ptr of container of the in-arcs of the given vertex Id
  virtual std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> inHyperarcs(
          const VertexIdType& vertexId) const = 0;

  // out-going hyperarcs
  /// Return number of outgoing hyperarcs of the vertex with the given id
  virtual size_t outDegree(const VertexIdType& vertexId) const = 0;
  /// Check if the vertex with the given id has outgoing hyperarcs
  virtual bool hasOutHyperarcs(const VertexIdType& vertexId) const = 0;
  /// Const hyperarc iterator pointing to the begin of the out-hyperarcs
  virtual hyperarc_iterator outHyperarcsBegin(
          const VertexIdType& vertexId) const = 0;
  /// Const hyperarc iterator pointing to the end of the outgoing hyperarcs
  virtual hyperarc_iterator outHyperarcsEnd(
          const VertexIdType& vertexId) const = 0;
  /// Pair of hyperarc iterators pointing to the begin/end of the out-arcs
  virtual std::pair<hyperarc_iterator, hyperarc_iterator> outHyperarcsBeginEnd(
          const VertexIdType& vertexId) const = 0;
  /// Get shared_ptr of container of the out-arcs of the given vertex Id
  virtual std::shared_ptr<const GraphElementContainer<
      Hyperarc_t*>> outHyperarcs(const VertexIdType& vertexId) const = 0;

  /// Remove a vertex with the given name from the directed hypergraph
  virtual void removeVertex(const std::string& vertexName,
                            bool removeImpliedHyperarcs = true) = 0;
  /// Remove a vertex with the given Id from the directed hypergraph
  virtual void removeVertex(const VertexIdType& vertexId,
                            bool removeImpliedHyperarcs = true) = 0;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex/Hyperarc dependent access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if the vertex is a tail vertex of the hyperarc
  virtual bool isTailVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const = 0;
  /// Check if the vertex is a head vertex of the hyperarc
  virtual bool isHeadVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const = 0;
  /// Check if the vertex is a tail or a head vertex of the hyperarc
  virtual bool isVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const = 0;

  /* **************************************************************************
  * **************************************************************************
  *                        HyperarcProperty access
  * **************************************************************************
  * *************************************************************************/
 public:
  /// Get properties of the hyperarc with the provided id.
  virtual const HyperarcProperty* getHyperarcProperties(
          const HyperedgeIdType& hyperarcId) const = 0;
  /// Get properties of the hyperarc with the provided id.
  virtual HyperarcProperty* getHyperarcProperties_(
          const HyperedgeIdType& hyperarcId) const = 0;

  /* **************************************************************************
   * **************************************************************************
   *                  Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get root hypergraph
  virtual const DirectedHypergraphInterface* getRootHypergraph() const = 0;
  /// Return shared_ptr to hyperarc container
  virtual HyperarcContainerPtr getRootHyperarcContainerPtr() const = 0;
};



/* **************************************************************************
 * **************************************************************************
 *               Non-member functions
 * **************************************************************************
 * *************************************************************************/

/**
 * \brief Retrieve graph's arcs into a container
 *
 * \details
 * Retrieve hypergraph's arcs and insert them into a container through the
 * provided output iterator.
 *
 * \param hypergraph graph from which to retrieve the hyperarcs
 * \param output_iter
 * \parblock
 * OutputIterator to the container to fill \n
 * The underlying container's value_type should be `const Hyperarc_t*`
 * \endparblock
 * \param fn (optional) if provided, only those hyperarcs for which
 * `fn(hyperarc.id())` evaluates to true will be inserted
 *
 * \par Examples
 * \parblock
 * Simple example
 * \code
 hglib::DirectedHypergraph<> g;
 for (auto vertexName : vertices) {
   g.addVertex(vertexName);
 }
 for (auto arc : arcs) {
   g.addHyperarc(arc);
 }

 std::vector<const hglib::DirectedHyperedge*> hyperarcContainer;
 hglib::hyperarcs(g, std::back_inserter(hyperarcContainer));

 std::set<const hglib::DirectedHyperedge*> s;
 hglib::hyperarcs(g, std::inserter(s, s.begin()));
 * \endcode
 *
 * Retrieve hyperarcs with a simple constraint
 * \code
 enum class Colour {
    red, blue, green, black
  };
  struct EdgeProperty {
    string name = "Default";
    Colour colour = Colour::green;
  };

  hglib::DirectedHypergraph<hglib::DirectedVertex,
                            hglib::DirectedHyperedge,
                            hglib::emptyProperty,
                            EdgeProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first);
  }

  // Get green hyperarcs
  std::vector<const hglib::DirectedHyperedge*> greenHyperarcs;
  hglib::hyperarcs(g, std::back_inserter(greenHyperarcs),
                   [&](hglib::HyperedgeIdType hyperarcId) -> bool {
                     return g.getHyperarcProperties(hyperarcId)->colour ==
                            Colour::green;
                   });
 * \endcode
 * \endparblock
 *
 * \par Complexity
 * Linear most of the time\n
 * O(n * O(\a fn)) if \a fn is provided
 *
 * \relates DirectedHypergraphInterface
 */
template <typename HGraph, typename OutputIterator>
void hyperarcs(const HGraph& hypergraph,
               OutputIterator output_iter,
               std::function<bool(hglib::HyperedgeIdType)>&& fn = nullptr) {
  // Check that output_iter satisfies the OutputIterator requirements
  static_assert(
      std::is_same<
          typename std::iterator_traits<OutputIterator>::iterator_category,
          typename std::output_iterator_tag>::value,
      "provided iterator not an OutputIterator");

  // Insert hyperarcs through output_iter
  for (const auto& hyperarc : hypergraph.hyperarcs()) {
    // Insert if no fn provided or fn returns true
    if (not fn or fn(hyperarc->id())) {
      output_iter = hyperarc;
    }
  }
}

}  // namespace hglib
#endif  // HGLIB_DIRECTEDHYPERGRAPHINTERFACE_H
