// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_DIRECTEDHYPERGRAPH_H
#define HGLIB_DIRECTEDHYPERGRAPH_H

#include <memory>
#include <string>
#include <type_traits>

#include "filtered_vector.h"

#include "DirectedHypergraphInterface.h"
#include "hglib/hypergraph/Hypergraph.h"
#include "hglib/utils/observation/Observable.h"
#include "hglib/hyperedge/directed/ArgumentsToCreateDirectedHyperedge.h"
#include "hglib/hyperedge/directed/DirectedHyperedgeBase.h"
#include "hglib/hyperedge/directed/DirectedHyperedge.h"
#include "hglib/utils/IsParentOf.h"
#include "hglib/utils/types.h"

namespace hglib {

/**
 * \class DirectedHypergraph
 *
 * \brief This is a DirectedHypergraph class template.
 *
 * \details
 * This class template allows to model <em>directed hypergraphs</em>. There are
 * template parameters for the vertex and hyperarc type. Additionally one can
 * specify a property type for the vertices, hyperarcs, and the hypergraph
 * itself. All template parameters have their default values (see below),
 * \em e.g. the default vertex type is the class template DirectedVertex.
 * You can use your own classes as template parameters but there are some
 * restrictions to assure some functionalities:
 * - The vertex type class must be a class template that is derived from the
 *   DirectedVertex<T> class, where \em T corresponds to the hyperarc type,
 *   \em e.g. DirectedHyperedge.
 * - The hyperarc type class must be derived from DirectedHyperedgeBase<T>,
 *   where \em T corresponds to the directed vertex type, \em e.g.
 *   DirectedVertex\<DirectedHyperedge\>.
 *
 * \parblock
 * This class template contains functions to edit a directed hypergraph and to
 * iterate over:
 * - the vertices and hyperarcs of the directed hypergraph,
 * - the in- and outgoing hyperarcs of a vertex,
 * - the tail and head vertices of a hyperarc.
 *
 * Each vertex and hyperarc has a numerical identifier allowing to have a
 * constant access to it.
 * \endparblock
 *
 * \par Properties
 * \parblock
 * The vertex/hyperarc/hypergraph properties provide a way to put additional
 * attributes on these elements, \em e.g. to assign a colour to a vertex, or
 * a weight to a hyperarc.
 * \code
 * enum Colour {
 *    red, blue, green
 *  };
 * struct VertexProperty {
 *    Colour colour = Colour::green;
 * };
 * struct EdgeProperty {
 *    double weight = 0.0;
 * };
 *
 * hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
 *        VertexProperty, EdgeProperty> g;
 * \endcode
 * \endparblock
 *
 * \tparam Vertex_t Vertex type. Defaults to DirectedVertex.
 * \tparam Hyperarc_t Hyperarc type. Defaults to DirectedHyperedge.
 * \tparam VertexProperty Vertex property type. Defaults to emptyProperty.
 * \tparam EdgeProperty Hyperedge property type. Defaults to emptyProperty.
 * \tparam HypergraphProperty Directed hypergraph property type. Defaults to
 * emptyProperty.
 */
template <template <typename> typename VertexTemplate_t = DirectedVertex,
          typename Hyperarc_t = DirectedHyperedge,
          typename VertexProperty = emptyProperty,
          typename HyperarcProperty = emptyProperty,
          typename HypergraphProperty = emptyProperty>
class DirectedHypergraph : public DirectedHypergraphInterface<
        VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
        HypergraphProperty>,
                           public Hypergraph<VertexTemplate_t,
                               Hyperarc_t, VertexProperty,
                               HyperarcProperty, HypergraphProperty> {
  // Check that the vertex and hyperarc type is correct
  // A directed vertex is needed.
  static_assert(std::is_base_of<DirectedVertex<Hyperarc_t>,
                    VertexTemplate_t<Hyperarc_t>>::value,
                "A vertex of a directed hypergraph must be derived from "
                    "DirectedVertex<T>, where T is derived from "
                    "DirectedHyperedgeBase, e.g. "
                    "DirectedVertex<DirectedHyperedge> or "
                    "DirectedVertex<NamedDirectedHyperedge>");
  // A hyperarc is needed
  static_assert(
      std::is_base_of<DirectedHyperedgeBase<VertexTemplate_t<Hyperarc_t>>,
          Hyperarc_t>::value, "A hyperarc of a directed hypergraph must be "
          "derived from DirectedHyperedgeBase, e.g. use as second template "
          "parameter DirectedHyperedge or NamedDirectedHyperedge");

  // Declare any instantiation of this template class as friend
  template <template <typename> class, typename, typename, typename, typename>
  friend class DirectedHypergraph;


 public:
  /// Alias for the directed vertex type
  using Vertex_t = typename DirectedHypergraphInterface<VertexTemplate_t,
          Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::Vertex_t;
  /// Alias for Hyperarc type
  using Hyperarc_type = Hyperarc_t;
  /// Alias for Hyperarc property type
  using HyperarcProperty_type = HyperarcProperty;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexContainer;
  /// Alias for conditional range of a container of Hyperarc_t ptr
  using HyperarcContainer = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty,
          HyperarcProperty, HypergraphProperty>::HyperarcContainer;
  ///
  using vertex_iterator = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::vertex_iterator;
  ///
  using hyperarc_iterator = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::hyperarc_iterator;
  /// \brief Alias for the data container that stores the names of tails and
  /// heads of a hyperarc
  using TailAndHeadNames = std::pair<std::vector<std::string>,
                                     std::vector<std::string>>;
  /// \brief Alias for the data container that stores the ids of tails and
  /// heads of a hyperarc
  using TailAndHeadIds = std::pair<std::vector<VertexIdType>,
                                   std::vector<VertexIdType>>;
  /// Alias for unique_ptr of GraphElementContainer<Hyperarc_t*>
  using HyperarcContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::HyperarcContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<HyperarcProperty*>
  using HyperarcPropertyContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::HyperarcPropertyContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexPropertyContainerPtr;

  /* **************************************************************************
   * **************************************************************************
   *                        Constructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// DirectedHypergraph constructor
  explicit DirectedHypergraph(bool allowMultiHyperarcs = true);
  /// Copy constructor
  explicit DirectedHypergraph(
          const DirectedHypergraph& directedHypergraph);
  /// Conversion constructor
  template <typename OtherVP, typename OtherEP, typename OtherGP>
  explicit DirectedHypergraph(
      const DirectedHypergraph<VertexTemplate_t,
                               Hyperarc_t,
                               OtherVP,
                               OtherEP,
                               OtherGP>& other);
  /// DirectedHypergraph destructor
  virtual ~DirectedHypergraph();

  /// Copy Assignment operator
  DirectedHypergraph& operator=(const DirectedHypergraph& source);

  /* **************************************************************************
   * **************************************************************************
   *                    General
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if multi-hyperarcs are allowed in this hypergraph
  bool allowMultiHyperarcs() const override;

  /* **************************************************************************
  * **************************************************************************
  *                        DirectedHyperedge access
  * **************************************************************************
  * *************************************************************************/
 public:
  /// Number of hyperarcs in the hypergraph
  size_t nbHyperarcs() const override;
  /// Size of the hyperarc container
  size_t hyperarcContainerSize() const override;

  /// Add a hyperarc
  const Hyperarc_t* addHyperarc(
          decltype(hglib::NAME),
          const TailAndHeadNames& tails_heads,
          const typename Hyperarc_t::SpecificAttributes& attributes = {})
    override;
  /// Add a hyperarc
  const Hyperarc_t* addHyperarc(
          const TailAndHeadIds& tails_heads,
          const typename Hyperarc_t::SpecificAttributes& attributes = {})
    override;

  /// Remove a hyperarc with the given id from the hypergraph
  void removeHyperarc(const HyperedgeIdType& hyperarcId) override;

  /// Search hyperarc list for an hyperarc with the provided id
  const Hyperarc_t* hyperarcById(const HyperedgeIdType & hyperarcId) const
    override;

  /// Const hyperarc iterator pointing to begin of the hyperarcs.
  hyperarc_iterator hyperarcsBegin() const override;
  /// Const hyperarc iterator pointing to end of the hyperarcs.
  hyperarc_iterator hyperarcsEnd() const override;
  /// Begin/End iterator pair of the hyperarcs of the directed hypergraph
  std::pair<hyperarc_iterator, hyperarc_iterator> hyperarcsBeginEnd() const
    override;
  /// Return a reference to the container of hyperarcs.
  const HyperarcContainer& hyperarcs() const override;

  // Tails
  /// Return number of tail vertices in the hyperarc with given id.
  size_t nbTailVertices(const HyperedgeIdType & hyperarcId) const override;
  /// Check if the hyperarc with the given id has tail vertices.
  bool hasTailVertices(const HyperedgeIdType & hyperarcId) const override;
  /// Const vertex iterator pointing to the begin of the tail vertices
  vertex_iterator tailsBegin(const HyperedgeIdType & hyperarcId) const
    override;
  /// Const vertex iterator pointing to the end of the tail vertices
  vertex_iterator tailsEnd(const HyperedgeIdType & hyperarcId) const override;
  /// Pair of vertex iterators pointing to the begin/end of the tail vertices
  std::pair<vertex_iterator, vertex_iterator> tailsBeginEnd(
          const HyperedgeIdType & hyperarcId) const override;
  /// Get shared_ptr of container of the tail vertices of given hyperarc id
  std::shared_ptr<const GraphElementContainer<Vertex_t*>> tails(
          const HyperedgeIdType& hyperarcId) const override;

  // Heads
  /// Return number of head vertices in the hyperarc with the given id.
  size_t nbHeadVertices(const HyperedgeIdType & hyperarcId) const override;
  /// Check if the hyperarc with the given id has head vertices.
  bool hasHeadVertices(const HyperedgeIdType & hyperarcId) const override;
  /// Const vertex iterator pointing to the begin of the head vertices
  vertex_iterator headsBegin(const HyperedgeIdType & hyperarcId) const
    override;
  /// Const vertex iterator pointing to the end of the head vertices
  vertex_iterator headsEnd(const HyperedgeIdType & hyperarcId) const override;
  /// Pair of vertex iterators pointing to the begin/end of the head vertices
  std::pair<vertex_iterator, vertex_iterator> headsBeginEnd(
          const HyperedgeIdType & hyperarcId) const override;
  /// Get shared_ptr of container of the head vertices of given hyperarc id
  std::shared_ptr<const GraphElementContainer<Vertex_t*>> heads(
          const HyperedgeIdType& hyperarcId) const override;

  /// Establish consecutive hyperarc ids
  void resetHyperarcIds();
  /// Establish consecutive vertex and hyperarc ids
  void resetIds();

  /* **************************************************************************
   * **************************************************************************
   *                       DirectedVertex access
   * **************************************************************************
   * *************************************************************************/
 public:
  // incoming hyperarcs
  /// Return number of incoming hyperarcs of the vertex with the given id
  size_t inDegree(const VertexIdType& vertexId) const override;
  /// Check if the vertex with the given id has incoming hyperarcs
  bool hasInHyperarcs(const VertexIdType& vertexId) const override;
  /// Const hyperarc iterator pointing to the begin of the in-hyperarcs
  hyperarc_iterator inHyperarcsBegin(const VertexIdType& vertexId) const
    override;
  /// Const hyperarc iterator pointing to the end of the incoming hyperarcs
  hyperarc_iterator inHyperarcsEnd(const VertexIdType& vertexId) const
    override;
  /// Pair of hyperarc iterators pointing to the begin/end of the in-arcs
  std::pair<hyperarc_iterator, hyperarc_iterator> inHyperarcsBeginEnd(
          const VertexIdType& vertexId) const override;
  /// Get shared_ptr of container of the in-arcs of the given vertex Id
  std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> inHyperarcs(
      const VertexIdType& vertexId) const override;

  // out-going hyperarcs
  /// Return number of outgoing hyperarcs of the vertex with the given id
  size_t outDegree(const VertexIdType& vertexId) const override;
  /// Check if the vertex with the given id has outgoing hyperarcs
  bool hasOutHyperarcs(const VertexIdType& vertexId) const override;
  /// Const hyperarc iterator pointing to the begin of the out-hyperarcs
  hyperarc_iterator outHyperarcsBegin(const VertexIdType& vertexId) const
    override;
  /// Const hyperarc iterator pointing to the end of the outgoing hyperarcs
  hyperarc_iterator outHyperarcsEnd(const VertexIdType& vertexId) const
    override;
  /// Pair of hyperarc iterators pointing to the begin/end of the out-arcs
  std::pair<hyperarc_iterator, hyperarc_iterator> outHyperarcsBeginEnd(
          const VertexIdType& vertexId) const override;
  /// Get shared_ptr of container of the out-arcs of the given vertex Id
  std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> outHyperarcs(
          const VertexIdType& vertexId) const override;

  /// Remove a vertex with the given name from the directed hypergraph.
  void removeVertex(const std::string& vertexName,
                    bool removeImpliedHyperarcs = true) override;
  /// Remove a vertex with the given Id from the directed hypergraph.
  void removeVertex(const VertexIdType& vertexId,
                    bool removeImpliedHyperarcs = true) override;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex/DirectedHyperedge dependent access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if the vertex is a tail vertex of the hyperarc
  bool isTailVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const override;
  /// Check if the vertex is a head vertex of the hyperarc
  bool isHeadVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const override;
  /// Check if the vertex is a tail or a head vertex of the hyperarc
  bool isVertexOfHyperarc(const VertexIdType& vertexId,
                          const HyperedgeIdType& hyperarcId) const override;

  /* **************************************************************************
  * **************************************************************************
  *                        HyperarcProperty access
  * **************************************************************************
  * *************************************************************************/
 public:
  /// Get properties of the hyperarc with the provided id
  const HyperarcProperty* getHyperarcProperties(
          const HyperedgeIdType& hyperarcId) const override;
  /// Get properties of the hyperarc with the provided id
  HyperarcProperty* getHyperarcProperties_(const HyperedgeIdType& hyperarcId)
  const override;

  /* **************************************************************************
   * **************************************************************************
   *                  Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get root hypergraph
  const DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t,
          VertexProperty, HyperarcProperty,
          HypergraphProperty>* getRootHypergraph() const override;

  /* **************************************************************************
   * **************************************************************************
   *                        Protected methods
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Construct a hyperarc and a HyperarcProperty object
  const Hyperarc_t* buildHyperarcAndItsDependencies(
          std::unique_ptr<GraphElementContainer<Vertex_t*>> tails,
          std::unique_ptr<GraphElementContainer<Vertex_t*>> heads,
          const typename Hyperarc_t::SpecificAttributes& attributes);
  /// Remove vertex dependencies.
  void removeVertexDependencies(const Vertex_t& vertex);
  /// Remove hyperarc dependencies
  void removeHyperarcDependencies(const Hyperarc_t& hyperarc);
  /// Remove nullptr entries from hyperarc properties container
  void removeNullEntriesFromHyperarcProperties();
  /// Return shared_ptr to hyperarc container
  HyperarcContainerPtr getRootHyperarcContainerPtr() const override;


  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Hyperarcs of the graph
  HyperarcContainerPtr hyperarcs_;
  /// Property per hyperarc
  HyperarcPropertyContainerPtr hyperarcProperties_;
  /// Hyperarc max Id
  HyperedgeIdType hyperarcMaxId_;
};


/**
 * \brief Operator<<
 *
 * \details
 * This function prints first isolated vertices, followed by a list of
 * hyperarcs.
 *
 * \par Example output
 * \code
   # Isolated vertices
   "B", "Bar"
   # Hyperarcs
   {"A", "C"} -> {"Foo", "Y"}
   {"6"} -> {"", "X"}
 * \endcode
 *
 *
 * \param out std::ostream
 * \param hypergraph To be printed hypergraph.
 *
 * \return std::ostream
 *
 * \relates DirectedHypergraph
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::ostream& operator<< (std::ostream& out,
                          const DirectedHypergraph<VertexTemplate_t,
                                  Hyperarc_t, VertexProperty, HyperarcProperty,
                                  HypergraphProperty>& hypergraph) {
  // print isolated vertices
  out << "# Isolated vertices\n";
  std::string padding = "";
  for (const auto& vertex : hypergraph.vertices()) {
    if (not hypergraph.hasInHyperarcs(vertex->id()) and
            not hypergraph.hasOutHyperarcs(vertex->id())) {
      out << padding << *vertex;
      padding = ", ";
    }
  }
  if (padding.compare(", ") == 0) {  // at least one isolated vertex
    out << '\n';
  }

  // print hyperarcs
  out << "# Hyperarcs\n";
  for (const auto& hyperarc : hypergraph.hyperarcs()) {
    out << *hyperarc << std::endl;
  }

  return out;
}
}  // namespace hglib

#include "DirectedHypergraph.hpp"
#endif  // HGLIB_DIRECTEDHYPERGRAPH_H
