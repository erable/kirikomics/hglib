// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "DirectedHypergraph.h"

#include <algorithm>
#include <stdexcept>
#include <string>
#include <unordered_set>
#include <vector>

#include "filtered_vector.h"

#include "DirectedSubHypergraph.h"

namespace hglib {

 /**
  * \brief DirectedHypergraph constructor
  *
  * \details
  * Creates a DirectedHypergraph allowing or not to have multiple
  * hyperarcs with the same tail and head vertices. Vertices and then hyperarcs
  * must be added via the functions addVertex() and addHyperarc().
  *
  * \param allowMultiHyperarcs Flag to signal if multi-hyperarcs are allowed.
  * Defaults to true.
  */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
DirectedHypergraph(bool allowMultiHyperarcs) :
        DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t,
                VertexProperty, EdgeProperty, HypergraphProperty>(),
        Hypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
                EdgeProperty, HypergraphProperty>(allowMultiHyperarcs),
        hyperarcMaxId_(-1) {
  // create hyperarcs_
  hyperarcs_ = create_filtered_vector<Hyperarc_t*>(
           create_filtered_vector<Hyperarc_t*>(),
           std::make_unique<NullptrFilter<Hyperarc_t*>>());
  // create hyperarcProperties_
  hyperarcProperties_ = create_filtered_vector<HyperarcProperty_type*>(
          create_filtered_vector<HyperarcProperty_type*>(),
          std::make_unique<NullptrFilter<HyperarcProperty_type*>>());
}


/**
 * \brief Copy constructor
 *
 * \details
 * Makes a deep copy of the given directed hypergraph.
 *
 * \param other The to be copied directed hypergraph.
 *
 * \par Complexity
 * Linear in the number of vertices and hyperarcs of other.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
DirectedHypergraph(const DirectedHypergraph& other) :
        DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>(),
        Hypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
                HyperarcProperty, HypergraphProperty>(
                other),
        hyperarcMaxId_(-1) {
  try {
    // create hyperarcs_
    hyperarcs_ = create_filtered_vector<Hyperarc_t*>(
            create_filtered_vector<Hyperarc_t*>(),
            std::make_unique<NullptrFilter<Hyperarc_t*>>());
    // create hyperarcProperties_
    hyperarcProperties_ =
            create_filtered_vector<HyperarcProperty_type*>(
            create_filtered_vector<HyperarcProperty_type*>(),
            std::make_unique<NullptrFilter<HyperarcProperty_type*>>());
    // copy edges and associated edge properties
    for (std::size_t i = 0; i < other.hyperarcs_->size(); ++i) {
      Hyperarc_t* hyperarc = other.hyperarcs_->operator[](i);
      ++hyperarcMaxId_;
      // hyperarc was deleted in other
      if (hyperarc == nullptr) {
        hyperarcs_->push_back(nullptr);
        hyperarcProperties_->push_back(nullptr);
      } else {
        Hyperarc_t* copy = new Hyperarc_t(*hyperarc);
        // add tails to hyperarc
        for (const auto& vertex : *(hyperarc->tails_.get())) {
          auto& tail = this->vertices_->operator[](vertex->id());
          copy->addTailVertex(tail);
          tail->addOutHyperarc(copy);
        }
        // add heads to hyperarc
        for (const auto& vertex : *(hyperarc->heads_.get())) {
          auto& head = this->vertices_->operator[](vertex->id());
          copy->addHeadVertex(head);
          head->addInHyperarc(copy);
        }
        // add hyperarc to hypergraph
        hyperarcs_->push_back(copy);
        // copy hyperarc property
        HyperarcProperty* hyperarcPropertyCopy = new HyperarcProperty(
                *other.hyperarcProperties_->operator[](i));
        hyperarcProperties_->push_back(hyperarcPropertyCopy);
      }
    }
  } catch (std::bad_alloc& e) {
    for (auto& hyperarc : *(hyperarcs_.get())) {
      delete hyperarc;
    }
    for (auto& hyperarcProperty : *(hyperarcProperties_.get())) {
      delete hyperarcProperty;
    }
    throw;
  }
}

/**
 * \brief Conversion constructor
 *
 * \details
 * Allows to convert a graph to another graph with the same Vertex and Edge
 * types but another set of (Vertex-, Edge- and Graph-) Properties
 *
 * \param other Hypergraph to be copied
 */
template <template <typename> typename VertexTemplate_t,
          typename Hyperarc_t,
          typename VertexProperty,
          typename HyperarcProperty,
          typename HypergraphProperty>
template <typename OtherVP, typename OtherEP, typename OtherGP>
DirectedHypergraph<
    VertexTemplate_t,
    Hyperarc_t,
    VertexProperty,
    HyperarcProperty,
    HypergraphProperty>::
DirectedHypergraph(const DirectedHypergraph<VertexTemplate_t,
                                            Hyperarc_t,
                                            OtherVP,
                                            OtherEP,
                                            OtherGP>& other) :
    DirectedHypergraphInterface<VertexTemplate_t,
                                Hyperarc_t,
                                VertexProperty,
                                HyperarcProperty,
                                HypergraphProperty>(),
    Hypergraph<VertexTemplate_t,
               Hyperarc_t,
               VertexProperty,
               HyperarcProperty,
               HypergraphProperty>(other),
    hyperarcMaxId_(-1) {
  try {
    // Initialise (empty) hyperarcs_
    hyperarcs_ = create_filtered_vector<Hyperarc_t*>(
        create_filtered_vector<Hyperarc_t*>(),
        std::make_unique<NullptrFilter<Hyperarc_t*>>());
    // Initialise (empty) hyperarcProperties_
    hyperarcProperties_ =
        create_filtered_vector<HyperarcProperty_type*>(
            create_filtered_vector<HyperarcProperty_type*>(),
            std::make_unique<NullptrFilter<HyperarcProperty_type*>>());
    // Copy hyperarcs and init associated properties
    for (std::size_t i = 0; i < other.hyperarcs_->size(); ++i) {
      Hyperarc_t* hyperarc = other.hyperarcs_->operator[](i);
      ++hyperarcMaxId_;
      // if hyperarc was deleted in other
      if (hyperarc == nullptr) {
        hyperarcs_->push_back(nullptr);
        hyperarcProperties_->push_back(nullptr);
      } else {
        Hyperarc_t* copy = new Hyperarc_t(*hyperarc);
        // Add tails to hyperarc
        for (const auto& vertex : *(hyperarc->tails_.get())) {
          auto& tail = this->vertices_->operator[](vertex->id());
          copy->addTailVertex(tail);
          tail->addOutHyperarc(copy);
        }
        // Add heads to hyperarc
        for (const auto& vertex : *(hyperarc->heads_.get())) {
          auto& head = this->vertices_->operator[](vertex->id());
          copy->addHeadVertex(head);
          head->addInHyperarc(copy);
        }
        // Add hyperarc to hypergraph
        hyperarcs_->push_back(copy);
        // Init hyperarc property
        hyperarcProperties_->push_back(new HyperarcProperty());
      }
    }
  } catch (std::bad_alloc& e) {
    for (auto& hyperarc : *(hyperarcs_.get())) {
      delete hyperarc;
    }
    for (auto& hyperarcProperty : *(hyperarcProperties_.get())) {
      delete hyperarcProperty;
    }
    throw;
  }
}

/**
 * \brief DirectedHypergraph destructor
 *
 * \details
 * Destructs the directed hypergraph. The destructors of the elements are
 * called.
 *
 * \par Complexity
 * Linear in the number of vertices and hyperarcs.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
~DirectedHypergraph() {
  // Set the parent of all childs in the subgraph-hierarchy to null
  std::unordered_set<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
      VertexProperty, HyperarcProperty, HypergraphProperty>*> observers;
  for (auto itEvent = this->observers_.begin();
       itEvent != this->observers_.end(); ++itEvent) {
    for (auto itObserver : itEvent->second) {
      auto subgraph = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
          Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>*>(itObserver);
      observers.insert(subgraph);
    }
  }
  for (auto& subgraph : observers) {
    subgraph->setParentOfChildsToNull();
  }

  // Delete pointers to hyperarcs
  for (auto& hyperarc : *(hyperarcs_.get())) {
    delete hyperarc;
  }
  // Delete pointers to hyperarc properties
  for (auto& hyperarcProperty : *(hyperarcProperties_.get())) {
    delete hyperarcProperty;
  }
}


/**
 * \brief Copy assignment operator
 *
 * \details
 * Replaces the contents (vertices, hyperarcs, properties) of the directed
 * hypergraph.
 *
 * \param source Another DirectedHypergraph to use as data source.
 * \return
 * \code *this \endcode
 *
 * \par Complexity
 * Linear in the number of vertices and hyperarcs of *this and source.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>&
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
operator=(const DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>& source) {
  // check for self assignment
  if (this != &source) {
    // Call assignment operator of Hypergraph
    Hypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
            HyperarcProperty, HypergraphProperty>::operator=(source);

    // Set the parent of all childs in the subgraph-hierarchy to null
    std::unordered_set<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
            VertexProperty, HyperarcProperty, HypergraphProperty>*> observers;
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver : itEvent->second) {
        auto subgraph = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
                Hyperarc_t, VertexProperty, HyperarcProperty,
                HypergraphProperty>*>(itObserver);
        observers.insert(subgraph);
      }
    }
    for (auto& subgraph : observers) {
      subgraph->setParentOfChildsToNull();
    }

    // deallocate hyperarcs_
    for (auto& hyperarc : *(hyperarcs_.get())) {
      delete hyperarc;
    }
    hyperarcs_->clear();

    // deallocate hyperarcProperties_
    for (auto& eProp : *(hyperarcProperties_.get())) {
      delete eProp;
    }
    hyperarcProperties_->clear();


    // re-init the arc max Id
    hyperarcMaxId_ = -1;

    try {
      // copy arcs and associated arc properties
      for (std::size_t i = 0; i < source.hyperarcs_->size(); ++i) {
        Hyperarc_t* hyperarc = source.hyperarcs_->operator[](i);
        ++hyperarcMaxId_;
        // hyperarc was deleted in directedHypergraph
        if (hyperarc == nullptr) {
          hyperarcs_->push_back(nullptr);
          hyperarcProperties_->push_back(nullptr);
        } else {
          Hyperarc_t* copy = new Hyperarc_t(*hyperarc);
          // add tails to hyperarc
          for (const auto& vertex : *(hyperarc->tails_.get())) {
            auto& tail = this->vertices_->operator[](vertex->id());
            copy->addTailVertex(tail);
            tail->addOutHyperarc(copy);
          }
          // add heads to hyperarc
          for (const auto& vertex : *(hyperarc->heads_.get())) {
            auto& head = this->vertices_->operator[](vertex->id());
            copy->addHeadVertex(head);
            head->addInHyperarc(copy);
          }
          // add hyperarc to hypergraph
          hyperarcs_->push_back(copy);
          // copy hyperarc property
          HyperarcProperty* hyperarcPropertyCopy = new HyperarcProperty(
                  *source.hyperarcProperties_->operator[](i));
          hyperarcProperties_->push_back(hyperarcPropertyCopy);
        }
      }
    } catch (std::bad_alloc& e) {
      for (auto& hyperarc : *(hyperarcs_.get())) {
        delete hyperarc;
      }
      for (auto& hyperarcProperty : *(hyperarcProperties_.get())) {
        delete hyperarcProperty;
      }
      throw;
    }
  }
  return *this;
}


/**
 * \brief Check if this directed hypergraph supports multi-hyperarcs.
 *
 * \return True if multi-hyperarcs are supported, false otherwise.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
allowMultiHyperarcs() const {
  return this->allowMultiHyperedges_;
}


/**
 * \brief Number of hyperarcs in the hypergraph
 *
 * \return Number of hyperarcs in the hypergraph.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
size_t DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::
nbHyperarcs() const {
  size_t nbHyperarcs = 0;
  auto it = hyperarcs_->cbegin();
  auto end = hyperarcs_->cend();
  for (; it != end; ++it) {
    ++nbHyperarcs;
  }
  return nbHyperarcs;
}


/**
 * \brief Size of the hyperarc container
 *
 * \details
 * Pointers to hyperarc objects are stored in a customized vector. If a
 * hyperarc is removed from the hypergraph, then the corresponding entry in
 * this vector is set to a nullptr. The size of the container remains unchanged.
 * Thus, the return value of this function may differ from nbHyperarcs().
 *
 * \return Size of the hyperarc container.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarcContainerSize() const {
  return hyperarcs_->size();
}


/**
 * \brief Search hyperarc list for an hyperarc with the provided id
 *
 * \param hyperarcId Identifier of the hyperarc.
 * \return Pointer to hyperarc if found, nullptr if hyperarcId is between 0
 * and hyperarcContainerSize() - 1, throw out_of_range exception otherwise
 *
 * \throw out_of_range If given Id is not in the range
 * [0, hyperarcContainerSize() )
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const Hyperarc_t* DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::
hyperarcById(const HyperedgeIdType & hyperarcId) const {
  try {
    return hyperarcs_->at(hyperarcId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "hyperarcById: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to begin of the hyperarcs.
 *
 * \return Const hyperarc iterator pointing to begin of the hyperarcs.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the hyperarc container.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::hyperarc_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
hyperarcsBegin() const {
  return hyperarcs_->cbegin();
}


/**
 * \brief Const hyperarc iterator pointing to end of the hyperarcs.
 *
 * \return Const hyperarc iterator pointing to end of the hyperarcs.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::hyperarc_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
hyperarcsEnd() const {
  return hyperarcs_->cend();
}


/**
 * \brief Begin/End iterator pair of the hyperarcs of the directed hypergraph
 *
 * \details
 * Pair of hyperarc iterators pointing to the begin and the end of the
 * hyperarcs of the directed hypergraph.
 *
 * \return Pair of hyperarc iterators pointing to the begin and the end of the
 * hyperarcs of the directed hypergraph.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the hyperarc container.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
std::pair<typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::hyperarc_iterator,
        typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
                VertexProperty, EdgeProperty, HypergraphProperty>::
        hyperarc_iterator> DirectedHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, EdgeProperty, HypergraphProperty>::
hyperarcsBeginEnd() const {
  return std::make_pair(hyperarcsBegin(), hyperarcsEnd());
}


/**
 * \brief Return a reference to the container of hyperarcs.
 *
 * Can be used in a range based for loop as follows:
 * \code
 * for (const auto& hyperarc : g.hyperarcs()) {
 *   std::cout << "Id of the hyperarc: " << hyperarc->id() << '\n';
 * }
 * \endcode
 *
 * \return Reference to hyperarc container
 *
 * \par Complexity
 * Constant (return the reference). However, the complexity to iterate over all
 * hyperarcs of a directed hypergraph using a range based for loop or a
 * classic for loop using hyperarcsBegin() and hyperarcsEnd() is the same.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::HyperarcContainer&
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
hyperarcs() const {
  return *(hyperarcs_.get());
}


/**
 * \brief Return number of tail vertices in the hyperarc with given id.
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return Number of tail vertices in the hyperarc with given id.
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
nbTailVertices(const HyperedgeIdType& hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::nbTailVertices "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->tails_->size();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "nbTailVertices: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Check if the hyperarc with the given id has tail vertices.
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return True if the hyperarc with the given identifier has tail vertices,
 * false otherwise.
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hasTailVertices(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "hasTailVertices "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return not hyperarc->tails_->empty();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "nbTailVertices: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the begin of the tail vertices
 *
 * \details
 * Const vertex iterator pointing to the begin of the tail vertices of the
 * hyperarc with the given identifier.
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return Const vertex iterator pointing to the begin of the tail vertices of
 * the hyperarc with the given identifier.
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
tailsBegin(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::tailsBegin "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->tails_->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "tailsBegin: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the end of the tail vertices
 *
 * \details
 * Const vertex iterator pointing to the end of the tail vertices of the
 * hyperarc with the given identifier.
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return Const vertex iterator pointing to the end of the tail vertices of
 * the hyperarc with the given identifier.
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
tailsEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::tailsEnd "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->tails_->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "tailsEnd: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Pair of vertex iterators pointing to the begin/end of the tail
 * vertices
 *
 * \details
 * This function returns a pair of const vertex iterators pointing to the begin
 * and the end of the tail vertices of the hyperarc with the given identifier.
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return Pair of const vertex iterators pointing to the begin and the end of
 * the tail vertices of the hyperarc with the given identifier.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::vertex_iterator,
        typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>::
        vertex_iterator> DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
tailsBeginEnd(const HyperedgeIdType& hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::tailsEnd "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return std::make_pair(hyperarc->tails_->cbegin(),
                          hyperarc->tails_->cend());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "tailsBeginEnd: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Get shared_ptr of container of the tail vertices of given hyperarc id
 *
 * \details
 * Can be used in a range based for loop as follows:
 * \code
 * auto tailsPtr = g.tails(hyperarcId);
 * for (const auto& tail : *tailsPtr) {
 *   std::cout << tail->id() << " is a tail vertex of " << hyperarcId << '\n';
 * }
 * \endcode
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return shared_ptr of the container of the tail vertices of the given
 * hyperarc id.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<typename DirectedHypergraph<
    VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
    HypergraphProperty>::Vertex_t*>>
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
tails(const HyperedgeIdType& hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::tails "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->tails_;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "tails: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}

/**
 * \brief Return number of head vertices in the hyperarc with the given id.
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return Number of head vertices in the hyperarc with given id.
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
nbHeadVertices(const HyperedgeIdType& hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::nbHeadVertices "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->heads_->size();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "nbHeadVertices: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Check if the hyperarc with the given id has head vertices.
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return True if the hyperarc with the given identifier has head vertices,
 * false otherwise.
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hasHeadVertices(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "hasHeadVertices "
                                       "with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return not hyperarc->heads_->empty();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "hasHeadVertices: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the begin of the head vertices
 *
 * \details
 * Const vertex iterator pointing to the begin of the head vertices of the
 * hyperarc with the given identifier.
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return Const vertex iterator pointing to the begin of the head vertices of
 * the hyperarc with the given identifier.
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
headsBegin(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::headsBegin"
                                       " with "
                                       "the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->heads_->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "headsBegin: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the end of the head vertices
 *
 * \details
 * Const vertex iterator pointing to the end of the head vertices of the
 * hyperarc with the given identifier.
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return Const vertex iterator pointing to the end of the head vertices of
 * the hyperarc with the given identifier.
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
headsEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::headsEnd"
                                       " with the "
                                       "hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->heads_->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "headsEnd: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Pair of vertex iterators pointing to the begin/end of the head
 * vertices
 *
 * \details
 * This function returns a pair of const vertex iterators pointing to the begin
 * and the end of the head vertices of the hyperarc with the given identifier.
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return Pair of const vertex iterators pointing to the begin and the end of
 * the head vertices of the hyperarc with the given identifier.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::vertex_iterator,
        typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>::
        vertex_iterator> DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
headsBeginEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::headsEnd"
                                       " with the "
                                       "hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return std::make_pair(hyperarc->heads_->cbegin(),
                          hyperarc->heads_->cend());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "headsEnd: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}

/**
 * \brief Get shared_ptr of container of the head vertices of given hyperarc id
 *
 * \details
 * Can be used in a range based for loop as follows:
 * \code
 * auto headsPtr = g.heads(hyperarcId);
 * for (const auto& head : *headsPtr) {
 *   std::cout << head->id() << " is a head vertex of " << hyperarcId << '\n';
 * }
 * \endcode
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return shared_ptr of the container of the head vertices of the given
 * hyperarc id.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<typename DirectedHypergraph<
    VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
    HypergraphProperty>::Vertex_t*>> DirectedHypergraph<VertexTemplate_t,
    Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
heads(const HyperedgeIdType& hyperarcId) const {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::heads with the "
                                       "hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperarc->heads_;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "heads: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Establish consecutive hyperarc ids
 *
 * \details
 * After deleting hyperarcs from the hypergraph, the hyperarc ids
 * are not consecutive anymore. This function reassigns
 * consecutive ids to the hyperarcs.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
resetHyperarcIds() {
  // Notify observers
  this->NotifyObservers(ObservableEvent::RESET_EDGE_IDS);
  // erase all nullptrs
  // remove nullptrs from underlying vector: std::remove puts all nullptrs at
  // the end of the vector
  auto posFirstNullptr = std::remove(hyperarcs_->data(),
                                     hyperarcs_->data() +
                                             hyperarcs_->size(),
                                     static_cast<Hyperarc_t*>(NULL));
  // Compute nb of nullptr
  size_t nbNullptr = (hyperarcs_->data() + hyperarcs_->size()) -
          posFirstNullptr;
  // Resize
  hyperarcs_->resize(hyperarcs_->size() - nbNullptr);

  // assign new Ids
  HyperedgeIdType id(-1);
  for (auto it = hyperarcs_->begin(); it != hyperarcs_->end();
       ++it) {
    (*it)->setId(++id);  // assign new Id
  }

  // set vertexMaxId_ to the highest assigned id
  hyperarcMaxId_ = id;

  // remove nullptr entries from vector of hyperarc properties
  removeNullEntriesFromHyperarcProperties();
}


/**
 * \brief Establish consecutive vertex and hyperarc ids
 *
 * \details
 * After deleting vertices and hyperarcs from the hypergraph the vertex and
 * hyperarc ids are not consecutive anymore. This function reassigns
 * consecutive ids to the vertices and the hyperarcs.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
resetIds() {
  this->resetVertexIds();
  resetHyperarcIds();
}


/**
 * \brief Return number of incoming hyperarcs of the vertex with the given id
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Number of incoming hyperarcs of the vertex with the given Id.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
inDegree(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::inDegree"
                                       " with the "
                                       "vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->inHyperarcs_->size();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "inDegree: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Check if the vertex with the given id has incoming hyperarcs
 *
 * \param vertexId Identifier of a vertex.
 * \return True if the vertex with the given identifier has incoming hyperarcs,
 * false otherwise.
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hasInHyperarcs(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::hasInHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return not vertex->inHyperarcs_->empty();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "hasInHyperarcs: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to the begin of the in-hyperarcs
 *
 * \details
 * Const hyperarc iterator pointing to the begin of the incoming hyperarcs of
 * the vertex with the given identifier.
 *
 * \param vertexId Identifier of a vertex.
 * \return Const hyperarc iterator pointing to the begin of the incoming
 * hyperarcs of the vertex with the given identifier.
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
*/
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
inHyperarcsBegin(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "inHyperarcsBegin"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->inHyperarcs_->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "inHyperarcsBegin: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to the end of the incoming hyperarcs
 *
 * \details
 * Const hyperarc iterator pointing to the end of the incoming hyperarcs of the
 * vertex with the given identifier.
 *
 * \param vertexId Identifier of a vertex.
 * \return Const hyperarc iterator pointing to the end of the incoming
 * hyperarcs of the vertex with the given identifier.
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
*/
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
inHyperarcsEnd(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::inHyperarcsEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->inHyperarcs_->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "inHyperarcsEnd: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Pair of hyperarc iterators pointing to the begin/end of the in-arcs
 *
 * \details
 * This function returns a pair of const hyperarc iterators pointing to the
 * begin and the end of the incoming hyperarcs of the vertex with the given
 * identifier.
 *
 * \param vertexId Identifier of a vertex.
 * \return Pair of const hyperarc iterator pointing to the begin/end of the
 * incoming hyperarcs of the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
*/
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator, typename DirectedHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator> DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
inHyperarcsBeginEnd(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::inHyperarcsEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return std::make_pair(vertex->inHyperarcs_->cbegin(),
                          vertex->inHyperarcs_->cend());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "inHyperarcsBeginEnd: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Get shared_ptr of container of the in-arcs of the given vertex Id
 *
 * \details
 * Can be used in a range based for loop as follows:
 * \code
 * auto inarcsPtr = g.inHyperarcs(vertexId);
 * for (const auto& arc : *inarcsPtr) {
 *   std::cout << arc->id() << " is a incoming hyperarc of vertex "
 *             << vertexId << '\n';
 * }
 * \endcode
 *
 * \param vertexId Identifier of a vertex.
 * \return shared_ptr of the container of the incoming hyperarcs of the given
 * vertex id.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> DirectedHypergraph<
    VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
    HypergraphProperty>::
inHyperarcs(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::inHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->inHyperarcs_;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "inHyperarcs: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Return number of outgoing hyperarcs of the vertex with the given id
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Number of outgoing hyperarcs of the vertex with the given Id.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
outDegree(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::outDegree"
                                       " with the "
                                       "vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->outHyperarcs_->size();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "outDegree: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Check if the vertex with the given id has outgoing hyperarcs
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return True if the vertex with the given identifier has outgoing hyperarcs,
 * false otherwise.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hasOutHyperarcs(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "hasOutHyperarcs "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return not vertex->outHyperarcs_->empty();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "hasOutHyperarcs: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to the begin of the out-hyperarcs
 *
 * \details
 * Const hyperarc iterator pointing to the begin of the outgoing hyperarcs of
 * the vertex with the given identifier.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Const hyperarc iterator pointing to the begin of the outgoing
 * hyperarcs of the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
*/
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
outHyperarcsBegin(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "outHyperarcsBegin"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->outHyperarcs_->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "outHyperarcsBegin: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to the end of the outgoing hyperarcs
 *
 * \details
 * Const hyperarc iterator pointing to the end of the outgoing hyperarcs of the
 * vertex with the given identifier.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Const hyperarc iterator pointing to the end of the outgoing
 * hyperarcs of the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
*/
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
outHyperarcsEnd(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "outHyperarcsEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->outHyperarcs_->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "outHyperarcsEnd: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Pair of hyperarc iterators pointing to the begin/end of the out-arcs
 *
 * \details
 * This function returns a pair of const hyperarc iterators pointing to the
 * begin and the end of the outgoing hyperarcs of the vertex with the given
 * identifier.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Pair of const hyperarc iterator pointing to the begin/end of the
 * outgoing hyperarcs of the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
*/
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator, typename DirectedHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator> DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
outHyperarcsBeginEnd(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "outHyperarcsEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return std::make_pair(vertex->outHyperarcs_->cbegin(),
                          vertex->outHyperarcs_->cend());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "outHyperarcsBeginEnd: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Get shared_ptr of container of the out-arcs of the given vertex Id
 *
 * \details
 * Can be used in a range based for loop as follows:
 * \code
 * auto outarcsPtr = g.outHyperarcs(vertexId);
 * for (const auto& arc : *outarcsPtr) {
 *   std::cout << arc->id() << " is a outgoing hyperarc of vertex "
 *             << vertexId << '\n';
 * }
 * \endcode
 *
 * \param vertexId Identifier of a vertex.
 * \return shared_ptr of the container of the outgoing hyperarcs of the given
 * vertex id.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> DirectedHypergraph<
    VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
    HypergraphProperty>::
outHyperarcs(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::outHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->outHyperarcs_;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "outHyperarcs: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Remove a vertex with the given name from the directed hypergraph.
 *
 * \details
 * This function removes a vertex with the given name from the directed
 * hypergraph. Incoming and outgoing hyperarcs of this vertex are also removed
 * if the second parameter is set to true.
 *
 * \param vertexName Name of a vertex
 * \param removeImpliedHyperarcs Flag that shows whether hyperarcs that contain
 * the vertex with the given name should be removed from the hypergraph.
 * Defaults to true.
 *
 * \throw invalid_argument If the directed hypergraph contains no vertex with
 * the given name.
 *
 * \par Complexity
 * Linear.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeVertex(const std::string& vertexName,
             bool removeImpliedHyperarcs) {
  // Find vertex by name in the hypergraph and call the method removeVertex
  // that takes the id of the vertex
  const auto& vertex = this->vertexByName(vertexName);
  if (vertex == nullptr) {
    std::string errorMessage("Called DirectedHypergraph::removeVertex"
                                     " with the vertex name " + vertexName +
                                     " which does not exists in the directed "
                                             "hypergraph.\n");
    throw std::invalid_argument(errorMessage);
  }

  removeVertex(vertex->id(), removeImpliedHyperarcs);
}


/**
 * \brief Remove a vertex with the given Id from the directed hypergraph.
 *
 * \details
 * This function removes a vertex with the given Id from the directed
 * hypergraph. Incoming and outgoing hyperarcs of this vertex are also removed
 * if the second parameter is set to true. This function is faster than the
 * other removeVertex function (via vertex name) as the access to the vertex is
 * constant instead of linear.
 *
 * \param vertexId Identifier of a vertex
 * \param removeImpliedHyperarcs Flag that shows whether hyperarcs that contain
 * the vertex with the given Id should be removed from the hypergraph.
 * Defaults to true.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeVertex(const VertexIdType& vertexId,
             bool removeImpliedHyperarcs) {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   *
   * Remove all implied hyperarcs if flag is set to true (default).
   * Otherwise, remove only the vertex v with given id 'vertexId' from the
   * tails and heads of the outgoing and incoming hyperarcs of v. Remove
   * hyperarcs that have neither tails nor heads.
   *
   * Delete the pointer at index vertexId in vertices_ and vertexProperties_
   * and assign nullptr.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::removeVertex"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }

    if (removeImpliedHyperarcs) {
      // hyperarcs that will be removed from the hypergraph
      std::vector<Hyperarc_t*> hyperarcsToBeRemoved;
      for (auto& hyperarc : *(hyperarcs_.get())) {
        // all hyperarcs that contain the vertex will be removed from the
        // hypergraph
        if (hyperarc != nullptr and isVertexOfHyperarc(vertexId,
                                                       hyperarc->id())) {
          hyperarcsToBeRemoved.emplace_back(hyperarc);
        }
      }
      // remove hyperarcs from hypergraph
      for (auto& hyperarc : hyperarcsToBeRemoved) {
        removeHyperarc(hyperarc->id());
      }
    } else {
      // remove vertex from tails_ (heads_) of its outgoing (incoming)
      // hyperarcs
      removeVertexDependencies(*vertex);
      // remove hyperarcs without tails and heads
      for (auto& hyperarc : *(hyperarcs_.get())) {
        if (hyperarc != nullptr and
            (not hasTailVertices(hyperarc->id()) and
             not hasHeadVertices(hyperarc->id()))) {
          removeHyperarc(hyperarc->id());
        }
      }
    }

    VertexIdType idx(vertexId);
    // Notify observers of change
    Remove_vertex_args args;
    args.vertexId_ = idx;
    args.removeImpliedHyperarcs_ = removeImpliedHyperarcs;
    this->NotifyObservers(ObservableEvent::VERTEX_REMOVED, &args);

    // assign NULL in vertices_ and vertexProperties_ at the index that
    // corresponds to the id of the vertex
    this->setVertexAndVertexPropertyEntryToNullptr(idx);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "removeVertex:"
            " index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
      throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Check if the vertex is a tail vertex of the hyperarc
 *
 * \details
 * This function checks whether the vertex with the given vertex Id is a tail
 * vertex of the hyperarc with the given hyperarc Id.
 *
 * \param vertexId Identifier of a vertex.
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return True if the vertex is a tail vertex of the hyperarc, false
 * otherwise
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ) or the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear in the number of tail vertices of the given hyperarc.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
isTailVertexOfHyperarc(const VertexIdType& vertexId,
                       const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the vertex/hyperarc with the given ids.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex/hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "isTailVertexOfHyperarc with the "
                                       "hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "isTailVertexOfHyperarc with the "
                                       "vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    for (const auto& tail : *(hyperarc->tails_.get())) {
      if (tail->id() == vertex->id()) {
        return true;
      }
    }
    return false;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "isTailVertexOfHyperarc: " << '\n' <<
              "index vertex = " << vertexId << "; min = 0; max = " <<
              this->vertices_->size() << '\n' <<
              "index hyperarc = " << hyperarcId << "; min = 0; max = " <<
              hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Check if the vertex is a head vertex of the hyperarc
 *
 * \details
 * This function checks whether the vertex with the given vertex Id is a head
 * vertex of the hyperarc with the given hyperarc Id.
 *
 * \param vertexId Identifier of a vertex.
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return True if the vertex is a head vertex of the hyperarc, false
 * otherwise
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ) or the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear in the number of head vertices of the given hyperarc.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
isHeadVertexOfHyperarc(const VertexIdType& vertexId,
                       const HyperedgeIdType & hyperarcId) const {
  /*
   * Get first the pointer to the vertex/hyperarc with the given ids.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex/hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "isHeadVertexOfHyperarc with the "
                                       "hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::"
                                       "isHeadVertexOfHyperarc with the "
                                       "vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    for (const auto& tail : *(hyperarc->heads_.get())) {
      if (tail->id() == vertex->id()) {
        return true;
      }
    }
    return false;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "isTailVertexOfHyperarc: " << '\n' <<
              "index vertex = " << vertexId << "; min = 0; max = " <<
              this->vertices_->size() << '\n' <<
              "index hyperarc = " << hyperarcId << "; min = 0; max = " <<
              hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Check if the vertex is a tail or a head vertex of the hyperarc
 *
 * \details
 * This function checks whether the vertex with the given vertex Id is a tail
 * or a head vertex of the hyperarc with the given hyperarc Id.
 *
 * \param vertexId Identifier of a vertex.
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return True if the vertex is a tail or head vertex of the hyperarc, false
 * otherwise
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ) or the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear in the number of tail and head vertices of the given hyperarc.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
isVertexOfHyperarc(const VertexIdType& vertexId,
                   const HyperedgeIdType & hyperarcId) const {
  try {
    if (isTailVertexOfHyperarc(vertexId, hyperarcId) or
          isHeadVertexOfHyperarc(vertexId, hyperarcId)) {
      return true;
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "Out of range in DirectedHypergraph::"
            "isVertexOfHyperarc\n";
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    std::cerr << "Invalid argument exception in DirectedHypergraph::"
            "isVertexOfHyperarc\n";
    throw;
  }
  return false;
}


/**
 * \brief Get properties of the hyperarc with the provided id
 *
 * \details
 * This function returns a pointer to a const (read only) hyperarc property
 * associated to the hyperarc with the specified hyperarc Id. If the hyperarc
 * was removed earlier from the network, then this function returns a nullptr.
 *
 * \param hyperarcId Identifier of the hyperarc.
 *
 * \return Pointer to const hyperarc property type, nullptr if the hyperarc was
 * removed earlier from the directed hypergraph.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const HyperarcProperty* DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getHyperarcProperties(const HyperedgeIdType& hyperarcId) const {
  try {
    const auto& property = hyperarcProperties_->at(hyperarcId);
    if (property == nullptr) {
      throw std::invalid_argument("Error in DirectedHypergraph::"
                                      "getHyperarcProperties(): Hyperarc Id "
                                  + std::to_string(hyperarcId)
                                  + " is not declared in this hypergraph.");
    }
    return property;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "getHyperarcProperties: index = " << hyperarcId <<
              "; min = 0; " <<
              "max = " << hyperarcProperties_->size() << '\n';
    throw;
  }
}

/**
 * \brief Get properties of the hyperarc with the provided id
 *
 * \details
 * This function returns a pointer to a hyperarc property associated to the
 * hyperarc with the specified hyperarc Id. This is a non-const version of
 * getHyperarcProperties(). If the hyperarc was removed earlier from the
 * network, then this function returns a nullptr.
 *
 * \param hyperarcId Identifier of the hyperarc.
 *
 * \return Pointer to hyperarc property type, nullptr if the hyperarc was
 * removed earlier from the directed hypergraph.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
HyperarcProperty* DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getHyperarcProperties_(const HyperedgeIdType& hyperarcId) const {
  try {
    return const_cast<HyperarcProperty*>(this->getHyperarcProperties(
            hyperarcId));
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "getHyperarcProperties_: index = " << hyperarcId <<
              "; min = 0; " <<
              "max = " << hyperarcProperties_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument("Error in DirectedHypergraph::"
                                    "getHyperarcProperties_(): Hyperarc Id "
                                + std::to_string(hyperarcId)
                                + " is not declared in this hypergraph.");
  }
}


/**
 * \brief Get root hypergraph
 *
 * \details
 * A directed hypergraph can have directed sub-hypergraphs (views). This
 * hierarchy is implemented through the observer and decorator pattern. A
 * subgraph observes its parent graph. This function returns the root
 * hypergraph of the hierarchy.
 *
 * \return
 * \code this \endcode
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>*
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
getRootHypergraph() const {
  return this;
}


/**
 * \brief Add a hyperarc
 *
 * \details
 * Add a hyperarc with the provided tail vertex names, head vertex names,
 * and additional parameters (if needed to create a hyperarc of type
 * Hyperarc_t) to the hypergraph.
 *
 * \param tails_heads Tail and head vertex names.
 * \param args Additional arguments to create a hyperarc of type Hyperarc_t.
 *
 * \return Pointer to const hyperarc on success, otherwise an exception is
 * thrown.
 *
 * \throw invalid_argument If no vertex with a given vertex name exists in the
 * hypergraph.
 * \throw invalid_argument If multi-hyperarcs are not allowed and there exists
 * already a hyperarc with the same tail and head vertices in the hypergraph.
 *
 * \par Complexity
 * Linear in the number of vertices (search vertices with given names) and,
 * if multi-hyperarcs are not allowed, linear in the number of hyperarcs.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const Hyperarc_t* DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
 VertexProperty, HyperarcProperty, HypergraphProperty>::
addHyperarc(decltype(hglib::NAME),
            const TailAndHeadNames& tails_heads,
            const typename Hyperarc_t::SpecificAttributes& attributes) {
  /*
   * Get first the ids of the vertices with given names. An invalid_argurment
   * exception is thrown if a vertex name does not exists in the hypergraph.
   * Then, call addHyperarc(TailAndHeadIds, args) to add the hyperarc.
   */
  std::vector<VertexIdType> tailIds =
          this->getVertexIdsFromNames(tails_heads.first);
  std::vector<VertexIdType> headIds =
          this->getVertexIdsFromNames(tails_heads.second);
  return addHyperarc(std::make_pair(tailIds, headIds), attributes);
}


/**
 * \brief Add a hyperarc
 *
 * \details
 * Add a hyperarc with the provided tail vertex Ids, head vertex Ids,
 * and additional parameters (if needed to create a hyperarc of type
 * Hyperarc_t) to the hypergraph.
 *
 * \param tails_heads Tail and head vertex Ids.
 * \param args Additional arguments to create a hyperarc of type Hyperarc_t.
 *
 * \return Pointer to const hyperarc on success, otherwise an exception is
 * thrown.
 *
 * \throw out_of_range If a given vertex Id is not in the range
 * [0, vertexContainerSize() )
 * \throw invalid_argument If a given vertex Id was removed before from the
 * hypergraph (The function vertexById() returns a nullptr in this case).
 * \throw invalid_argument If multi-hyperarcs are not allowed and there exists
 * already a hyperarc with the same tail and head vertices in the hypergraph.
 *
 * \par Complexity
 * Linear in the number of tail and head vertices (search vertices with given
 * Ids in constant time) and, if multi-hyperarcs are not allowed, linear in the
 * number of hyperarcs.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const Hyperarc_t* DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
addHyperarc(const TailAndHeadIds& tails_heads,
            const typename Hyperarc_t::SpecificAttributes& attributes) {
  // Check if all tail and head vertices are declared in the hypergraph.
  // If not, an exception is thrown.
  // In the case that multi-hyperarcs are not allowed we check if there exists
  // already a hyperarc with the same tails and heads. If this is the case we
  // throw an exception. Otherwise and in case multi-hyperarcs are allowed we
  // add the hyperarc to the hypergraph.

  // The following 2 vectors will hold pointers to the tail (resp. head)
  // DirectedVertex_t corresponding to the provided vertex ids
  std::vector<const Vertex_t*> tails_p_const, heads_p_const;
  for (const auto& vertexId : tails_heads.first) {
    try {
      const auto &vertex = this->vertexById(vertexId);
      if (vertex == nullptr) {
        throw std::invalid_argument("Error in DirectedHypergraph::"
                                        "addHyperarc(): Tail vertex " +
                                        std::to_string(vertexId) +
                                        " is not declared in this hypergraph.");
      }
      tails_p_const.push_back(vertex);
    } catch (const std::out_of_range& oor) {
      throw std::out_of_range("Error in DirectedHypergraph::addHyperarc(): "
                                  "Tail vertex " +
                                  std::to_string(vertexId) +
                                  " is not declared in this hypergraph.");
    }
  }
  for (const auto& vertexId : tails_heads.second) {
    try {
      const auto &vertex = this->vertexById(vertexId);
      if (vertex == nullptr) {
        throw std::invalid_argument("Error in DirectedHypergraph::"
                                        "addHyperarc(): Head vertex " +
                                        std::to_string(vertexId) +
                                        " is not declared in this hypergraph.");
      }
      heads_p_const.push_back(vertex);
    } catch (const std::out_of_range& oor) {
      throw std::out_of_range("Error in DirectedHypergraph::addHyperarc(): "
                                  "Head vertex " +
                              std::to_string(vertexId) +
                              " is not declared in this hypergraph.");
    }
  }

  if (not this->allowMultiHyperedges_) {
    // check if there exists a hyperarc with the same tails and heads
    for (const auto& hyperarc : *(hyperarcs_.get())) {
      if (hyperarc != nullptr and
          hyperarc->DirectedHyperedgeBase<Vertex_t>::
                  compare(tails_p_const, heads_p_const)) {
        // build error message
        std::string tails("");
        for (auto& tailId : tails_heads.first) {
          tails += std::to_string(tailId) + ", ";
        }
        std::string heads("");
        for (auto& headId : tails_heads.second) {
          heads += std::to_string(headId) + ", ";
        }
        std::string errorMessage("Multiple hyperarcs are not allowed in this "
                                         "hypergraph, but the hyperarc with "
                                         "tail ids: " + tails + " and"
                                         " head ids: " + heads +
                                 " already exists.\n");
        throw std::invalid_argument(errorMessage);
      }
    }
  }

  // Non-const pointers are needed for the Hyperarc_t ctor
  std::unique_ptr<GraphElementContainer<Vertex_t*>> tails_p =
          create_filtered_vector<Vertex_t*>();
  std::unique_ptr<GraphElementContainer<Vertex_t*>> heads_p =
          create_filtered_vector<Vertex_t*>();
  for (const Vertex_t* vPtr : tails_p_const) {
    tails_p->push_back(const_cast<Vertex_t*>(vPtr));
  }
  for (const Vertex_t* vPtr : heads_p_const) {
    heads_p->push_back(const_cast<Vertex_t*>(vPtr));
  }
  // add hyperarc
  const auto hyperarc =
          buildHyperarcAndItsDependencies(std::move(tails_p),
                                          std::move(heads_p),
                                          attributes);
  return hyperarc;
}


/**
 * \brief Remove a hyperarc with the given id from the hypergraph
 *
 * \details
 * This function removes a hyperarc with the given id from the hypergraph. This
 * hyperarc \em e is also removed from the incoming (outgoing) hyperarcs of the
 * heads (tails) of \em e. This change is propagated to all views
 * (sub-hypergraphs).
 *
 * \param hyperarcId Identifier of the hyperarc.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear in the number of tails and heads of the hyperarc.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
removeHyperarc(const HyperedgeIdType& hyperarcId) {
  /*
   * Get first the pointer to the hyperarc with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperarc with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   *
   * Remove hyperarc h with given id from the incoming/outgoing hyperarc list
   * of the head/tail vertices of h.
   * Delete the pointer at index hyperarcId in hyperarcs_ and
   * hyperarcProperties_ and assign nullptr.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedHypergraph::removeHyperarc"
                                       " with "
                                       "the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }

    HyperedgeIdType idx(hyperarcId);
    removeHyperarcDependencies(*hyperarc);
    // Notify observers of change
    this->NotifyObservers(ObservableEvent::HYPEREDGE_REMOVED, &idx);

    // delete hyperarc
    delete hyperarcs_->operator[](idx);
    hyperarcs_->operator[](idx) = nullptr;
    // assign nullptr in hyperarcProperties_ at the index that corresponds
    // to the id of the hyperarc
    delete hyperarcProperties_->operator[](idx);
    hyperarcProperties_->operator[](idx) = nullptr;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedHypergraph::"
            "removeHyperarc: index = " << hyperarcId << "; min = 0; max = "
              << hyperarcs_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}

/* ****************************************************************************
 ******************************************************************************
 *                           Protected methods
 ******************************************************************************
 *****************************************************************************/


/**
 * \brief Construct a hyperarc and a HyperarcProperty object
 *
 * \details
 * Creates a hyperarc object of type Hyperarc_t and hyperarc dependencies are
 * solved, e.g. the hyperarc is added as incoming/outgoing hyperarc of its
 * heads/tails.
 * A HyperarcProperty object is created via the default constructor. Both, the
 * hyperarc and the HyperarcProperty are added to the directed hypergraph and
 * can be accessed via the assigned Id of the hyperarc.
 *
 * \param tails Unique_ptr to container of pointers to tail vertices.
 * \param heads Unique_ptr to container of pointers to head vertices.
 * \param attributes Additional parameters that are needed to build a hyperarc
 * of type Hyperarc_t.
 *
 * \return Pointer to const Hyperarc_t.
 *
 * \par Complexity
 * The time complexity depends on the constructors of Hyperarc_t and
 * HyperarcProperty.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const Hyperarc_t* DirectedHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
buildHyperarcAndItsDependencies(
        std::unique_ptr<GraphElementContainer<Vertex_t*>> tails,
        std::unique_ptr<GraphElementContainer<Vertex_t*>> heads,
        const typename Hyperarc_t::SpecificAttributes& attributes) {
  // A hyperarc x is created and a
  // pointer to this hyperarc is added to the list of hyperarcs of the
  // hypergraph. DirectedHyperedge dependencies are solved:
  // 1. Pointer to hyperarc x is added to the out-hyperarcs list of the tail
  // vertices of x.
  // 2. Pointer to hyperarc x is added to the in-hyperarcs list of the head
  // vertices of x.
  // A hyperarc property object is created using the default ctor.

  // create the hyperarc and add it to the list of hyperarcs of the hypergraph
  auto attr_p =
      std::make_unique<typename Hyperarc_t::SpecificAttributes>(attributes);
  auto hyperarc = new Hyperarc_t(++hyperarcMaxId_, std::move(tails),
                                 std::move(heads), std::move(attr_p));
  hyperarcs_->push_back(hyperarc);

  // Solve hyperarc dependencies
  // add hyperarc to outHyperarcs_ of tail vertices of the hyperarc
  for (auto&& tail : *(hyperarc->tails_.get())) {
    tail->addOutHyperarc(hyperarc);
  }
  // add hyperarc to inHyperarcs_ of head vertices of the hyperarc
  for (auto&& head : *(hyperarc->heads_.get())) {
    head->addInHyperarc(hyperarc);
  }

  // build hyperarc property and add it to the list of hyperarc properties
  auto hyperarcProperty = new HyperarcProperty();
  hyperarcProperties_->push_back(hyperarcProperty);
  return hyperarc;
}


/**
 * \brief Remove vertex dependencies.
 *
 * \details
 * Vertex dependencies of vertex \em v are removed, that is:
 * - the vertex \em v is removed from the list of tail vertices of the outgoing
 * hyperarcs of \em v,
 * - the vertex \em v is removed from the list of head vertices of the incoming
 * hyperarcs of \em v.
 *
 * \param Const reference to a vertex.
 *
 * \par Complexity
 * Linear in the number of incoming (outgoing) hyperarcs of vertex \em v and
 * the respective number of heads (tails) of these hyperarcs.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeVertexDependencies(const Vertex_t& vertex) {
  // remove the vertex from the list of tails of the vertex's outgoing hyperarcs
  for (auto&& edge : *(vertex.outHyperarcs_.get())) {
    edge->removeTailVertex(vertex);
  }
  // remove the vertex from the list of heads of the vertex's incoming hyperarcs
  for (auto&& edge : *(vertex.inHyperarcs_.get())) {
    edge->removeHeadVertex(vertex);
  }
}

/**
 * \brief Remove hyperarc dependencies
 *
 * \details
 * This function removes hyperarc dependencies of hyperarc \em h, that is:
 * - the hyperarc \em h is removed from the outgoing hyperarcs of the tail
 * vertices of the hyperarc \em h,
 * - the hyperarc \em h is removed from the incoming hyperarcs of the head
 * vertices of the hyperarc \em h.
 *
 * \param Const reference to a hyperarc.
 *
 * \par Complexity
 * Linear in the number of tail (head) vertices of hyperarc \em h and
 * the respective number of outgoing (incoming) hyperarcs of these vertices.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeHyperarcDependencies(const Hyperarc_t& hyperarc) {
  // remove hyperarc from outgoing hyperarc of tails
  for (auto&& tail : *(hyperarc.tails_.get())) {
    tail->removeOutHyperarc(hyperarc);
  }
  // remove hyperarc from incoming hyperarc of heads
  for (auto&& head : *(hyperarc.heads_.get())) {
    head->removeInHyperarc(hyperarc);
  }
}


/**
 * \brief Remove nullptr entries from hyperarc properties container
 *
 * \details
 * Remove nullptr entries from hyperarc properties container. This function
 * changes the size of the hyperarc properties container.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeNullEntriesFromHyperarcProperties() {
  // remove nullptrs from underlying vector: std::remove puts all nullptrs at
  // the end of the vector
  auto posFirstNullptr = std::remove(hyperarcProperties_->data(),
                                     hyperarcProperties_->data() +
                                             hyperarcProperties_->size(),
                                     static_cast<HyperarcProperty*>(NULL));
  // Compute nb of nullptr
  size_t nbNullptr = (hyperarcProperties_->data() +
          hyperarcProperties_->size()) - posFirstNullptr;
  // Resize
  hyperarcProperties_->resize(hyperarcProperties_->size() - nbNullptr);
}


/**
 * \brief Return shared_ptr to hyperarc container
 *
 * \details
 * A DirectedSubHypergraph, a view of a hypergraph, is implemented as a
 * combination of the observer and the decorator pattern. The hyperarc
 * container of the hypergraph is decorated with a particular filter in the
 * DirectedSubHypergraph. This function provides access to the hyperarc
 * container.
 *
 * \return Shared_ptr to hyperarc container.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::HyperarcContainerPtr
DirectedHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
getRootHyperarcContainerPtr() const {
  return hyperarcs_;
}
}  // namespace hglib
