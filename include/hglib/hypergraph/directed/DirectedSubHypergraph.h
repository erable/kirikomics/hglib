// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_SUBDIRECTEDHYPERGRAPH_H
#define HGLIB_SUBDIRECTEDHYPERGRAPH_H

#include <memory>
#include <string>
#include <unordered_set>
#include <vector>

#include "DirectedHypergraphInterface.h"
#include "hglib/utils/observation/Observer.h"
#include "hglib/hyperedge/directed/DirectedHyperedge.h"
#include "hglib/utils/types.h"
#include "hglib/vertex/directed/DirectedVertex.h"

namespace hglib {

/**
 * \brief This is a DirectedSubHypergraph class template.
 *
 * \details
 * This class template allows to build views of a directed hypergraph, that
 * is to consider a subset of vertices and hyperarcs. A hierarchy of views can
 * be built, where each sub-hypergraph observes changes in its parent
 * (sub-) hypergraph. The removal of vertices and hyperarcs in the parent
 * hypergraph yields in the removal of these vertices/hyperarcs from all its
 * children views. Calling resetVertexIds() or resetHyperarcIds() in the root
 * hypergraph results in updated vertex/hyperarc identifiers; these changes are
 * forwarded to all views.\n
 * A directed sub-hypergraph can be built through a factory method:
 * \code
 * // Data
 * std::vector<std::string> vertices {"A", "B", "Foo", "Bar", "", "6"};
 * std::vector<std::pair<std::vector<std::string>, std::string>> hyperarcs {
 *   {{{"A"}, {"Foo"}}, "edge1"},
 *   {{{"A", "B"}, {"Bar", ""}}, "edge2"},
 *   {{{"6"}, {""}}, "edge3"}
 * };
 *
 * // Build the root hypergraph
 * hglib::DirectedHypergraph<> g;
 * // Add some vertices
 * for (auto vertexName : vertices) {
 *   g.addVertex(vertexName);
 * }
 * // Add hyperarcs
 * for (auto hyperarc : hyperarcs) {
 *   // Unnamed hyperarcs (Do not use hyperarc.second to add a hyperarc)
 *   g.addHyperarc(hglib::NAME, hyperarc.first);
 * }
 *
 * // Build a subgraph of the root hypergraph g
 * std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
 * std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {1, 2};
 * auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
 *                                                    whitelistedHyperarcs);
 *
 * // Build a sub-sub-hypergraph
 * whitelistedVertices = {1};
 * whitelistedHyperarcs = {1};
 * auto subsubgraph = hglib::createDirectedSubHypergraph(subgraph.get(),
 *                                                       whitelistedVertices,
 *                                                       whitelistedHyperarcs);
 * \endcode
 *
 * \tparam VertexTemplate_t Vertex type. Defaults to DirectedVertex.
 * \tparam Hyperarc_t Hyperarc type. Defaults to DirectedHyperedge.
 * \tparam VertexProperty Vertex property type. Defaults to emptyProperty.
 * \tparam HyperarcProperty Hyperarc property type. Defaults to emptyProperty.
 * \tparam HypergraphProperty Directed hypergraph property type. Defaults to
 * emptyProperty.
 */
template <template <typename> typename VertexTemplate_t = DirectedVertex,
        typename Hyperarc_t = DirectedHyperedge,
        typename VertexProperty = emptyProperty,
        typename HyperarcProperty = emptyProperty,
        typename HypergraphProperty = emptyProperty>
class DirectedSubHypergraph :
    public DirectedHypergraphInterface<VertexTemplate_t,
                                       Hyperarc_t,
                                       VertexProperty,
                                       HyperarcProperty,
                                       HypergraphProperty>,
    public Observer {
  template <template <typename> typename DirectedVertex_t,
            typename Arc_t,
            typename VertexProp,
            typename EdgeProp,
            typename HypergraphProp>
  friend class DirectedHypergraph;

 public:
  /// Alias for the directed vertex type
  using Vertex_t = typename DirectedHypergraphInterface<VertexTemplate_t,
          Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::Vertex_t;
  /// Alias for Hyperarc type
  using Hyperarc_type = Hyperarc_t;
  /// Alias for Hyperarc property type
  using HyperarcProperty_type = HyperarcProperty;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexContainer;
  /// Alias for conditional range of a container of Hyperarc_t ptr
  using HyperarcContainer = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::HyperarcContainer;
  ///
  using vertex_iterator = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::vertex_iterator;
  ///
  using hyperarc_iterator = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::hyperarc_iterator;
  /// \brief Alias for the data container that stores the names of tails and
  /// heads of a hyperarc
  using TailAndHeadNames = std::pair<std::vector<std::string>,
                                     std::vector<std::string>>;
  /// \brief Alias for the data container that stores the ids of tails and
  /// heads of a hyperarc
  using TailAndHeadIds = std::pair<std::vector<VertexIdType>,
                                   std::vector<VertexIdType>>;
  /// Alias for unique_ptr of GraphElementContainer<Hyperarc_t*>
  using HyperarcContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::HyperarcContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<HyperarcProperty*>
  using HyperarcPropertyContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::HyperarcPropertyContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr = typename DirectedHypergraphInterface<
          VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>::VertexPropertyContainerPtr;

  /* **************************************************************************
   * **************************************************************************
   *                        Constructor/Destructor
   * **************************************************************************
   * *************************************************************************/
  /// DirectedSubHypergraph constructor
  DirectedSubHypergraph(DirectedHypergraphInterface<VertexTemplate_t,
          Hyperarc_t, VertexProperty, HyperarcProperty,
          HypergraphProperty>* parent,
                        const std::unordered_set<VertexIdType>&
                        whitelistedVertexIds,
                        const std::unordered_set<HyperedgeIdType>&
                        whitelistedHyperarcIds);

  /// DirectedSubHypergraph copy constructor
  DirectedSubHypergraph(const DirectedSubHypergraph& directedSubHypergraph);
  /// DirectedSubHypergraph destructor
  virtual ~DirectedSubHypergraph();
  /// Copy assignment operator
  DirectedSubHypergraph& operator=(const DirectedSubHypergraph& source);

  /* **************************************************************************
   * **************************************************************************
   *                    General
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if multi-hyperarcs are allowed in this hypergraph
  bool allowMultiHyperarcs() const override;

  /* **************************************************************************
   * **************************************************************************
   *                        DirectedHyperedge access
   * **************************************************************************
   * *************************************************************************/
  /// Number of hyperarcs in the hypergraph
  size_t nbHyperarcs() const override;
  /// Size of the hyperarc container
  size_t hyperarcContainerSize() const override;

  /// Add a hyperarc
  const Hyperarc_t* addHyperarc(
          decltype(hglib::NAME),
          const TailAndHeadNames& tails_heads,
          const typename Hyperarc_t::SpecificAttributes& attributes = {})
    override;

  /// Add a hyperarc
  const Hyperarc_t* addHyperarc(
          const TailAndHeadIds& tails_heads,
          const typename Hyperarc_t::SpecificAttributes& attributes = {})
    override;
  /// Add a hyperarc
  const Hyperarc_t* addHyperarc(const HyperedgeIdType& hyperarcId);

  /// Remove a hyperarc with the given id from the sub-hypergraph
  void removeHyperarc(const HyperedgeIdType& hyperarcId) override;

  /// Search hyperarc list for an hyperarc with the provided id
  const Hyperarc_t* hyperarcById(
          const HyperedgeIdType & hyperarcId) const override;

  /// Const hyperarc iterator pointing to the begin of the hyperarcs
  hyperarc_iterator hyperarcsBegin() const override;
  /// Const hyperarc iterator pointing to the end of the hyperarcs
  hyperarc_iterator hyperarcsEnd() const override;
  /// Begin/End iterator pair of the hyperarcs of the directed hypergraph
  std::pair<hyperarc_iterator, hyperarc_iterator> hyperarcsBeginEnd() const
    override;
  /// Return a reference to the container of hyperarcs
  const HyperarcContainer& hyperarcs() const override;

  // Tails
  /// Return number of tail vertices in the hyperarc with given id
  size_t nbTailVertices(const HyperedgeIdType & hyperarcId) const override;
  /// Check if the hyperarc with the given id has tail vertices
  bool hasTailVertices(const HyperedgeIdType & hyperarcId) const override;
  /// Const vertex iterator pointing to the begin of the tail vertices
  vertex_iterator tailsBegin(const HyperedgeIdType & hyperarcId) const
    override;
  /// Const vertex iterator pointing to the end of the tail vertices
  vertex_iterator tailsEnd(const HyperedgeIdType & hyperarcId) const override;
  /// Pair of vertex iterators pointing to the begin/end of the tail vertices
  std::pair<vertex_iterator, vertex_iterator> tailsBeginEnd(
          const HyperedgeIdType & hyperarcId) const override;
  /// Get shared_ptr of container of the tail vertices of given hyperarc id
  std::shared_ptr<const GraphElementContainer<Vertex_t*>> tails(
          const HyperedgeIdType& hyperarcId) const override;

  // Heads
  /// Return number of head vertices in the hyperarc with the given id
  size_t nbHeadVertices(const HyperedgeIdType & hyperarcId) const override;
  /// Check if the hyperarc with the given id has head vertices
  bool hasHeadVertices(const HyperedgeIdType & hyperarcId) const override;
  /// Const vertex iterator pointing to the begin of the head vertices
  vertex_iterator headsBegin(const HyperedgeIdType & hyperarcId) const
    override;
  /// Const vertex iterator pointing to the end of the head vertices
  vertex_iterator headsEnd(const HyperedgeIdType & hyperarcId) const override;
  /// Pair of vertex iterators pointing to the begin/end of the head vertices
  std::pair<vertex_iterator, vertex_iterator> headsBeginEnd(
          const HyperedgeIdType & hyperarcId) const override;
  /// Get shared_ptr of container of the head vertices of given hyperarc id
  std::shared_ptr<const GraphElementContainer<Vertex_t*>> heads(
          const HyperedgeIdType& hyperarcId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                       DirectedVertex access
   * **************************************************************************
   * *************************************************************************/
  /// Size of the vertex container
  size_t vertexContainerSize() const override;
  /// Number of vertices in the sub-hypergraph
  size_t nbVertices() const override;
  /// Search vertex list for a vertex with the provided name
  const Vertex_t* vertexByName(const std::string& vertexName) const override;
  /// Search vertex list for a vertex with the provided id
  const Vertex_t* vertexById(const VertexIdType & vertexId) const override;
  /// Const vertex iterator pointing to the begin of the vertices
  vertex_iterator verticesBegin() const override;
  /// Const vertex iterator pointing to the end of the vertices
  vertex_iterator verticesEnd() const override;
  /// Begin/End iterator pair of the vertices of the directed sub-hypergraph
  std::pair<vertex_iterator, vertex_iterator> verticesBeginEnd() const
    override;

  /// Return a reference to the container of vertices
  const VertexContainer& vertices() const override;

  /// Add a vertex
  const Vertex_t* addVertex(
          const typename Vertex_t::SpecificAttributes& attributes = {})
    override;
  /// Add a vertex
  const Vertex_t* addVertex(
          const std::string& name,
          const typename Vertex_t::SpecificAttributes& attributes = {})
    override;
  /// Add a vertex
  const Vertex_t* addVertex(const VertexIdType& vertexId);

  /// Get arguments to create an existing vertex
  ArgumentsToCreateVertex<Vertex_t> argumentsToCreateVertex(
          const hglib::VertexIdType& vertexId) const override;
  // incoming hyperarcs
  /// Return number of incoming hyperarcs of the vertex with the given id
  size_t inDegree(const VertexIdType& vertexId) const override;
  /// Check if the vertex with the given id has incoming hyperarcs
  bool hasInHyperarcs(const VertexIdType& vertexId) const override;
  /// Const hyperarc iterator pointing to the begin of the in-hyperarcs
  hyperarc_iterator inHyperarcsBegin(
          const VertexIdType& vertexId) const override;
  /// Const hyperarc iterator pointing to the end of the in-hyperarcs
  hyperarc_iterator inHyperarcsEnd(
          const VertexIdType& vertexId) const override;
  /// Pair of hyperarc iterators pointing to the begin/end of the in-arcs
  std::pair<hyperarc_iterator, hyperarc_iterator> inHyperarcsBeginEnd(
          const VertexIdType& vertexId) const override;
  /// Get shared_ptr of container of the in-arcs of the given vertex Id
  std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> inHyperarcs(
      const VertexIdType& vertexId) const override;

  // out-going hyperarcs
  /// Return number of outgoing hyperarcs of the vertex with the given id
  size_t outDegree(const VertexIdType& vertexId) const override;
  /// Check if the vertex with the given id has outgoing hyperarcs
  bool hasOutHyperarcs(const VertexIdType& vertexId) const override;
  /// Const hyperarc iterator pointing to the begin of the out-hyperarcs
  hyperarc_iterator outHyperarcsBegin(
          const VertexIdType& vertexId) const override;
  /// Const hyperarc iterator pointing to the end of the out-hyperarcs
  hyperarc_iterator outHyperarcsEnd(
          const VertexIdType& vertexId) const override;
  /// Pair of hyperarc iterators pointing to the begin/end of the out-arcs
  std::pair<hyperarc_iterator, hyperarc_iterator> outHyperarcsBeginEnd(
          const VertexIdType& vertexId) const override;
  /// Get shared_ptr of container of the out-arcs of the given vertex Id
  std::shared_ptr<const GraphElementContainer<Hyperarc_t*>> outHyperarcs(
      const VertexIdType& vertexId) const override;

  /// Remove a vertex with the given name from the directed sub-hypergraph
  void removeVertex(const std::string& vertexName,
                            bool removeImpliedHyperarcs = true) override;
  /// Remove a vertex with the given Id from the directed hypergraph
  void removeVertex(const VertexIdType& vertexId,
                            bool removeImpliedHyperarcs = true) override;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex/DirectedHyperedge dependent access
   * **************************************************************************
   * *************************************************************************/
  /// Check if the vertex is a tail vertex of the hyperarc
  bool isTailVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const override;
  /// Check if the vertex is a head vertex of the hyperarc
  bool isHeadVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const override;
  /// Check if the vertex is a tail or a head vertex of the hyperarc
  bool isVertexOfHyperarc(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperarcId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                        HyperarcProperty access
   * **************************************************************************
   * *************************************************************************/
  /// Get properties of the hyperarc with the provided id
  const HyperarcProperty* getHyperarcProperties(
          const HyperedgeIdType& hyperarcId) const override;
  /// Get properties of the hyperarc with the provided id
  HyperarcProperty* getHyperarcProperties_(
          const HyperedgeIdType& hyperarcId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                    Vertex property access methods
   * **************************************************************************
   * *************************************************************************/
  /// Get properties of the vertex with the provided id
  const VertexProperty* getVertexProperties(
          const VertexIdType& vertexId) const override;
  /// Get properties of the vertex with the provided id
  VertexProperty* getVertexProperties_(
          const VertexIdType& vertexId) const override;

  /* **************************************************************************
   * **************************************************************************
   *               Hypergraph property access methods
   * **************************************************************************
   * *************************************************************************/
  /// Get properties of the sub-hypergraph
  const HypergraphProperty* getHypergraphProperties() const override;
  /// Get properties of the sub-hypergraph
  HypergraphProperty* getHypergraphProperties_() const override;

  /* **************************************************************************
   * **************************************************************************
   *                Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get ancestors
  void getAncestors(std::vector<HypergraphInterface<VertexTemplate_t,
      Hyperarc_t, VertexProperty, HyperarcProperty,
      HypergraphProperty>*>*) const override;
  /// Get root hypergraph
  const DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t,
          VertexProperty, HyperarcProperty,
          HypergraphProperty>* getRootHypergraph() const override;

 protected:
  /// Observer update method
  void Update(const Observable& o, ObservableEvent e, void* arg) override;
  /// Remove the given vertex from the filtered out vertices in ancestors
  void removeFilteredOutVertexInAncestors(const VertexTemplate_t<Hyperarc_t>*);
  /// Remove the given hyperarc from the filtered out hyperarcs in ancestors
  void removeFilteredOutHyperarcInAncestors(const Hyperarc_t*);
  /// Add vertex property in ancestors
  void addVertexPropertyInAncestors(const VertexIdType& id);
  /// Add hyperarc property in ancestors
  void addHyperarcPropertyInAncestors(const HyperedgeIdType& hyperarcId);

  /* **************************************************************************
   * **************************************************************************
   *                        Protected methods
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Set pointer to parent hypergraph to nullptr
  void setParentOfChildsToNull();
  /// Throw an exception if the pointer to the parent is a nullptr
  void throwExceptionOnNullptrParent() const;
  /// Update the vertex container upon a call to resetVertexIds()
  void updateVertexContainerUponResetVertexIds();
  /// Update the hyperarc container upon a call to resetHyperarcIds()
  void updateHyperarcContainerUponResetHyperarcIds();
  /// Return shared_ptr to vertices container of the root hypergraph
  VertexContainerPtr getRootVerticesContainerPtr() const override;
  /// Return shared_ptr to hyperarc container of the root hypergraph
  HyperarcContainerPtr getRootHyperarcContainerPtr() const override;

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Parent hypergraph
  DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t, VertexProperty,
          HyperarcProperty, HypergraphProperty>* parent_;
  /// Container of whitelisted vertices
  VertexContainerPtr whitelistedVerticesList_;
  /// Property per vertex
  VertexPropertyContainerPtr whitelistedVertexProperties_;
  /// Container of whitelisted hyperarcs
  HyperarcContainerPtr whitelistedHyperarcsList_;
  /// Property per hyperarc
  HyperarcPropertyContainerPtr whitelistedHyperarcProperties_;
  /// SubHypergraph property
  HypergraphProperty* subHypergraphProperty_;
};


/**
 * Factory method to create a directed sub-hypergraph.
 *
 * @tparam DirectedHypergraph_t
 * @param g
 * @param whitelistedVertexIds
 * @param whitelistedHyperarcIds
 * @return unique_ptr to directed sub-hypergraph of type
 * DirectedSubHypergraph<DirectedHypergraph_t>
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::unique_ptr<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>>
createDirectedSubHypergraph(DirectedHypergraphInterface<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty,
        HypergraphProperty>* parent,
                            const std::unordered_set<VertexIdType>&
                            whitelistedVertexIds,
                            const std::unordered_set<HyperedgeIdType>&
                            whitelistedHyperarcIds) {
  return std::make_unique<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
          VertexProperty, HyperarcProperty, HypergraphProperty>>(
          parent, whitelistedVertexIds, whitelistedHyperarcIds);
}


/**
 * Factory method to copy a given directed sub-hypergraph
 *
 * @tparam DirectedHypergraph_t
 * @param g
 * @return unique_ptr to directed sub-hypergraph of type
 * DirectedSubHypergraph<DirectedHypergraph_t>
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::unique_ptr<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>>
copyDirectedSubHypergraph(
        const DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>& g) {
  return std::make_unique<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
          VertexProperty, HyperarcProperty, HypergraphProperty>>(g);
}
}  // namespace hglib

#include "DirectedSubHypergraph.hpp"
#endif  // HGLIB_SUBDIRECTEDHYPERGRAPH_H
