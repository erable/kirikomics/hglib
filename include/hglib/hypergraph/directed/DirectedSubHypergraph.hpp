// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "DirectedSubHypergraph.h"

#include <unordered_set>

#include "filtered_vector.h"

namespace hglib {

/**
 * \brief DirectedSubHypergraph constructor
 *
 * \details
 * The constructor allows to create an instance of a directed sub-hypergraph,
 * that is a view of a parent directed hypergraph \em p considering a subset
 * of \em p's vertices and hyperarcs.
 *
 * \param parent Parent directed hypergraph of type
 * DirectedHypergraphInterface\<Template_args\>. This can be a pointer to a
 * DirectedHypergraph\<Template_args\> or
 * DirectedSubHypergraph\<Template_args\>.
 * \param whitelistedVertexIds unordered_set of vertex Ids that are part of the
 * view.
 * \param whitelistedHyperarcIds unordered_set of hyperarc Ids that are part of
 * the view.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
DirectedSubHypergraph(DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t,
                       VertexProperty, HyperarcProperty,
                       HypergraphProperty>* parent,
                       const std::unordered_set<VertexIdType>&
                       whitelistedVertexIds,
                       const std::unordered_set<HyperedgeIdType>&
                       whitelistedHyperarcIds) :
        DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>(),
        Observer(),
        parent_(parent) {
  // build whitelisted vertex FilteredVector
  std::vector<Vertex_t*> whitelistFilterVertices;
  // Create vector of vertex properties
  whitelistedVertexProperties_ = create_filtered_vector<VertexProperty*>(
      create_filtered_vector<VertexProperty*>(),
      std::make_unique<NullptrFilter<VertexProperty*>>());
  // Fill both containers with nullptrs
  for (size_t i = 0; i < parent_->vertexContainerSize(); ++i) {
    whitelistFilterVertices.push_back(nullptr);
    whitelistedVertexProperties_->push_back(nullptr);
  }
  // Set whitelisted entries (vertices + vertex properties)
  for (VertexIdType vertexId : whitelistedVertexIds) {
    try {
      const auto& vertex = parent_->vertexById(vertexId);
      if (vertex != nullptr) {
        whitelistFilterVertices[vertexId] = const_cast<Vertex_t*>(vertex);
        whitelistedVertexProperties_->operator[](vertexId) =
            new VertexProperty(*(parent_->getVertexProperties_(vertexId)));
      } else {
        // delete already created vertex property pointers
        for (auto& vertexProperty : *whitelistedVertexProperties_) {
          delete vertexProperty;
        }
        throw std::invalid_argument("Vertex " + std::to_string(vertexId) +
                                        " does not exist in the parent "
                                            "hypergaph.");
      }
    } catch (const std::out_of_range& oor) {
      std::string errorMessage("");
      if (parent_->vertexContainerSize() == 0) {
        errorMessage = "There is no vertex in the parent graph. You cannot"
                " add vertices to the subgraph\n";
      } else {
        errorMessage = "Vertex with id: " + std::to_string(vertexId)
                       + " is out of range -> min=0; max="
                       + std::to_string((parent_->vertexContainerSize() - 1))
                       + '\n';
      }

      // delete already created vertex property pointers
      for (auto& vertexProperty : *whitelistedVertexProperties_) {
        delete vertexProperty;
      }
      throw std::out_of_range(errorMessage);
    }
  }
  // create actual vertices list
  whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
      parent_->getRootVerticesContainerPtr(),
      std::make_unique<WhitelistFilter<Vertex_t*>>(whitelistFilterVertices));

  // build whitelisted hyperarc FilteredVector
  std::vector<Hyperarc_t*> whitelistFilterHyperarcs;
  // Create vector of hyperarc properties
  whitelistedHyperarcProperties_ = create_filtered_vector<HyperarcProperty*>(
      create_filtered_vector<HyperarcProperty*>(),
      std::make_unique<NullptrFilter<HyperarcProperty*>>());
  // Fill both containers with nullptrs
  for (size_t i = 0; i < parent_->hyperarcContainerSize(); ++i) {
    whitelistFilterHyperarcs.push_back(nullptr);
    whitelistedHyperarcProperties_->push_back(nullptr);
  }
  for (HyperedgeIdType hyperarcId : whitelistedHyperarcIds) {
    try {
      // Check if hyperarc can be added to subgraph
      const auto& hyperarc = parent_->hyperarcById(hyperarcId);
      bool tailOrHeadInSubgraph = false;
      if (hyperarc != nullptr) {
        // Check if there is at least one tail or head vertex in the subgraph.
        // Get the tails and heads from the parent graph as the current
        // hyperarc is not yet added to the container of whitelisted hyperarcs
        // in this subgraph -> tails(hyperarc->id()) throws exception.
        // Check in this subgraph if a tail or head vertex is present.
        auto tailsPtr = parent_->tails(hyperarc->id());
        for (const auto& tail : *tailsPtr) {
          if (vertexById(tail->id()) != nullptr) {
            tailOrHeadInSubgraph = true;
            break;
          }
        }
        // Check head vertices
        if (not tailOrHeadInSubgraph) {
          auto headsPtr = parent_->heads(hyperarc->id());
          for (const auto& head : *headsPtr) {
            if (vertexById(head->id()) != nullptr) {
              tailOrHeadInSubgraph = true;
              break;
            }
          }
        }
      }
      if (hyperarc != nullptr and tailOrHeadInSubgraph == true) {
        whitelistFilterHyperarcs[hyperarcId] = const_cast<
            Hyperarc_t*>(hyperarc);
        whitelistedHyperarcProperties_->operator[](hyperarcId) =
            new HyperarcProperty(*(parent_->getHyperarcProperties_(
                hyperarcId)));
      } else {
        // delete already created vertex property pointers
        for (auto& vertexProperty : *whitelistedVertexProperties_) {
          delete vertexProperty;
        }
        // delete already created hyperarc property pointers
        for (auto& hyperarcProperty : *whitelistedHyperarcProperties_) {
          delete hyperarcProperty;
        }
        if (hyperarc == nullptr) {
          throw std::invalid_argument("Hyperarc " + std::to_string(hyperarcId) +
                                      " does not exist in the parent "
                                          "hypergaph.");
        } else {
          throw std::invalid_argument("At least one tail or head vertex of "
                                          "hyperarc "
                                      + std::to_string(hyperarcId) +
                                      " must be part of the subgraph.");
        }
      }
    } catch (const std::out_of_range& oor) {
      // delete already created vertex property pointers
      for (auto& vertexProperty : *whitelistedVertexProperties_) {
        delete vertexProperty;
      }
      // delete already created hyperarc property pointers
      for (auto& hyperarcProperty : *whitelistedHyperarcProperties_) {
        delete hyperarcProperty;
      }

      // Error message
      std::string errorMessage("");
      if (parent_->hyperarcContainerSize() == 0) {
        errorMessage = "There is no hyperarc in the parent graph. You cannot"
                " add hyperarcs to the subgraph\n";
      } else {
        errorMessage = "DirectedHyperedge with id: "
                       + std::to_string(hyperarcId)
                       + " is out of range -> min=0; max="
                       + std::to_string((parent_->hyperarcContainerSize() - 1))
                       + '\n';
      }
      throw std::out_of_range(errorMessage);
    }
  }
  // create actual hyperarc list
  whitelistedHyperarcsList_ = create_filtered_vector<Hyperarc_t*>(
      parent->getRootHyperarcContainerPtr(),
      std::make_unique<WhitelistFilter<Hyperarc_t*>>(whitelistFilterHyperarcs));

  // add itself as observer of the parent
  parent_->AddObserver(this, ObservableEvent::VERTEX_REMOVED);
  parent_->AddObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
  parent_->AddObserver(this, ObservableEvent::RESET_VERTEX_IDS);
  parent_->AddObserver(this, ObservableEvent::RESET_EDGE_IDS);

  // Create HypergraphProperty object
  subHypergraphProperty_ = new HypergraphProperty(
          *parent_->getHypergraphProperties_());
}

/**
 * \brief DirectedSubHypergraph copy constructor
 *
 * \details
 * Makes a deep copy of the given directed sub-hypergraph. This sub-hypergraph
 * becomes a view of the parent of the to be copied sub-hypergraph.
 *
 * \param other The to be copied directed sub-hypergraph.
 *
 * \par Complexity
 * Linear in the number of vertices and hyperarcs of other.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
DirectedSubHypergraph(const DirectedSubHypergraph& other) :
        Observer(),
        parent_(other.parent_) {
  // copy whitelisted vertex FilteredVector
  std::vector<Vertex_t*> whitelistFilterVertices;
  for (size_t i = 0; i < parent_->vertexContainerSize(); ++i) {
    whitelistFilterVertices.push_back(nullptr);
  }
  for (const auto& vertex : other.vertices()) {
    whitelistFilterVertices[vertex->id()] =
                const_cast<Vertex_t*>(vertex);
  }
  // create actual vertices list
  whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
      parent_->getRootVerticesContainerPtr(),
      std::make_unique<WhitelistFilter<Vertex_t*>>(whitelistFilterVertices));


  // copy whitelisted hyperarc FilteredVector
  std::vector<Hyperarc_t*> whitelistFilterHyperarcs;
  for (size_t i = 0; i < parent_->hyperarcContainerSize(); ++i) {
    whitelistFilterHyperarcs.push_back(nullptr);
  }
  for (const auto& hyperarc : other.hyperarcs()) {
    whitelistFilterHyperarcs[hyperarc->id()] =
            const_cast<Hyperarc_t*>(hyperarc);
  }
  // create actual hyperarc list
  whitelistedHyperarcsList_ = create_filtered_vector<Hyperarc_t*>(
      parent_->getRootHyperarcContainerPtr(),
      std::make_unique<WhitelistFilter<Hyperarc_t*>>(whitelistFilterHyperarcs));


  // Copy vertex properties
  whitelistedVertexProperties_ = create_filtered_vector<VertexProperty*>(
      create_filtered_vector<VertexProperty*>(),
      std::make_unique<NullptrFilter<VertexProperty*>>());
  for (std::size_t i = 0; i < other.whitelistedVertexProperties_->size(); ++i) {
    // vertex is not part of the other sub-hypergraph
    if (other.whitelistedVertexProperties_->operator[](i) == nullptr) {
      whitelistedVertexProperties_->push_back(nullptr);
    } else {  // Copy vertex property
      VertexProperty* vertexPropertyCopy = new VertexProperty(
          *(other.whitelistedVertexProperties_->operator[](i)));
      whitelistedVertexProperties_->push_back(vertexPropertyCopy);
    }
  }

  // Copy hyperarc properties
  whitelistedHyperarcProperties_ = create_filtered_vector<HyperarcProperty*>(
      create_filtered_vector<HyperarcProperty*>(),
      std::make_unique<NullptrFilter<HyperarcProperty*>>());
  for (std::size_t i = 0; i < other.whitelistedHyperarcProperties_->size();
       ++i) {
    // hyperarc is not part of the other sub-hypergraph
    if (other.whitelistedHyperarcProperties_->operator[](i) == nullptr) {
      whitelistedHyperarcProperties_->push_back(nullptr);
    } else {  // Copy hyperarc property
      HyperarcProperty* hyperarcPropertyCopy = new HyperarcProperty(
          *(other.whitelistedHyperarcProperties_->operator[](i)));
      whitelistedHyperarcProperties_->push_back(hyperarcPropertyCopy);
    }
  }

  // Attach itself as observer to parent graph
  parent_->AddObserver(this, ObservableEvent::VERTEX_REMOVED);
  parent_->AddObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
  parent_->AddObserver(this, ObservableEvent::RESET_VERTEX_IDS);
  parent_->AddObserver(this, ObservableEvent::RESET_EDGE_IDS);

  // Create HypergraphProperty object
  subHypergraphProperty_ = new HypergraphProperty(
          *other.getHypergraphProperties_());
}


/**
 * \brief DirectedSubHypergraph destructor
 *
 * \details
 * Consider the hierarchy of a root hypergraph \em r, a sub-hypergraph \em s of
 * \em r, and a sub-hypergraph \em s2 of \em s. Deleting \em s yields in that
 * the parent of \em s2 becomes the root hypergraph \em r.
 *
 * \par Complexity
 * O(n * m), with n the number of children of \em s, and m the number of
 * observable events (all children of \em s observe now its former grand-parent
 * \em r).
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
~DirectedSubHypergraph() {
  delete subHypergraphProperty_;

  // Delete vertex property pointers
  for (auto& vertexProperty : *whitelistedVertexProperties_) {
    delete vertexProperty;
  }
  // Delete hyperarc property pointers
  for (auto& hyperarcProperty : *whitelistedHyperarcProperties_) {
    delete hyperarcProperty;
  }

  // Detach itself as observer of the parent graph
  if (parent_ != nullptr) {
    parent_->DeleteObserver(this, ObservableEvent::VERTEX_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::RESET_VERTEX_IDS);
    parent_->DeleteObserver(this, ObservableEvent::RESET_EDGE_IDS);

    // Subgraphs whose parent (this subgraph) becomes the current grand-parent.
    // Add subgraphs as observers to the current grand-parent.
    // Clear list of observers of this subgraph.
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver = itEvent->second.begin();
           itObserver != itEvent->second.end(); ++itObserver) {
        auto subgraph = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
                Hyperarc_t, VertexProperty, HyperarcProperty,
                HypergraphProperty> *>(*itObserver);
        subgraph->parent_ = parent_;
        subgraph->parent_->AddObserver(subgraph, itEvent->first);
      }
      this->observers_[itEvent->first].clear();
    }
  }
}

/**
 * \brief Copy assignment operator
 *
 * \details
 * Replaces the contents (vertices, hyperarcs) and the observed parent of the
 * directed sub-hypergraph.
 *
 * \param source Another DirectedSubHypergraph to use as data source.
 *
 * \return
 * \code *this \endcode
 *
 * \par Complexity
 * O(n * m), with n the number of children views of this, and m the number of
 * observable events.\n
 * Linear in the number of vertices and hyperarcs of the source.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>& DirectedSubHypergraph<
        VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
operator=(const DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>& source) {
  // check for self assignment
  if (this != &source) {
    // deallocate whitelistedVertexProperties_
    for (auto& vProp : *whitelistedVertexProperties_) {
      delete vProp;
    }
    whitelistedVertexProperties_->clear();

    // Delete hyperarc property pointers
    for (auto& hyperarcProperty : *whitelistedHyperarcProperties_) {
      delete hyperarcProperty;
    }
    whitelistedHyperarcProperties_->clear();

    // Set the parent of all childs in the subgraph-hierarchy to null
    std::unordered_set<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
            VertexProperty, HyperarcProperty, HypergraphProperty>*> observers;
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver : itEvent->second) {
        auto subgraph = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
                Hyperarc_t, VertexProperty, HyperarcProperty,
                HypergraphProperty>*>(itObserver);
        observers.insert(subgraph);
      }
    }
    for (auto& subgraph : observers) {
      subgraph->setParentOfChildsToNull();
      this->DeleteObserver(subgraph, ObservableEvent::VERTEX_REMOVED);
      this->DeleteObserver(subgraph, ObservableEvent::HYPEREDGE_REMOVED);
      this->DeleteObserver(subgraph, ObservableEvent::RESET_VERTEX_IDS);
      this->DeleteObserver(subgraph, ObservableEvent::RESET_EDGE_IDS);
    }

    // Detach itself as observer from parent graph
    parent_->DeleteObserver(this, ObservableEvent::VERTEX_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::RESET_VERTEX_IDS);
    parent_->DeleteObserver(this, ObservableEvent::RESET_EDGE_IDS);
    // Assign parent
    parent_ = source.parent_;

    // copy whitelisted vertex FilteredVector
    std::vector<Vertex_t*> whitelistFilterVertices;
    for (size_t i = 0; i < parent_->vertexContainerSize(); ++i) {
      whitelistFilterVertices.push_back(nullptr);
    }
    for (const auto& vertex : source.vertices()) {
      whitelistFilterVertices[vertex->id()] =
              const_cast<Vertex_t*>(vertex);
    }
    // create actual vertices list
    whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
        parent_->getRootVerticesContainerPtr(),
        std::make_unique<WhitelistFilter<Vertex_t*>>(whitelistFilterVertices));


    // copy whitelisted hyperarc FilteredVector
    std::vector<Hyperarc_t*> whitelistFilterHyperarcs;
    for (size_t i = 0; i < parent_->hyperarcContainerSize(); ++i) {
      whitelistFilterHyperarcs.push_back(nullptr);
    }
    for (const auto& hyperarc : source.hyperarcs()) {
      whitelistFilterHyperarcs[hyperarc->id()] =
              const_cast<Hyperarc_t*>(hyperarc);
    }
    // create actual hyperarc list
    whitelistedHyperarcsList_ = create_filtered_vector<Hyperarc_t*>(
        parent_->getRootHyperarcContainerPtr(),
        std::make_unique<WhitelistFilter<Hyperarc_t*>>(
            whitelistFilterHyperarcs));

    // Copy vertex properties
    whitelistedVertexProperties_ = create_filtered_vector<VertexProperty*>(
        create_filtered_vector<VertexProperty*>(),
        std::make_unique<NullptrFilter<VertexProperty*>>());
    for (std::size_t i = 0; i < source.whitelistedVertexProperties_->size();
         ++i) {
      // vertex is not part of the other sub-hypergraph
      if (source.whitelistedVertexProperties_->operator[](i) == nullptr) {
        whitelistedVertexProperties_->push_back(nullptr);
      } else {  // Copy vertex property
        VertexProperty* vertexPropertyCopy = new VertexProperty(
            *(source.whitelistedVertexProperties_->operator[](i)));
        whitelistedVertexProperties_->push_back(vertexPropertyCopy);
      }
    }

    // Copy hyperarc properties
    whitelistedHyperarcProperties_ = create_filtered_vector<HyperarcProperty*>(
        create_filtered_vector<HyperarcProperty*>(),
        std::make_unique<NullptrFilter<HyperarcProperty*>>());
    for (std::size_t i = 0; i < source.whitelistedHyperarcProperties_->size();
         ++i) {
      // hyperarc is not part of the other sub-hypergraph
      if (source.whitelistedHyperarcProperties_->operator[](i) == nullptr) {
        whitelistedHyperarcProperties_->push_back(nullptr);
      } else {  // Copy hyperarc property
        HyperarcProperty* hyperarcPropertyCopy = new HyperarcProperty(
            *(source.whitelistedHyperarcProperties_->operator[](i)));
        whitelistedHyperarcProperties_->push_back(hyperarcPropertyCopy);
      }
    }

    // init hypergraph property
    delete subHypergraphProperty_;
    subHypergraphProperty_ = new HypergraphProperty(
            *source.getHypergraphProperties_());

    // Attach itself as observer to parent graph
    parent_->AddObserver(this, ObservableEvent::VERTEX_REMOVED);
    parent_->AddObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
    parent_->AddObserver(this, ObservableEvent::RESET_VERTEX_IDS);
    parent_->AddObserver(this, ObservableEvent::RESET_EDGE_IDS);
  }
  return *this;
}


/**
 * \brief Check if multi-hyperarcs are allowed in this hypergraph
 *
 * \return True if multi-hyperarcs are supported, false otherwise.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
allowMultiHyperarcs() const {
  throwExceptionOnNullptrParent();
  return parent_->allowMultiHyperarcs();
}


/**
 * \brief Number of hyperarcs in the hypergraph
 *
 * \return Number of hyperarcs in the hypergraph.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
nbHyperarcs() const {
  throwExceptionOnNullptrParent();
  size_t nbHyperarcs(0);
  for (auto it = whitelistedHyperarcsList_->begin();
       it != whitelistedHyperarcsList_->end(); ++it) {
    ++nbHyperarcs;
  }
  return nbHyperarcs;
}


/**
 * \brief Size of the hyperarc container
 *
 * \details
 * Pointers to hyperarc objects are stored in a customized vector. If a
 * hyperarc is removed from the hypergraph, then the corresponding entry in
 * this vector is set to a nullptr. The size of the container remains unchanged.
 * Thus, the return value of this function may differ from nbHyperarcs().
 *
 * \return Size of the hyperarc container.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarcContainerSize() const {
  throwExceptionOnNullptrParent();
  return parent_->hyperarcContainerSize();
}


/**
 * \brief Add a hyperarc
 *
 * \details
 * Remove a hyperarc from the filtered out hyperarcs of this directed
 * sub-hypergraph and from all ancestor sub-hypergraph (except the root
 * hypergraph). A hyperarc with the given arguments is first searched in the
 * root hypergraph using the function Hyperarc_t::compare(). If found, the
 * hyperarc is removed from the filtered out hyperarcs in this sub-hypergraph
 * and its ancestor sub-hypergraphs. This means that the hyperarc is visible in
 * those sub-hypergraphs. This function is less efficient than
 * addHyperarc(const HyperedgeIdType&).
 *
 * \param tails_heads Tail and head vertex names.
 * \param args Additional arguments to create a hyperarc of type Hyperarc_t.
 *
 * \return Pointer to const hyperarc on success, otherwise an exception is
 * thrown.
 *
 * \throw invalid_argument If no hyperarc was found in the root hypergraph.
 *
 * \par Complexity
 * Linear in number of hyperarcs (hyperarc search).\n
 * Linear in the number of ancestor sub-hypergraphs.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Hyperarc_type*
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
addHyperarc(decltype(hglib::NAME),
            const TailAndHeadNames& tails_heads,
            const typename Hyperarc_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();
  // Get pointer to root hypergraph
  auto rootHypergraph = getRootHypergraph();
  // Get vertex pointers of specified tails/heads
  std::vector<const Vertex_t*> tails, heads;
  // Add tail pointers
  for (const auto& vertexName : tails_heads.first) {
    const auto& vertexPtr = rootHypergraph->vertexByName(vertexName);
    if (vertexPtr == nullptr) {
      throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                      "addHyperarc(): Can't find a hyperarc "
                                      "with given arguments in the root "
                                      "hypergraph. Can't find tail vertex "
                                  + vertexName);
    }
    tails.push_back(vertexPtr);
  }
  // Add head pointers
  for (const auto& vertexName : tails_heads.second) {
    const auto& vertexPtr = rootHypergraph->vertexByName(vertexName);
    if (vertexPtr == nullptr) {
      throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                      "addHyperarc(): Can't find a hyperarc "
                                      "with given arguments in the root "
                                      "hypergraph. Can't find head vertex "
                                  + vertexName);
    }
    heads.push_back(vertexPtr);
  }
  // Search for the hyperarc in the root hypergraph
  Hyperarc_t* arcInRootGraph = nullptr;
  for (const auto& arc : rootHypergraph->hyperarcs()) {
    if (arc->compare(tails, heads, attributes) == true) {
      arcInRootGraph = arc;
      break;
    }
  }
  if (arcInRootGraph == nullptr) {
    throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                    "addHyperarc(): Can't find a hyperarc "
                                    "with given arguments in the root "
                                    "hypergraph.");
  }
  // Add hyperarc to this subgraph and the subgraphs up to the root hypergraph
  addHyperarc(arcInRootGraph->id());
  return arcInRootGraph;
}


/**
 * \brief Add a hyperarc
 *
 * \details
 * Remove a hyperarc from the filtered out hyperarcs of this directed
 * sub-hypergraph and from all ancestor sub-hypergraph (except the root
 * hypergraph). A hyperarc with the given arguments is first searched in the
 * root hypergraph using the function Hyperarc_t::compare(). If found, the
 * hyperarc is removed from the filtered out hyperarcs in this sub-hypergraph
 * and its ancestor sub-hypergraphs. This means that the hyperarc is visible in
 * those sub-hypergraphs. This function is less efficient than
 * addHyperarc(const HyperedgeIdType&).
 *
 * \param tails_heads Tail and head vertex Ids.
 * \param args Additional arguments to create a hyperarc of type Hyperarc_t.
 *
 * \return Pointer to const hyperarc on success, otherwise an exception is
 * thrown.
 *
 * \throw invalid_argument If no hyperarc exists in the root hypergraph.
 *
 * \par Complexity
 * Linear in number of hyperarcs (hyperarc search).\n
 * Linear in the number of ancestor sub-hypergraphs.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Hyperarc_type*
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
addHyperarc(const TailAndHeadIds& tails_heads,
            const typename Hyperarc_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();
  // Get pointer to root hypergraph
  auto rootHypergraph = getRootHypergraph();
  // Get vertex pointers of specified tails/heads
  std::vector<const Vertex_t*> tails, heads;
  // Add tail pointers
  for (const auto& vertexId : tails_heads.first) {
    try {
      const auto &vertexPtr = rootHypergraph->vertexById(vertexId);
      if (vertexPtr == nullptr) {
        throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                        "addHyperarc(): Can't find a hyperarc "
                                        "with given arguments in the root "
                                        "hypergraph. Can't find tail vertex "
                                    + std::to_string(vertexId));
      }
      tails.push_back(vertexPtr);
    } catch (const std::out_of_range& oor) {
      throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                      "addHyperarc(): Can't find a hyperarc "
                                      "with given arguments in the root "
                                      "hypergraph. Can't find tail vertex "
                                  + std::to_string(vertexId));
    }
  }
  // Add head pointers
  for (const auto& vertexId : tails_heads.second) {
    try {
      const auto& vertexPtr = rootHypergraph->vertexById(vertexId);
      if (vertexPtr == nullptr) {
        throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                        "addHyperarc(): Can't find a hyperarc "
                                        "with given arguments in the root "
                                        "hypergraph. Can't find head vertex "
                                    + std::to_string(vertexId));
      }
      heads.push_back(vertexPtr);
    } catch (const std::out_of_range& oor) {
      throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                      "addHyperarc(): Can't find a hyperarc "
                                      "with given arguments in the root "
                                      "hypergraph. Can't find head vertex "
                                  + std::to_string(vertexId));
    }
  }
  // Search for the hyperarc in the root hypergraph
  Hyperarc_t* arcInRootGraph = nullptr;
  for (const auto& arc : rootHypergraph->hyperarcs()) {
    if (arc->compare(tails, heads, attributes) == true) {
      arcInRootGraph = arc;
      break;
    }
  }
  if (arcInRootGraph == nullptr) {
    throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                    "addHyperarc(): Can't find a hyperarc "
                                    "with given arguments in the root "
                                    "hypergraph.");
  }
  // Add vertex to this subgraph and the subgraphs up to the root hypergraph
  addHyperarc(arcInRootGraph->id());
  return arcInRootGraph;
}


/**
 * \brief Add a hyperarc
 *
 * \details
 * Remove a hyperarc from the filtered out hyperarcs of this directed
 * sub-hypergraph and from all ancestor sub-hypergraph (except the root
 * hypergraph). A hyperarc with the given Id is first searched in the
 * root hypergraph. If found, the hyperarc is removed from the filtered out
 * hyperarcs in this sub-hypergraph and its ancestor sub-hypergraphs. This
 * means that the hyperarc is visible in those sub-hypergraphs.
 *
 * \param hyperarcId Identifier of the hyperarc.
 *
 * \return Pointer to const hyperarc on success, otherwise an exception is
 * thrown.
 *
 * \throw out_of_range If the hyperarc Id is not in the range
 * [0, hyperarcContainerSize() )
 * \throw invalid_argument If the hyperarc with the given Id was removed earlier
 * from the root hypergraph.
 *
 * \par Complexity
 * Constant (hyperarc search).\n
 * Linear in the number of ancestor sub-hypergraphs.
 */
template <template <typename> typename VertexTemplate_t,
    typename Hyperarc_t,
    typename VertexProperty,
    typename HyperarcProperty,
    typename HypergraphProperty>
const Hyperarc_t* DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
    VertexProperty, HyperarcProperty, HypergraphProperty>::
addHyperarc(const HyperedgeIdType& hyperarcId) {
  throwExceptionOnNullptrParent();
  // Check whether a hyperarc with the given Id exists in the root hypergraph.
  // If no, throw an exception.
  // If yes, add the hyperarc to the subgraph and to all subgraphs in the
  // hierarchy between this subgraph and the root hypergraph.
  try {
    auto rootHypergraph = getRootHypergraph();
    const auto& hyperarc = rootHypergraph->hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                      "addHyperarc(): Can't find a hyperarc"
                                      " with given Id "
                                  + std::to_string(hyperarcId)
                                  + " in the root hypergraph.");
    } else {
      // Check that there is at least one tail or head vertex in the subgraph
      bool tailOrHeadPresentInSubgraph = false;
      try {
        // Check tails
        auto tailsPtr = rootHypergraph->tails(hyperarcId);
        for (const auto& tail : *tailsPtr) {
          if (vertexById(tail->id()) != nullptr) {
            tailOrHeadPresentInSubgraph = true;
            break;
          }
        }
        if (tailOrHeadPresentInSubgraph == false) {
          // Check heads
          auto headsPtr = rootHypergraph->heads(hyperarcId);
          for (const auto& head : *headsPtr) {
            if (vertexById(head->id()) != nullptr) {
              tailOrHeadPresentInSubgraph = true;
              break;
            }
          }
        }
        if (tailOrHeadPresentInSubgraph == false) {
          throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                          "addHyperarc() with given Id "
                                      + std::to_string(hyperarcId)
                                      + ": At least one tail or head vertex "
                                      + "have to be in the subgraph.");
        }
      } catch (const std::out_of_range& oor) {}
    }
    // Add the hyperarc to the subgraph and to all subgraphs in the hierarchy
    // between this subgraph and the root hypergraph.
    removeFilteredOutHyperarcInAncestors(hyperarc);

    // Add hyperarc property to this subgraph and the subgraphs up to the root
    // hypergraph
    addHyperarcPropertyInAncestors(hyperarcId);

    // return pointer to hyperarc
    return hyperarc;
  } catch (const std::out_of_range& oor) {
    throw std::out_of_range("Error in DirectedSubHypergraph::addHyperarc(): "
                                "Can't find a hyperarc with given Id "
                            + std::to_string(hyperarcId)
                            + " in the root hypergraph.");
  }
}


/**
 * \brief Search hyperarc list for an hyperarc with the provided id
 *
 * \param hyperarcId Identifier of the hyperarc.
 *
 * \return Pointer to hyperarc if found, nullptr if hyperarcId is between 0
 * and hyperarcContainerSize() - 1, throw out_of_range exception otherwise
 *
 * \throw out_of_range If given Id is not in the range
 * [0, hyperarcContainerSize() )
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Hyperarc_type*
DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarcById(const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check in constant time if the hyperarc id is in the whitelist of the sub-
   * hypergraph. If yes, we get the ptr to hyperarc from teh parent hypergraph.
   * If no, we return a nullptr.
   *
   * A out_of_range exception is thrown if the given hyperarc id is out of
   * range.
   */
  try {
    const auto& hyperarc = whitelistedHyperarcsList_->at(hyperarcId);
    return whitelistedHyperarcsList_->filter_out(hyperarc) ?
           nullptr : hyperarc;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "hyperarcById: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  }
}


/**
 * \brief Remove a hyperarc with the given id from the sub-hypergraph
 *
 * \details
 * This function removes a hyperarc from this and all children sub-hypergraphs
 * by adding the given hyperarc Id to a filter.
 *
 * \param hyperarcId Identifier of the hyperarc.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * O(\em n * O(\em fn)), with \em n the number of sub-hypergraphs of this
 * sub-hypergraph, and \em fn the function to add the hyperarc Id to the filter.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeHyperarc(const HyperedgeIdType& hyperarcId) {
  throwExceptionOnNullptrParent();
  /*
   * Check if there exists a hyperarc in the sub-hypergraph with the given id.
   * If yes, remove it from the whitelist.
   * If no, an exception is thrown.
   */
  try {
    const auto hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::removeHyperarc"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }

    // Notify observers of change
    HyperedgeIdType nonConstHyperarcId(hyperarcId);
    this->NotifyObservers(ObservableEvent::HYPEREDGE_REMOVED,
                          &nonConstHyperarcId);
    // Remove hyperarc
    whitelistedHyperarcsList_->addFilteredOutItem(
            const_cast<Hyperarc_t*>(hyperarc));
    // Remove hyperarc property
    delete whitelistedHyperarcProperties_->operator[](hyperarcId);
    whitelistedHyperarcProperties_->operator[](hyperarcId) = nullptr;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "hyperarcById: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to the begin of the hyperarcs.
 *
 * \return Const hyperarc iterator pointing to the begin of the hyperarcs.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the hyperarc container.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hyperarcsBegin() const {
  throwExceptionOnNullptrParent();
  return whitelistedHyperarcsList_->cbegin();
}


/**
 * \brief Const hyperarc iterator pointing to the end of the hyperarcs.
 *
 * \return Const hyperarc iterator pointing to the end of the hyperarcs.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hyperarcsEnd() const {
  throwExceptionOnNullptrParent();
  return whitelistedHyperarcsList_->cend();
}


/**
 * \brief Begin/End iterator pair of the hyperarcs of the directed hypergraph
 *
 * \details
 * Pair of hyperarc iterators pointing to the begin and the end of the
 * hyperarcs of the directed hypergraph.
 *
 * \return Pair of hyperarc iterators pointing to the begin and the end of the
 * hyperarcs of the directed hypergraph.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the hyperarc container.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator, typename DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator> DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarcsBeginEnd() const {
  throwExceptionOnNullptrParent();
  return std::make_pair(whitelistedHyperarcsList_->cbegin(),
                        whitelistedHyperarcsList_->cend());
}

/**
 * \brief Return a reference to the container of hyperarcs
 *
 * Can be used in a range based for loop as follows:
 * \code
 * for (const auto& hyperarc : subgraph->hyperarcs()) {
 *   std::cout << "Id of the hyperarc: " << hyperarc->id() << '\n';
 * }
 * \endcode
 *
 * \return Reference to hyperarc container
 *
 * \par Complexity
 * Constant (return the reference). However, the complexity to iterate over all
 * hyperarcs of a directed hypergraph using a range based for loop or a
 * classic for loop using hyperarcsBegin() and hyperarcsEnd() is the same.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty,
        HypergraphProperty>::HyperarcContainer& DirectedSubHypergraph<
        VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
        HypergraphProperty>::
hyperarcs() const {
  throwExceptionOnNullptrParent();
  return *(whitelistedHyperarcsList_.get());
}


/**
 * \brief Return number of tail vertices in the hyperarc with given id
 *
 * \details
 * This function returns the number of tail vertices in the hyperarc with given
 * id considering only those vertices in this sub-hypergraph.
 *
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return Number of tail vertices in the hyperarc with given id.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
nbTailVertices(const HyperedgeIdType & hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::nbTailVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // count number of tail vertices in this subgraph
    size_t nbTails(0);
    auto it = tailsBegin(hyperarcId);
    auto end = tailsEnd(hyperarcId);
    for (; it != end; ++it) {
      ++nbTails;
    }
    return nbTails;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "nbTailVertices: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Check if the hyperarc with the given id has tail vertices.
 *
 * \details
 * Check if the hyperarc with the given id has tail vertices considering only
 * those vertices in this sub-hypergraph.
 *
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return True if the hyperarc with the given identifier has tail vertices,
 * false otherwise.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear in the number of leading tail vertices that are not part of this sub-
 * hypergraph.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hasTailVertices(const HyperedgeIdType & hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::hasTailVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // compare begin and end
    auto begin = tailsBegin(hyperarcId);
    auto end = tailsEnd(hyperarcId);
    return (begin != end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "hasTailVertices: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the begin of the tail vertices
 *
 * \details
 * Const vertex iterator pointing to the begin of the tail vertices of the
 * hyperarc with the given identifier. Only vertices that are present in this
 * sub-hypergraph are considered.
 *
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return Const vertex iterator pointing to the begin of the tail vertices of
 * the hyperarc with the given identifier.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear in the number of leading tail vertices that are not part of this sub-
 * hypergraph.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
tailsBegin(const HyperedgeIdType & hyperarcId) const {
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  throwExceptionOnNullptrParent();
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::tailsBegin"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }

    // create iterator
    std::shared_ptr<FilteredVector<Vertex_t*>> tmp =
            create_filtered_vector<Vertex_t*>(
                    hyperarc->tails_,
                    whitelistedVerticesList_->filter());
    return tmp->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "tailsBegin: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the end of the tail vertices
 *
 * \details
 * Const vertex iterator pointing to the end of the tail vertices of the
 * hyperarc with the given identifier. Only vertices that are present in this
 * sub-hypergraph are considered.
 *
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return Const vertex iterator pointing to the end of the tail vertices of
 * the hyperarc with the given identifier.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
tailsEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  throwExceptionOnNullptrParent();
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::tailsEnd"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Vertex_t*>> tmp =
            create_filtered_vector<Vertex_t*>(
                    hyperarc->tails_,
                    whitelistedVerticesList_->filter());
    return tmp->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "tailsEnd: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Pair of vertex iterators pointing to the begin/end of the tail
 * vertices
 *
 * \details
 * This function returns a pair of const vertex iterators pointing to the begin
 * and the end of the tail vertices of the hyperarc with the given identifier.
 * Only vertices that are present in this sub-hypergraph are considered.
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return Pair of const vertex iterators pointing to the begin and the end of
 * the tail vertices of the hyperarc with the given identifier.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear in the number of leading tail vertices that are not part of this sub-
 * hypergraph.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::vertex_iterator,
        typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>::
        vertex_iterator> DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
tailsBeginEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  throwExceptionOnNullptrParent();
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::tailsBeginEnd"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator pair
    auto begin = tailsBegin(hyperarcId);
    auto end = tailsEnd(hyperarcId);
    return std::make_pair(begin, end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "tailsBeginEnd: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}

/**
 * \brief Get shared_ptr of container of the tail vertices of given hyperarc id
 *
 * \details
 * Can be used in a range based for loop as follows:
 * \code
 * auto tailsPtr = subgraph->tails(hyperarcId);
 * for (const auto& tail : *tailsPtr) {
 *   std::cout << tail->id() << " is a tail vertex of " << hyperarcId << '\n';
 * }
 * \endcode
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return shared_ptr of the container of the tail vertices of the given
 * hyperarc id.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<typename DirectedSubHypergraph<
    VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
    HypergraphProperty>::Vertex_t*>>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
tails(const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::tails"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
     errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    return create_filtered_vector(hyperarc->tails_,
                                  whitelistedVerticesList_->filter());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "tails: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Return number of head vertices in the hyperarc with given id
 *
 * \details
 * This function returns the number of head vertices in the hyperarc with given
 * id considering only those vertices in this sub-hypergraph.
 *
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return Number of head vertices in the hyperarc with given id.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
nbHeadVertices(const HyperedgeIdType & hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::nbHeadVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // count number of head vertices in this subgraph
    size_t nbHeads(0);
    auto it = headsBegin(hyperarcId);
    auto end = headsEnd(hyperarcId);
    for (; it != end; ++it) {
      ++nbHeads;
    }
    return nbHeads;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "nbHeadVertices: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Check if the hyperarc with the given id has head vertices.
 *
 * \details
 * Check if the hyperarc with the given id has head vertices considering only
 * those vertices in this sub-hypergraph.
 *
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return True if the hyperarc with the given identifier has head vertices,
 * false otherwise.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear in the number of leading head vertices that are not part of this sub-
 * hypergraph.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hasHeadVertices(const HyperedgeIdType & hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::hasHeadVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }

    auto begin = headsBegin(hyperarcId);
    auto end = headsEnd(hyperarcId);
    return (begin != end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "hasHeadVertices: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the begin of the head vertices
 *
 * \details
 * Const vertex iterator pointing to the begin of the head vertices of the
 * hyperarc with the given identifier. Only vertices that are present in this
 * sub-hypergraph are considered.
 *
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return Const vertex iterator pointing to the begin of the head vertices of
 * the hyperarc with the given identifier.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear in the number of leading head vertices that are not part of this sub-
 * hypergraph.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
headsBegin(const HyperedgeIdType & hyperarcId) const {
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto &hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::headsBegin"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // Create iterator
    std::shared_ptr<FilteredVector<Vertex_t*>> tmp =
            create_filtered_vector<Vertex_t*>(
                    hyperarc->heads_,
                    whitelistedVerticesList_->filter());
    return tmp->cbegin();
  } catch (const std::out_of_range &oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "headsBegin: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument &ia) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the end of the head vertices
 *
 * \details
 * Const vertex iterator pointing to the end of the head vertices of the
 * hyperarc with the given identifier. Only vertices that are present in this
 * sub-hypergraph are considered.
 *
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return Const vertex iterator pointing to the end of the head vertices of
 * the hyperarc with the given identifier.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
headsEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  throwExceptionOnNullptrParent();
  try {
    const auto &hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::headsEnd"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Vertex_t*>> tmp =
            create_filtered_vector<Vertex_t*>(
                    hyperarc->heads_,
                    whitelistedVerticesList_->filter());
    return tmp->cend();
  } catch (const std::out_of_range &oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "headsEnd: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument &ia) {
    throw;
  }
}


/**
 * \brief Pair of vertex iterators pointing to the begin/end of the head
 * vertices
 *
 * \details
 * This function returns a pair of const vertex iterators pointing to the begin
 * and the end of the head vertices of the hyperarc with the given identifier.
 * Only vertices that are present in this sub-hypergraph are considered.
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return Pair of const vertex iterators pointing to the begin and the end of
 * the head vertices of the hyperarc with the given identifier.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear in the number of leading head vertices that are not part of this sub-
 * hypergraph.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::vertex_iterator,
        typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
                VertexProperty, HyperarcProperty, HypergraphProperty>::
        vertex_iterator> DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
headsBeginEnd(const HyperedgeIdType & hyperarcId) const {
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  throwExceptionOnNullptrParent();
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::headsBeginEnd"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create pair of iterators
    auto begin = headsBegin(hyperarcId);
    auto end = headsEnd(hyperarcId);
    return std::make_pair(begin, end);
  } catch (const std::out_of_range &oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "headsBeginEnd: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument &ia) {
    throw;
  }
}


/**
 * \brief Get shared_ptr of container of the head vertices of given hyperarc id
 *
 * \details
 * Can be used in a range based for loop as follows:
 * \code
 * auto headsPtr = subgraph->heads(hyperarcId);
 * for (const auto& head : *headsPtr) {
 *   std::cout << head->id() << " is a head vertex of " << hyperarcId << '\n';
 * }
 * \endcode
 *
 * \param hyperarcId Identifier of a hyperarc.
 * \return shared_ptr of the container of the head vertices of the given
 * hyperarc id.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<typename DirectedSubHypergraph<
    VertexTemplate_t, Hyperarc_t, VertexProperty, HyperarcProperty,
    HypergraphProperty>::Vertex_t*>> DirectedSubHypergraph<VertexTemplate_t,
    Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
heads(const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperarc is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::heads"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
     errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create FilteredVector
    return create_filtered_vector(hyperarc->heads_,
                                  whitelistedVerticesList_->filter());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "heads: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Size of the vertex container
 *
 * \details
 * Pointers to vertex objects are stored in a customized vector. If a
 * vertex is removed from the hypergraph, then the corresponding entry in
 * this vector is set to a nullptr. The size of the container remains unchanged.
 * Thus, the return value of this function may differ from nbVertices().
 *
 * \return Size of the vertex container.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
vertexContainerSize() const {
  throwExceptionOnNullptrParent();
  return parent_->vertexContainerSize();
}


/**
 * \brief Number of vertices in the sub-hypergraph
 *
 * \return Number of vertices in the sub-hypergraph.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
nbVertices() const {
  throwExceptionOnNullptrParent();
  size_t nbVertices(0);
  for (auto it = whitelistedVerticesList_->begin();
       it != whitelistedVerticesList_->end(); ++it) {
    ++nbVertices;
  }
  return nbVertices;
}

/**
 * \brief Search vertex list for a vertex with the provided name
 *
 * \details
 * Search vertex list for a vertex with the provided name considering only the
 * vertices in this sub-hypergraph.
 *
 * \param vertexName Name of the vertex
 *
 * \return Pointer to const vertex if a vertex with the specified name exists
 * in the sub-hypergraph, otherwise a nullptr.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Vertex_t*
DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
vertexByName(const std::string& vertexName) const {
  throwExceptionOnNullptrParent();
  for (const auto& vertex : *(whitelistedVerticesList_.get())) {
    if (vertex->name().compare(vertexName) == 0) {
      return vertex;
    }
  }
  return nullptr;
}


/**
 * \brief Search vertex list for a vertex with the provided id
 *
 * \details
 * Search vertex list for a vertex with the provided Id considering only the
 * vertices in this sub-hypergraph.
 *
 * \param vertexId Identifier of the vertex
 *
 * \return Pointer to const vertex if a vertex with the specified Id exists
 * in the sub-hypergraph, otherwise a nullptr.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() )
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Vertex_t*
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
vertexById(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = whitelistedVerticesList_->at(vertexId);
    return whitelistedVerticesList_->filter_out(vertex) ? nullptr : vertex;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in SubHypergraph::vertexById: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the begin of the vertices
 *
 * \return Const vertex iterator pointing to begin of the vertices.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the vertex container.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
verticesBegin() const {
  throwExceptionOnNullptrParent();
  return whitelistedVerticesList_->cbegin();
}


/**
 * \brief Const vertex iterator pointing to the end of the vertices
 *
 * \return Const vertex iterator pointing to end of the vertices.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::vertex_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
verticesEnd() const {
  throwExceptionOnNullptrParent();
  return whitelistedVerticesList_->cend();
}


/**
 * \brief Begin/End iterator pair of the vertices of the directed sub-hypergraph
 *
 * \details
 * Pair of vertex iterators pointing to the begin and the end of the
 * vertices of the directed sub-hypergraph.
 *
 * \return Pair of vertex iterators pointing to the begin and the end of the
 * vertices of the directed sub-hypergraph.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the vertex container.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::vertex_iterator,
        typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::vertex_iterator>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
verticesBeginEnd() const {
  throwExceptionOnNullptrParent();
  return std::make_pair(whitelistedVerticesList_->cbegin(),
                        whitelistedVerticesList_->cend());
}


/**
 * \brief Return a reference to the container of vertices
 *
 * \details
 * Can be used in a range based for loop as follows:
 * \code
 * for (const auto& vertex : subgraph->vertices()) {
 *   std::cout << "Id of the vertex: " << vertex->id() << '\n';
 * }
 * \endcode
 *
 * \return Reference to vertex container
 *
 * \par Complexity
 * Constant (return the reference). However, the complexity to iterate over all
 * vertices of a directed sub-hypergraph using a range based for loop or a
 * classic for loop using verticesBegin() and verticesEnd() is the same.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::VertexContainer&
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
vertices() const {
  throwExceptionOnNullptrParent();
  return *(whitelistedVerticesList_.get());
}


/**
 * \brief Add a vertex
 *
 * \details
 * Remove a vertex from the filtered out vertices of this directed
 * sub-hypergraph. However, with this function it can not be determined which
 * vertex is meant. Provide the vertex name or Id to remove a vertex from the
 * filtered out vertices. Use either
 * addVertex(const std::string&, const typename Vertex_t::SpecificAttributes&)
 * or addVertex(const VertexIdType&).
 *
 * \param attributes Vertex type specific attributes to create a vertex.
 *
 * \return Pointer to const vertex on success, throw an exception otherwise.
 *
 * \throw invalid_argument Throws always an invalid_argument exception.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Vertex_t*
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
addVertex(const typename Vertex_t::SpecificAttributes&) {
  throwExceptionOnNullptrParent();
  throw std::invalid_argument("Error in DirectedSubHypergraph::addVertex(): "
                                  "Impossible to search for a vertex without a"
                                  " name.");
}


/**
 * \brief Add a vertex
 *
 * \details
 * Remove a vertex from the filtered out vertices of this directed
 * sub-hypergraph and from all ancestor sub-hypergraph (except the root
 * hypergraph). A vertex with the given name and vertex type SpecificAttributes
 * is first searched in the root hypergraph using the function
 * VertexTemplate_t<Hyperarc_t>::compare(). If found, the vertex is removed
 * from the filtered out vertices in this sub-hypergraph and its ancestor
 * sub-hypergraphs. This means that the vertex is visible in those
 * sub-hypergraphs. This function is less efficient than
 * addVertex(const VertexIdType&)
 *
 * \param name Vertex name.
 * \param attributes Vertex type specific attributes to create a vertex.
 *
 * \return Pointer to const vertex on success, throw an exception otherwise.
 *
 * \throw invalid_argument If a vertex with the given arguments does not exists
 * in the root-hypergraph.
 *
 * \par Complexity
 * Linear in number of vertices (vertex search).\n
 * Linear in the number of ancestor sub-hypergraphs.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::Vertex_t*
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
addVertex(const std::string& name,
          const typename Vertex_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();
  // Search for the vertex in the root hypergraph
  Vertex_t* vertexInRootGraph = nullptr;
  for (const auto& vertex : getRootHypergraph()->vertices()) {
    if (vertex->compare(name, attributes) == true) {
      vertexInRootGraph = vertex;
      break;
    }
  }
  if (vertexInRootGraph == nullptr) {
    throw std::invalid_argument("Error in DirectedSubHypergraph::addVertex(): "
                                    "Can't find a vertex with given name "
                                + name + " in the root hypergraph.");
  }
  // Add vertex to this subgraph and the subgraphs up to the root hypergraph
  addVertex(vertexInRootGraph->id());
  return vertexInRootGraph;
}


/**
 * \brief Add a vertex
 *
 * \details
 * Remove a vertex from the filtered out vertices of this directed
 * sub-hypergraph and from all ancestor sub-hypergraph (except the root
 * hypergraph). A vertex with the given Id is first searched in the root
 * hypergraph. If found, the vertex is removed from the filtered out vertices
 * in this sub-hypergraph and its ancestor sub-hypergraphs. This means that the
 * vertex is visible in those sub-hypergraphs.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Pointer to const vertex on success, throw an exception otherwise.
 *
 * \throw out_of_range If the vertex Id is not in the range
 * [0, vertexContainerSize() )
 * \throw invalid_argument If the vertex with the given Id was removed earlier
 * from the root hypergraph.
 *
 * \par Complexity
 * Constant (vertex search).\n
 * Linear in the number of ancestor sub-hypergraphs.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
    typename VertexProperty, typename HyperarcProperty,
    typename HypergraphProperty>
const typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
    VertexProperty, HyperarcProperty, HypergraphProperty>::Vertex_t*
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
    HyperarcProperty, HypergraphProperty>::
addVertex(const VertexIdType& vertexId) {
  throwExceptionOnNullptrParent();
  // Check whether a vertex with the given Id exists in the root hypergraph.
  // If no, throw an exception.
  // If yes, add the vertex to the subgraph and to all subgraphs in the
  // hierarchy between this subgraph and the root hypergraph.
  try {
    const auto& vertex = getRootHypergraph()->vertexById(vertexId);
    if (vertex == nullptr) {
      throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                      "addVertex(): Can't find a vertex with"
                                      " given Id " + std::to_string(vertexId)
                                  + " in the root hypergraph.");
    }
    // Add the vertex to the subgraph and to all subgraphs in the hierarchy
    // between this subgraph and the root hypergraph.
    removeFilteredOutVertexInAncestors(vertex);
    // Add vertex property to this subgraph and the subgraphs up to the root
    // hypergraph
    addVertexPropertyInAncestors(vertexId);
    return vertex;
  } catch (const std::out_of_range& oor) {
    throw std::out_of_range("Error in DirectedSubHypergraph::addVertex(): "
                                "Can't find a vertex with given Id "
                            + std::to_string(vertexId)
                            + " in the root hypergraph.");
  }
}


/**
 * \brief Get arguments to create an existing vertex
 *
 * \details
 * This function returns an ArgumentsToCreateVertex object that can
 * be used to insert a vertex in the originating or in a different hypergraph.
 * An ArgumentsToCreateVertex object consists of two parts,
 * (i) the vertex name, (ii) specific arguments needed to create a vertex of
 * type VertexTemplate_t<Hyperarc_t>.
 *
 * The returned object can be transformed to add a different vertex
 * to the hypergraph. This function can be useful in generic methods that
 * accepts any kind of hypergraph and that transform them.
 *
 * \par Example
 * \parblock
 * \code
 *
 * // Create a sub-hypergraph of hypergraph g
 * // ...with vertex Ids 1, 2, 3
 * std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
 * // ...with hyperarc Ids 1, 2
 * std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {1, 2};
 * // createDirectedSubHypergraph returns a shared_ptr
 * auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                                      whitelistedHyperarcs);
 *
 * // Get arguments to create vertex with Id 1
 * auto args = subgraph->argumentsToCreateVertex(1);
 * // Add vertex to another hypergraph g2.
 * const auto& vertex2 = g2.addVertex(args.name(), args.specificAttributes());
 * \endcode
 * \endparblock
 *
 * \param vertexId Identifier of the vertex.
 *
 * \return ArgumentsToCreateVertex by value.
 *
 * \throw out_of_range If the vertex Id is not in the range
 * [0, vertexContainerSize() )
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * The complexity depends on the function argumentsToCreateVertex() of
 * VertexTemplate_t<Hyperarc_t>.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
ArgumentsToCreateVertex<typename DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty,
        HypergraphProperty>::Vertex_t> DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
argumentsToCreateVertex(const hglib::VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = whitelistedVerticesList_->at(vertexId);
    if (not whitelistedVerticesList_->filter_out(vertex)) {
      return whitelistedVerticesList_->operator[](
              vertexId)->argumentsToCreateVertex();
    } else {
      throw std::invalid_argument("Vertex with " + std::to_string(vertexId) +
                                  " is not part of the subgraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in SubHypergraph::vertexById: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}


/**
 * \brief Return number of incoming hyperarcs of the vertex with the given id
 *
 * \details
 * This function returns the number of incoming hyperarcs of the vertex with
 * the given id. Only hyperarcs that are present in this sub-hypergraph are
 * counted.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Number of incoming hyperarcs of the vertex with the given Id.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
inDegree(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::inDegree"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // count number of in-arcs of given vertex in this subgraph
    size_t inDegree(0);
    auto it = inHyperarcsBegin(vertexId);
    auto end = inHyperarcsEnd(vertexId);
    for (; it != end; ++it) {
      ++inDegree;
    }
    return inDegree;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "inDegree: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Check if the vertex with the given id has incoming hyperarcs
 *
 * \details
 * Check if the vertex with the given id has incoming hyperarcs in this
 * sub-hypergraph.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return True if the vertex with the given identifier has incoming hyperarcs
 * in this sub-hypergraph, false otherwise.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear in the number of leading incoming hyperarcs of the given vertex that
 * are not in this sub-hypergraph.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hasInHyperarcs(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::hasInHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // check if there are in-arcs
    auto begin = inHyperarcsBegin(vertexId);
    auto end = inHyperarcsEnd(vertexId);
    return (begin != end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "hasInHyperarcs: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to the begin of the in-hyperarcs
 *
 * \details
 * Const hyperarc iterator pointing to the begin of the incoming hyperarcs of
 * the vertex with the given identifier. Only hyperarcs that are present in
 * this sub-hypergraph are considered.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Const hyperarc iterator pointing to the begin of the incoming
 * hyperarcs of the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear in the number of leading incoming hyperarcs of the given vertex that
 * are not in this sub-hypergraph.
*/
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
inHyperarcsBegin(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::inHyperarcsBegin"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Hyperarc_t*>> tmp =
            create_filtered_vector<Hyperarc_t*>(
                    vertex->inHyperarcs_,
                    whitelistedHyperarcsList_->filter());
    return tmp->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "inHyperarcsBegin: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to the end of the in-hyperarcs
 *
 * \details
 * Const hyperarc iterator pointing to the end of the incoming hyperarcs of
 * the vertex with the given identifier. Only hyperarcs that are present in
 * this sub-hypergraph are considered.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Const hyperarc iterator pointing to the end of the incoming
 * hyperarcs of the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
*/
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
inHyperarcsEnd(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::inHyperarcsEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Hyperarc_t*>> tmp =
            create_filtered_vector<Hyperarc_t*>(
                    vertex->inHyperarcs_,
                    whitelistedHyperarcsList_->filter());
    return tmp->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "inHyperarcsEnd: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Pair of hyperarc iterators pointing to the begin/end of the in-arcs
 *
 * \details
 * This function returns a pair of const hyperarc iterators pointing to the
 * begin and the end of the incoming hyperarcs of the vertex with the given
 * identifier. Only hyperarcs that are present in this sub-hypergraph are
 * considered.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Pair of const hyperarc iterator pointing to the begin/end of the
 * incoming hyperarcs of the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear in the number of leading incoming hyperarcs of the given vertex that
 * are not in this sub-hypergraph.
*/
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator, typename DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator> DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
inHyperarcsBeginEnd(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::"
                                       "inHyperarcsBeginEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create pair of iterators
    auto begin = inHyperarcsBegin(vertexId);
    auto end = inHyperarcsEnd(vertexId);
    return std::make_pair(begin, end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "inHyperarcsBeginEnd: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Get shared_ptr of container of the in-arcs of the given vertex Id
 *
 * \details
 * This function returns a shared_ptr of the container of the in-arcs of the
 * given vertex Id. Only hyperarcs that are present in this sub-hypergraph are
 * considered. This function allows to use a range based for loop as follows:
 * \code
 * // Create a sub-hypergraph of hypergraph g
 * // ...with vertex Ids 1, 2, 3
 * std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
 * // ...with hyperarc Ids 1, 2
 * std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {1, 2};
 * // createDirectedSubHypergraph returns a shared_ptr
 * auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
 *                                                    whitelistedHyperarcs);
 *
 * // Iterate over incoming hyperarcs of some vertex
 * hglib::VertexIdType vertexId(1);
 * auto inarcsPtr = subgraph->inHyperarcs(vertexId);
 * for (const auto& arc : *inarcsPtr) {
 *   std::cout << arc->id() << " is a incoming hyperarc of vertex "
 *             << vertexId << '\n';
 * }
 * \endcode
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return shared_ptr of the container of the incoming hyperarcs of the given
 * vertex id.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant.\n
 * However, the complexity to iterate over all incoming hyperarcs of a vertex
 * using a range based for loop or a classic for loop using inHyperarcsBegin()
 * and inHyperarcsEnd() is the same.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<Hyperarc_t*>>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
    HyperarcProperty, HypergraphProperty>::
inHyperarcs(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::inHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
     errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create FilteredVector
    return create_filtered_vector(vertex->inHyperarcs_,
                                  whitelistedHyperarcsList_->filter());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "inHyperarcs: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}

/**
 * \brief Return number of outgoing hyperarcs of the vertex with the given id
 *
 * \details
 * This function returns the number of outgoing hyperarcs of the vertex with
 * the given id. Only hyperarcs that are present in this sub-hypergraph are
 * counted.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Number of outgoing hyperarcs of the vertex with the given Id.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
size_t DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
outDegree(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::outDegree"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // count number of out-arcs of given vertex in this subgraph
    size_t outDegree(0);
    auto it = outHyperarcsBegin(vertexId);
    auto end = outHyperarcsEnd(vertexId);
    for (; it != end; ++it) {
      ++outDegree;
    }
    return outDegree;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "outDegree: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Check if the vertex with the given id has outgoing hyperarcs
 *
 * \details
 * Check if the vertex with the given id has outgoing hyperarcs in this
 * sub-hypergraph.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return True if the vertex with the given identifier has outgoing hyperarcs
 * in this sub-hypergraph, false otherwise.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear in the number of leading outgoing hyperarcs of the given vertex that
 * are not in this sub-hypergraph.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
hasOutHyperarcs(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::hasOutHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // check if there are out-arcs
    auto begin = outHyperarcsBegin(vertexId);
    auto end = outHyperarcsEnd(vertexId);
    return (begin != end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "hasOutHyperarcs: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to the begin of the out-hyperarcs
 *
 * \details
 * Const hyperarc iterator pointing to the begin of the outgoing hyperarcs of
 * the vertex with the given identifier. Only hyperarcs that are present in
 * this sub-hypergraph are considered.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Const hyperarc iterator pointing to the begin of the outgoing
 * hyperarcs of the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear in the number of leading outgoing hyperarcs of the given vertex that
 * are not in this sub-hypergraph.
*/
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
outHyperarcsBegin(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::outHyperarcsBegin"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Hyperarc_t*>> tmp =
            create_filtered_vector<Hyperarc_t*>(
                    vertex->outHyperarcs_,
                    whitelistedHyperarcsList_->filter());
    return tmp->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "outHyperarcsBegin: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const hyperarc iterator pointing to the end of the out-hyperarcs
 *
 * \details
 * Const hyperarc iterator pointing to the end of the outgoing hyperarcs of
 * the vertex with the given identifier. Only hyperarcs that are present in
 * this sub-hypergraph are considered.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Const hyperarc iterator pointing to the end of the outgoing
 * hyperarcs of the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
*/
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::hyperarc_iterator
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
outHyperarcsEnd(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::outHyperarcsEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Hyperarc_t*>> tmp =
            create_filtered_vector<Hyperarc_t*>(
                    vertex->outHyperarcs_,
                    whitelistedHyperarcsList_->filter());
    return tmp->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "outHyperarcsEnd: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Pair of hyperarc iterators pointing to the begin/end of the out-arcs
 *
 * \details
 * This function returns a pair of const hyperarc iterators pointing to the
 * begin and the end of the outgoing hyperarcs of the vertex with the given
 * identifier. Only hyperarcs that are present in this sub-hypergraph are
 * considered.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Pair of const hyperarc iterator pointing to the begin/end of the
 * outgoing hyperarcs of the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear in the number of leading outgoing hyperarcs of the given vertex that
 * are not in this sub-hypergraph.
*/
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::pair<typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator, typename DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
hyperarc_iterator> DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
outHyperarcsBeginEnd(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::"
                                       "outHyperarcsBeginEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create pair of iterators
    auto begin = outHyperarcsBegin(vertexId);
    auto end = outHyperarcsEnd(vertexId);
    return std::make_pair(begin, end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "outHyperarcsBeginEnd: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}

/**
 * \brief Get shared_ptr of container of the out-arcs of the given vertex Id
 *
 * \details
 * This function returns a shared_ptr of the container of the out-arcs of the
 * given vertex Id. Only hyperarcs that are present in this sub-hypergraph are
 * considered. This function allows to use a range based for loop as follows:
 * \code
 * // Create a sub-hypergraph of hypergraph g
 * // ...with vertex Ids 1, 2, 3
 * std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
 * // ...with hyperarc Ids 1, 2
 * std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {1, 2};
 * // createDirectedSubHypergraph returns a shared_ptr
 * auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
 *                                                    whitelistedHyperarcs);
 *
 * // Iterate over outgoing hyperarcs of some vertex
 * hglib::VertexIdType vertexId(1);
 * auto outarcsPtr = subgraph->outHyperarcs(vertexId);
 * for (const auto& arc : *outarcsPtr) {
 *   std::cout << arc->id() << " is a outgoing hyperarc of vertex "
 *             << vertexId << '\n';
 * }
 * \endcode
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return shared_ptr of the container of the outgoing hyperarcs of the given
 * vertex id.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant.\n
 * However, the complexity to iterate over all outgoing hyperarcs of a vertex
 * using a range based for loop or a classic for loop using outHyperarcsBegin()
 * and outHyperarcsEnd() is the same.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<Hyperarc_t*>>
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
      HyperarcProperty, HypergraphProperty>::
outHyperarcs(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::outHyperarcs"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
     errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create FilteredVector
    return create_filtered_vector(vertex->outHyperarcs_,
                                  whitelistedHyperarcsList_->filter());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "outHyperarcs: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}

/**
 * \brief Remove a vertex with the given name from the directed sub-hypergraph
 *
 * \details
 * This function removes a vertex with the given name from the directed
 * sub-hypergraph. Incoming and outgoing hyperarcs of this vertex are also
 * removed if the second parameter is set to true. The vertex and the hyperarcs
 * remain in the root-hypergraph. They are only removed from the
 * sub-hypergraph (view).
 *
 * \param vertexName Name of a vertex
 * \param removeImpliedHyperarcs Flag that shows whether hyperarcs that contain
 * the vertex with the given name should be removed from the hypergraph.
 * Defaults to true.
 *
 * \throw invalid_argument If there exists no vertex with the given name.
 *
 * \par Complexity
 * Linear.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeVertex(const std::string& vertexName,
             bool removeImpliedHyperarcs) {
  throwExceptionOnNullptrParent();
  // Find vertex by name in the hypergraph and call the method removeVertex
  // that takes the id of the vertex
  const auto& vertex = this->vertexByName(vertexName);
  if (vertex == nullptr) {
    std::string errorMessage("Called DirectedSubHypergraph::removeVertex"
                                     " with the vertex name " + vertexName +
                             " which does not exists in the directed "
                                     "subhypergraph.\n");
    throw std::invalid_argument(errorMessage);
  }

  removeVertex(vertex->id(), removeImpliedHyperarcs);
}


/**
 * \brief Remove a vertex with the given Id from the directed hypergraph
 *
 * \details
 * This function removes a vertex with the given Id from the directed
 * sub-hypergraph. Incoming and outgoing hyperarcs of this vertex are also
 * removed if the second parameter is set to true. The vertex and the hyperarcs
 * remain in the root-hypergraph. They are only removed from the
 * sub-hypergraph (view). This function is faster than the
 * other removeVertex function (via vertex name) as the access to the vertex is
 * constant instead of linear.
 *
 * \param vertexId Identifier of a vertex
 * \param removeImpliedHyperarcs Flag that shows whether hyperarcs that contain
 * the vertex with the given Id should be removed from the hypergraph.
 * Defaults to true.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
removeVertex(const VertexIdType& vertexId,
             bool removeImpliedHyperarcs) {
  throwExceptionOnNullptrParent();
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before, or does not take part of the
   * sub-hypergraph.
   * Catch IndexOutOfRangeException if given id is out of bound.
   *
   * Remove all implied hyperarcs from the sub-hypergraph if flag is set to
   * true (default).
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::removeVertex"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    if (removeImpliedHyperarcs) {
      // hyperarcs that will be removed from the hypergraph
      std::vector<Hyperarc_type*> hyperarcsToBeRemoved;
      for (auto& hyperarc : *(whitelistedHyperarcsList_.get())) {
        // all hyperarcs that contain the vertex will be removed from the
        // hypergraph
        if (isVertexOfHyperarc(vertexId, hyperarc->id())) {
          hyperarcsToBeRemoved.emplace_back(hyperarc);
        }
      }
      // remove hyperarcs from hypergraph
      for (const auto& hyperarc : hyperarcsToBeRemoved) {
        removeHyperarc(hyperarc->id());
      }
    }

    // Notify observers of change
    Remove_vertex_args args;
    args.vertexId_ = vertexId;
    args.removeImpliedHyperarcs_ = removeImpliedHyperarcs;
    this->NotifyObservers(ObservableEvent::VERTEX_REMOVED, &args);

    // Remove vertex
    whitelistedVerticesList_->addFilteredOutItem(
            const_cast<Vertex_t*>(vertex));
    // Remove vertex property
    delete whitelistedVertexProperties_->operator[](vertexId);
    whitelistedVertexProperties_->operator[](vertexId) = nullptr;

    // Remove hyperarcs from subgraph if they have neither tails nor heads
    if (not removeImpliedHyperarcs) {
      std::vector<Hyperarc_type*> hyperarcsToBeRemoved;
      for (const auto& hyperarc : *(whitelistedHyperarcsList_.get())) {
        if (hasTailVertices(hyperarc->id()) == false and
            hasHeadVertices(hyperarc->id()) == false) {
          hyperarcsToBeRemoved.push_back(hyperarc);
        }
      }
      // remove hyperarcs from hypergraph
      for (const auto& hyperarc : hyperarcsToBeRemoved) {
        removeHyperarc(hyperarc->id());
      }
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "removeVertex:"
            " index = " << vertexId << "; min = 0; max = "
              << this->whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Check if the vertex is a tail vertex of the hyperarc
 *
 * \details
 * This function checks whether the vertex with the given vertex Id is a tail
 * vertex of the hyperarc with the given hyperarc Id. Only vertices and
 * hyperarcs that are present in this sub-hypergraph are considered.
 *
 * \param vertexId Identifier of a vertex.
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return True if the vertex is a tail vertex of the hyperarc, false
 * otherwise
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ) or the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear in the number of tail vertices of the given hyperarc.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
isTailVertexOfHyperarc(const VertexIdType& vertexId,
                       const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex and hyperarc are part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::"
                                       "isTailVertexOfHyperarc"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "isTailVertexOfHyperarc: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }

  try {
    const auto hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::"
                                       "isTailVertexOfHyperarc"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "isTailVertexOfHyperarc: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }

  auto it = tailsBegin(hyperarcId);
  auto end = tailsEnd(hyperarcId);
  for (; it != end; ++it) {
     if ((*it)->id() == vertexId) {
      return true;
    }
  }
  return false;
}


/**
 * \brief Check if the vertex is a head vertex of the hyperarc
 *
 * \details
 * This function checks whether the vertex with the given vertex Id is a head
 * vertex of the hyperarc with the given hyperarc Id. Only vertices and
 * hyperarcs that are present in this sub-hypergraph are considered.
 *
 * \param vertexId Identifier of a vertex.
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return True if the vertex is a head vertex of the hyperarc, false
 * otherwise
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ) or the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear in the number of head vertices of the given hyperarc.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
isHeadVertexOfHyperarc(const VertexIdType& vertexId,
                       const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex and hyperarc are part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::"
                                       "isHeadVertexOfHyperarc"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "isHeadVertexOfHyperarc: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }

  try {
    const auto hyperarc = hyperarcById(hyperarcId);
    if (hyperarc == nullptr) {
      std::string errorMessage("Called DirectedSubHypergraph::"
                                       "isHeadVertexOfHyperarc"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperarcId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in DirectedSubHypergraph::"
            "isHeadVertexOfHyperarc: "
            "index = " << hyperarcId << "; min = 0; max = " <<
              whitelistedHyperarcsList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
  auto it = headsBegin(hyperarcId);
  auto end = headsEnd(hyperarcId);
  for (; it != end; ++it) {
    if ((*it)->id() == vertexId) {
      return true;
    }
  }
  return false;
}


/**
 * \brief Check if the vertex is a tail or a head vertex of the hyperarc
 *
 * \details
 * This function checks whether the vertex with the given vertex Id is a tail
 * or head vertex of the hyperarc with the given hyperarc Id. Only vertices and
 * hyperarcs that are present in this sub-hypergraph are considered.
 *
 * \param vertexId Identifier of a vertex.
 * \param hyperarcId Identifier of a hyperarc.
 *
 * \return True if the vertex is a tail or head vertex of the hyperarc, false
 * otherwise
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ) or the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 * \throw invalid_argument If there is a nullptr in the hyperarc container at
 * the position with the given hyperarc Id.
 *
 * \par Complexity
 * Linear in the number of tail and head vertices of the given hyperarc.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
bool DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
isVertexOfHyperarc(const VertexIdType& vertexId,
                   const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  return (isTailVertexOfHyperarc(vertexId, hyperarcId) or
          isHeadVertexOfHyperarc(vertexId, hyperarcId));
}


/**
 * \brief Get properties of the hyperarc with the provided id
 *
 * \details
 * This function returns a pointer to a const (read only) hyperarc property
 * associated to the hyperarc with the specified hyperarc Id.
 *
 * \param hyperarcId Identifier of the hyperarc.
 *
 * \return Pointer to const hyperarc property type. Throws an exception if the
 * hyperarc is not part of the directed sub-hypergraph.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there exists no hyperarc with the given Id in the
 * directed sub-hypergraph.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
const HyperarcProperty* DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getHyperarcProperties(const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& hyperarcProp = whitelistedHyperarcProperties_->at(hyperarcId);
    if (hyperarcProp == nullptr) {
      throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                      "getHyperarcProperties: hyperarc with id "
                                  + std::to_string(hyperarcId)
                                  + " is not part of the subgraph.");
    }
    return hyperarcProp;
  } catch (const std::out_of_range& oor) {
    throw std::out_of_range("IndexOutOfRangeException in "
                                "DirectedSubHypergraph::"
                                "getHyperarcProperties: index = "
                            + std::to_string(hyperarcId)
                            + "; min = 0; max = "
                            + std::to_string(
        whitelistedHyperarcsList_->size()));
  }
}


/**
 * \brief Get properties of the hyperarc with the provided id
 *
 * \details
 * This function returns a pointer to a hyperarc property associated to the
 * hyperarc with the specified hyperarc Id. This is a non-const version of
 * getHyperarcProperties().
 *
 * \param hyperarcId Identifier of the hyperarc.
 *
 * \return Pointer to hyperarc property type. Throws an exception if the
 * hyperarc is not part of the directed sub-hypergraph.
 *
 * \throw out_of_range If the given hyperarc Id is not in the range
 * [0, hyperarcContainerSize() ).
 * \throw invalid_argument If there exists no hyperarc with the given Id in the
 * directed sub-hypergraph.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
HyperarcProperty* DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getHyperarcProperties_(const HyperedgeIdType& hyperarcId) const {
  throwExceptionOnNullptrParent();
  try {
    return const_cast<HyperarcProperty*>(getHyperarcProperties(hyperarcId));
  } catch (const std::out_of_range& oor) {
    throw std::out_of_range("IndexOutOfRangeException in "
                                "DirectedSubHypergraph::"
                                "getHyperarcProperties_: index = "
                            + std::to_string(hyperarcId) + "; min = 0; max = "
                            + std::to_string(
        whitelistedHyperarcProperties_->size()));
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                    "getHyperarcProperties_: hyperarc with id "
                                + std::to_string(hyperarcId)
                                + " is not part of the sub-hypergraph.");
  }
}


/**
 * \brief Get properties of the vertex with the provided id
 *
 * \details
 * This function returns a pointer to a const (read only) vertex property
 * associated to the vertex with the specified vertex Id.
 *
 * \param vertexId Identifier of the vertex.
 *
 * \return Pointer to const vertex property type. Throws an exception if the
 * vertex is not part of the directed sub-hypergraph.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there exists no vertex with the given Id in the
 * directed sub-hypergraph.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const VertexProperty* DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getVertexProperties(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertexProp = whitelistedVertexProperties_->at(vertexId);
    if (vertexProp == nullptr) {
      throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                      "getVertexProperties: vertex with " +
                                  std::to_string(vertexId) +
                                  " is not part of the subgraph.");
    }
    return vertexProp;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in"
            " DirectedSubHypergraph::getVertexProperties: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}


/**
 * \brief Get properties of the vertex with the provided id
 *
 * \details
 * This function returns a pointer to a vertex property associated to the
 * vertex with the specified vertex Id. This is a non-const version of
 * getVertexProperties().
 *
 * \param vertexId Identifier of the vertex.
 *
 * \return Pointer to vertex property type. Throws an exception if the
 * vertex is not part of the directed sub-hypergraph.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there exists no vertex with the given Id in the
 * directed sub-hypergraph.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
VertexProperty* DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getVertexProperties_(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    return const_cast<VertexProperty*>(getVertexProperties(vertexId));
  } catch (const std::out_of_range& oor) {
    throw std::out_of_range("IndexOutOfRangeException in "
                                "DirectedSubHypergraph::getVertexProperties_: "
                                "index = " + std::to_string(vertexId)
                            + "; min = 0; max = " + std::to_string(
        whitelistedVertexProperties_->size()));
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument("Error in DirectedSubHypergraph::"
                                    "getVertexProperties_: vertex with " +
                                std::to_string(vertexId) +
                                " is not part of the sub-hypergraph.");
  }
}


/**
 * \brief Get properties of the sub-hypergraph
 *
 * \details
 * This function returns a pointer to a const (read only) HypergraphProperty
 * type associated with this sub-hypergraph.
 *
 * \return Pointer to const HypergraphProperty type.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
const HypergraphProperty* DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getHypergraphProperties() const {
  throwExceptionOnNullptrParent();
  return subHypergraphProperty_;
}


/**
 * \brief Get properties of the sub-hypergraph
 *
 * \details
 * This function returns a pointer to a HypergraphProperty type associated with
 * this sub-hypergraph. This is a non-const version of
 * getHypergraphProperties().
 *
 * \return Pointer to HypergraphProperty type.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
HypergraphProperty* DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
getHypergraphProperties_() const {
  throwExceptionOnNullptrParent();
  return const_cast<HypergraphProperty*>(getHypergraphProperties());
}


/**
 * \brief Observer update method
 *
 * \details
 * This function is called in the observed hypergraph to delegate the
 * information about an event that requires an update in this sub-hypergraph.
 *
 * \param o Observable Reference to observed hypergraph.
 * \param e Event Observed event.
 * \param arg Optional arguments.
 *
 * \par Complexity
 * The complexity depends on the action that is executed to respond to a given
 * event.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t,
        VertexProperty, HyperarcProperty, HypergraphProperty>::
Update(const Observable& o, ObservableEvent e, void* arg) {
  throwExceptionOnNullptrParent();
  if (&o == parent_) {
    switch (e) {
      case ObservableEvent::VERTEX_REMOVED: {
        Remove_vertex_args* args = static_cast<Remove_vertex_args*>(arg);
        try {
          const auto& vertex = whitelistedVerticesList_->at(args->vertexId_);
          if (not whitelistedVerticesList_->filter_out(vertex)) {
            removeVertex(args->vertexId_, args->removeImpliedHyperarcs_);
          }
        } catch (const std::out_of_range& oor) {
          std::cerr << "IndexOutOfRangeException in"
                  " DirectedSubHypergraph::Update: "
                  "index = " << args->vertexId_ << "; min = 0; max = " <<
                    whitelistedVerticesList_->size() << '\n';
          throw;
        }
        break;
      }
      case ObservableEvent::HYPEREDGE_REMOVED: {
        HyperedgeIdType* hyperarcId = static_cast<HyperedgeIdType*>(arg);
        try {
          const auto& hyperarc = whitelistedHyperarcsList_->at(*hyperarcId);
          if (not whitelistedHyperarcsList_->filter_out(hyperarc)) {
            removeHyperarc(*hyperarcId);
          }
        } catch (const std::out_of_range& oor) {
          std::cerr << "IndexOutOfRangeException in"
                  " DirectedSubHypergraph::Update: "
                  "index = " << *hyperarcId << "; min = 0; max = " <<
                    whitelistedHyperarcsList_->size() << '\n';
          throw;
        }
        break;
      }
      case ObservableEvent::RESET_VERTEX_IDS: {
        updateVertexContainerUponResetVertexIds();
        break;
      }
      case ObservableEvent::RESET_EDGE_IDS: {
        updateHyperarcContainerUponResetHyperarcIds();
        break;
      }
    }
  }
}


/**
 * \brief Remove the given vertex from the filtered out vertices in ancestors
 *
 * \details
 * Remove the given vertex from the filtered out vertices in this sub-hypergraph
 * and all its ancestor sub-hypergraphs (not the root hypergraph).
 *
 * \param vertexPtr Pointer to vertex to be removed from the filtered out
 * vertices
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
    typename VertexProperty, typename HyperarcProperty,
    typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
    HyperarcProperty, HypergraphProperty>::
removeFilteredOutVertexInAncestors(
    const VertexTemplate_t<Hyperarc_t>* vertexPtr) {
  // Add vertex to this sub-hypergraph
  whitelistedVerticesList_->removeFilteredOutItem(
      const_cast<Vertex_t*>(vertexPtr));
  // Add vertex to parent hypergraph if it is not the root
  auto directedSubHG = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
      Hyperarc_t, VertexProperty, HyperarcProperty,
      HypergraphProperty>*>(parent_);
  if (directedSubHG) {  // parent is not the root hypergraph
    directedSubHG->removeFilteredOutVertexInAncestors(vertexPtr);
  }
}


/**
 * \brief Remove the given hyperarc from the filtered out hyperarcs in ancestors
 *
 * \details
 * Remove the given hyperarc from the filtered out hyperarcs in this sub-
 * hypergraph and all its ancestor sub-hypergraphs (not the root hypergraph).
 *
 * \param arcPtr Pointer to hyperarc to be removed from the filtered out
 * hyperarcs
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
    typename VertexProperty, typename HyperarcProperty,
    typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
    HyperarcProperty, HypergraphProperty>::
removeFilteredOutHyperarcInAncestors(const Hyperarc_t* arcPtr) {
  // Add hyperarc to this sub-hypergraph
  whitelistedHyperarcsList_->removeFilteredOutItem(
      const_cast<Hyperarc_t*>(arcPtr));
  // Add hyperarc to parent hypergraph if it is not the root
  auto directedSubHG = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
      Hyperarc_t, VertexProperty, HyperarcProperty,
      HypergraphProperty>*>(parent_);
  if (directedSubHG) {  // parent is not the root hypergraph
    directedSubHG->removeFilteredOutHyperarcInAncestors(arcPtr);
  }
}


/**
 * \brief Add vertex property in ancestors
 *
 * \details
 * Add a vertex property for the given vertex identifier in this sub-
 * hypergraph and all its ancestor sub-hypergraphs (not the root hypergraph).
 *
 * \param vertexId Vertex identifier
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
    typename VertexProperty, typename HyperarcProperty,
    typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
    HyperarcProperty, HypergraphProperty>::
addVertexPropertyInAncestors(const VertexIdType& vertexId) {
  // If at the given position there exists no nullptr then in the hierarchy
  // above there is no nullptr either
  if (vertexId < whitelistedVertexProperties_->size() and
      whitelistedVertexProperties_->operator[](vertexId) != nullptr) {
    return;
  }

  // Add vertex property to parent hypergraph if it is not the root
  auto directedSubHG = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
      Hyperarc_t, VertexProperty, HyperarcProperty,
      HypergraphProperty>*>(parent_);
  if (directedSubHG) {  // parent is not the root hypergraph
    directedSubHG->addVertexPropertyInAncestors(vertexId);
  }
  // Add vertex property to this sub-hypergraph
  if (vertexId >= whitelistedVertexProperties_->size()) {
    // fill gaps with nullptrs and then add the property
    size_t nbGaps = vertexId - whitelistedVertexProperties_->size();
    while (nbGaps-- > 0) {
      whitelistedVertexProperties_->push_back(nullptr);
    }
    whitelistedVertexProperties_->push_back(
        new VertexProperty(*(parent_->getVertexProperties_(vertexId))));
  } else {
    // Set property at given position
    whitelistedVertexProperties_->operator[](vertexId) =
        new VertexProperty(*(parent_->getVertexProperties_(vertexId)));
  }
}


/**
 * \brief Add hyperarc property in ancestors
 *
 * \details
 * Add a hyperarc property for the given hyperarc identifier in this sub-
 * hypergraph and all its ancestor sub-hypergraphs (not the root hypergraph).
 *
 * \param hyperarcId Hyperarc identifier
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
    typename VertexProperty, typename HyperarcProperty,
    typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
    HyperarcProperty, HypergraphProperty>::
addHyperarcPropertyInAncestors(const HyperedgeIdType& hyperarcId) {
  // If at the given position there exists no nullptr then in the hierarchy
  // above there is no nullptr either
  if (hyperarcId < whitelistedHyperarcProperties_->size() and
      whitelistedHyperarcProperties_->operator[](hyperarcId) != nullptr) {
    return;
  }

  // Add hyperarc property to parent hypergraph if it is not the root
  auto directedSubHG = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
      Hyperarc_t, VertexProperty, HyperarcProperty,
      HypergraphProperty>*>(parent_);
  if (directedSubHG) {  // parent is not the root hypergraph
    directedSubHG->addHyperarcPropertyInAncestors(hyperarcId);
  }
  // Add hyperarc property to this sub-hypergraph
  if (hyperarcId >= whitelistedHyperarcProperties_->size()) {
    // fill gaps with nullptrs and then add the property
    size_t nbGaps = hyperarcId - whitelistedHyperarcProperties_->size();
    while (nbGaps-- > 0) {
      whitelistedHyperarcProperties_->push_back(nullptr);
    }
    whitelistedHyperarcProperties_->push_back(
        new HyperarcProperty(*(parent_->getHyperarcProperties_(hyperarcId))));
  } else {
    // Set property at given position
    whitelistedHyperarcProperties_->operator[](hyperarcId) =
        new HyperarcProperty(*(parent_->getHyperarcProperties_(hyperarcId)));
  }
}


/**
 * \brief Get ancestors
 *
 * \details
 * Add the sub-hypergraph's ancestors in the hierarchy to the passed vector.
 * The root hypergraph will be the last entry.
 *
 * \param ancestors Vector to be filled with the ancestors of the
 * sub-hypergraph.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
    typename VertexProperty, typename HyperarcProperty,
    typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
    HyperarcProperty, HypergraphProperty>::
getAncestors(std::vector<HypergraphInterface<VertexTemplate_t, Hyperarc_t,
    VertexProperty, HyperarcProperty, HypergraphProperty>*>* ancestors) const {
  ancestors->push_back(parent_);
  parent_->getAncestors(ancestors);
}


/**
 * \brief Get root hypergraph
 *
 * \details
 * A directed hypergraph can have directed sub-hypergraphs (views). This
 * hierarchy is implemented through the observer and decorator pattern. A
 * subgraph observes its parent graph. This function returns the root
 * hypergraph of the hierarchy.
 *
 * \return A pointer to the directed root hypergraph
 * (DirectedHypergraphInterface).
 *
 * \par Complexity
 * Linear in the number of levels in the hierarchy between this sub-hypergraph
 * and the root hypergraph.
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperarc_t,
        typename VertexProperty,
        typename HyperarcProperty,
        typename HypergraphProperty>
const DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_t, VertexProperty,
HyperarcProperty, HypergraphProperty>* DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_t, VertexProperty, HyperarcProperty, HypergraphProperty>::
getRootHypergraph() const {
  throwExceptionOnNullptrParent();
  return parent_->getRootHypergraph();
}


/* ****************************************************************************
 * ****************************************************************************
 *            Protected methods
 * ****************************************************************************
 * ***************************************************************************/

/**
 * \brief Set pointer to parent hypergraph to nullptr
 *
 * \details
 * If the destructor of the root directed hypergraph was called we invalidate
 * all parents of its child hierarchy by setting the respective pointer to the
 * parent hypergraph to nullptr.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
setParentOfChildsToNull() {
  throwExceptionOnNullptrParent();
  std::unordered_set<Observer*> observers;
  for (auto itEvent = this->observers_.begin();
       itEvent != this->observers_.end(); ++itEvent) {
    for (auto itObserver : itEvent->second) {
      observers.insert(itObserver);
    }
  }
  for (auto& observer : observers) {
    auto subgraph = dynamic_cast<DirectedSubHypergraph<VertexTemplate_t,
            Hyperarc_t, VertexProperty, HyperarcProperty,
            HypergraphProperty>*>(observer);
    subgraph->setParentOfChildsToNull();
  }

  // Detach as observer of parent graph
  parent_->DeleteObserver(this, ObservableEvent::VERTEX_REMOVED);
  parent_->DeleteObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
  parent_->DeleteObserver(this, ObservableEvent::RESET_VERTEX_IDS);
  parent_->DeleteObserver(this, ObservableEvent::RESET_EDGE_IDS);
  // Set parent to nullptr
  parent_ = nullptr;
}


/**
 * \brief Throw an exception if the pointer to the parent is a nullptr
 *
 * \details
 * Throw a exception if one tries to use a member function on a
 * subgraph whose parent was set to nullptr previously.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
throwExceptionOnNullptrParent() const {
  if (parent_ == nullptr) {
    throw std::invalid_argument("Previously another hypergraph was assigned"
                                       " to the original parent of this"
                                       " sub-hypergraph. This sub-hypergraph"
                                       " has no parent hypergraph anymore and"
                                       " is thus in an invalid state.");
  }
}


/**
 * \brief Update the vertex container upon a call to resetVertexIds()
 *
 * \details
 * In response to the call resetVertexIds() in the root hypergraph, the
 * content of the whitelisted vertices must be updated.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
updateVertexContainerUponResetVertexIds() {
  /*
   * Consider the containers of vertices in the root and sub-graphs, where
   * X corresponds to a nullptr and a number to the id of the vertex pointed
   * by the entry:
   *
   * graph:          |X|1|2|3|X|
   * subgraph1:      |X|X|2|3|X|
   * subsubgraph1_1: |X|X|X|3|X|
   * subgraph2:      |X|1|X|X|X|
   *
   * If the method resetVertexIds was called on the graph we expect the
   * following:
   *
   * graph:          |0|1|2|
   * subgraph1:      |X|1|2|
   * subsubgraph1_1: |X|X|2|
   * subgraph2:      |0|X|X|
   *
   * The method updateVertexContainerUponResetVertexIds in
   * DirectedSubHypergraph notify its subgraphs about this change,
   * before changing its own container of vertices.
   */
  throwExceptionOnNullptrParent();

  // Notify subgraphs of this change
  this->NotifyObservers(ObservableEvent::RESET_VERTEX_IDS);

  if (whitelistedVerticesList_->empty()) {
    return;
  }
  std::vector<Vertex_t*> newWhitelistedVertexContainer;
  auto it = whitelistedVerticesList_->data();
  auto end = whitelistedVerticesList_->data() +
          whitelistedVerticesList_->size();
  for (; it != end; ++it) {
    const auto& vertex = *it;
    if (vertex != nullptr) {
      if (whitelistedVerticesList_->filter_out(vertex)) {
        newWhitelistedVertexContainer.push_back(nullptr);
      } else {
        newWhitelistedVertexContainer.push_back(vertex);
      }
    }
  }

  whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
      parent_->getRootVerticesContainerPtr(),
      std::make_unique<WhitelistFilter<Vertex_t*>>(
          newWhitelistedVertexContainer));


  // Root hypergraph
  auto rootHypergraph = getRootHypergraph();
  // Update vertex properties (the parent hypergraph was not changed yet)
  std::vector<VertexProperty*> tmpProperties;
  auto itProp = whitelistedVertexProperties_->data();
  auto endProp = whitelistedVertexProperties_->data()
                 + whitelistedVertexProperties_->size();
  int idx = 0;
  for (; itProp != endProp; ++itProp) {
    const auto& vertexProp = *itProp;
    // Retain property
    if (vertexProp != nullptr) {
      tmpProperties.push_back(getVertexProperties_(idx));
    } else if (rootHypergraph->vertexById(idx) != nullptr) {
      // Property is not visible in this sub-hypergraph, but in the root
      // hypergraph
      tmpProperties.push_back(nullptr);
    }
    ++idx;
  }
  // Create new vertex property container
  whitelistedVertexProperties_ = create_filtered_vector<VertexProperty*>(
      create_filtered_vector<VertexProperty*>(),
      std::make_unique<NullptrFilter<VertexProperty*>>());
  for (const auto& item : tmpProperties) {
    whitelistedVertexProperties_->push_back(item);
  }
}


/**
 * \brief Update the hyperarc container upon a call to resetHyperarcIds()
 *
 * \details
 * In response to the call resetHyperarcIds() in the root hypergraph, the
 * content of the whitelisted hyperarcs must be updated.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
void DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
updateHyperarcContainerUponResetHyperarcIds() {
  /*
   * Consider the containers of hyperarcs in the root and sub-graphs, where
   * X corresponds to a nullptr and a number to the id of the hyperarc pointed
   * by the entry:
   *
   * graph:          |X|1|2|3|X|
   * subgraph1:      |X|X|2|3|X|
   * subsubgraph1_1: |X|X|X|3|X|
   * subgraph2:      |X|1|X|X|X|
   *
   * If the method resetHyperarcIds was called on the graph we expect the
   * following:
   *
   * graph:          |0|1|2|
   * subgraph1:      |X|1|2|
   * subsubgraph1_1: |X|X|2|
   * subgraph2:      |0|X|X|
   *
   * The method updateHyperarcContainerUponResetHyperarcIds in
   * DirectedSubHypergraph notify its subgraphs about this change, before
   * changing its own container of vertices.
   */
  throwExceptionOnNullptrParent();

  // Notify subgraphs of this change
  this->NotifyObservers(ObservableEvent::RESET_EDGE_IDS);

  if (whitelistedHyperarcsList_->empty()) {
    return;
  }
  std::vector<Hyperarc_t*> newWhitelistedHyperarcContainer;
  auto it = whitelistedHyperarcsList_->data();
  auto end = whitelistedHyperarcsList_->data() +
             whitelistedHyperarcsList_->size();
  for (; it != end; ++it) {
    const auto& hyperarc = *it;
    if (hyperarc != nullptr) {
      if (whitelistedHyperarcsList_->filter_out(hyperarc)) {
        newWhitelistedHyperarcContainer.push_back(nullptr);
      } else {
        newWhitelistedHyperarcContainer.push_back(hyperarc);
      }
    }
  }

  whitelistedHyperarcsList_ = create_filtered_vector<Hyperarc_t*>(
      parent_->getRootHyperarcContainerPtr(),
      std::make_unique<WhitelistFilter<Hyperarc_t*>>(
          newWhitelistedHyperarcContainer));


  // Root hypergraph
  auto rootHypergraph = getRootHypergraph();
  // Update hyperarc properties (the parent hypergraph was not changed yet)
  std::vector<HyperarcProperty*> tmpProperties;
  auto itProp = whitelistedHyperarcProperties_->data();
  auto endProp = whitelistedHyperarcProperties_->data()
                 + whitelistedHyperarcProperties_->size();
  int idx = 0;
  for (; itProp != endProp; ++itProp) {
    const auto& hyperarcProp = *itProp;
    // Retain property
    if (hyperarcProp != nullptr) {
      tmpProperties.push_back(getHyperarcProperties_(idx));
    } else if (rootHypergraph->hyperarcById(idx) != nullptr) {
      // Property is not visible in this sub-hypergraph, but in the root
      // hypergraph
      tmpProperties.push_back(nullptr);
    }
    ++idx;
  }
  // Create new hyperarc property container
  whitelistedHyperarcProperties_ = create_filtered_vector<HyperarcProperty*>(
      create_filtered_vector<HyperarcProperty*>(),
      std::make_unique<NullptrFilter<HyperarcProperty*>>());
  for (const auto& item : tmpProperties) {
    whitelistedHyperarcProperties_->push_back(item);
  }
}


/**
 * \brief Return shared_ptr to vertices container of the root hypergraph
 *
 * \return shared_ptr to vertices container of the root hypergraph
 *
 * \par Complexity
 * Linear in the number of levels in the hierarchy between this sub-hypergraph
 * and the root hypergraph.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::VertexContainerPtr
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
getRootVerticesContainerPtr() const {
  throwExceptionOnNullptrParent();
  return parent_->getRootVerticesContainerPtr();
}


/**
 * \brief Return shared_ptr to hyperarc container of the root hypergraph
 *
 * \return shared_ptr to hyperarc container of the root hypergraph
 *
 * \par Complexity
 * Linear in the number of levels in the hierarchy between this sub-hypergraph
 * and the root hypergraph.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperarc_t,
        typename VertexProperty, typename HyperarcProperty,
        typename HypergraphProperty>
typename DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::HyperarcContainerPtr
DirectedSubHypergraph<VertexTemplate_t, Hyperarc_t, VertexProperty,
        HyperarcProperty, HypergraphProperty>::
getRootHyperarcContainerPtr() const {
  throwExceptionOnNullptrParent();
  return parent_->getRootHyperarcContainerPtr();
}
}  // namespace hglib
