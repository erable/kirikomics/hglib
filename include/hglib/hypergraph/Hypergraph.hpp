// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "Hypergraph.h"

#include <iostream>
#include <stdexcept>
#include <algorithm>
#include <memory>

#include "filtered_vector.h"

#include "hglib/utils/HasInsertMemberFunction.h"
#include "hglib/utils/types.h"

namespace hglib {

/**
 * \brief Hypergraph constructor
 *
 * \details
 * Creates a Hypergraph allowing or not to have multiple
 * hyperedges with the same vertices (undirected hypergraph), or the same
 * tail and head vertices (directed hypergraph). Vertices and then hyperedges
 * must be added via the functions addVertex() and addHyperedge() or
 * addHyperarc().
 *
 * \param allowMultiHyperedges Flag to signal if multi-hyperedges are allowed.
 * Defaults to true.
 */
template <template <typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename EdgeProperty,
          typename HypergraphProperty>
Hypergraph<VertexTemplate_t,
           Hyperedge_t,
           VertexProperty,
           EdgeProperty,
           HypergraphProperty>::
Hypergraph(bool allowMultiHyperedges) :
        HypergraphInterface<VertexTemplate_t, Hyperedge_t, VertexProperty,
                EdgeProperty, HypergraphProperty>(),
        vertexMaxId_(-1),
        allowMultiHyperedges_(allowMultiHyperedges) {
  // create vertices_
  vertices_ = create_filtered_vector<Vertex_t*>(
          create_filtered_vector<Vertex_t*>(),
          std::make_unique<NullptrFilter<Vertex_t*>>());

  // create vertexProperties_
  vertexProperties_ = create_filtered_vector<VertexProperty*>(
          create_filtered_vector<VertexProperty*>(),
          std::make_unique<NullptrFilter<VertexProperty*>>());
  // Create HypergraphProperty object
  hypergraphProperty_ = new HypergraphProperty();
}

/**
 * \brief Hypergraph copy constructor
 *
 * \param other To be copied hypergraph.
 */
template <template <typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename EdgeProperty,
          typename HypergraphProperty>
Hypergraph<VertexTemplate_t,
           Hyperedge_t,
           VertexProperty,
           EdgeProperty,
           HypergraphProperty>::
Hypergraph(const Hypergraph& other) :
    vertexMaxId_(-1),
    allowMultiHyperedges_(other.allowMultiHyperedges_) {
  try {
    // create vertices_
    vertices_ = create_filtered_vector<Vertex_t*>(
            create_filtered_vector<Vertex_t*>(),
            std::make_unique<NullptrFilter<Vertex_t*>>());
    // create vertexProperties_
    vertexProperties_ = create_filtered_vector<VertexProperty*>(
            create_filtered_vector<VertexProperty*>(),
            std::make_unique<NullptrFilter<VertexProperty*>>());

    // copy vertices and associated vertex properties
    for (std::size_t i = 0; i < other.vertices_->size(); ++i) {
      Vertex_t* vertex = other.vertices_->operator[](i);
      ++vertexMaxId_;
      // vertex was deleted in directedHypergraph
      if (vertex == nullptr) {
        vertices_->push_back(nullptr);
        vertexProperties_->push_back(nullptr);
      } else {
        // create new vertex
        Vertex_t* vertexCopy = new Vertex_t(*vertex);
        vertices_->push_back(vertexCopy);
        // Copy vertex property
        VertexProperty* vertexPropertyCopy = new VertexProperty(
                *(other.vertexProperties_->operator[](i)));
        vertexProperties_->push_back(vertexPropertyCopy);
      }
    }
    // copy graph properties
    hypergraphProperty_ =
            new HypergraphProperty(*(other.hypergraphProperty_));
  } catch (std::bad_alloc &e) {
    for (auto& vertex : *(vertices_.get())) {
      delete vertex;
    }
    for (auto& vertexProperty : *(vertexProperties_.get())) {
      delete vertexProperty;
    }
    delete hypergraphProperty_;
    throw;
  }
}

/**
 * \brief Conversion constructor
 *
 * \details
 * Allows to convert a graph to another graph with the same Vertex and Edge
 * types but another set of (Vertex-, Edge- and Graph-) Properties
 *
 * \param other Hypergraph to be copied
 */
template <template <typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename EdgeProperty,
          typename GraphProperty>
template <typename OtherVP, typename OtherEP, typename OtherGP>
Hypergraph<VertexTemplate_t,
           Hyperedge_t,
           VertexProperty,
           EdgeProperty,
           GraphProperty>::
Hypergraph(const Hypergraph<VertexTemplate_t,
                            Hyperedge_t,
                            OtherVP,
                            OtherEP,
                            OtherGP>& other) :
    vertexMaxId_(-1),
    allowMultiHyperedges_(other.allowMultiHyperedges_) {
  try {
    // create vertices_
    vertices_ = create_filtered_vector<Vertex_t*>(
        create_filtered_vector<Vertex_t*>(),
        std::make_unique<NullptrFilter<Vertex_t*>>());
    // create vertexProperties_
    vertexProperties_ = create_filtered_vector<VertexProperty*>(
        create_filtered_vector<VertexProperty*>(),
        std::make_unique<NullptrFilter<VertexProperty*>>());

    // copy vertices and initialize associated vertex properties
    for (std::size_t i = 0; i < other.vertices_->size(); ++i) {
      Vertex_t* vertex = other.vertices_->operator[](i);
      ++vertexMaxId_;
      // if vertex was deleted in directedHypergraph
      if (vertex == nullptr) {
        vertices_->push_back(nullptr);
        vertexProperties_->push_back(nullptr);
      } else {
        // Copy vertex
        vertices_->push_back(new Vertex_t(*vertex));
        // Init vertex property
        vertexProperties_->push_back(new VertexProperty());
      }
    }
    // Init graph properties
    hypergraphProperty_ = new GraphProperty();
  } catch (std::bad_alloc &e) {
    for (auto& vertex : *(vertices_.get())) {
      delete vertex;
    }
    for (auto& vertexProperty : *(vertexProperties_.get())) {
      delete vertexProperty;
    }
    delete hypergraphProperty_;
    throw;
  }
}


/**
 * \brief Hypergraph destructor
 *
 * \details
 * Destructs the hypergraph. The destructors of the elements are called.
 *
 * \par Complexity
 * Linear in the number of vertices.
 */
template <template <typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename EdgeProperty,
          typename HypergraphProperty>
Hypergraph<VertexTemplate_t,
           Hyperedge_t,
           VertexProperty,
           EdgeProperty,
           HypergraphProperty>::
~Hypergraph() {
  for (auto& vertex : *(vertices_.get())) {
    delete vertex;
  }
  for (auto& vertexProperty : *(vertexProperties_.get())) {
    delete vertexProperty;
  }
  delete hypergraphProperty_;
}


/// Assignment operator
/**
 * \brief Copy Assignment operator
 *
 * \details
 * Replaces the contents (vertices, properties) of the hypergraph.
 *
 * \param source Another Hypergraph to use as data source.
 * \return
 * \code *this \endcode
 *
 * \par Complexity
 * Linear in the number of vertices of *this and source.
 */
template <template <typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename EdgeProperty,
          typename HypergraphProperty>
Hypergraph<VertexTemplate_t,
           Hyperedge_t,
           VertexProperty,
           EdgeProperty,
           HypergraphProperty>&
Hypergraph<VertexTemplate_t,
           Hyperedge_t,
           VertexProperty,
           EdgeProperty,
           HypergraphProperty>::
operator=(const Hypergraph& source) {
  // check for self assignment
  if (this != &source) {
    // deallocate vertices_
    for (auto &vertex : *(vertices_.get())) {
      delete vertex;
    }
    vertices_->clear();

    // deallocate vertexProperties_
    for (auto& vProp : *(vertexProperties_.get())) {
      delete vProp;
    }
    vertexProperties_->clear();

    // deallocate hypergraph property
    delete hypergraphProperty_;

    // re-init the vertex max Id
    vertexMaxId_ = -1;

    // copy of flag
    allowMultiHyperedges_ = source.allowMultiHyperedges_;

    // copy vertices and associated vertex properties
    try {
      for (std::size_t i = 0; i < source.vertices_->size(); ++i) {
        Vertex_t* vertex = source.vertices_->operator[](i);
        ++vertexMaxId_;
        // vertex was deleted in directedHypergraph
        if (vertex == nullptr) {
          vertices_->push_back(nullptr);
          vertexProperties_->push_back(nullptr);
        } else {
          // Get arguments to build a vertex
          /*auto args = vertex->getArgsToBuildDirectedVertex();
          // create new vertex
          callAddVertex(args);*/
          Vertex_t* vertexCopy = new Vertex_t(*vertex);
          vertices_->push_back(vertexCopy);
          // Copy vertex property
          VertexProperty* vertexPropertyCopy = new VertexProperty(
                  *source.vertexProperties_->operator[](i));
          vertexProperties_->push_back(vertexPropertyCopy);
        }
      }

      // copy graph properties
      hypergraphProperty_ = new HypergraphProperty(
              *(source.hypergraphProperty_));
    } catch (std::bad_alloc &e) {
      for (auto& vertex : *(vertices_.get())) {
        delete vertex;
      }
      for (auto& vertexProperty : *(vertexProperties_.get())) {
        delete vertexProperty;
      }
      delete hypergraphProperty_;
      throw;
    }
  }
  return *this;
}


/* ****************************************************************************
 * ****************************************************************************
 *                          Public methods
 * ****************************************************************************
 *****************************************************************************/

/**
 * \brief Size of the vertex container
 *
 * \details
 * Pointers to vertex objects are stored in a customized vector. If a
 * vertex is removed from the hypergraph, then the corresponding entry in
 * this vector is set to a nullptr. The size of the container remains unchanged.
 * Thus, the return value of this function may differ from nbVertices().
 *
 * \return Size of the vertex container.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
size_t Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
vertexContainerSize() const {
  return vertices_->size();
}

/**
 * \brief Number of vertices in the hypergraph
 *
 * \return Number of vertices in the hypergraph.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
size_t Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
nbVertices() const {
  size_t nb = 0;
  for (auto& vertex : *(vertices_.get())) {
    if (vertex != nullptr) {
      ++nb;
    }
  }
  return nb;
}


/**
 * \brief Search vertex list for a vertex with the provided name
 *
 * \details
 * Search the vertex list for a vertex with the provided name.
 *
 * \param vertexName Name of the vertex
 *
 * \return Pointer to const vertex if a vertex with the specified name exists
 * in the hypergraph, otherwise a nullptr.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::Vertex_t*
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
vertexByName(const std::string& vertexName) const {
  for (const auto& vertex : *(vertices_.get())) {
    if (vertex != nullptr and vertex->name_.compare(vertexName) == 0) {
      return vertex;
    }
  }
  return nullptr;
}


/**
 * \brief Search vertex list for a vertex with the provided id
 *
 * \details
 * Search vertex list for a vertex with the provided Id.
 *
 * \param vertexId Identifier of the vertex
 *
 * \return Pointer to const vertex if a vertex with the specified Id exists
 * in the hypergraph, otherwise a nullptr.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() )
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::Vertex_t*
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
vertexById(const VertexIdType & vertexId) const {
  try {
    return vertices_->at(vertexId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in Hypergraph::vertexById: "
              "index = " << vertexId << "; min = 0; max = " <<
              vertices_->size() << '\n';
    throw;
  }
}

/**
 * \brief Add a vertex
 *
 * \details
 * Add a vertex with a default name to the hypergraph.
 *
 * \param attributes Vertex type specific attributes to create a vertex.
 *
 * \return Pointer to const vertex on success, throw an exception otherwise.
 *
 * \throw invalid_argument If a vertex with the same name already exists in the
 * hypergraph.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::Vertex_t*
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
addVertex(const typename Vertex_t::SpecificAttributes& attributes) {
  // As we do not know if the name was passed as an argument, we first create
  // the vertex and check then if there is already vertex of the same name. If
  // yes an exception is thrown, otherwise the vertex is added to the
  // hpergraph.
  auto vertex = new Vertex_t(++vertexMaxId_, attributes);
  // Check if a vertex with the same name exists
  for (auto& v : *(vertices_.get())) {
    if (v != nullptr and v->name().compare(vertex->name()) == 0) {
      --vertexMaxId_;
      auto vname = vertex->name();
      delete vertex;
      throw std::invalid_argument("Vertex " + vname +
                                  " already exists in this hypergraph.");
    }
  }
  vertices_->push_back(vertex);

  // build entry in vertexProperties
  auto vertexProperty = new VertexProperty();
  vertexProperties_->push_back(vertexProperty);
  return vertex;
}


/**
 * \brief Add a vertex
 *
 * \details
 * Add a vertex with the given name to the hypergraph.
 *
 * \param name Vertex name.
 * \param attributes Vertex type specific attributes to create a vertex.
 *
 * \return Pointer to const vertex on success, throw an exception otherwise.
 *
 * \throw invalid_argument If a vertex with the same name already exists in the
 * hypergraph.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::Vertex_t*
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
addVertex(const std::string& name,
          const typename Vertex_t::SpecificAttributes& attributes) {
  // Check if a vertex with the same name exists
  for (auto& v : *(vertices_.get())) {
    if (v->name().compare(name) == 0) {
      throw std::invalid_argument("Vertex " + name +
                                  " already exists in this hypergraph.");
    }
  }
  auto vertex = new Vertex_t(++vertexMaxId_, name, attributes);
  vertices_->push_back(vertex);

  // build entry in vertexProperties
  auto vertexProperty = new VertexProperty();
  vertexProperties_->push_back(vertexProperty);
  return vertex;
}


/**
 * \brief Establish consecutive vertex ids
 *
 *  \details
 * After deleting vertices from the hypergraph, the vertex ids
 * are not consecutive anymore. This function reassigns
 * consecutive ids to the vertices.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
void Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
resetVertexIds() {
  // Notify subgraphs of this change
  this->NotifyObservers(ObservableEvent::RESET_VERTEX_IDS);
  // remove nullptrs from underlying vector: std::remove puts all nullptrs at
  // the end of the vector
  auto posFirstNullptr = std::remove(vertices_->data(),
                                     vertices_->data() + vertices_->size(),
                                     static_cast<Vertex_t*>(NULL));
  // Compute nb of nullptr
  size_t nbNullptr = (vertices_->data() + vertices_->size()) - posFirstNullptr;
  // Resize
  vertices_->resize(vertices_->size() - nbNullptr);

  // assign new Ids
  hglib::VertexIdType id(-1);
  for (auto it = vertices_->begin(); it != vertices_->end(); ++it) {
    (*it)->setId(++id);  // assign new Id
  }

  // set vertexMaxId_ to the highest assigned id
  vertexMaxId_ = id;

  // remove nullptr entries in vertexProperties_
  removeNullEntriesFromVertexProperties();
}


/**
 * \brief Get arguments to create an existing vertex
 *
 * \details
 * This function returns an ArgumentsToCreateVertex object that can
 * be used to insert a vertex in the originating or in a different hypergraph.
 * An ArgumentsToCreateVertex object consists of two parts,
 * (i) the vertex name, (ii) specific arguments needed to create a vertex of
 * type VertexTemplate_t<Hyperedge_t>.
 *
 * The returned object can be transformed to add a different vertex
 * to the hypergraph. This function can be useful in generic methods that
 * accepts any kind of hypergraph and that transform them.
 *
 * \par Example
 * \parblock
 * \code
 *
 * // Get arguments to create vertex with Id 1
 * auto args = g.argumentsToCreateVertex(1);
 * // Add vertex to another hypergraph g2.
 * const auto& vertex2 = g2.addVertex(args.name(), args.specificAttributes());
 * \endcode
 * \endparblock
 *
 * \param vertexId Identifier of the vertex.
 *
 * \return ArgumentsToCreateVertex by value.
 *
 * \throw out_of_range If the vertex Id is not in the range
 * [0, vertexContainerSize() )
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * The complexity depends on the function argumentsToCreateVertex() of
 * VertexTemplate_t<Hyperedge_t>.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
ArgumentsToCreateVertex<typename Hypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::Vertex_t>
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
argumentsToCreateVertex(const hglib::VertexIdType& vertexId) const {
  try {
    const auto &vertex = vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called Hypergraph::"
                                       "argumentsToCreateVertex "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->argumentsToCreateVertex();
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Get properties of the vertex with the provided id
 *
 * \details
 * This function returns a pointer to a const (read only) vertex property
 * associated to the vertex with the specified vertex Id.
 *
 * \param vertexId Identifier of the vertex.
 *
 * \return Pointer to const vertex property type. Throws an exception if the
 * vertex is not part of the hypergraph.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there exists no vertex with the given Id in the
 * directed sub-hypergraph.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const VertexProperty* Hypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::
getVertexProperties(const VertexIdType& vertexId) const {
  try {
    return vertexProperties_->at(vertexId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in Hypergraph::"
            "getVertexProperties: "
            "index = " << vertexId << "; min = 0; max = " <<
              vertexProperties_->size() << '\n';
    throw;
  }
}

/**
 * \brief Get properties of the vertex with the provided id
 *
 * \details
 * This function returns a pointer to a vertex property associated to the
 * vertex with the specified vertex Id. This is a non-const version of
 * getVertexProperties().
 *
 * \param vertexId Identifier of the vertex.
 *
 * \return Pointer to vertex property type. Throws an exception if the
 * vertex is not part of the hypergraph.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there exists no vertex with the given Id in the
 * directed sub-hypergraph.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
VertexProperty* Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
getVertexProperties_(const VertexIdType& vertexId) const {
  try {
    return vertexProperties_->at(vertexId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in Hypergraph::"
            "getVertexProperties_: index = " << vertexId << "; min = 0; max = "
              << vertexProperties_->size() << '\n';
    throw;
  }
}


/**
 * \brief Get properties of the hypergraph
 *
 * \details
 * This function returns a pointer to a const (read only) HypergraphProperty
 * type associated with this hypergraph.
 *
 * \return Pointer to const HypergraphProperty type.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const HypergraphProperty* Hypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::
getHypergraphProperties() const {
  return hypergraphProperty_;
}


/**
 * \brief Get properties of the hypergraph
 *
 * \details
 * This function returns a pointer to a HypergraphProperty type associated with
 * this hypergraph. This is a non-const version of getHypergraphProperties().
 *
 * \return Pointer to HypergraphProperty type.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
HypergraphProperty* Hypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::
getHypergraphProperties_() const {
  return const_cast<HypergraphProperty*>(getHypergraphProperties());
}


/**
 * \brief Const vertex iterator pointing to the begin of the vertices
 *
 * \return Const vertex iterator pointing to begin of the vertices.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the vertex container.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::vertex_iterator
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
verticesBegin() const {
  return vertices_->cbegin();
}

/**
 * \brief Const vertex iterator pointing to the end of the vertices
 *
 * \return Const vertex iterator pointing to end of the vertices.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::vertex_iterator
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
verticesEnd() const {
  return vertices_->cend();
}


/**
 * \brief Begin/End iterator pair of the vertices of the hypergraph
 *
 * \details
 * Pair of vertex iterators pointing to the begin and the end of the
 * vertices of the hypergraph.
 *
 * \return Pair of vertex iterators pointing to the begin and the end of the
 * vertices of the hypergraph.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the vertex container.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
std::pair<typename Hypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::vertex_iterator,
        typename Hypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, EdgeProperty,
                HypergraphProperty>::vertex_iterator>
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
verticesBeginEnd() const {
  return std::make_pair(vertices_->cbegin(), vertices_->cend());
}


/**
 * \brief Return a reference to the container of vertices
 *
 * \details
 * Can be used in a range based for loop as follows:
 * \code
 * for (const auto& vertex : g.vertices()) {
 *   std::cout << "Id of the vertex: " << vertex->id() << '\n';
 * }
 * \endcode
 *
 * \return Reference to vertex container
 *
 * \par Complexity
 * Constant (return the reference). However, the complexity to iterate over all
 * vertices of a hypergraph using a range based for loop or a
 * classic for loop using verticesBegin() and verticesEnd() is the same.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
const typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::VertexContainer&
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
vertices() const {
  return *(vertices_.get());
}


/**
 * \brief Get ancestors
 *
 * \details
 * Add the hypergraph's ancestors in the hierarchy to the passed vector. As a
 * (Undirected-/Directed-) Hypergraph is always the root of the hierarchy
 * nothing is added to the vector.
 *
 * \param ancestors Vector to be filled with the ancestors of the hypergraph
 * (none).
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
    typename VertexProperty, typename EdgeProperty,
    typename HypergraphProperty>
void Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
    HypergraphProperty>::
getAncestors(std::vector<HypergraphInterface<VertexTemplate_t, Hyperedge_t,
    VertexProperty, EdgeProperty, HypergraphProperty>*>*) const {
}


/* ****************************************************************************
 * ****************************************************************************
 *                          Protected methods
 * ****************************************************************************
 * ***************************************************************************/

/**
 * \brief Search vertex list for a vertex with the provided name
 *
 * \details
 * Search the vertex list for a vertex with the provided name. Non-const
 * version of vertexByName().
 *
 * \param vertexName Name of the vertex
 *
 * \return Pointer to vertex if a vertex with the specified name exists
 * in the hypergraph, otherwise a nullptr.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::Vertex_t*
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
vertexByName_(const std::string& vertexName) const {
  return const_cast<Vertex_t*>(vertexByName(vertexName));
}


/**
 * \brief Remove nullptr entries from vertex properties container
 *
 * \details
 * Remove nullptr entries from vertex properties container. This function
 * changes the size of the vertex properties container.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
void Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
removeNullEntriesFromVertexProperties() {
  // remove nullptrs from underlying vector: std::remove puts all nullptrs at
  // the end of the vector
  auto posFirstNullptr = std::remove(vertexProperties_->data(),
                                     vertexProperties_->data() +
                                             vertexProperties_->size(),
                                     static_cast<VertexProperty*>(NULL));
  // Compute nb of nullptr
  size_t nbNullptr = (vertexProperties_->data() + vertexProperties_->size()) -
          posFirstNullptr;
  // Resize
  vertexProperties_->resize(vertexProperties_->size() - nbNullptr);
}


/**
 * \brief Get Ids of vertices for given vertex names
 *
 * \param vertexNames Vector of vertex names.
 *
 * \return Vector of Ids of vertices.
 *
 * \throw invalid_argument If no vertex with a given name is found in the
 * hypergraph.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
std::vector<hglib::VertexIdType> Hypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, EdgeProperty, HypergraphProperty>::
getVertexIdsFromNames(const std::vector<std::string>& vertexNames) const {
  std::vector<hglib::VertexIdType> ids;
  for (const auto& vertexName : vertexNames) {
    const Vertex_t* vertex = vertexByName(vertexName);
    if (vertex == nullptr) {
      throw std::invalid_argument("Vertex " + vertexName +
                                  " is not declared in this hypergraph.");
    } else {
      ids.push_back(vertex->id());
    }
  }
  return ids;
}


/**
 * \brief Set vertex and vertex property entry at given position to nullptr
 *
 * \param idx Identifier of the vertex.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
void Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::
setVertexAndVertexPropertyEntryToNullptr(const VertexIdType& idx) {
  // assign NULL in vertices_ at the index that corresponds
  // to the id of the vertex
  delete vertices_->operator[](idx);
  vertices_->operator[](idx) = nullptr;

  // assign NULL in vertexProperties_ at the index that corresponds
  // to the id of the vertex
  delete vertexProperties_->operator[](idx);
  vertexProperties_->operator[](idx) = nullptr;
}


/**
 * \brief Return shared_ptr to vertices container
 *
 * \details
 * A DirectedSubHypergraph and a UndirectedSubHypergraph, a view of a
 * hypergraph, is implemented as a combination of the observer and the
 * decorator pattern. The vertex container of the hypergraph is decorated with
 * a particular filter in the DirectedSubHypergraph or UndirectedSubHypergraph.
 * This function provides access to the vertex container.
 *
 * \return Shared_ptr to vertex container.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename EdgeProperty,
        typename HypergraphProperty>
typename Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        EdgeProperty, HypergraphProperty>::VertexContainerPtr
Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty, EdgeProperty,
        HypergraphProperty>::
getRootVerticesContainerPtr() const {
  return vertices_;
}
}  // namespace hglib
