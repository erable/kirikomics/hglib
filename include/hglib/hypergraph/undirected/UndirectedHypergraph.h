// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_UNDIRECTEDHYPERGRAPH_H
#define HGLIB_UNDIRECTEDHYPERGRAPH_H

#include <memory>
#include <string>
#include <type_traits>

#include "filtered_vector.h"

#include "UndirectedHypergraphInterface.h"
#include "hglib/hyperedge/undirected/UndirectedHyperedgeBase.h"
#include "hglib/hyperedge/undirected/UndirectedHyperedge.h"
#include "hglib/hyperedge/undirected/ArgumentsToCreateUndirectedHyperedge.h"
#include "hglib/utils/types.h"
#include "hglib/vertex/undirected/UndirectedVertex.h"

namespace hglib {

/**
 * \class UndirectedHypergraph
 *
 * \brief This is a UndirectedHypergraph class template.
 *
 * \details
 * This class template allows to model <em>undirected hypergraphs</em>. There
 * are template parameters for the vertex and hyperedge type. Additionally one
 * can specify a property type for the vertices, hyperedges, and the hypergraph
 * itself. All template parameters have their default values (see below),
 * \em e.g. the default vertex type is the class template UndirectedVertex.
 * You can use your own classes as template parameters but there are some
 * restrictions to assure some functionalities:
 * - The vertex type class must be a class template that is derived from the
 *   UndirectedVertex<T> class, where \em T corresponds to the hyperedge type,
 *   \em e.g. UndirectedHyperedge.
 * - The hyperedge type class must be derived from UndirectedHyperedgeBase<T>,
 *   where \em T corresponds to the undirected vertex type, \em e.g.
 *   UndirectedVertex\<UndirectedHyperedge\>.
 *
 * \parblock
 * This class template contains functions to edit an undirected hypergraph and
 * to iterate over:
 * - the vertices and hyperedges of the undirected hypergraph,
 * - the hyperedges that contain a vertex,
 * - the vertices of a hyperedge.
 *
 * Each vertex and hyperedge has a numerical identifier allowing to have a
 * constant access to it.
 * \endparblock
 *
 * \par Properties
 * \parblock
 * The vertex/hyperedge/hypergraph properties provide a way to put additional
 * attributes on these elements, \em e.g. to assign a colour to a vertex, or
 * a weight to a hyperedge.
 * \code
 * enum Colour {
 *    red, blue, green
 *  };
 * struct VertexProperty {
 *    Colour colour = Colour::green;
 * };
 * struct EdgeProperty {
 *    double weight = 0.0;
 * };
 *
 * hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
 *        VertexProperty, EdgeProperty> g;
 * \endcode
 * \endparblock
 *
 * \tparam VertexTemplate_t Vertex type. Defaults to UndirectedVertex.
 * \tparam Hyperedge_t Hyperedge type. Defaults to UndirectedHyperedge.
 * \tparam VertexProperty Vertex property type. Defaults to emptyProperty.
 * \tparam HyperedgeProperty Hyperedge property type. Defaults to emptyProperty.
 * \tparam HypergraphProperty Undirected hypergraph property type. Defaults to
 * emptyProperty.
 */
template <template <typename> typename VertexTemplate_t = UndirectedVertex,
        typename Hyperedge_t = UndirectedHyperedge,
        typename VertexProperty = emptyProperty,
        typename HyperedgeProperty = emptyProperty,
        typename HypergraphProperty = emptyProperty>
class UndirectedHypergraph : public UndirectedHypergraphInterface<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>,
                                  public Hypergraph<VertexTemplate_t,
                                          Hyperedge_t, VertexProperty,
                                          HyperedgeProperty,
                                          HypergraphProperty> {
  // Check that the vertex and hyperedge type is correct
  // An undirected vertex is needed.
  static_assert(std::is_base_of<UndirectedVertex<Hyperedge_t>,
      VertexTemplate_t<Hyperedge_t>>::value, "A vertex of an undirected "
                    "hypergraph must be derived from UndirectedVertex<T>, "
      "where T is derived from HyperedgeBase, e.g. "
      "UndirectedVertex<UndirectedHyperedge> or "
      "UndirectedVertex<NamedUndirectedHyperedge>");
  // An undirected hyperedge is needed
  static_assert(
      std::is_base_of<UndirectedHyperedgeBase<VertexTemplate_t<Hyperedge_t>>,
          Hyperedge_t>::value, "A hyperedge of a hypergraph must be derived "
          "from HyperedgeBase, e.g. use as second template parameter "
          "UndirectedHyperedge or NamedUndirectedHyperedge");

  // Declare any instantiation of this template class as friend
  template <template <typename> class, typename, typename, typename, typename>
  friend class UndirectedHypergraph;

 public:
  /// Alias for the directed vertex type
  using Vertex_t = typename UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::Vertex_t;
  /// Alias for Hyperedge type
  using Hyperedge_type = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::Hyperedge_type;
  /// Alias for Hyperedge property type
  using HyperedgeProperty_t = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgeProperty_t;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexContainer;
  /// Alias for conditional range of a container of Hyperedge_t ptr
  using HyperedgeContainer = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgeContainer;
  /// Alias for iterator over a container of vertices
  using vertex_iterator = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::vertex_iterator;
  /// Alias for iterator over a container of hyperedges
  using hyperedge_iterator = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::hyperedge_iterator;
  /// \brief Alias for the data container that stores the names of vertices
  /// of a hyperedge
  using VertexNames = typename UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexNames;
  /// \brief Alias for the data container that stores the ids of vertices
  /// of a hyperedge
  using VertexIds = typename UndirectedHypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexIds;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexPropertyContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<Hyperedge_t*>
  using HyperedgeContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgeContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<HyperedgeProperty*>
  using HyperedgePropertyContainerPtr = typename UndirectedHypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::HyperedgePropertyContainerPtr;

  /* **************************************************************************
   * **************************************************************************
   *                    Constructor/Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// UndirectedHypergraph constructor
  explicit UndirectedHypergraph(bool allowMultiHyperedges = true);
  /// UndirectedHypergraph copy constructor
  explicit UndirectedHypergraph(
          const UndirectedHypergraph& undirectedHypergraph);
  /// Conversion constructor
  template <typename OtherVP, typename OtherEP, typename OtherGP>
  explicit UndirectedHypergraph(
      const UndirectedHypergraph<VertexTemplate_t,
                                 Hyperedge_t,
                                 OtherVP,
                                 OtherEP,
                                 OtherGP>& other);
  /// Copy assignment operator
  UndirectedHypergraph& operator=(
          const UndirectedHypergraph& source);
  /// UndirectedHypergraph destructor
  virtual ~UndirectedHypergraph();

  /* **************************************************************************
   * **************************************************************************
   *                    General
   * **************************************************************************
   * *************************************************************************/
  /// Check if multi-hyperedges are allowed in this hypergraph
  bool allowMultiHyperedges() const override;

  /* **************************************************************************
   * **************************************************************************
   *                        Hyperedge access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Number of hyperedges in the hypergraph
  size_t nbHyperedges() const override;
  /// Size of the hyperedge container
  size_t hyperedgeContainerSize() const override;

  /// Add a hyperedge
  const Hyperedge_t* addHyperedge(
          decltype(hglib::NAME),
          const VertexNames& vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes = {})
    override;

  /// Add a hyperedge
  const Hyperedge_t* addHyperedge(
          const VertexIds& vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes = {})
  override;

  /// Remove a hyperedge with the given id from the hypergraph
  void removeHyperedge(const HyperedgeIdType& hyperedgeId) override;

  /// Search hyperedge list for an hyperedge with the provided id
  const Hyperedge_t* hyperedgeById(
          const HyperedgeIdType & hyperedgeId) const override;

  /// Const hyperedge iterator pointing to the begin of the hyperedges
  hyperedge_iterator hyperedgesBegin() const override;
  /// Const hyperedge iterator pointing to the end of the hyperedges
  hyperedge_iterator hyperedgesEnd() const override;
  /// Begin/End iterator pair of the hyperedges of the hypergraph
  std::pair<hyperedge_iterator,
          hyperedge_iterator> hyperedgesBeginEnd() const override;
  /// Return a reference to the container of hyperedges
  const HyperedgeContainer& hyperedges() const override;

  // Vertices of a given hyperedge
  /// Return number of vertices in the hyperedge with the given id
  size_t nbImpliedVertices(const HyperedgeIdType& hyperedgeId) const override;
  /// Check if the hyperedge with the given id has vertices
  bool hasVertices(const HyperedgeIdType& hyperedgeId) const override;
  /// Const vertex iterator pointing to the begin of the implied vertices
  vertex_iterator impliedVerticesBegin(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Const vertex iterator pointing to the end of the implied vertices
  vertex_iterator impliedVerticesEnd(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Pair of vertex iterators pointing to the begin/end of the implied vertices
  std::pair<vertex_iterator, vertex_iterator> impliedVerticesBeginEnd(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Get shared_ptr of the container of implied vertices
  std::shared_ptr<const GraphElementContainer<Vertex_t*>> impliedVertices(
          const HyperedgeIdType& hyperedgeId) const override;

  /// Establish consecutive hyperedge ids
  void resetHyperedgeIds();
  /// Establish consecutive vertex and hyperedge ids
  void resetIds();

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex access
   * **************************************************************************
   * *************************************************************************/
 public:
  // Hyperedges that contain a given vertex
  /// Number of hyperedges that contain the vertex with the given Id
  size_t nbHyperedges(const VertexIdType& vertexId) const override;
  /// Check if the vertex with the given Id is part of a hyperedge
  bool isContainedInAnyHyperedge(const VertexIdType& vertexId) const override;
  /// Iterator pointing to the begin of the hyperedges that contain the vertex
  hyperedge_iterator hyperedgesBegin(
          const VertexIdType& vertexId) const override;
  /// Iterator pointing to the end of the hyperedges that contain the vertex
  hyperedge_iterator hyperedgesEnd(
          const VertexIdType& vertexId) const override;
  /// Pair of iterators pointing to the begin/end of the vertex's hyperedges
  std::pair<hyperedge_iterator, hyperedge_iterator> hyperedgesBeginEnd(
          const VertexIdType& vertexId) const override;
  /// Get shared_ptr of container of hyperedges that contain the vertex
  std::shared_ptr<const GraphElementContainer<Hyperedge_t*>> hyperedges(
          const VertexIdType& vertexId) const override;

  /// Remove a vertex with the given name from the hypergraph
  void removeVertex(const std::string& vertexName,
                            bool removeImpliedHyperedges = true) override;
  /// Remove a vertex with the given Id from the hypergraph
  void removeVertex(const VertexIdType& vertexId,
                            bool removeImpliedHyperedges = true) override;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex/Hyperedge dependent access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Check if the given vertex is part of the given hyperedge
  bool isVertexOfHyperedge(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperedgeId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                        HyperedgeProperty access
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get properties of the hyperedge with the provided id
  const HyperedgeProperty* getHyperedgeProperties(
          const HyperedgeIdType& hyperedgeId) const override;
  /// Get properties of the hyperedge with the provided id
  HyperedgeProperty* getHyperedgeProperties_(
          const HyperedgeIdType& hyperedgeId) const override;

  /* **************************************************************************
   * **************************************************************************
   *                  Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Get root hypergraph
  const UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
          VertexProperty, HyperedgeProperty,
          HypergraphProperty>* getRootHypergraph() const override;

  /* **************************************************************************
   * **************************************************************************
   *                        Protected methods
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Construct a hyperedge and a HyperedgeProperty object
  const Hyperedge_t* buildHyperedgeAndItsDependencies(
          std::unique_ptr<GraphElementContainer<Vertex_t*>> vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes);
  /// Remove vertex dependencies
  void removeVertexDependencies(const Vertex_t& vertex);
  /// Remove hyperedge dependencies
  void removeHyperedgeDependencies(const Hyperedge_t& hyperedge);
  /// Remove nullptr entries from hyperedge properties container
  void removeNullEntriesFromHyperedgeProperties();
  /// Return shared_ptr to hyperedge container
  HyperedgeContainerPtr getRootHyperedgeContainerPtr() const override;

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Hyperedges of the graph
  HyperedgeContainerPtr hyperedges_;
  /// Property per hyperedge
  HyperedgePropertyContainerPtr hyperedgeProperties_;
  /// Hyperarc max Id
  HyperedgeIdType hyperedgeMaxId_;
};


/**
 * \brief Operator<<
 *
 * \details
 * This function prints first isolated vertices, followed by a list of
 * hyperedges.
 *
 * \par Example output
 * \code
   # Isolated vertices
   "B", "Bar"
   # Hyperarcs
   {"A", "C", "Foo", "Y"}
   {"6", "", "X"}
 * \endcode
 *
 *
 * \param out std::ostream
 * \param hypergraph To be printed hypergraph.
 *
 * \return std::ostream
 *
 * \relates UndirectedHypergraph
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::ostream& operator<< (std::ostream& out,
                          const UndirectedHypergraph<VertexTemplate_t,
                                  Hyperedge_t, VertexProperty,
                                  HyperedgeProperty,
                                  HypergraphProperty>& hypergraph) {
  // print isolated vertices
  out << "# Isolated vertices\n";
  std::string padding = "";
  for (const auto& vertex : hypergraph.vertices()) {
    if (not hypergraph.isContainedInAnyHyperedge(vertex->id())) {
      out << padding << *vertex;
      padding = ", ";
    }
  }
  if (padding.compare(", ") == 0) {
    out << '\n';
  }

  // print hyperedges
  out << "# Hyperedges\n";
  for (const auto& hyperedge : hypergraph.hyperedges()) {
    out << *hyperedge << std::endl;
  }

  return out;
}
}  // namespace hglib
#include "UndirectedHypergraph.hpp"
#endif  // HGLIB_UNDIRECTEDHYPERGRAPH_H
