// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "UndirectedSubHypergraph.h"

#include <unordered_set>
#include <vector>

#include "filtered_vector.h"

namespace hglib {

/**
 * \brief UndirectedSubHypergraph constructor
 *
 * \details
 * The constructor allows to create an instance of an undirected sub-hypergraph,
 * that is a view of a parent undirected hypergraph \em p considering a subset
 * of \em p's vertices and hyperedges.
 *
 * \param parent Parent undirected hypergraph of type
 * UndirectedHypergraphInterface\<Template_args\>. This can be a pointer to an
 * UndirectedHypergraph\<Template_args\> or
 * UndirectedSubHypergraph\<Template_args\>.
 * \param whitelistedVertexIds unordered_set of vertices that are part of the
 * view.
 * \param whitelistedHyperedgeIds unordered_set of hyperedges that are part of
 * the view.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
UndirectedSubHypergraph<VertexTemplate_t,
                        Hyperedge_t,
                        VertexProperty,
                        HyperedgeProperty,
                        HypergraphProperty>::
UndirectedSubHypergraph(UndirectedHypergraphInterface<VertexTemplate_t,
                        Hyperedge_t, VertexProperty, HyperedgeProperty,
                        HypergraphProperty>* parent,
                        const std::unordered_set<VertexIdType>&
                        whitelistedVertexIds,
                        const std::unordered_set<HyperedgeIdType>&
                        whitelistedHyperedgeIds) :
        UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty, HypergraphProperty>(),
        Observer(),
        parent_(parent) {
  // build whitelisted vertex filtered_vector
  std::vector<Vertex_t*> whitelistFilterVertices;
  // Create vector of vertex properties
  whitelistedVertexProperties_ = create_filtered_vector<VertexProperty*>(
      create_filtered_vector<VertexProperty*>(),
      std::make_unique<NullptrFilter<VertexProperty*>>());
  // Fill both containers with nullptrs
  for (size_t i = 0; i < parent_->vertexContainerSize(); ++i) {
    whitelistFilterVertices.push_back(nullptr);
    whitelistedVertexProperties_->push_back(nullptr);
  }
  for (VertexIdType vertexId : whitelistedVertexIds) {
    try {
      const auto& vertex = parent_->vertexById(vertexId);
      if (vertex != nullptr) {
        whitelistFilterVertices[vertexId] = const_cast<Vertex_t *>(vertex);
        whitelistedVertexProperties_->operator[](vertexId) =
            new VertexProperty(*(parent_->getVertexProperties_(vertexId)));
      } else {
        // delete already created vertex property pointers
        for (auto& vertexProperty : *whitelistedVertexProperties_) {
          delete vertexProperty;
        }
        throw std::invalid_argument("Vertex " + std::to_string(vertexId) +
                                    " does not exist in the parent "
                                        "hypergaph.");
      }
    } catch (const std::out_of_range& oor) {
      // delete already created vertex property pointers
      for (auto& vertexProperty : *whitelistedVertexProperties_) {
        delete vertexProperty;
      }

      std::string errorMessage("");
      if (parent_->vertexContainerSize() == 0) {
        errorMessage = "There is no vertex in the parent graph. You cannot"
                " add vertices to the subgraph\n";
      } else {
        errorMessage = "Vertex with id: " + std::to_string(vertexId)
                       + " is out of range -> min=0; max="
                       + std::to_string((parent_->vertexContainerSize() - 1))
                       + '\n';
      }
      throw std::out_of_range(errorMessage);
    }
  }
  // create actual vertices list
  whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
      parent_->getRootVerticesContainerPtr(),
      std::make_unique<WhitelistFilter<Vertex_t*>>(whitelistFilterVertices));


  // build whitelisted hyperedge filtered_vector
  std::vector<Hyperedge_t*> whitelistFilterHyperedges;
  // Create vector of hyperarc properties
  whitelistedHyperedgeProperties_ = create_filtered_vector<HyperedgeProperty*>(
      create_filtered_vector<HyperedgeProperty*>(),
      std::make_unique<NullptrFilter<HyperedgeProperty*>>());
  // Fill both containers with nullptrs
  for (size_t i = 0; i < parent_->hyperedgeContainerSize(); ++i) {
    whitelistFilterHyperedges.push_back(nullptr);
    whitelistedHyperedgeProperties_->push_back(nullptr);
  }
  for (HyperedgeIdType hyperedgeId : whitelistedHyperedgeIds) {
    try {
      const auto& hyperedge = parent_->hyperedgeById(hyperedgeId);
      bool vertexOfReactionInSubgraph = false;
      if (hyperedge != nullptr) {
        // Check if there is at least one vertex in the subgraph.
        // Get the vertices from the parent graph as the current
        // hyperedge is not yet added to the container of whitelisted hyperedges
        // in this subgraph -> impliedVertices(hyperedge->id()) throws
        // exception. Check in this subgraph if a vertex is present.
        auto verticesPtr = parent_->impliedVertices(hyperedge->id());
        for (const auto &vertex : *verticesPtr) {
          if (vertexById(vertex->id()) != nullptr) {
            vertexOfReactionInSubgraph = true;
            break;
          }
        }
      }

      if (hyperedge != nullptr and vertexOfReactionInSubgraph == true) {
        whitelistFilterHyperedges[hyperedgeId] = const_cast<
                Hyperedge_t*>(hyperedge);
        whitelistedHyperedgeProperties_->operator[](hyperedgeId) =
            new HyperedgeProperty(*(parent_->getHyperedgeProperties_(
                hyperedgeId)));
      } else {
        // delete already created vertex property pointers
        for (auto& vertexProperty : *whitelistedVertexProperties_) {
          delete vertexProperty;
        }
        // delete already created hyperedge property pointers
        for (auto& hyperedgeProperty : *whitelistedHyperedgeProperties_) {
          delete hyperedgeProperty;
        }

        if (hyperedge == nullptr) {
          throw std::invalid_argument("Hyperedge " + std::to_string(hyperedgeId)
                                      + " does not exist in the parent "
                                      + "hypergaph.");
        } else {
          throw std::invalid_argument("At least one vertex of hyperedge "
                                      + std::to_string(hyperedgeId) +
                                      " must be part of the subgraph.");
        }
      }
    } catch (const std::out_of_range& oor) {
      // delete already created vertex property pointers
      for (auto& vertexProperty : *whitelistedVertexProperties_) {
        delete vertexProperty;
      }
      // delete already created hyperedge property pointers
      for (auto& hyperedgeProperty : *whitelistedHyperedgeProperties_) {
        delete hyperedgeProperty;
      }

      std::string errorMessage("");
      if (parent_->hyperedgeContainerSize() == 0) {
        errorMessage = "There is no hyperedge in the parent graph. You cannot"
                " add hyperedges to the subgraph\n";
      } else {
        errorMessage = "Hyperedge with id: " + std::to_string(hyperedgeId)
                       + " is out of range -> min=0; max="
                       + std::to_string((parent_->hyperedgeContainerSize() - 1))
                       + '\n';
      }
      throw std::out_of_range(errorMessage);
    }
  }
  // create actual hyperedge list
  whitelistedHyperedgesList_ = create_filtered_vector<Hyperedge_t*>(
      parent->getRootHyperedgeContainerPtr(),
      std::make_unique<WhitelistFilter<Hyperedge_t*>>(
          whitelistFilterHyperedges));

  // add itself as observer of the parent
  parent_->AddObserver(this, ObservableEvent::VERTEX_REMOVED);
  parent_->AddObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
  parent_->AddObserver(this, ObservableEvent::RESET_VERTEX_IDS);
  parent_->AddObserver(this, ObservableEvent::RESET_EDGE_IDS);

  // Create HypergraphProperty object
  subHypergraphProperty_ = new HypergraphProperty(
          *parent_->getHypergraphProperties_());
}


/**
 * \brief UndirectedSubHypergraph copy constructor
 *
 * \details
 * Makes a deep copy of the given undirected sub-hypergraph. This sub-hypergraph
 * becomes a view of the parent of the to be copied sub-hypergraph.
 *
 * \param other The to be copied undirected sub-hypergraph.
 *
 * \par Complexity
 * Linear in the number of vertices and hyperedges of other.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
UndirectedSubHypergraph(const UndirectedSubHypergraph& other) :
        Observer(),
        parent_(other.parent_) {
  // copy whitelisted vertex filtered_vector
  std::vector<Vertex_t*> whitelistFilterVertices;
  for (size_t i = 0; i < parent_->vertexContainerSize(); ++i) {
    whitelistFilterVertices.push_back(nullptr);
  }
  for (const auto& vertex : other.vertices()) {
    whitelistFilterVertices[vertex->id()] =
            const_cast<Vertex_t*>(vertex);
  }
  // create actual vertices list
  whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
      parent_->getRootVerticesContainerPtr(),
      std::make_unique<WhitelistFilter<Vertex_t*>>(whitelistFilterVertices));


  // copy whitelisted hyperedge filtered_vector
  std::vector<Hyperedge_t*> whitelistFilterHyperedges;
  for (size_t i = 0; i < parent_->hyperedgeContainerSize(); ++i) {
    whitelistFilterHyperedges.push_back(nullptr);
  }
  for (const auto& hyperedge : other.hyperedges()) {
    whitelistFilterHyperedges[hyperedge->id()] =
            const_cast<Hyperedge_t*>(hyperedge);
  }
  // create actual hyperedge list
  whitelistedHyperedgesList_ = create_filtered_vector<Hyperedge_t*>(
      parent_->getRootHyperedgeContainerPtr(),
      std::make_unique<WhitelistFilter<Hyperedge_t*>>(
          whitelistFilterHyperedges));

  // Copy vertex properties
  whitelistedVertexProperties_ = create_filtered_vector<VertexProperty*>(
      create_filtered_vector<VertexProperty*>(),
      std::make_unique<NullptrFilter<VertexProperty*>>());
  for (std::size_t i = 0; i < other.whitelistedVertexProperties_->size(); ++i) {
    // vertex is not part of the other sub-hypergraph
    if (other.whitelistedVertexProperties_->operator[](i) == nullptr) {
      whitelistedVertexProperties_->push_back(nullptr);
    } else {  // Copy vertex property
      VertexProperty* vertexPropertyCopy = new VertexProperty(
          *(other.whitelistedVertexProperties_->operator[](i)));
      whitelistedVertexProperties_->push_back(vertexPropertyCopy);
    }
  }

  // Copy hyperedge properties
  whitelistedHyperedgeProperties_ = create_filtered_vector<HyperedgeProperty*>(
      create_filtered_vector<HyperedgeProperty*>(),
      std::make_unique<NullptrFilter<HyperedgeProperty*>>());
  for (std::size_t i = 0; i < other.whitelistedHyperedgeProperties_->size();
       ++i) {
    // hyperedge is not part of the other sub-hypergraph
    if (other.whitelistedHyperedgeProperties_->operator[](i) == nullptr) {
      whitelistedHyperedgeProperties_->push_back(nullptr);
    } else {  // Copy hyperedge property
      HyperedgeProperty* hyperedgePropertyCopy = new HyperedgeProperty(
          *(other.whitelistedHyperedgeProperties_->operator[](i)));
      whitelistedHyperedgeProperties_->push_back(hyperedgePropertyCopy);
    }
  }

  // Attach itself as observer to parent graph
  parent_->AddObserver(this, ObservableEvent::VERTEX_REMOVED);
  parent_->AddObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
  parent_->AddObserver(this, ObservableEvent::RESET_VERTEX_IDS);
  parent_->AddObserver(this, ObservableEvent::RESET_EDGE_IDS);

  // Create HypergraphProperty object
  subHypergraphProperty_ = new HypergraphProperty(
          *other.getHypergraphProperties_());
}


/**
 * \brief UndirectedSubHypergraph destructor
 *
 * \details
 * Consider the hierarchy of a root hypergraph \em r, a sub-hypergraph \em s of
 * \em r, and a sub-hypergraph \em s2 of \em s. Deleting \em s yields in that
 * the parent of \em s2 becomes the root hypergraph \em r.
 *
 * \par Complexity
 * O(n * m), with n the number of children of \em s, and m the number of
 * observable events (all children of \em s observe now its former grand-parent
 * \em r).
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
~UndirectedSubHypergraph() {
  delete subHypergraphProperty_;

  // Delete vertex property pointers
  for (auto& vertexProperty : *whitelistedVertexProperties_) {
    delete vertexProperty;
  }
  // Delete hyperedge property pointers
  for (auto& hyperedgeProperty : *whitelistedHyperedgeProperties_) {
    delete hyperedgeProperty;
  }

  // Detach itself as observer of the parent graph
  if (parent_ != nullptr) {
    parent_->DeleteObserver(this, ObservableEvent::VERTEX_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::RESET_VERTEX_IDS);
    parent_->DeleteObserver(this, ObservableEvent::RESET_EDGE_IDS);

    // Subgraphs whose parent (this subgraph) becomes the current grand-parent.
    // Add subgraphs as observers to the current grand-parent.
    // Clear list of observers of this subgraph.
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver = itEvent->second.begin();
           itObserver != itEvent->second.end(); ++itObserver) {
        auto subgraph = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
                Hyperedge_t, VertexProperty, HyperedgeProperty,
                HypergraphProperty>*>(*itObserver);
        subgraph->parent_ = parent_;
        subgraph->parent_->AddObserver(subgraph, itEvent->first);
      }
      this->observers_[itEvent->first].clear();
    }
  }
}


/**
 * \brief Copy assignment operator
 *
 * \details
 * Replaces the contents (vertices, hyperedges) and the observed parent of the
 * undirected sub-hypergraph.
 *
 * \param source Another UndirectedSubHypergraph to use as data source.
 *
 * \return
 * \code *this \endcode
 *
 * \par Complexity
 * O(n * m), with n the number of children views of this, and m the number of
 * observable events.\n
 * Linear in the number of vertices and hyperedges of the source.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
    HyperedgeProperty, HypergraphProperty>& UndirectedSubHypergraph<
    VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
    HypergraphProperty>::
operator=(const UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>& source) {
  // check for self assignment
  if (this != &source) {
    // deallocate whitelistedVertexProperties_
    for (auto& vProp : *whitelistedVertexProperties_) {
      delete vProp;
    }
    whitelistedVertexProperties_->clear();

    // deallocate whitelistedHyperedgeProperties_
    for (auto& hyperedgeProperty : *whitelistedHyperedgeProperties_) {
      delete hyperedgeProperty;
    }
    whitelistedHyperedgeProperties_->clear();

    // Set the parent of all childs in the subgraph-hierarchy to null
    std::unordered_set<UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
            VertexProperty, HyperedgeProperty, HypergraphProperty>*> observers;
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver : itEvent->second) {
        auto subgraph = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
                Hyperedge_t, VertexProperty, HyperedgeProperty,
                HypergraphProperty>*>(itObserver);
        observers.insert(subgraph);
      }
    }
    for (auto& subgraph : observers) {
      subgraph->setParentOfChildsToNull();
      this->DeleteObserver(subgraph, ObservableEvent::VERTEX_REMOVED);
      this->DeleteObserver(subgraph, ObservableEvent::HYPEREDGE_REMOVED);
      this->DeleteObserver(subgraph, ObservableEvent::RESET_VERTEX_IDS);
      this->DeleteObserver(subgraph, ObservableEvent::RESET_EDGE_IDS);
    }

    // Detach itself as observer from parent graph
    parent_->DeleteObserver(this, ObservableEvent::VERTEX_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
    parent_->DeleteObserver(this, ObservableEvent::RESET_VERTEX_IDS);
    parent_->DeleteObserver(this, ObservableEvent::RESET_EDGE_IDS);

    // Assign parent
    parent_ = source.parent_;

    // copy whitelisted vertex filtered_vector
    std::vector<Vertex_t*> whitelistFilterVertices;
    for (size_t i = 0; i < parent_->vertexContainerSize(); ++i) {
      whitelistFilterVertices.push_back(nullptr);
    }
    for (const auto& vertex : source.vertices()) {
      whitelistFilterVertices[vertex->id()] =
              const_cast<Vertex_t*>(vertex);
    }
    // create actual vertices list
    whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
            parent_->getRootVerticesContainerPtr(),
            std::make_unique<WhitelistFilter<Vertex_t*>>(
                whitelistFilterVertices));


    // copy whitelisted hyperedge filtered_vector
    std::vector<Hyperedge_t*> whitelistFilterHyperedges;
    for (size_t i = 0; i < parent_->hyperedgeContainerSize(); ++i) {
      whitelistFilterHyperedges.push_back(nullptr);
    }
    for (const auto& hyperedge : source.hyperedges()) {
      whitelistFilterHyperedges[hyperedge->id()] =
              const_cast<Hyperedge_t*>(hyperedge);
    }
    // create actual hyperedge list
    whitelistedHyperedgesList_ = create_filtered_vector<Hyperedge_t*>(
            parent_->getRootHyperedgeContainerPtr(),
            std::make_unique<WhitelistFilter<Hyperedge_t*>>(
                whitelistFilterHyperedges));


    // Copy vertex properties
    whitelistedVertexProperties_ = create_filtered_vector<VertexProperty*>(
        create_filtered_vector<VertexProperty*>(),
        std::make_unique<NullptrFilter<VertexProperty*>>());
    for (std::size_t i = 0; i < source.whitelistedVertexProperties_->size();
         ++i) {
      // vertex is not part of the other sub-hypergraph
      if (source.whitelistedVertexProperties_->operator[](i) == nullptr) {
        whitelistedVertexProperties_->push_back(nullptr);
      } else {  // Copy vertex property
        VertexProperty *vertexPropertyCopy = new VertexProperty(
            *(source.whitelistedVertexProperties_->operator[](i)));
        whitelistedVertexProperties_->push_back(vertexPropertyCopy);
      }
    }

    // Copy hyperedge properties
    whitelistedHyperedgeProperties_ =
        create_filtered_vector<HyperedgeProperty*>(
            create_filtered_vector<HyperedgeProperty*>(),
            std::make_unique<NullptrFilter<HyperedgeProperty*>>());
    for (std::size_t i = 0; i < source.whitelistedHyperedgeProperties_->size();
         ++i) {
      // hyperedge is not part of the other sub-hypergraph
      if (source.whitelistedHyperedgeProperties_->operator[](i) == nullptr) {
        whitelistedHyperedgeProperties_->push_back(nullptr);
      } else {  // Copy hyperedge property
        HyperedgeProperty* hyperedgePropertyCopy = new HyperedgeProperty(
            *(source.whitelistedHyperedgeProperties_->operator[](i)));
        whitelistedHyperedgeProperties_->push_back(hyperedgePropertyCopy);
      }
    }

    // init hypergraph property
    delete subHypergraphProperty_;
    subHypergraphProperty_ = new HypergraphProperty(
            *source.getHypergraphProperties_());

    // Attach itself as observer to parent graph
    parent_->AddObserver(this, ObservableEvent::VERTEX_REMOVED);
    parent_->AddObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
    parent_->AddObserver(this, ObservableEvent::RESET_VERTEX_IDS);
    parent_->AddObserver(this, ObservableEvent::RESET_EDGE_IDS);
  }
  return *this;
}


/**
 * \brief Check if multi-hyperedges are allowed in this hypergraph
 *
 * \details
 * As a sub-hypergraph is only a view of the root hypergraph, this function
 * returns whether multi-hyperedges are supported in the root hypergraph.
 *
 * \return True if multi-hyperedges are supported, false otherwise.
 *
 * \par Complexity
 * Linear in the number of levels in the hierarchy between the root hypergraph
 * and this.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
bool UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
allowMultiHyperedges() const {
  throwExceptionOnNullptrParent();
  return parent_->allowMultiHyperedges();
}


/**
 * \brief Number of hyperedges in the sub-hypergraph
 *
 * \return Number of hyperedges in the sub-hypergraph.
 *
 * \par Complexity
 * Linear
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
size_t UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
nbHyperedges() const {
  throwExceptionOnNullptrParent();
  size_t nbHyperedges = 0;
  auto it = whitelistedHyperedgesList_->cbegin();
  auto end = whitelistedHyperedgesList_->cend();
  for (; it != end; ++it) {
    ++nbHyperedges;
  }
  return nbHyperedges;
}


/**
 * \brief Size of the hyperedge container
 *
 * \details
 * Pointers to hyperedge objects are stored in a customized vector. If a
 * hyperedge is removed from the hypergraph, then the corresponding entry in
 * this vector is set to a nullptr. The size of the container remains unchanged.
 * Thus, the return value of this function may differ from nbHyperedges().
 *
 * \return Size of the hyperedge container.
 *
 * \par Complexity
 * Linear in the number of levels in the hierarchy between the root hypergraph
 * and this.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
size_t UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgeContainerSize() const {
  throwExceptionOnNullptrParent();
  return parent_->hyperedgeContainerSize();
}


/**
 * \brief Add a hyperedge
 *
 * \details
 * Remove a hyperedge from the filtered out hyperedges of this undirected
 * sub-hypergraph and from all ancestor sub-hypergraph (except the root
 * hypergraph). A hyperedge with the given arguments is first searched in the
 * root hypergraph using the function Hyperedge_t::compare(). If found, the
 * hyperedge is removed from the filtered out hyperedges in this sub-hypergraph
 * and its ancestor sub-hypergraphs. This means that the hyperedge is visible in
 * those sub-hypergraphs. This function is less efficient than
 * addHyperedge(const HyperedgeIdType&).
 *
 * \param vertices Vertex names.
 * \param args Additional arguments to create a hyperedge of type Hyperedge_t.
 *
 * \return Pointer to const hyperedge on success, otherwise an exception is
 * thrown.
 *
 * \throw invalid_argument If no hyperedge was found in the root hypergraph.
 *
 * \par Complexity
 * Linear in number of hyperedges (hyperedge search).\n
 * Linear in the number of ancestor sub-hypergraphs.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const Hyperedge_t* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
addHyperedge(decltype(hglib::NAME),
             const VertexNames& vertices,
             const typename Hyperedge_t::SpecificAttributes& args) {
  throwExceptionOnNullptrParent();
  // Get pointer to root hypergraph
  auto rootHypergraph = getRootHypergraph();
  // Get vertex pointers of specified vertices
  std::vector<const Vertex_t*> vertexPtrs;
  // Add vertex pointers
  for (const auto& vertexName : vertices) {
    const auto& vertexPtr = rootHypergraph->vertexByName(vertexName);
    if (vertexPtr == nullptr) {
      throw std::invalid_argument("Error in UndirectedSubHypergraph::"
                                      "addHyperedge(): Can't find a hyperedge "
                                      "with given arguments in the root "
                                      "hypergraph. Can't find vertex "
                                  + vertexName);
    }
    vertexPtrs.push_back(vertexPtr);
  }
  // Search for the hyperedge in the root hypergraph
  Hyperedge_t* edgeInRootGraph = nullptr;
  for (const auto& edge : rootHypergraph->hyperedges()) {
    if (edge->compare(vertexPtrs, args) == true) {
      edgeInRootGraph = edge;
      break;
    }
  }
  if (edgeInRootGraph == nullptr) {
    throw std::invalid_argument("Error in UndirectedSubHypergraph::"
                                    "addHyperedge(): Can't find a hyperedge "
                                    "with given arguments in the root "
                                    "hypergraph.");
  }
  // Add hyperedge to this subgraph and the subgraphs up to the root hypergraph
  addHyperedge(edgeInRootGraph->id());
  return edgeInRootGraph;
}


/**
 * \brief Add a hyperedge
 *
 * \details
 * Add a hyperedge with the provided vertex Ids, and additional parameters
 * (if needed to create a hyperedge of type Hyperedge_t) to the root-hypergraph.
 * If done with success, then this hyperedge is added to all sub-hypergraphs in
 * the hierarchy from the root-hypergraph to this sub-hypergraph.
 *
 * \param vertices Vertex identifiers.
 * \param args Additional arguments to create a hyperedge of type Hyperedge_t.
 *
 * \return Pointer to const hyperedge on success, otherwise an exception is
 * thrown.
 *
 * \throw out_of_range If a given vertex Id is not in the range
 * [0, vertexContainerSize() )
 * \throw invalid_argument If multi-hyperedges are not allowed and there exists
 * already a hyperedge with the same vertices in the hypergraph.
 *
 * \par Complexity
 * Linear in the number of vertices (search vertices with given
 * Ids in constant time) and, if multi-hyperedges are not allowed, linear in the
 * number of hyperedges.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const Hyperedge_t* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
addHyperedge(const VertexIds& vertices,
             const typename Hyperedge_t::SpecificAttributes& args) {
  throwExceptionOnNullptrParent();
  // Get pointer to root hypergraph
  auto rootHypergraph = getRootHypergraph();
  // Get vertex pointers of specified vertices
  std::vector<const Vertex_t*> vertexPtrs;
  // Add vertex pointers
  for (const auto& vertexId : vertices) {
    try {
      const auto& vertexPtr = rootHypergraph->vertexById(vertexId);
      if (vertexPtr == nullptr) {
        throw std::invalid_argument("Error in UndirectedSubHypergraph::"
                                        "addHyperedge(): Can't find a "
                                        "hyperedge with given arguments in "
                                        "the root hypergraph. Can't find "
                                        "vertex " + std::to_string(vertexId));
      }
      vertexPtrs.push_back(vertexPtr);
    } catch (const std::out_of_range&) {
      throw std::out_of_range("Error in UndirectedSubHypergraph::"
                                  "addHyperedge(): Can't find a hyperedge "
                                  "with given arguments in the root "
                                  "hypergraph. Can't find vertex "
                                  + std::to_string(vertexId));
    }
  }
  // Search for the hyperedge in the root hypergraph
  Hyperedge_t* edgeInRootGraph = nullptr;
  for (const auto& edge : rootHypergraph->hyperedges()) {
    if (edge->compare(vertexPtrs, args) == true) {
      edgeInRootGraph = edge;
      break;
    }
  }
  if (edgeInRootGraph == nullptr) {
    throw std::invalid_argument("Error in UndirectedSubHypergraph::"
                                    "addHyperedge(): Can't find a hyperedge "
                                    "with given arguments in the root "
                                    "hypergraph.");
  }
  // Add hyperedge to this subgraph and the subgraphs up to the root hypergraph
  addHyperedge(edgeInRootGraph->id());
  return edgeInRootGraph;
}


/**
 * \brief Add a hyperedge
 *
 * \details
 * Remove a hyperedge from the filtered out hyperedges of this undirected
 * sub-hypergraph and from all ancestor sub-hypergraph (except the root
 * hypergraph). A hyperedge with the given Id is first searched in the
 * root hypergraph. If found, the hyperedge is removed from the filtered out
 * hyperedges in this sub-hypergraph and its ancestor sub-hypergraphs. This
 * means that the hyperedge is visible in those sub-hypergraphs.
 *
 * \param hyperedgeId Identifier of the hyperedge.
 *
 * \return Pointer to const hyperedge on success, otherwise an exception is
 * thrown.
 *
 * \throw out_of_range If the hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() )
 * \throw invalid_argument If the hyperedge with the given Id was removed
 * earlier from the root hypergraph.
 *
 * \par Complexity
 * Constant (hyperedge search).\n
 * Linear in the number of ancestor sub-hypergraphs.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const Hyperedge_t* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
    VertexProperty, HyperedgeProperty, HypergraphProperty>::
addHyperedge(const HyperedgeIdType& hyperedgeId) {
  throwExceptionOnNullptrParent();
  // Check whether a hyperedge with the given Id exists in the root hypergraph.
  // If no, throw an exception.
  // If yes, add the hyperedge to the subgraph and to all subgraphs in the
  // hierarchy between this subgraph and the root hypergraph.
  try {
    auto rootHypergraph = getRootHypergraph();
    const auto& hyperedge = rootHypergraph->hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      throw std::invalid_argument("Error in UndirectedSubHypergraph::"
                                      "addHyperedge(): Can't find a hyperedge"
                                      " with given Id "
                                  + std::to_string(hyperedgeId)
                                  + " in the root hypergraph.");
    } else {
      // Check that there is at least one vertex in the subgraph
      bool vertexPresentInSubgraph = false;
      try {
        // Check implied vertices
        auto verticesPtr = rootHypergraph->impliedVertices(hyperedgeId);
        for (const auto& vertex : *verticesPtr) {
          if (vertexById(vertex->id()) != nullptr) {
            vertexPresentInSubgraph = true;
            break;
          }
        }
        if (vertexPresentInSubgraph == false) {
          throw std::invalid_argument("Error in UndirectedSubHypergraph::"
                                          "addHyperedge() with given Id "
                                      + std::to_string(hyperedgeId)
                                      + ": At least one vertex "
                                      + "has to be in the subgraph.");
        }
      } catch (const std::out_of_range& oor) {}
    }
    // Add the hyperedge to the subgraph and to all subgraphs in the hierarchy
    // between this subgraph and the root hypergraph.
    removeFilteredOutHyperedgeInAncestors(hyperedge);

    // Add hyperedge property to this subgraph and the subgraphs up to the root
    // hypergraph
    addHyperedgePropertyInAncestors(hyperedgeId);

    // Return pointer to hyperedge
    return hyperedge;
  } catch (const std::out_of_range& oor) {
    throw std::out_of_range("Error in DirectedSubHypergraph::addHyperarc(): "
                                "Can't find a hyperarc with given Id "
                            + std::to_string(hyperedgeId)
                            + " in the root hypergraph.");
  }
}


/**
 * \brief Remove a hyperedge with the given id from the hypergraph
 *
 * \details
 * This function removes a hyperedge from this and all children sub-hypergraphs
 * by adding the given hyperedge Id to a filter.
 *
 * \param hyperedgeId Identifier of the hyperedge.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * O(\em n * O(\em fn)), with \em n the number of sub-hypergraphs of this
 * sub-hypergraph, and \em fn the function to add the hyperedge Id to the
 * filter.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
removeHyperedge(const HyperedgeIdType& hyperedgeId) {
  throwExceptionOnNullptrParent();
  /*
   * Check if there exists a hyperedge in the sub-hypergraph with the given id.
   * If yes, remove it from the whitelist.
   * If no, an exception is thrown.
   */
  try {
    const auto hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "removeHyperedge"
                                       " with the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }

    // Notify observers of change
    HyperedgeIdType nonConstHyperedgeId(hyperedgeId);
    this->NotifyObservers(ObservableEvent::HYPEREDGE_REMOVED,
                          &nonConstHyperedgeId);

    // Remove hyperedge
    whitelistedHyperedgesList_->addFilteredOutItem(
            const_cast<Hyperedge_t*>(hyperedge));

    // Remove hyperedge property
    delete whitelistedHyperedgeProperties_->operator[](hyperedgeId);
    whitelistedHyperedgeProperties_->operator[](hyperedgeId) = nullptr;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hyperedgeById: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Search hyperedge list for an hyperedge with the provided id
 *
 * \param hyperedgeId Identifier of the hyperedge.
 *
 * \return Pointer to hyperedge if found, nullptr if hyperedgeId is between 0
 * and hyperedgeContainerSize() - 1, throw out_of_range exception otherwise
 *
 * \throw out_of_range If given Id is not in the range
 * [0, hyperedgeContainerSize() )
 *
 * \par Complexity
 * Constant
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const Hyperedge_t* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
hyperedgeById(const HyperedgeIdType & hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check in constant time if the hyperedge id is in the whitelist of the sub-
   * hypergraph. If yes, we get the ptr to hyperedge from the parent hypergraph.
   * If no, we return a nullptr.
   *
   * A out_of_range exception is thrown if the given hyperedge id is out of
   * range.
   */
  try {
    const auto& hyperedge = whitelistedHyperedgesList_->at(hyperedgeId);
    return whitelistedHyperedgesList_->filter_out(hyperedge) ?
           nullptr : hyperedge;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hyperedgeById: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  }
}


/**
 * \brief Const hyperedge iterator pointing to the begin of the hyperedges
 *
 * \return Const hyperedge iterator pointing to the begin of the hyperedges.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the hyperedge container.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::hyperedge_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesBegin() const {
  throwExceptionOnNullptrParent();
  return whitelistedHyperedgesList_->cbegin();
}


/**
 * \brief Const hyperedge iterator pointing to the end of the hyperedges
 *
 * \return Const hyperedge iterator pointing to the end of the hyperedges.
 *
 * \par Complexity
 * Constant
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::hyperedge_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesEnd() const {
  throwExceptionOnNullptrParent();
  return whitelistedHyperedgesList_->cend();
}


/**
 * \brief Begin/End iterator pair of the hyperedges of the sub-hypergraph
 *
 * \details
 * Pair of hyperedge iterators pointing to the begin and the end of the
 * hyperedges of the sub-hypergraph.
 *
 * \return Pair of hyperedge iterators pointing to the begin and the end of the
 * hyperedges of the sub-hypergraph.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the hyperedge container.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
std::pair<typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator,
        typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::hyperedge_iterator>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesBeginEnd() const {
  throwExceptionOnNullptrParent();
  return std::make_pair(whitelistedHyperedgesList_->cbegin(),
                        whitelistedHyperedgesList_->cend());
}


/**
 * \brief Return a reference to the container of hyperedges
 *
 * \details
 * This function allows the use of a range based for loop as follows:
 * \code
 * // Create a sub-hypergraph of hypergraph g
 * std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
 * std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {1, 2};
 * auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
 *                                                      whitelistedHyperedges);
 * // Range based for loop
 * for (const auto& hyperedge : subgraph->hyperedges()) {
 *   std::cout << "Id of the hyperedge in the subgraph: "
 *             << hyperedge->id() << '\n';
 * }
 * \endcode
 *
 * \return Reference to hyperedge container
 *
 * \par Complexity
 * Constant (return the reference). However, the complexity to iterate over all
 * hyperedges of a sub-hypergraph using a range based for loop or a
 * classic for loop using hyperedgesBegin() and hyperedgesEnd() is the same.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::HyperedgeContainer&
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedges() const {
  throwExceptionOnNullptrParent();
  return *(whitelistedHyperedgesList_.get());
}


/**
 * \brief Return number of vertices in the hyperedge with the given id
 *
 * \details
 * This function returns the number of vertices in the hyperedge with given
 * id considering only those vertices in this sub-hypergraph.
 *
 * \param hyperedgeId Identifier of a hyperedge.
 *
 * \return Number of vertices in the hyperedge with the given id.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Linear
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
size_t UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
nbImpliedVertices(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperedge is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::nbVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // count number of implied vertices in this hyperedge in the subgraph
    size_t nbVerticesInHyperedge(0);
    auto it = impliedVerticesBegin(hyperedgeId);
    auto end = impliedVerticesEnd(hyperedgeId);
    for (; it != end; ++it) {
      ++nbVerticesInHyperedge;
    }
    return nbVerticesInHyperedge;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "nbVertices: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Check if the hyperedge with the given id has vertices
 *
 * \details
 * This function returns whether the hyperedge with given id contains vertices
 * considering only those vertices in this sub-hypergraph.
 *
 * \param hyperedgeId Identifier of a hyperedge.
 *
 * \return True if the hyperedge with the given identifier has vertices,
 * false otherwise.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Linear in the number of leading vertices that are not part of this sub-
 * hypergraph.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
bool UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hasVertices(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperedge is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::hasVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // compare begin and end
    auto begin = impliedVerticesBegin(hyperedgeId);
    auto end = impliedVerticesEnd(hyperedgeId);
    return (begin != end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hasVertices: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the begin of the implied vertices
 *
 * \details
 * Const vertex iterator pointing to the begin of the vertices of the
 * hyperedge with the given identifier. Only vertices present in this sub-
 * hypergraph are considered.
 *
 * \param hyperedgeId Identifier of a hyperedge.
 *
 * \return Const vertex iterator pointing to the begin of the vertices of
 * the hyperedge with the given identifier.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Linear in the number of leading vertices that are not part of this sub-
 * hypergraph.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::vertex_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
impliedVerticesBegin(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperedge is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "impliedVerticesBegin"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Vertex_t*>> tmp =
            create_filtered_vector<Vertex_t*>(
                    hyperedge->vertices_,
                    whitelistedVerticesList_->filter());
    return tmp->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "impliedVerticesBegin: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the end of the implied vertices
 *
 * \details
 * Const vertex iterator pointing to the end of the vertices of the
 * hyperedge with the given identifier. Only vertices present in this sub-
 * hypergraph are considered.
 *
 * \param hyperedgeId Identifier of a hyperedge.
 *
 * \return Const vertex iterator pointing to the end of the vertices of
 * the hyperedge with the given identifier.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Constant
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::vertex_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
impliedVerticesEnd(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperedge is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "impliedVerticesEnd"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Vertex_t*>> tmp =
            create_filtered_vector<Vertex_t*>(
                    hyperedge->vertices_,
                    whitelistedVerticesList_->filter());
    return tmp->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "impliedVerticesEnd: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Pair of vertex iterators pointing to the begin/end of the implied
 * vertices
 *
 * \details
 * This function returns a pair of const vertex iterators pointing to the begin
 * and the end of the implied vertices of the hyperedge with the given
 * identifier. Only vertices present in this sub-hypergraph are considered.
 *
 * \param hyperedgeId Identifier of a hyperedge.
 *
 * \return Pair of const vertex iterators pointing to the begin and the end of
 * the implied vertices of the hyperedge with the given identifier.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Linear in the number of leading vertices that are not part of this sub-
 * hypergraph.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
std::pair<typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::vertex_iterator,
        typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::vertex_iterator>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
impliedVerticesBeginEnd(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperedge is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "impliedVerticesBeginEnd"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator pair
    auto begin = impliedVerticesBegin(hyperedgeId);
    auto end = impliedVerticesEnd(hyperedgeId);
    return std::make_pair(begin, end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "impliedVerticesBeginEnd: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Get shared_ptr of the container of implied vertices
 *
 * \details
 * This function returns a shared_ptr of the container of the implied vertices
 * of a hyperedge with the given Id. It can be used in a range based for loop
 * as follows:
 * \code
 * // Create a sub-hypergraph of hypergraph g
 * std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
 * std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {1, 2};
 * auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
 *                                                      whitelistedHyperedges);
 * // Range based for loop
 * auto verticesPtr = subgraph->impliedVertices(hyperedgeId);
 * for (const auto& vertex : *verticesPtr) {
 *   std::cout << vertex->id() << " is a vertex of the hyperedge "
 *             << hyperedgeId << '\n';
 * }
 * \endcode
 *
 * \param hyperedgeId Identifier of a hyperedge.
 *
 * \return Shared_ptr of the container of the implied vertices
 * of a hyperedge with the given Id.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Constant
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<typename UndirectedSubHypergraph<
    VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
    HypergraphProperty>::Vertex_t*>> UndirectedSubHypergraph<VertexTemplate_t,
    Hyperedge_t, VertexProperty, HyperedgeProperty, HypergraphProperty>::
impliedVertices(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the hyperedge is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "impliedVertices"
                                       " with the hyperarc id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the directed sub-"
          "hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    // create FilteredVector
    return create_filtered_vector(hyperedge->vertices_,
                                  whitelistedVerticesList_->filter());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "impliedVertices: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Size of the vertex container
 *
 * \details
 * Pointers to vertex objects are stored in a customized vector. If a
 * vertex is removed from the hypergraph, then the corresponding entry in
 * this vector is set to a nullptr. The size of the container remains unchanged.
 * Thus, the return value of this function may differ from nbVertices().
 *
 * \return Size of the vertex container.
 *
 * \par Complexity
 * Constant
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
size_t UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
vertexContainerSize() const {
  throwExceptionOnNullptrParent();
  return parent_->vertexContainerSize();
}


/**
 * \brief Number of vertices in the sub-hypergraph
 *
 * \return Number of vertices in the sub-hypergraph.
 *
 * \par Complexity
 * Linear
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
size_t UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
nbVertices() const {
  throwExceptionOnNullptrParent();
  size_t nbVertices(0);
  auto it = whitelistedVerticesList_->cbegin();
  auto end = whitelistedVerticesList_->cend();
  for (; it != end; ++it) {
    ++nbVertices;
  }
  return nbVertices;
}


/**
 * \brief Search vertex list for a vertex with the provided name
 *
 * \details
 * Search vertex list for a vertex with the provided name considering only the
 * vertices in this sub-hypergraph.
 *
 * \param vertexName Name of the vertex
 *
 * \return Pointer to const vertex if a vertex with the specified name exists
 * in the sub-hypergraph, otherwise a nullptr.
 *
 * \par Complexity
 * Linear
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::Vertex_t*
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
vertexByName(const std::string& vertexName) const {
  throwExceptionOnNullptrParent();
  for (const auto& vertex : *(whitelistedVerticesList_.get())) {
    if (vertex->name().compare(vertexName) == 0) {
      return vertex;
    }
  }
  return nullptr;
}


/**
 * \brief Search vertex list for a vertex with the provided id
 *
 * \details
 * Search vertex list for a vertex with the provided Id considering only the
 * vertices in this sub-hypergraph.
 *
 * \param vertexId Identifier of the vertex
 *
 * \return Pointer to const vertex if a vertex with the specified Id exists
 * in the sub-hypergraph, otherwise a nullptr.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() )
 *
 * \par Complexity
 * Constant
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::Vertex_t*
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
vertexById(const VertexIdType & vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = whitelistedVerticesList_->at(vertexId);
    return whitelistedVerticesList_->filter_out(vertex) ? nullptr : vertex;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "vertexById: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the begin of the vertices
 *
 * \return Const vertex iterator pointing to begin of the vertices.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the vertex container.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::vertex_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
verticesBegin() const {
  throwExceptionOnNullptrParent();
  return whitelistedVerticesList_->cbegin();
}


/**
 * \brief Const vertex iterator pointing to the end of the vertices
 *
 * \return Const vertex iterator pointing to end of the vertices.
 *
 * \par Complexity
 * Constant
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::vertex_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
verticesEnd() const {
  throwExceptionOnNullptrParent();
  return whitelistedVerticesList_->cend();
}


/**
 * \brief Begin/End iterator pair of the vertices of the sub-hypergraph
 *
 * \details
 * Pair of vertex iterators pointing to the begin and the end of the
 * vertices of the undirected sub-hypergraph.
 *
 * \return Pair of vertex iterators pointing to the begin and the end of the
 * vertices of the undirected sub-hypergraph.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the vertex container.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
std::pair<typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::vertex_iterator,
        typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::vertex_iterator>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
verticesBeginEnd() const {
  throwExceptionOnNullptrParent();
  return std::make_pair(whitelistedVerticesList_->cbegin(),
                        whitelistedVerticesList_->cend());
}



/**
 * \brief Return a reference to the container of vertices
 *
 * \details
 * This function allows to use a range based for loop as follows:
 * \code
 * for (const auto& vertex : subgraph->vertices()) {
 *   std::cout << "Id of the vertex: " << vertex->id() << '\n';
 * }
 * \endcode
 *
 * \return Reference to vertex container
 *
 * \par Complexity
 * Constant (return the reference). However, the complexity to iterate over all
 * vertices of a undirected sub-hypergraph using a range based for loop or a
 * classic for loop using verticesBegin() and verticesEnd() is the same.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::VertexContainer&
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
vertices() const {
  throwExceptionOnNullptrParent();
  return *(whitelistedVerticesList_.get());
}


/**
 * \brief Add a vertex
 *
 * \details
 * Remove a vertex from the filtered out vertices of this undirected
 * sub-hypergraph. However, with this function it can not be determined which
 * vertex is meant. Provide the vertex name or Id to remove a vertex from the
 * filtered out vertices. Use either
 * addVertex(const std::string&, const typename Vertex_t::SpecificAttributes&)
 * or addVertex(const VertexIdType&).
 *
 * \param attributes Vertex type specific attributes to add a vertex.
 *
 * \return Pointer to const vertex on success, throw an exception otherwise.
 *
 * \throw invalid_argument Throws always an invalid_argument exception.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::Vertex_t*
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
addVertex(const typename Vertex_t::SpecificAttributes&) {
  throwExceptionOnNullptrParent();
  throw std::invalid_argument("Error in UndirectedSubHypergraph::addVertex(): "
                                  "Impossible to search for a vertex without a"
                                  " name.");
}


/**
 * \brief Add a vertex
 *
 * \details
 * Remove a vertex from the filtered out vertices of this undirected
 * sub-hypergraph and from all ancestor sub-hypergraph (except the root
 * hypergraph). A vertex with the given name and vertex type SpecificAttributes
 * is first searched in the root hypergraph using the function
 * VertexTemplate_t<Hyperedge_t>::compare(). If found, the vertex is removed
 * from the filtered out vertices in this sub-hypergraph and its ancestor
 * sub-hypergraphs. This means that the vertex is visible in those
 * sub-hypergraphs. This function is less efficient than
 * addVertex(const VertexIdType&)
 *
 * \param name Vertex name.
 * \param attributes Vertex type specific attributes to add a vertex.
 *
 * \return Pointer to const vertex on success, throw an exception otherwise.
 *
 * \throw invalid_argument If a vertex with the given arguments does not exists
 * in the root-hypergraph.
 *
 * \par Complexity
 * Linear in number of vertices (vertex search).\n
 * Linear in the number of ancestor sub-hypergraphs.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::Vertex_t*
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
addVertex(const std::string& name,
          const typename Vertex_t::SpecificAttributes& attributes) {
  throwExceptionOnNullptrParent();
  // Search for the vertex in the root hypergraph
  Vertex_t* vertexInRootGraph = nullptr;
  for (const auto& vertex : getRootHypergraph()->vertices()) {
    if (vertex->compare(name, attributes) == true) {
      vertexInRootGraph = vertex;
      break;
    }
  }
  if (vertexInRootGraph == nullptr) {
    throw std::invalid_argument("Error in UndirectedSubHypergraph::"
                                    "addVertex(): Can't find a vertex with"
                                    " given name " + name
                                + " in the root hypergraph.");
  }
  // Add vertex to this subgraph and the subgraphs up to the root hypergraph
  addVertex(vertexInRootGraph->id());
  return vertexInRootGraph;
}


/**
 * \brief Add a vertex
 *
 * \details
 * Remove a vertex from the filtered out vertices of this undirected
 * sub-hypergraph and from all ancestor sub-hypergraph (except the root
 * hypergraph). A vertex with the given Id is first searched in the root
 * hypergraph. If found, the vertex is removed from the filtered out vertices
 * in this sub-hypergraph and its ancestor sub-hypergraphs. This means that the
 * vertex is visible in those sub-hypergraphs.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Pointer to const vertex on success, throw an exception otherwise.
 *
 * \throw out_of_range If the vertex Id is not in the range
 * [0, vertexContainerSize() )
 * \throw invalid_argument If the vertex with the given Id was removed earlier
 * from the root hypergraph.
 *
 * \par Complexity
 * Constant (vertex search).\n
 * Linear in the number of ancestor sub-hypergraphs.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
    VertexProperty, HyperedgeProperty, HypergraphProperty>::Vertex_t*
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
    HyperedgeProperty, HypergraphProperty>::
addVertex(const VertexIdType& vertexId) {
  throwExceptionOnNullptrParent();
  // Check whether a vertex with the given Id exists in the root hypergraph.
  // If no, throw an exception.
  // If yes, add the vertex to the subgraph and to all subgraphs in the
  // hierarchy between this subgraph and the root hypergraph.
  try {
    const auto &vertex = getRootHypergraph()->vertexById(vertexId);
    if (vertex == nullptr) {
      throw std::invalid_argument("Error in UndirectedSubHypergraph::"
                                      "addVertex(): Can't find a vertex with"
                                      " given Id " + std::to_string(vertexId)
                                  + " in the root hypergraph.");
    }
    // Add the vertex to the subgraph and to all subgraphs in the hierarchy
    // between this subgraph and the root hypergraph.
    removeFilteredOutVertexInAncestors(vertex);
    // Add vertex property to this subgraph and the subgraphs up to the root
    // hypergraph
    addVertexPropertyInAncestors(vertexId);
    return vertex;
  } catch (const std::out_of_range& oor) {
    throw std::out_of_range("Error in UndirectedSubHypergraph::addVertex(): "
                                "Can't find a vertex with given Id "
                            + std::to_string(vertexId)
                            + " in the root hypergraph.");
  }
}


/**
 * \brief Get arguments to create an existing vertex
 *
 * \details
 * This function returns an ArgumentsToCreateVertex object that can
 * be used to insert a vertex in the originating or in a different hypergraph.
 * An ArgumentsToCreateVertex object consists of two parts,
 * (i) the vertex name, (ii) specific arguments needed to create a vertex of
 * type VertexTemplate_t<Hyperedge_t>.
 *
 * The returned object can be transformed to add a different vertex
 * to the hypergraph. This function can be useful in generic methods that
 * accepts any kind of hypergraph and that transform them.
 *
 * \par Example
 * \parblock
 * \code
 *
 * // Create a sub-hypergraph of hypergraph g
 * // ...with vertex Ids 1, 2, 3
 * std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
 * // ...with hyperarc Ids 1, 2
 * std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {1, 2};
 * // createUndirectedSubHypergraph returns a shared_ptr
 * auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
 *                                                      whitelistedHyperedges);
 *
 * // Get arguments to create vertex with Id 1
 * auto args = subgraph->argumentsToCreateVertex(1);
 * // Add vertex to another hypergraph g2.
 * const auto& vertex2 = g2.addVertex(args.name(), args.specificAttributes());
 * \endcode
 * \endparblock
 *
 * \param vertexId Identifier of the vertex.
 *
 * \return ArgumentsToCreateVertex by value.
 *
 * \throw out_of_range If the vertex Id is not in the range
 * [0, vertexContainerSize() )
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * The complexity depends on the function argumentsToCreateVertex() of
 * VertexTemplate_t<Hyperedge_t>.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
ArgumentsToCreateVertex<typename UndirectedSubHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::Vertex_t> UndirectedSubHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
argumentsToCreateVertex(const hglib::VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertex = whitelistedVerticesList_->at(vertexId);
    if (not whitelistedVerticesList_->filter_out(vertex)) {
      return whitelistedVerticesList_->operator[](
              vertexId)->argumentsToCreateVertex();
    } else {
      throw std::invalid_argument("Vertex with " + std::to_string(vertexId) +
                                  " is not part of the subgraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "vertexById: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}


/**
 * \brief Number of hyperedges that contain the vertex with the given Id
 *
 * \details
 * This function returns the number of hyperedges of the vertex with
 * the given id. Only hyperedges that are present in this sub-hypergraph are
 * counted.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Number of hyperedges that contain the vertex with the given Id.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
size_t UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
nbHyperedges(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::nbHyperedges"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // count number of hyperedges
    size_t nbHyperedgesWithGivenVertex(0);
    auto it = hyperedgesBegin(vertexId);
    auto end = hyperedgesEnd(vertexId);
    for (; it != end; ++it) {
      ++nbHyperedgesWithGivenVertex;
    }
    return nbHyperedgesWithGivenVertex;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "nbHyperedges: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Check if the vertex with the given Id is part of a hyperedge
 *
 * \details
 * This function returns whether the vertex with the given Id is part of a
 * hyperedge considering only those hyperedges in this sub-hypergraph.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return True if the vertex with the given identifier is part of a hyperedge,
 * false otherwise.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear in the number of leading hyperedges of the given vertex that are not
 * in this sub-hypergraph.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
bool UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
isContainedInAnyHyperedge(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "isContainedInAnyHyperedge"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // check if there hyperedges
    auto begin = hyperedgesBegin(vertexId);
    auto end = hyperedgesEnd(vertexId);
    return (begin != end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "isContainedInAnyHyperedge: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Iterator pointing to the begin of the hyperedges that contain the
 * vertex
 *
 * \details
 * This function returns a const hyperedge iterator pointing to the begin of
 * the hyperedges that contain the vertex with the given identifier. Only
 * hyperedges that are present in this sub-hypergraph are taken into account.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Const hyperedge iterator pointing to the begin of the hyperedges that
 * contain the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear in the number of leading hyperedges of the given vertex that
 * are not in this sub-hypergraph.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::hyperedge_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesBegin(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "hyperedgesBegin"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Hyperedge_t*>> tmp =
            create_filtered_vector<Hyperedge_t*>(
                    vertex->hyperedges_,
                    whitelistedHyperedgesList_->filter());
    return tmp->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hyperedgesBegin: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Iterator pointing to the end of the hyperedges that contain the
 * vertex
 *
 * \details
 * This function returns a const hyperedge iterator pointing to the end of
 * the hyperedges that contain the vertex with the given identifier. Only
 * hyperedges that are present in this sub-hypergraph are taken into account.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Const hyperedge iterator pointing to the end of the hyperedges that
 * contain the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::hyperedge_iterator
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesEnd(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "hyperedgesEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create iterator
    std::shared_ptr<FilteredVector<Hyperedge_t*>> tmp =
            create_filtered_vector<Hyperedge_t*>(
                    vertex->hyperedges_,
                    whitelistedHyperedgesList_->filter());
    return tmp->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hyperedgesEnd: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Pair of iterators pointing to the begin/end of the vertex's hyperedges
 *
 * \details
 * This function returns a pair of const hyperedge iterators pointing to the
 * begin and the end of the hyperedges that contain the vertex with the given
 * identifier. Only hyperedges that are present in this sub-hypergraph are
 * taken into account.
 *
 * \param vertexId Identifier of a vertex.
 * \return Pair of const hyperedge iterator pointing to the begin/end of the
 * hyperedges that contain the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear in the number of leading hyperedges of the given vertex that
 * are not in this sub-hypergraph.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
std::pair<typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator,
        typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::hyperedge_iterator>
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesBeginEnd(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "hyperedgesBeginEnd"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    // create pair of iterators
    auto begin = hyperedgesBegin(vertexId);
    auto end = hyperedgesEnd(vertexId);
    return std::make_pair(begin, end);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hyperedgesBeginEnd: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}



/**
 * \brief Get shared_ptr of container of hyperedges that contain the vertex
 *
 * \details
 * This function returns a shared_ptr of the container of the hyperedges that
 * contain the vertex with the given Id. This allows the use of a range based
 * for loop as follows:
 * \code
 *
 * // Create a sub-hypergraph of hypergraph g
 * // ...with vertex Ids 1, 2, 3
 * std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
 * // ...with hyperarc Ids 1, 2
 * std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {1, 2};
 * // createUndirectedSubHypergraph returns a shared_ptr
 * auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
 *                                                      whitelistedHyperedges);
 *
 * // Range based for loop
 * auto hyperedgesPtr = subgraph->hyperedges(vertexId);
 * for (const auto& edge : *hyperedgesPtr) {
 *   std::cout << edge->id() << " is a hyperedge of vertex "
 *             << vertexId << '\n';
 * }
 * \endcode
 *
 * \param vertexId Identifier of a vertex.
 * \return shared_ptr of the container of the hyperedges that contain the vertex
 * with the given Id.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<
    Hyperedge_t*>> UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
    VertexProperty, HyperedgeProperty, HypergraphProperty>::
hyperedges(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex is part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "hyperedges"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed"
          " sub-hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    // create FilteredVector
    return create_filtered_vector(vertex->hyperedges_,
                                  whitelistedHyperedgesList_->filter());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "hyperedges: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }
}


/**
 * \brief Remove a vertex with the given name from the sub-hypergraph
 *
 * \details
 * This function removes a vertex with the given name from the undirected
 * sub-hypergraph. Implied hyperedges of this vertex are also removed from the
 * view if the second parameter is set to true. The vertex and the hyperedges
 * remain in the root-hypergraph. They are only removed from the
 * sub-hypergraph (view).
 *
 * \param vertexName Name of a vertex.
 * \param removeImpliedHyperedges Flag that shows whether hyperedges that
 * contain the vertex with the given name should be removed from the sub-
 * hypergraph. Defaults to true.
 *
 * \throw invalid_argument If the undirected sub-hypergraph contains no vertex
 * with the given name.
 *
 * \par Complexity
 * Linear.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
removeVertex(const std::string& vertexName,
             bool removeImpliedHyperedges) {
  throwExceptionOnNullptrParent();
  // Find vertex by name in the hypergraph and call the method removeVertex
  // that takes the id of the vertex
  const auto& vertex = this->vertexByName(vertexName);
  if (vertex == nullptr) {
    std::string errorMessage("Called UndirectedSubHypergraph::removeVertex"
                                     " with the vertex name " + vertexName +
                             " which does not exists in the undirected "
                                     "subhypergraph.\n");
    throw std::invalid_argument(errorMessage);
  }

  removeVertex(vertex->id(), removeImpliedHyperedges);
}


/**
 * \brief Remove a vertex with the given Id from the sub-hypergraph
 *
 * \details
 * This function removes a vertex with the given Id from the undirected
 * sub-hypergraph. Implied hyperedges of this vertex are also removed from the
 * view if the second parameter is set to true. The vertex and the hyperedges
 * remain in the root-hypergraph. They are only removed from the
 * sub-hypergraph (view).
 *
 * \param vertexId Identifier of a vertex.
 * \param removeImpliedHyperedges Flag that shows whether hyperedges that
 * contain the vertex with the given Id should be removed from the sub-
 * hypergraph. Defaults to true.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
removeVertex(const VertexIdType& vertexId,
             bool removeImpliedHyperedges) {
  throwExceptionOnNullptrParent();
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before, or does not take part of the
   * sub-hypergraph.
   * Catch IndexOutOfRangeException if given id is out of bound.
   *
   * Remove all implied hyperedges from the sub-hypergraph if flag is set to
   * true (default).
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::removeVertex"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
    if (removeImpliedHyperedges) {
      // hyperedges that will be removed from the hypergraph
      std::vector<Hyperedge_t*> hyperedgesToBeRemoved;
      for (auto& hyperedge : *(whitelistedHyperedgesList_.get())) {
        // all hyperedges that contain the vertex will be removed from the
        // hypergraph
        if (isVertexOfHyperedge(vertexId, hyperedge->id())) {
          hyperedgesToBeRemoved.emplace_back(hyperedge);
        }
      }
      // remove hyperedges from hypergraph
      for (const auto& hyperedge : hyperedgesToBeRemoved) {
        removeHyperedge(hyperedge->id());
      }
    }

    // Notify observers of change
    Remove_vertex_args args;
    args.vertexId_ = vertexId;
    args.removeImpliedHyperarcs_ = removeImpliedHyperedges;
    this->NotifyObservers(ObservableEvent::VERTEX_REMOVED, &args);

    // remove vertex from sub-hypergraph
    whitelistedVerticesList_->addFilteredOutItem(
            const_cast<Vertex_t*>(vertex));
    // Remove vertex property
    delete whitelistedVertexProperties_->operator[](vertexId);
    whitelistedVertexProperties_->operator[](vertexId) = nullptr;

    // Remove hyperedges from subgraph if they have no vertices
    if (not removeImpliedHyperedges) {
      std::vector<Hyperedge_type*> hyperedgesToBeRemoved;
      for (const auto& hyperedge : *(whitelistedHyperedgesList_.get())) {
        if (hasVertices(hyperedge->id()) == false) {
          hyperedgesToBeRemoved.push_back(hyperedge);
        }
      }
      // remove hyperedges from sub-hypergraph
      for (const auto& hyperedge : hyperedgesToBeRemoved) {
        removeHyperedge(hyperedge->id());
      }
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "removeVertex:"
            " index = " << vertexId << "; min = 0; max = "
              << this->whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Check if the given vertex is part of the given hyperedge
 *
 * \details
 * This function checks whether the vertex with the given vertex Id is a vertex
 * of the hyperedge with the given hyperedge Id. Only vertices and hyperedges
 * that are present in this sub-hypergraph are taken into account.
 *
 * \param vertexId Identifier of a vertex.
 * \param hyperedgeId Identifier of a hyperedge.
 *
 * \return True if the vertex is a part of the hyperedge, false otherwise.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ) or the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Linear in the number of vertices of the given hyperedge.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
bool UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
isVertexOfHyperedge(const VertexIdType& vertexId,
                    const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  /*
   * Check first if the vertex and hyperedge are part of the sub-hypergraph.
   * If no, throw an exception.
   */
  try {
    const auto vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "isVertexOfHyperedge"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the directed sub-hypergraph."
              "\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "isVertexOfHyperedge: "
            "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }

  try {
    const auto hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedSubHypergraph::"
                                       "isVertexOfHyperedge"
                                       " with the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected sub-"
              "hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedSubHypergraph::"
            "isVertexOfHyperedge: "
            "index = " << hyperedgeId << "; min = 0; max = " <<
              whitelistedHyperedgesList_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& ia) {
    throw;
  }

  auto it = impliedVerticesBegin(hyperedgeId);
  auto end = impliedVerticesEnd(hyperedgeId);
  for (; it != end; ++it) {
    if ((*it)->id() == vertexId) {
      return true;
    }
  }
  return false;
}


/**
 * \brief Get properties of the hyperedge with the provided id
 *
 * \details
 * This function returns a pointer to a const (read only) hyperedge property
 * associated to the hyperedge with the specified hyperedge Id.
 *
 * \param hyperedgeId Identifier of the hyperedge.
 *
 * \return Pointer to const hyperedge property type if the hyperedge is part of
 * the undirected hypergraph.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there exists no hyperedge with the given Id in
 * this sub-hypergraph.
 *
 * \par Complexity
 * Linear in the number of levels in the hierarchy between the root hypergraph
 * and this.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const HyperedgeProperty* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
getHyperedgeProperties(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& hyperedgeProp =
        whitelistedHyperedgeProperties_->at(hyperedgeId);
    if (hyperedgeProp == nullptr) {
      throw std::invalid_argument("Error in UndirectedSubHypergraph::"
                                      "getHyperedgeProperties: hyperedge with "
                                      "id "
                                  + std::to_string(hyperedgeId)
                                  + " is not part of the subgraph.");
    }
    return hyperedgeProp;
  } catch (const std::out_of_range& oor) {
    throw std::out_of_range("IndexOutOfRangeException in "
                                "UndirectedSubHypergraph::"
                                "getHyperedgeProperties: index = "
                            + std::to_string(hyperedgeId)
                            + "; min = 0; max = "
                            + std::to_string(
        whitelistedHyperedgeProperties_->size()));
  }
}


/**
 * \brief Get properties of the hyperedge with the provided id
 *
 * \details
 * This function returns a pointer to a hyperedge property associated to the
 * hyperedge with the specified hyperedge Id. This is a non-const version of
 * getHyperedgeProperties().
 *
 * \param hyperedgeId Identifier of the hyperedge.
 *
 * \return Pointer to hyperedge property type if the hyperedge is part of
 * the undirected hypergraph.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there exists no hyperedge with the given Id in
 * this sub-hypergraph.
 *
 * \par Complexity
 * Linear in the number of levels in the hierarchy between the root hypergraph
 * and this.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
HyperedgeProperty* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
getHyperedgeProperties_(const HyperedgeIdType& hyperedgeId) const {
  throwExceptionOnNullptrParent();
  try {
    return const_cast<HyperedgeProperty*>(getHyperedgeProperties(hyperedgeId));
  } catch (const std::out_of_range& oor) {
    throw std::out_of_range("IndexOutOfRangeException in "
                                "UndirectedSubHypergraph::"
                                "getHyperedgeProperties_: index = "
                            + std::to_string(hyperedgeId) + "; min = 0; max = "
                            + std::to_string(
        whitelistedHyperedgeProperties_->size()));
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument("Error in UndirectedSubHypergraph::"
                                    "getHyperedgeProperties_: hyperedge with "
                                    "id "
                                + std::to_string(hyperedgeId)
                                + " is not part of the sub-hypergraph.");
  }
}


/**
 * \brief Get properties of the vertex with the provided id
 *
 * \details
 * This function returns a pointer to a const (read only) vertex property
 * associated to the vertex with the specified vertex Id.
 *
 * \param vertexId Identifier of the vertex.
 *
 * \return Pointer to const vertex property type if the vertex is part of
 * the undirected hypergraph.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there exists no vertex with the given Id in
 * this sub-hypergraph.
 *
 * \par Complexity
 * Linear in the number of levels in the hierarchy between the root hypergraph
 * and this.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const VertexProperty* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
getVertexProperties(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    const auto& vertexProp = whitelistedVertexProperties_->at(vertexId);
    if (vertexProp == nullptr) {
      throw std::invalid_argument("Error in UndirectedSubHypergraph::"
                                      "getVertexProperties: vertex with " +
                                  std::to_string(vertexId) +
                                  " is not part of the subgraph.");
    }
    return vertexProp;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in"
        " UndirectedSubHypergraph::getVertexProperties: "
        "index = " << vertexId << "; min = 0; max = " <<
              whitelistedVerticesList_->size() << '\n';
    throw;
  }
}


/**
 * \brief Get properties of the vertex with the provided id
 *
 * \details
 * This function returns a pointer to a vertex property associated to the
 * vertex with the specified vertex Id. This is a non-const version of
 * getVertexProperties().
 *
 * \param vertexId Identifier of the vertex.
 *
 * \return Pointer to vertex property type if the vertex is part of the
 * undirected hypergraph.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there exists no vertex with the given Id in
 * this sub-hypergraph.
 *
 * \par Complexity
 * Linear in the number of levels in the hierarchy between the root hypergraph
 * and this.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
VertexProperty* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
getVertexProperties_(const VertexIdType& vertexId) const {
  throwExceptionOnNullptrParent();
  try {
    return const_cast<VertexProperty*>(getVertexProperties(vertexId));
  } catch (const std::out_of_range& oor) {
    throw std::out_of_range("IndexOutOfRangeException in "
                                "UndirectedSubHypergraph::"
                                "getVertexProperties_: index = "
                            + std::to_string(vertexId)
                            + "; min = 0; max = " + std::to_string(
        whitelistedVertexProperties_->size()));
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument("Error in UndirectedSubHypergraph::"
                                    "getVertexProperties_: vertex with " +
                                std::to_string(vertexId) +
                                " is not part of the sub-hypergraph.");
  }
}


/**
 * \brief Get properties of the sub-hypergraph
 *
 * \details
 * This function returns a pointer to a const (read only) HypergraphProperty
 * type associated with this sub-hypergraph.
 *
 * \return Pointer to const HypergraphProperty type.
 *
 * \par Complexity
 * Constant
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const HypergraphProperty* UndirectedSubHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty, HypergraphProperty>::
getHypergraphProperties() const {
  throwExceptionOnNullptrParent();
  return subHypergraphProperty_;
}


/**
 * \brief Get properties of the sub-hypergraph
 *
 * \details
 * This function returns a pointer to a HypergraphProperty type associated with
 * this sub-hypergraph. This is a non-const version of
 * getHypergraphProperties().
 *
 * \return Pointer to HypergraphProperty type.
 *
 * \par Complexity
 * Constant
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
HypergraphProperty* UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
getHypergraphProperties_() const {
  throwExceptionOnNullptrParent();
  return const_cast<HypergraphProperty*>(getHypergraphProperties());
}


/**
 * \brief Get ancestors
 *
 * \details
 * Add the sub-hypergraph's ancestors in the hierarchy to the passed vector.
 * The root hypergraph will be the last entry.
 *
 * \param ancestors Vector to be filled with the ancestors of the
 * sub-hypergraph.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
    HyperedgeProperty, HypergraphProperty>::
getAncestors(std::vector<HypergraphInterface<VertexTemplate_t, Hyperedge_t,
    VertexProperty, HyperedgeProperty, HypergraphProperty>*>* ancestors) const {
  ancestors->push_back(parent_);
  parent_->getAncestors(ancestors);
}


/**
 * \brief Get root hypergraph
 *
 * \details
 * An undirected hypergraph can have undirected sub-hypergraphs (views). This
 * hierarchy is implemented through the observer and decorator pattern. A
 * subgraph observes its parent graph. This function returns the root
 * hypergraph of the hierarchy.
 *
 * \return A pointer to the undirected root hypergraph
 * (UndirectedHypergraphInterface).
 *
 * \par Complexity
 * Linear in the number of levels in the hierarchy between this sub-hypergraph
 * and the root hypergraph.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
const UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
      VertexProperty, HyperedgeProperty, HypergraphProperty>*
      UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
              HyperedgeProperty, HypergraphProperty>::
getRootHypergraph() const {
  throwExceptionOnNullptrParent();
  return parent_->getRootHypergraph();
}



/**
 * \brief Observer update method
 *
 * \details
 * This function is called in the observed hypergraph to delegate the
 * information about an event that requires an update in this sub-hypergraph.
 *
 * \param o Observable Reference to observed hypergraph.
 * \param e Event Observed event.
 * \param arg Optional arguments.
 *
 * \par Complexity
 * The complexity depends on the action that is executed to respond to a given
 * event.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
Update(const Observable& o, ObservableEvent e, void* arg) {
  throwExceptionOnNullptrParent();
  if (&o == parent_) {
    switch (e) {
      case ObservableEvent::VERTEX_REMOVED: {
        Remove_vertex_args* args = static_cast<Remove_vertex_args*>(arg);
        try {
          const auto& vertex = whitelistedVerticesList_->at(args->vertexId_);
          if (not whitelistedVerticesList_->filter_out(vertex)) {
            removeVertex(args->vertexId_, args->removeImpliedHyperarcs_);
          }
        } catch (const std::out_of_range& oor) {
          std::cerr << "IndexOutOfRangeException in"
                  " UndirectedSubHypergraph::Update: "
                  "index = " << args->vertexId_ << "; min = 0; max = " <<
                    whitelistedVerticesList_->size() << '\n';
          throw;
        }
        break;
      }
      case ObservableEvent::HYPEREDGE_REMOVED: {
        HyperedgeIdType* hyperedgeId = static_cast<HyperedgeIdType*>(arg);
        try {
          const auto& hyperedge = whitelistedHyperedgesList_->at(*hyperedgeId);
          if (not whitelistedHyperedgesList_->filter_out(hyperedge)) {
            removeHyperedge(*hyperedgeId);
          }
        } catch (const std::out_of_range& oor) {
          std::cerr << "IndexOutOfRangeException in"
                  " UndirectedSubHypergraph::Update: "
                  "index = " << *hyperedgeId << "; min = 0; max = " <<
                    whitelistedHyperedgesList_->size() << '\n';
          throw;
        }
        break;
      }
      case ObservableEvent::RESET_VERTEX_IDS: {
        updateVertexContainerUponResetVertexIds();
        break;
      }
      case ObservableEvent::RESET_EDGE_IDS: {
        updateHyperedgeContainerUponResetHyperedgeIds();
        break;
      }
    }
  }
}


/**
 * \brief Remove the given vertex from the filtered out vertices in ancestors
 *
 * \details
 * Remove the given vertex from the filtered out vertices in this sub-hypergraph
 * and all its ancestor sub-hypergraphs (not the root hypergraph).
 *
 * \param vertexPtr Pointer to vertex to be removed from the filtered out
 * vertices
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
    VertexProperty, HyperedgeProperty, HypergraphProperty>::
removeFilteredOutVertexInAncestors(
    const VertexTemplate_t<Hyperedge_t>* vertexPtr) {
  // Add vertex to this sub-hypergraph
  whitelistedVerticesList_->removeFilteredOutItem(
      const_cast<Vertex_t*>(vertexPtr));
  // Add vertex to parent hypergraph if it is not the root
  auto undirectedSubHG = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
  Hyperedge_t, VertexProperty, HyperedgeProperty,
  HypergraphProperty>*>(parent_);
  if (undirectedSubHG) {  // parent is not the root hypergraph
    undirectedSubHG->removeFilteredOutVertexInAncestors(vertexPtr);
  }
}


/**
 * \brief Remove the given hyperedge from the filtered out hyperedges in
 * ancestors
 *
 * \details
 * Remove the given hyperedge from the filtered out hyperedges in this sub-
 * hypergraph and all its ancestor sub-hypergraphs (not the root hypergraph).
 *
 * \param edgePtr Pointer to hyperedge to be removed from the filtered out
 * hyperedges
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
    VertexProperty, HyperedgeProperty, HypergraphProperty>::
removeFilteredOutHyperedgeInAncestors(const Hyperedge_t* edgePtr) {
  // Add hyperedge to this sub-hypergraph
  whitelistedHyperedgesList_->removeFilteredOutItem(
      const_cast<Hyperedge_t*>(edgePtr));
  // Add hyperedge to parent hypergraph if it is not the root
  auto undirectedSubHG = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
  Hyperedge_t, VertexProperty, HyperedgeProperty,
  HypergraphProperty>*>(parent_);
  if (undirectedSubHG) {  // parent is not the root hypergraph
    undirectedSubHG->removeFilteredOutHyperedgeInAncestors(edgePtr);
  }
}


/**
 * \brief Add vertex property in ancestors
 *
 * \details
 * Add a vertex property for the given vertex identifier in this sub-
 * hypergraph and all its ancestor sub-hypergraphs (not the root hypergraph).
 *
 * \param vertexId Vertex identifier
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
    VertexProperty, HyperedgeProperty, HypergraphProperty>::
addVertexPropertyInAncestors(const VertexIdType& vertexId) {
  // If at the given position there exists no nullptr then in the hierarchy
  // above there is no nullptr either
  if (vertexId < whitelistedVertexProperties_->size() and
      whitelistedVertexProperties_->operator[](vertexId) != nullptr) {
    return;
  }

  // Add vertex property to parent hypergraph if it is not the root
  auto undirectedSubHG = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
  Hyperedge_t, VertexProperty, HyperedgeProperty,
  HypergraphProperty>*>(parent_);
  if (undirectedSubHG) {  // parent is not the root hypergraph
    undirectedSubHG->addVertexPropertyInAncestors(vertexId);
  }
  // Add vertex property to this sub-hypergraph
  if (vertexId >= whitelistedVertexProperties_->size()) {
    // fill gaps with nullptrs and then add the property
    size_t nbGaps = vertexId - whitelistedVertexProperties_->size();
    while (nbGaps-- > 0) {
      whitelistedVertexProperties_->push_back(nullptr);
    }
    whitelistedVertexProperties_->push_back(
        new VertexProperty(*(parent_->getVertexProperties_(vertexId))));
  } else {
    // Set property at given position
    whitelistedVertexProperties_->operator[](vertexId) =
        new VertexProperty(*(parent_->getVertexProperties_(vertexId)));
  }
}


/**
 * \brief Add hyperedge property in ancestors
 *
 * \details
 * Add a hyperedge property for the given hyperedge identifier in this sub-
 * hypergraph and all its ancestor sub-hypergraphs (not the root hypergraph).
 *
 * \param hyperedgeId Hyperedge identifier
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
    VertexProperty, HyperedgeProperty, HypergraphProperty>::
addHyperedgePropertyInAncestors(const HyperedgeIdType& hyperedgeId) {
  // If at the given position there exists no nullptr then in the hierarchy
  // above there is no nullptr either
  if (hyperedgeId < whitelistedHyperedgeProperties_->size() and
      whitelistedHyperedgeProperties_->operator[](hyperedgeId) != nullptr) {
    return;
  }

  // Add hyperedge property to parent hypergraph if it is not the root
  auto undirectedSubHG = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
  Hyperedge_t, VertexProperty, HyperedgeProperty,
  HypergraphProperty>*>(parent_);
  if (undirectedSubHG) {  // parent is not the root hypergraph
    undirectedSubHG->addHyperedgePropertyInAncestors(hyperedgeId);
  }
  // Add hyperedge property to this sub-hypergraph
  if (hyperedgeId >= whitelistedHyperedgeProperties_->size()) {
    // fill gaps with nullptrs and then add the property
    size_t nbGaps = hyperedgeId - whitelistedHyperedgeProperties_->size();
    while (nbGaps-- > 0) {
      whitelistedHyperedgeProperties_->push_back(nullptr);
    }
    whitelistedHyperedgeProperties_->push_back(
        new HyperedgeProperty(
            *(parent_->getHyperedgeProperties_(hyperedgeId))));
  } else {
    // Set property at given position
    whitelistedHyperedgeProperties_->operator[](hyperedgeId) =
        new HyperedgeProperty(*(parent_->getHyperedgeProperties_(hyperedgeId)));
  }
}


/**
 * \brief Set pointer to parent hypergraph to nullptr
 *
 * \details
 * If the destructor of the root undirected hypergraph was called we invalidate
 * all parents of its child hierarchy by setting the respective pointer to the
 * parent hypergraph to nullptr.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
setParentOfChildsToNull() {
  throwExceptionOnNullptrParent();
  std::unordered_set<Observer*> observers;
  for (auto itEvent = this->observers_.begin();
       itEvent != this->observers_.end(); ++itEvent) {
    for (auto itObserver : itEvent->second) {
      observers.insert(itObserver);
    }
  }
  for (auto& observer : observers) {
    auto subgraph = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
            Hyperedge_t, VertexProperty, HyperedgeProperty,
            HypergraphProperty>*>(observer);
    subgraph->setParentOfChildsToNull();
  }

  // Detach as observer of parent graph
  parent_->DeleteObserver(this, ObservableEvent::VERTEX_REMOVED);
  parent_->DeleteObserver(this, ObservableEvent::HYPEREDGE_REMOVED);
  parent_->DeleteObserver(this, ObservableEvent::RESET_VERTEX_IDS);
  parent_->DeleteObserver(this, ObservableEvent::RESET_EDGE_IDS);
  // Set parent to nullptr
  parent_ = nullptr;
}



/**
 * \brief Throw an exception if the pointer to the parent is a nullptr
 *
 * \details
 * Throw a exception if one tries to use a member function on a
 * subgraph whose parent was set to nullptr previously.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
throwExceptionOnNullptrParent() const {
  if (parent_ == nullptr) {
    throw std::invalid_argument("Previously another hypergraph was assigned"
                                        " to the original parent of this"
                                        " sub-hypergraph. This sub-hypergraph"
                                        " has no parent hypergraph anymore and"
                                        " is thus in an invalid state.");
  }
}


/**
 * \brief Update the vertex container upon a call to resetVertexIds()
 *
 * \details
 * In response to the call resetVertexIds() in the root hypergraph, the
 * content of the whitelisted vertices must be updated.
 *
 * \par Complexity
 * Linear
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
updateVertexContainerUponResetVertexIds() {
  /*
   * Consider the containers of vertices in the root and sub-graphs, where
   * X corresponds to a nullptr and a number to the id of the vertex pointed
   * by the entry:
   *
   * graph:          |X|1|2|3|X|
   * subgraph1:      |X|X|2|3|X|
   * subsubgraph1_1: |X|X|X|3|X|
   * subgraph2:      |X|1|X|X|X|
   *
   * If the method resetVertexIds was called on the graph we expect the
   * following:
   *
   * graph:          |0|1|2|
   * subgraph1:      |X|1|2|
   * subsubgraph1_1: |X|X|2|
   * subgraph2:      |0|X|X|
   *
   * The method updateVertexContainerUponResetVertexIds in
   * UndirectedSubHypergraph notify its subgraphs about this change,
   * before changing its own container of vertices.
   */
  throwExceptionOnNullptrParent();

  // Notify subgraphs of this change
  this->NotifyObservers(ObservableEvent::RESET_VERTEX_IDS);

  if (whitelistedVerticesList_->empty()) {
    return;
  }
  std::vector<Vertex_t*> newWhitelistedVertexContainer;
  auto it = whitelistedVerticesList_->data();
  auto end = whitelistedVerticesList_->data() +
             whitelistedVerticesList_->size();
  for (; it != end; ++it) {
    const auto& vertex = *it;
    if (vertex != nullptr) {
      if (whitelistedVerticesList_->filter_out(vertex)) {
        newWhitelistedVertexContainer.push_back(nullptr);
      } else {
        newWhitelistedVertexContainer.push_back(vertex);
      }
    }
  }

  whitelistedVerticesList_ = create_filtered_vector<Vertex_t*>(
      parent_->getRootVerticesContainerPtr(),
      std::make_unique<WhitelistFilter<Vertex_t*>>(
          newWhitelistedVertexContainer));

  // Root hypergraph
  auto rootHypergraph = getRootHypergraph();
  // Update vertex properties (the parent hypergraph was not changed yet)
  std::vector<VertexProperty*> tmpProperties;
  auto itProp = whitelistedVertexProperties_->data();
  auto endProp = whitelistedVertexProperties_->data()
                 + whitelistedVertexProperties_->size();
  int idx = 0;
  for (; itProp != endProp; ++itProp) {
    const auto& vertexProp = *itProp;
    // Retain property
    if (vertexProp != nullptr) {
      tmpProperties.push_back(getVertexProperties_(idx));
    } else if (rootHypergraph->vertexById(idx) != nullptr) {
      // Property is not visible in this sub-hypergraph, but in the root
      // hypergraph
      tmpProperties.push_back(nullptr);
    }
    ++idx;
  }
  // Create new vertex property container
  whitelistedVertexProperties_ = create_filtered_vector<VertexProperty*>(
      create_filtered_vector<VertexProperty*>(),
      std::make_unique<NullptrFilter<VertexProperty*>>());
  for (const auto& item : tmpProperties) {
    whitelistedVertexProperties_->push_back(item);
  }
}


/**
 * \brief Update the hyperedge container upon a call to resetHyperedgeIds()
 *
 * \details
 * In response to the call resetHyperedgeIds() in the root hypergraph, the
 * content of the whitelisted hyperedges must be updated.
 *
 * \par Complexity
 * Linear
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
void UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
updateHyperedgeContainerUponResetHyperedgeIds() {
  /*
   * Consider the containers of hyperedges in the root and sub-graphs,
   * where X corresponds to a nullptr and a number to the id of the
   * hyperedge pointed by the entry:
   *
   * graph:          |X|1|2|3|X|
   * subgraph1:      |X|X|2|3|X|
   * subsubgraph1_1: |X|X|X|3|X|
   * subgraph2:      |X|1|X|X|X|
   *
   * If the method resetHyperedgeIds was called on the graph we expect the
   * following:
   *
   * graph:          |0|1|2|
   * subgraph1:      |X|1|2|
   * subsubgraph1_1: |X|X|2|
   * subgraph2:      |0|X|X|
   *
   * The method updateHyperedgeContainerUponResetHyperedgeIds in
   * UndirectedSubHypergraph notify its subgraphs about this change, before
   * changing its own container of vertices.
   */
  throwExceptionOnNullptrParent();

  // Notify subgraphs of this change
  this->NotifyObservers(ObservableEvent::RESET_EDGE_IDS);

  if (whitelistedHyperedgesList_->empty()) {
    return;
  }
  std::vector<Hyperedge_t*> newWhitelistedHyperedgeContainer;
  auto it = whitelistedHyperedgesList_->data();
  auto end = whitelistedHyperedgesList_->data() +
          whitelistedHyperedgesList_->size();
  for (; it != end; ++it) {
    const auto& hyperarc = *it;
    if (hyperarc != nullptr) {
      if (whitelistedHyperedgesList_->filter_out(hyperarc)) {
        newWhitelistedHyperedgeContainer.push_back(nullptr);
      } else {
        newWhitelistedHyperedgeContainer.push_back(hyperarc);
      }
    }
  }

  whitelistedHyperedgesList_ = create_filtered_vector<Hyperedge_t*>(
          parent_->getRootHyperedgeContainerPtr(),
          std::make_unique<WhitelistFilter<Hyperedge_t*>>(
                  newWhitelistedHyperedgeContainer));


  // Root hypergraph
  auto rootHypergraph = getRootHypergraph();
  // Update hyperedge properties (the parent hypergraph was not changed yet)
  std::vector<HyperedgeProperty*> tmpProperties;
  auto itProp = whitelistedHyperedgeProperties_->data();
  auto endProp = whitelistedHyperedgeProperties_->data()
                 + whitelistedHyperedgeProperties_->size();
  int idx = 0;
  for (; itProp != endProp; ++itProp) {
    const auto& hyperedgeProp = *itProp;
    // Retain property
    if (hyperedgeProp != nullptr) {
      tmpProperties.push_back(getHyperedgeProperties_(idx));
    } else if (rootHypergraph->hyperedgeById(idx) != nullptr) {
      // Property is not visible in this sub-hypergraph, but in the root
      // hypergraph
      tmpProperties.push_back(nullptr);
    }
    ++idx;
  }
  // Create new hyperedge property container
  whitelistedHyperedgeProperties_ = create_filtered_vector<HyperedgeProperty*>(
      create_filtered_vector<HyperedgeProperty*>(),
      std::make_unique<NullptrFilter<HyperedgeProperty*>>());
  for (const auto& item : tmpProperties) {
    whitelistedHyperedgeProperties_->push_back(item);
  }
}


/**
 * \brief Return shared_ptr to vertices container of the root hypergraph
 *
 * \return shared_ptr to vertices container of the root hypergraph
 *
 * \par Complexity
 * Linear in the number of levels in the hierarchy between this sub-hypergraph
 * and the root hypergraph.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::VertexContainerPtr
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
getRootVerticesContainerPtr() const {
  throwExceptionOnNullptrParent();
  return parent_->getRootVerticesContainerPtr();
}


/**
 * \brief Return shared_ptr to hyperedge container of the root hypergraph
 *
 * \return shared_ptr to hyperedge container of the root hypergraph
 *
 * \par Complexity
 * Linear in the number of levels in the hierarchy between this sub-hypergraph
 * and the root hypergraph.
 */
template <template<typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
typename UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::HyperedgeContainerPtr
UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
getRootHyperedgeContainerPtr() const {
  throwExceptionOnNullptrParent();
  return parent_->getRootHyperedgeContainerPtr();
}
}  // namespace hglib
