// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "UndirectedHypergraph.h"
#include "UndirectedSubHypergraph.h"

#include <unordered_set>

#include "filtered_vector.h"

namespace hglib {

/**
 * \brief UndirectedHypergraph constructor
 *
 * \details
 * Creates a UndirectedHypergraph allowing or not to have multiple
 * hyperedges with the same vertices. Vertices and then hyperedges
 * must be added via the functions addVertex() and addHyperedge().
 *
 * \param allowMultiHyperedges Flag to signal if multi-hyperedges are allowed.
 * Defaults to true.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
HyperedgeProperty, HypergraphProperty>::
UndirectedHypergraph(bool allowMultiHyperedges) :
        UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty, HypergraphProperty>(),
        Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
                HyperedgeProperty, HypergraphProperty>(allowMultiHyperedges),
        hyperedgeMaxId_(-1) {
  // create hyperedges_
  hyperedges_ = create_filtered_vector<Hyperedge_t*>(
          create_filtered_vector<Hyperedge_t*>(),
          std::make_unique<NullptrFilter<Hyperedge_t*>>());
  // create hyperedgeProperties_
  hyperedgeProperties_ = create_filtered_vector<HyperedgeProperty*>(
          create_filtered_vector<HyperedgeProperty*>(),
          std::make_unique<NullptrFilter<HyperedgeProperty*>>());
}


/**
 * \brief UndirectedHypergraph copy constructor
 *
 * \details
 * Makes a deep copy of the given undirected hypergraph.
 *
 * \param other The to be copied undirected hypergraph.
 *
 * \par Complexity
 * Linear in the number of vertices and hyperedges of other.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
UndirectedHypergraph(
        const UndirectedHypergraph& other) :
        UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty, HypergraphProperty>(),
        Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
                HyperedgeProperty, HypergraphProperty>(
                other),
        hyperedgeMaxId_(-1) {
  try {
    // create hyperedges_
    hyperedges_ = create_filtered_vector<Hyperedge_t*>(
            create_filtered_vector<Hyperedge_t*>(),
            std::make_unique<NullptrFilter<Hyperedge_t*>>());
    // create hyperedgeProperties_
    hyperedgeProperties_ = create_filtered_vector<HyperedgeProperty*>(
            create_filtered_vector<HyperedgeProperty*>(),
            std::make_unique<NullptrFilter<HyperedgeProperty*>>());
    // copy edges and associated edge properties
    for (std::size_t i = 0; i < other.hyperedges_->size(); ++i) {
      Hyperedge_t* hyperedge = other.hyperedges_->operator[](i);
      ++hyperedgeMaxId_;
      // hyperedge was deleted in other
      if (hyperedge == nullptr) {
        hyperedges_->push_back(nullptr);
        hyperedgeProperties_->push_back(nullptr);
      } else {
        Hyperedge_t* copy = new Hyperedge_t(*hyperedge);
        // add vertices to hyperedge
        for (const auto& vertex : *(hyperedge->vertices_.get())) {
          auto& v = this->vertices_->operator[](vertex->id());
          copy->addVertex(v);
          v->addHyperedge(copy);
        }
        // add hyperedge to hypergraph
        hyperedges_->push_back(copy);
        // copy hyperedge property
        HyperedgeProperty* hyperedgePropertyCopy = new HyperedgeProperty(
                *other.hyperedgeProperties_->operator[](i));
        hyperedgeProperties_->push_back(hyperedgePropertyCopy);
      }
    }
  } catch (std::bad_alloc& e) {
    for (auto& hyperedge : *(hyperedges_.get())) {
      delete hyperedge;
    }
    for (auto& hyperedgeProperty : *(hyperedgeProperties_.get())) {
      delete hyperedgeProperty;
    }
    throw;
  }
}

/**
 * \brief Conversion constructor
 *
 * \details
 * Allows to convert a graph to another graph with the same Vertex and Edge
 * types but another set of (Vertex-, Edge- and Graph-) Properties
 *
 * \param other Hypergraph to be copied
 */
template <template <typename> typename VertexTemplate_t,
          typename Hyperedge_t,
          typename VertexProperty,
          typename HyperedgeProperty,
          typename HypergraphProperty>
template <typename OtherVP, typename OtherEP, typename OtherGP>
UndirectedHypergraph<VertexTemplate_t,
                     Hyperedge_t,
                     VertexProperty,
                     HyperedgeProperty,
                     HypergraphProperty>::
UndirectedHypergraph(const UndirectedHypergraph<VertexTemplate_t,
                                                Hyperedge_t,
                                                OtherVP,
                                                OtherEP,
                                                OtherGP>& other) :
    UndirectedHypergraphInterface<VertexTemplate_t,
                                  Hyperedge_t,
                                  VertexProperty,
                                  HyperedgeProperty,
                                  HypergraphProperty>(),
    Hypergraph<VertexTemplate_t,
               Hyperedge_t,
               VertexProperty,
               HyperedgeProperty,
               HypergraphProperty>(other),
    hyperedgeMaxId_(-1) {
  try {
    // Initialise (empty) hyperedges_
    hyperedges_ = create_filtered_vector<Hyperedge_t*>(
        create_filtered_vector<Hyperedge_t*>(),
        std::make_unique<NullptrFilter<Hyperedge_t*>>());
    // Initialise (empty)  hyperedgeProperties_
    hyperedgeProperties_ = create_filtered_vector<HyperedgeProperty*>(
        create_filtered_vector<HyperedgeProperty*>(),
        std::make_unique<NullptrFilter<HyperedgeProperty*>>());
    // Copy edges and init associated edge properties
    for (std::size_t i = 0; i < other.hyperedges_->size(); ++i) {
      Hyperedge_t* hyperedge = other.hyperedges_->operator[](i);
      ++hyperedgeMaxId_;
      // if hyperedge was deleted in other
      if (hyperedge == nullptr) {
        hyperedges_->push_back(nullptr);
        hyperedgeProperties_->push_back(nullptr);
      } else {
        Hyperedge_t* copy = new Hyperedge_t(*hyperedge);
        // Add vertices to hyperedge
        for (const auto& vertex : *(hyperedge->vertices_.get())) {
          auto& v = this->vertices_->operator[](vertex->id());
          copy->addVertex(v);
          v->addHyperedge(copy);
        }
        // Add hyperedge to hypergraph
        hyperedges_->push_back(copy);
        // Init hyperedge property
        hyperedgeProperties_->push_back(new HyperedgeProperty());
      }
    }
  } catch (std::bad_alloc& e) {
    for (auto& hyperedge : *(hyperedges_.get())) {
      delete hyperedge;
    }
    for (auto& hyperedgeProperty : *(hyperedgeProperties_.get())) {
      delete hyperedgeProperty;
    }
    throw;
  }
}


/**
 * \brief Copy assignment operator
 *
 * \details
 * Replaces the contents (vertices, hyperedges, properties) of the undirected
 * hypergraph.
 *
 * \param source Another UndirectedHypergraph to use as data source.
 * \return
 * \code *this \endcode
 *
 * \par Complexity
 * Linear in the number of vertices and hyperedges of *this and source.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>& UndirectedHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
operator=(const UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>& source) {
  // check for self assignment
  if (this != &source) {
    // Call assignment oeprator of Hypergraph
    Hypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
            HyperedgeProperty, HypergraphProperty>::operator=(source);

    // Set the parent of all childs in the subgraph-hierarchy to null
    std::unordered_set<UndirectedSubHypergraph<VertexTemplate_t,
            Hyperedge_t, VertexProperty, HyperedgeProperty,
            HypergraphProperty>*> observers;
    for (auto itEvent = this->observers_.begin();
         itEvent != this->observers_.end(); ++itEvent) {
      for (auto itObserver : itEvent->second) {
        auto subgraph = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty,
                HypergraphProperty>*>(itObserver);
        observers.insert(subgraph);
      }
    }
    for (auto& subgraph : observers) {
      subgraph->setParentOfChildsToNull();
    }

    // deallocate hyperedges_
    for (auto& hyperedge : *(hyperedges_.get())) {
      delete hyperedge;
    }
    hyperedges_->clear();

    // deallocate hyperedgeProperties_
    for (auto& eProp : *(hyperedgeProperties_.get())) {
      delete eProp;
    }
    hyperedgeProperties_->clear();

    // re-init the edge max Id
    hyperedgeMaxId_ = -1;

    // copy edges and associated edge properties
    try {
      for (std::size_t i = 0; i < source.hyperedges_->size(); ++i) {
        Hyperedge_t* hyperedge = source.hyperedges_->operator[](i);
        ++hyperedgeMaxId_;
        // hyperedge was deleted in the source undirectedHypergraph
        if (hyperedge == nullptr) {
          hyperedges_->push_back(nullptr);
          hyperedgeProperties_->push_back(nullptr);
        } else {
          Hyperedge_t* copy = new Hyperedge_t(*hyperedge);
          // add vertices to hyperedge
          for (const auto& vertex : *(hyperedge->vertices_.get())) {
            auto& v = this->vertices_->operator[](vertex->id());
            copy->addVertex(v);
            v->addHyperedge(copy);
          }
          // add hyperedge to hypergraph
          hyperedges_->push_back(copy);
          // copy hyperedge property
          HyperedgeProperty* hyperedgePropertyCopy = new HyperedgeProperty(
                  *source.hyperedgeProperties_->operator[](i));
          hyperedgeProperties_->push_back(hyperedgePropertyCopy);
        }
      }
    } catch (std::bad_alloc& e) {
      for (auto& hyperedge : *(hyperedges_.get())) {
        delete hyperedge;
      }
      for (auto& hyperedgeProperty : *(hyperedgeProperties_.get())) {
        delete hyperedgeProperty;
      }
      throw;
    }
  }
  return *this;
}


/**
 * \brief UndirectedHypergraph destructor
 *
 * \details
 * Destructs the undirected hypergraph. The destructors of the elements are
 * called.
 *
 * \par Complexity
 * Linear in the number of vertices and hyperedges.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
~UndirectedHypergraph() {
  // Set the parent of all childs in the subgraph-hierarchy to null
  std::unordered_set<UndirectedSubHypergraph<VertexTemplate_t, Hyperedge_t,
      VertexProperty, HyperedgeProperty, HypergraphProperty>*> observers;
  for (auto itEvent = this->observers_.begin();
       itEvent != this->observers_.end(); ++itEvent) {
    for (auto itObserver : itEvent->second) {
      auto subgraph = dynamic_cast<UndirectedSubHypergraph<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>*>(itObserver);
      observers.insert(subgraph);
    }
  }
  for (auto& subgraph : observers) {
    subgraph->setParentOfChildsToNull();
  }

  // Delete pointers to hyperedges
  for (auto& hyperedge : *(hyperedges_.get())) {
    delete hyperedge;
  }
  // Delete pointers to hyperedge properties
  for (auto& hyperedgeProperty : *(hyperedgeProperties_.get())) {
    delete hyperedgeProperty;
  }
}


/**
 * \brief Check if multi-hyperedges are allowed in this hypergraph
 *
 * \return True if multi-hyperedges are supported, false otherwise.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
bool UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
allowMultiHyperedges() const {
  return this->allowMultiHyperedges_;
}


/**
 * \brief umber of hyperedges in the hypergraph
 *
 * \return Number of hyperedges in the hypergraph.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
nbHyperedges() const {
  size_t nbHyperedges = 0;
  auto it = hyperedges_->cbegin();
  auto end = hyperedges_->cend();
  for (; it != end; ++it) {
    ++nbHyperedges;
  }
  return nbHyperedges;
}


/**
 * \brief Size of the hyperedge container
 *
 * \details
 * Pointers to hyperedge objects are stored in a customized vector. If a
 * hyperedge is removed from the hypergraph, then the corresponding entry in
 * this vector is set to a nullptr. The size of the container remains unchanged.
 * Thus, the return value of this function may differ from nbHyperedges().
 *
 * @return Size of the hyperedge container.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
hyperedgeContainerSize() const {
  return hyperedges_->size();
}


/**
 * \brief Add a hyperedge with the provided vertices, and additional
 * parameters (if needed to create a hyperedge of type Hyperedge_t) to the
 * hypergraph
 */

/**
 * \brief Add a hyperedge
 *
 * Add a hyperedge with the provided vertex names, and additional parameters
 * (if needed to create a hyperedge of type Hyperedge_t) to the hypergraph.
 *
 * \param vertices Vertex names.
 * \param args Additional arguments to create a hyperedge of type Hyperedge_t.
 *
 * \return Pointer to const hyperedge on success, otherwise an exception is
 * thrown.
 *
 * \throw invalid_argument If no vertex with a given vertex name exists in the
 * hypergraph.
 * \throw invalid_argument If multi-hyperedges are not allowed and there exists
 * already a hyperedge with the same vertices in the hypergraph.
 *
 * \par Complexity
 * Linear in the number of vertices (search vertices with given names) and,
 * if multi-hyperedges are not allowed, linear in the number of hyperedges.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const Hyperedge_t* UndirectedHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
addHyperedge(decltype(hglib::NAME),
             const VertexNames& vertices,
             const typename Hyperedge_t::SpecificAttributes& args) {
  /*
   * Get first the ids of the vertices with given names. An invalid_argurment
   * exception is thrown if a vertex name does not exists in the hypergraph.
   * Then, call addHyperedge(VertexIds, args) to add the hyperedge.
   */
  std::vector<VertexIdType> vertexIds = this->getVertexIdsFromNames(vertices);
  return addHyperedge(vertexIds, args);
}


/**
 * \brief Add a hyperedge
 *
 * \details
 * Add a hyperedge with the provided vertex Ids, and additional parameters
 * (if needed to create a hyperedge of type Hyperedge_t) to the hypergraph.
 *
 * \param vertices Vertex Ids.
 * \param args Additional arguments to create a hyperedge of type Hyperedge_t.
 *
 * \return Pointer to const hyperedge on success, otherwise an exception is
 * thrown.
 *
 * \throw out_of_range If a given vertex Id is not in the range
 * [0, vertexContainerSize() )
 * \throw invalid_argument If a given vertex Id was removed before from the
 * hypergraph (The function vertexById() returns a nullptr in this case).
 * \throw invalid_argument If multi-hyperedges are not allowed and there exists
 * already a hyperedge with the same vertices in the hypergraph.
 *
 * \par Complexity
 * Linear in the number of vertices (search vertices with given
 * Ids in constant time) and, if multi-hyperedges are not allowed, linear in the
 * number of hyperedges.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const Hyperedge_t* UndirectedHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
addHyperedge(const VertexIds& vertices,
             const typename Hyperedge_t::SpecificAttributes& args) {
  // Check if all vertices are declared in the hypergraph.
  // If not, an exception is thrown.
  // In the case that multi-hyperedges are not allowed we check if there exists
  // already a hyperedge with the same vertices. If this is the case we
  // throw an exception. Otherwise and in case multi-hyperedges are allowed we
  // add the hyperedge to the hypergraph.

  // The following vector will hold pointers to the Vertex_t corresponding
  // to the provided vertex ids
  std::vector<const Vertex_t*> vertices_ptr_const;
  for (const auto& vertexId : vertices) {
    try {
      const auto &vertex = this->vertexById(vertexId);
      if (vertex == nullptr) {
        throw std::invalid_argument("Error in UndirectedHypergraph::"
                                        "addHyperedge(): Vertex " +
                                    std::to_string(vertexId) +
                                    " is not declared in this hypergraph.");
      }
      vertices_ptr_const.push_back(vertex);
    } catch (const std::out_of_range& oor) {
      throw std::out_of_range("Error in UndirectedHypergraph::addHyperedge(): "
                                  "Vertex " + std::to_string(vertexId) +
                              " is not declared in this hypergraph.");
    }
  }

  if (not this->allowMultiHyperedges_) {
    // check if there exists a hyperedge with the same vertices
    for (const auto& hyperedge : *(hyperedges_.get())) {
      if (hyperedge != nullptr and
              hyperedge->UndirectedHyperedgeBase<Vertex_t>::
                  compare(vertices_ptr_const)) {
        // build error message
        std::string verticesString("");
        for (auto& vertexId : vertices) {
          verticesString += std::to_string(vertexId) + ", ";
        }
        std::string errorMessage("Multiple hyperedges are not allowed in this "
                                         "hypergraph, but the hyperedge with "
                                         "vertices ids: " + verticesString +
                                 " already exists.\n");
        throw std::invalid_argument(errorMessage);
      }
    }
  }

  // Non-const pointers are needed for the Hyperedge_t ctor
  std::unique_ptr<GraphElementContainer<Vertex_t*>> vertices_ptr =
          create_filtered_vector<Vertex_t*>();
  for (const Vertex_t* vPtr : vertices_ptr_const) {
    vertices_ptr->push_back(const_cast<Vertex_t*>(vPtr));
  }
  // add hyperedge
  const auto hyperedge = buildHyperedgeAndItsDependencies(
          std::move(vertices_ptr), args);
  return hyperedge;
}


/**
 * \brief Remove a hyperedge with the given id from the hypergraph
 *
 * \details
 * This function removes a hyperedge with the given id from the hypergraph. This
 * hyperedge \em e is also removed from the hyperedges of the
 * vertices of \em e. This change is propagated to all views (sub-hypergraphs).
 *
 * \param hyperedgeId Identifier of the hyperedge.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Linear in the number of vertices of the hyperedge.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
removeHyperedge(const HyperedgeIdType& hyperedgeId) {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   *
   * Remove hyperedge h with given id from the hyperedge list
   * of the vertices of h.
   * Delete the pointer at index hyperedgeId in hyperedges_ and
   * hyperedgeProperties_ and assign nullptr.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "removeHyperedge with "
                                       "the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }

    HyperedgeIdType idx(hyperedgeId);
    removeHyperedgeDependencies(*hyperedge);
    // Notify observers of change
    this->NotifyObservers(ObservableEvent::HYPEREDGE_REMOVED, &idx);

    // delete hyperedge
    delete hyperedges_->operator[](idx);
    hyperedges_->operator[](idx) = nullptr;
    // assign nullptr in hyperedgeProperties_ at the index that corresponds
    // to the id of the hyperedge
    delete hyperedgeProperties_->operator[](idx);
    hyperedgeProperties_->operator[](idx) = nullptr;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "removeHyperedge: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Search hyperedge list for an hyperedge with the provided id
 *
 * \param hyperedgeId Identifier of the hyperedge.
 *
 * \return Pointer to hyperedge if found, nullptr if hyperedgeId is between 0
 * and hyperedgeContainerSize() - 1, throw out_of_range exception otherwise
 *
 * \throw out_of_range If given Id is not in the range
 * [0, hyperedgeContainerSize() )
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const Hyperedge_t* UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
hyperedgeById(const HyperedgeIdType& hyperedgeId) const {
  try {
    return hyperedges_->at(hyperedgeId);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "hyperedgeById: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  }
}


/**
 * \brief Const hyperedge iterator pointing to the begin of the hyperedges
 *
 * \return Const hyperedge iterator pointing to the begin of the hyperedges.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the hyperedge container.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator UndirectedHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
hyperedgesBegin() const {
  return hyperedges_->cbegin();
}


/**
 * \brief Const hyperedge iterator pointing to the end of the hyperedges
 *
 * \return Const hyperedge iterator pointing to the end of the hyperedges.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator UndirectedHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
hyperedgesEnd() const {
  return hyperedges_->cend();
}


/**
 * Begin/End iterator pair of the hyperedges of the hypergraph
 *
 * \details
 * Pair of hyperedge iterators pointing to the begin and the end of the
 * hyperedges of the hypergraph.
 *
 * \return Pair of hyperedge iterators pointing to the begin and the end of the
 * hyperedges of the hypergraph.
 *
 * \par Complexity
 * Linear in the number of leading nullptr entries in the hyperedge container.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::pair<typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator,
        typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::hyperedge_iterator>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesBeginEnd() const {
  return std::make_pair(hyperedgesBegin(), hyperedgesEnd());
}


/**
 * \brief Return a reference to the container of hyperedges
 *
 * \details
 * Can be used in a range based for loop as follows:
 * \code
 * for (const auto& hyperedge : g.hyperedges()) {
 *   std::cout << "Id of the hyperedge: " << hyperedge->id() << '\n';
 * }
 * \endcode
 *
 * \return Reference to hyperedge container
 *
 * \par Complexity
 * Constant (return the reference). However, the complexity to iterate over all
 * hyperedges of a hypergraph using a range based for loop or a
 * classic for loop using hyperedgesBegin() and hyperedgesEnd() is the same.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::HyperedgeContainer&
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedges() const {
  return *(hyperedges_.get());
}


/**
 * \brief Return number of vertices in the hyperedge with the given id
 *
 * \param hyperedgeId Identifier of a hyperedge.
 *
 * \return Number of vertices in the hyperedge with the given id.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
nbImpliedVertices(const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::nbVertices "
                                       "with the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperedge->vertices_->size();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "nbVertices: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Check if the hyperedge with the given id has vertices
 *
 * \param hyperedgeId Identifier of a hyperedge.
 *
 * \return True if the hyperedge with the given identifier has vertices,
 * false otherwise.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
bool UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hasVertices(const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::hasVertices "
                                       "with the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return not hyperedge->vertices_->empty();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "hasVertices: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Const vertex iterator pointing to the begin of the implied vertices
 *
 * \details
 * Const vertex iterator pointing to the begin of the vertices of the
 * hyperedge with the given identifier.
 *
 * \param hyperedgeId Identifier of a hyperedge.
 *
 * \return Const vertex iterator pointing to the begin of the vertices of
 * the hyperedge with the given identifier.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::vertex_iterator
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
impliedVerticesBegin(const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "verticesBegin with the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperedge->vertices_->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "verticesBegin: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}

/**
 * \brief Const vertex iterator pointing to the end of the implied vertices
 *
 * \details
 * Const vertex iterator pointing to the end of the vertices of the
 * hyperedge with the given identifier.
 *
 * \param hyperedgeId Identifier of a hyperedge.
 *
 * \return Const vertex iterator pointing to the end of the vertices of
 * the hyperedge with the given identifier.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::vertex_iterator
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
impliedVerticesEnd(const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "verticesEnd with the hyperedge id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperedge->vertices_->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "verticesEnd: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Pair of vertex iterators pointing to the begin/end of the implied
 * vertices
 *
 * \details
 * This function returns a pair of const vertex iterators pointing to the begin
 * and the end of the implied vertices of the hyperarc with the given
 * identifier.
 *
 * \param hyperedgeId Identifier of a hyperedge.
 * \return Pair of const vertex iterators pointing to the begin and the end of
 * the implied vertices of the hyperedge with the given identifier.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::pair<typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::vertex_iterator,
        typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::vertex_iterator>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
impliedVerticesBeginEnd(const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "verticesBeginEnd with the hyperedge"
                                       " id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return std::make_pair(hyperedge->vertices_->cbegin(),
                          hyperedge->vertices_->cend());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "verticesBeginEnd: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Get shared_ptr of the container of implied vertices
 *
 * \details
 * This function returns a shared_ptr of the container of the implied vertices
 * of a hyperedge with the given Id. It can be used in a range based for loop
 * as follows:
 * \code
 * auto verticesPtr = g.impliedVertices(hyperedgeId);
 * for (const auto& vertex : *verticesPtr) {
 *   std::cout << vertex->id() << " is a vertex of the hyperedge "
 *             << hyperedgeId << '\n';
 * }
 * \endcode
 *
 * \param hyperedgeId Identifier of a hyperedge.
 * \return Shared_ptr of the container of the implied vertices
 * of a hyperedge with the given Id.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<typename UndirectedHypergraph<
    VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
    HypergraphProperty>::Vertex_t*>> UndirectedHypergraph<VertexTemplate_t,
    Hyperedge_t, VertexProperty, HyperedgeProperty, HypergraphProperty>::
impliedVertices(const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the hyperedge with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "vertices with the hyperedge"
                                       " id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return hyperedge->vertices_;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "vertices: index = " << hyperedgeId << "; min = 0; max = "
              << hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Establish consecutive hyperedge ids
 *
 * \details
 * After deleting hyperedges from the hypergraph, the hyperedge ids
 * are not consecutive anymore. This function reassigns
 * consecutive ids to the hyperedges.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
resetHyperedgeIds() {
  // Notify observers
  this->NotifyObservers(ObservableEvent::RESET_EDGE_IDS);
  // erase all nullptrs
  // remove nullptrs from underlying vector: std::remove puts all nullptrs at
  // the end of the vector
  auto posFirstNullptr = std::remove(hyperedges_->data(),
                                     hyperedges_->data() +
                                     hyperedges_->size(),
                                     static_cast<Hyperedge_t*>(NULL));
  // Compute nb of nullptr
  size_t nbNullptr = (hyperedges_->data() + hyperedges_->size()) -
                     posFirstNullptr;
  // Resize
  hyperedges_->resize(hyperedges_->size() - nbNullptr);

  // assign new Ids
  HyperedgeIdType id(-1);
  for (auto it = hyperedges_->begin(); it != hyperedges_->end();
       ++it) {
    (*it)->setId(++id);  // assign new Id
  }

  // set vertexMaxId_ to the highest assigned id
  hyperedgeMaxId_ = id;

  // remove nullptr entries from vector of hyperarc properties
  removeNullEntriesFromHyperedgeProperties();
}


/**
 * \brief Establish consecutive vertex and hyperedge ids
 *
 * \details
 * After deleting vertices and hyperedges from the hypergraph the vertex and
 * hyperedge ids are not consecutive anymore. This function reassigns
 * consecutive ids to the vertices and the hyperedges.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
resetIds() {
  this->resetVertexIds();
  resetHyperedgeIds();
}



/**
 * \brief Number of hyperedges that contain the vertex with the given Id
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Number of hyperedges that contain the vertex with the given Id.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
size_t UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
nbHyperedges(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "nbHyperedges "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->hyperedges_->size();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "nbHyperedges: index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Check if the vertex with the given Id is part of a hyperedge
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return True if the vertex with the given identifier is part of a hyperedge,
 * false otherwise.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
bool UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
isContainedInAnyHyperedge(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "isContainedInAnyHyperedge "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return not vertex->hyperedges_->empty();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "isContainedInAnyHyperedge: index = " << vertexId << "; min = 0; "
            "max = " << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Iterator pointing to the begin of the hyperedges that contain the
 * vertex
 *
 * \details
 * This function returns a const hyperedge iterator pointing to the begin of
 * the hyperedges that contain the vertex with the given identifier.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Const hyperarc iterator pointing to the begin of the hyperedges that
 * contain the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator UndirectedHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
hyperedgesBegin(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "hyperedgesBegin "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->hyperedges_->cbegin();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "hyperedgesBegin: index = " << vertexId << "; min = 0; "
                      "max = " << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Iterator pointing to the end of the hyperedges that contain the vertex
 *
 * \details
 * This function returns a const hyperedge iterator pointing to the end of
 * the hyperedges that contain the vertex with the given identifier.
 *
 * \param vertexId Identifier of a vertex.
 *
 * \return Const hyperarc iterator pointing to the end of the hyperedges that
 * contain the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator UndirectedHypergraph<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty>::
hyperedgesEnd(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "hyperedgesEnd "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->hyperedges_->cend();
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "hyperedgesEnd: index = " << vertexId << "; min = 0; "
                      "max = " << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Pair of iterators pointing to the begin/end of the vertex's hyperedges
 *
 * \details
 * This function returns a pair of const hyperedge iterators pointing to the
 * begin and the end of the hyperedges that contain the vertex with the given
 * identifier.
 *
 * \param vertexId Identifier of a vertex.
 * \return Pair of const hyperedge iterator pointing to the begin/end of the
 * hyperedges that contain the vertex with the given identifier.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::pair<typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>::hyperedge_iterator,
        typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
                VertexProperty, HyperedgeProperty,
                HypergraphProperty>::hyperedge_iterator>
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
hyperedgesBeginEnd(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "hyperedgesBeginEnd "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return std::make_pair(vertex->hyperedges_->cbegin(),
                          vertex->hyperedges_->cend());
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "hyperedgesBeginEnd: index = " << vertexId << "; min = 0; "
                      "max = " << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Get shared_ptr of container of hyperedges that contain the vertex
 *
 * \details
 * This function returns a shared_ptr of the container of the hyperedges that
 * contain the vertex with the given Id. This allows the use of a range based
 * for loop as follows:
 * \code
 * auto hyperedgesPtr = g.hyperedges(vertexId);
 * for (const auto& edge : *hyperedgesPtr) {
 *   std::cout << edge->id() << " is a hyperedge of vertex "
 *             << vertexId << '\n';
 * }
 * \endcode
 *
 * \param vertexId Identifier of a vertex.
 * \return shared_ptr of the container of the hyperedges that contain the vertex
 * with the given Id.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
std::shared_ptr<const GraphElementContainer<
    Hyperedge_t*>> UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
    VertexProperty, HyperedgeProperty, HypergraphProperty>::
hyperedges(const VertexIdType& vertexId) const {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "hyperedges "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    return vertex->hyperedges_;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "hyperedges: index = " << vertexId << "; min = 0; "
                      "max = " << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Remove a vertex with the given name from the hypergraph
 *
 * \details
 * This function removes a vertex with the given name from the undirected
 * hypergraph. Implied hyperedges of this vertex are also removed
 * if the second parameter is set to true.
 *
 * \param vertexName Name of a vertex.
 * \param removeImpliedHyperedges Flag that shows whether hyperedges that
 * contain the vertex with the given name should be removed from the hypergraph.
 * Defaults to true.
 *
 * \throw invalid_argument If the undirected hypergraph contains no vertex with
 * the given name.
 *
 * \par Complexity
 * Linear.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
removeVertex(const std::string& vertexName,
             bool removeImpliedHyperedges) {
  // Find vertex by name in the hypergraph and call the method removeVertex
  // that takes the id of the vertex
  const auto& vertex = this->vertexByName(vertexName);
  if (vertex == nullptr) {
    std::string errorMessage("Called UndirectedHypergraph::removeVertex"
                                     " with the vertex name " + vertexName +
                             " which does not exists in the undirected "
                                     "hypergraph.\n");
    throw std::invalid_argument(errorMessage);
  }

  removeVertex(vertex->id(), removeImpliedHyperedges);
}


/**
 * \brief Remove a vertex with the given Id from the hypergraph
 *
 * \details
 * This function removes a vertex with the given Id from the undirected
 * hypergraph. Implied hyperedges of this vertex are also removed
 * if the second parameter is set to true.
 *
 * \param vertexId Identifier of a vertex.
 * \param removeImpliedHyperedges Flag that shows whether hyperedges that
 * contain the vertex with the given name should be removed from the hypergraph.
 * Defaults to true.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 *
 * \par Complexity
 * Linear.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
removeVertex(const VertexIdType& vertexId,
             bool removeImpliedHyperedges) {
  /*
   * Get first the pointer to the vertex with the given id.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   *
   * Remove all implied hyperedges if flag is set to true (default).
   * Otherwise, remove only the vertex v with given id 'vertexId' from the
   * vetices list of the hyperedges of v. Remove hyperedges that have no
   * vertices.
   *
   * Delete the pointer at index vertexId in vertices_ and vertexProperties_
   * and assign nullptr.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::removeVertex"
                                       " with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }

    if (removeImpliedHyperedges) {
      // hyperedges that will be removed from the hypergraph
      std::vector<Hyperedge_t*> hyperedgesToBeRemoved;
      for (auto& hyperedge : *(hyperedges_.get())) {
        // all hyperedges that contain the vertex will be removed from the
        // hypergraph
        if (hyperedge != nullptr and isVertexOfHyperedge(vertexId,
                                                         hyperedge->id())) {
          hyperedgesToBeRemoved.emplace_back(hyperedge);
        }
      }
      // remove hyperedges from hypergraph
      for (auto& hyperedge : hyperedgesToBeRemoved) {
        removeHyperedge(hyperedge->id());
      }
    } else {
      // remove vertex from vertices_ of its hyperedges
      removeVertexDependencies(*vertex);
      // remove hyperedges without vertices
      for (auto& hyperedge : *(hyperedges_.get())) {
        if (hyperedge != nullptr and not hasVertices(hyperedge->id())) {
          removeHyperedge(hyperedge->id());
        }
      }
    }

    VertexIdType idx(vertexId);
    // Notify observers of change
    Remove_vertex_args args;
    args.vertexId_ = idx;
    args.removeImpliedHyperarcs_ = removeImpliedHyperedges;
    this->NotifyObservers(ObservableEvent::VERTEX_REMOVED, &args);

    // assign NULL in vertices_ and vertexProperties_ at the index that
    // corresponds to the id of the vertex
    this->setVertexAndVertexPropertyEntryToNullptr(idx);
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "removeVertex:"
            " index = " << vertexId << "; min = 0; max = "
              << this->vertices_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Check if the given vertex is part of the given hyperedge
 *
 * \details
 * This function checks whether the vertex with the given vertex Id is a vertex
 * of the hyperedge with the given hyperedge Id.
 *
 * \param vertexId Identifier of a vertex.
 * \param hyperedgeId Identifier of a hyperedge.
 *
 * \return True if the vertex is a part of the hyperedge, false otherwise.
 *
 * \throw out_of_range If the given vertex Id is not in the range
 * [0, vertexContainerSize() ) or the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 * \throw invalid_argument If there is a nullptr in the vertex container at
 * the position with the given vertex Id.
 * \throw invalid_argument If there is a nullptr in the hyperedge container at
 * the position with the given hyperedge Id.
 *
 * \par Complexity
 * Linear in the number of vertices of the given hyperedge.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
bool UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
        HyperedgeProperty, HypergraphProperty>::
isVertexOfHyperedge(const VertexIdType& vertexId,
                    const HyperedgeIdType& hyperedgeId) const {
  /*
   * Get first the pointer to the vertex/hyperedge with the given ids.
   * Throw invalid_argument exception if a nullptr is returned,
   * which corresponds to the case that the vertex/hyperedge with the given id
   * was removed from the hypergraph before.
   * Catch IndexOutOfRangeException if given id is out of bound.
   */
  try {
    const auto& vertex = this->vertexById(vertexId);
    if (vertex == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "isVertexOfHyperedge "
                                       "with the vertex id ");
      errorMessage += std::to_string(vertexId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }
    const auto& hyperedge = hyperedgeById(hyperedgeId);
    if (hyperedge == nullptr) {
      std::string errorMessage("Called UndirectedHypergraph::"
                                       "isVertexOfHyperedge with the hyperedge"
                                       " id ");
      errorMessage += std::to_string(hyperedgeId);
      errorMessage += " which does not exists in the undirected hypergraph.\n";
      throw std::invalid_argument(errorMessage);
    }

    for (const auto& v : *(hyperedge->vertices_.get())) {
      if (v->id() == vertex->id()) {
        return true;
      }
    }
    return false;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "isVertexOfHyperedge: " << '\n' <<
              "index vertex = " << vertexId << "; min = 0; max = " <<
              this->vertices_->size() << '\n' <<
              "index hyperedge = " << hyperedgeId << "; min = 0; max = " <<
              hyperedges_->size() << '\n';
    throw;
  } catch (const std::invalid_argument& invalid_argument) {
    throw;
  }
}


/**
 * \brief Get properties of the hyperedge with the provided id
 *
 * \details
 * This function returns a pointer to a const (read only) hyperedge property
 * associated to the hyperedge with the specified hyperedge Id. If the hyperedge
 * was removed earlier from the network, then this function returns a nullptr.
 *
 * \param hyperedgeId Identifier of the hyperedge.
 *
 * \return Pointer to const hyperedge property type, nullptr if the hyperedge
 * was removed earlier from the undirected hypergraph.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const HyperedgeProperty* UndirectedHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty, HypergraphProperty>::
getHyperedgeProperties(const HyperedgeIdType& hyperedgeId) const {
  try {
    const auto& property = hyperedgeProperties_->at(hyperedgeId);
    if (property == nullptr) {
      throw std::invalid_argument("Error in UndirectedHypergraph::"
                                      "getHyperedgeProperties(): Hyperedge Id "
                                  + std::to_string(hyperedgeId)
                                  + " is not declared in this hypergraph.");
    }
    return property;
  } catch (const std::out_of_range& oor) {
    std::cerr << "IndexOutOfRangeException in UndirectedHypergraph::"
            "getHyperedgeProperties: index = " << hyperedgeId <<
              "; min = 0; " <<
              "max = " << hyperedgeProperties_->size() << '\n';
    throw;
  }
}


/**
 * \brief Get properties of the hyperedge with the provided id
 *
 * \details
 * This function returns a pointer to a hyperedge property
 * associated to the hyperedge with the specified hyperedge Id. If the hyperedge
 * was removed earlier from the network, then this function returns a nullptr.\n
 * This is a non-const version of getHyperedgeProperties().
 *
 * \param hyperedgeId Identifier of the hyperedge.
 *
 * \return Pointer to hyperedge property type, nullptr if the hyperedge
 * was removed earlier from the undirected hypergraph.
 *
 * \throw out_of_range If the given hyperedge Id is not in the range
 * [0, hyperedgeContainerSize() ).
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
HyperedgeProperty* UndirectedHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty, HypergraphProperty>::
getHyperedgeProperties_(const HyperedgeIdType& hyperedgeId) const {
  try {
    return const_cast<HyperedgeProperty*>(this->getHyperedgeProperties(
            hyperedgeId));
  } catch (const std::out_of_range& oor) {
    throw std::out_of_range("Error in UndirectedHypergraph::"
                                "getHyperedgeProperties_(): Hyperedge Id "
                                + std::to_string(hyperedgeId)
                                + " is not declared in this hypergraph.");
  } catch (const std::invalid_argument& ia) {
    throw std::invalid_argument("Error in UndirectedHypergraph::"
                                    "getHyperedgeProperties_(): Hyperedge Id "
                                + std::to_string(hyperedgeId)
                                + " is not declared in this hypergraph.");
  }
}


/**
 * \brief Get root hypergraph
 *
 * \details
 * A undirected hypergraph can have undirected sub-hypergraphs (views). This
 * hierarchy is implemented through the observer and decorator pattern. A
 * subgraph observes its parent graph. This function returns the root
 * hypergraph of the hierarchy.
 *
 * \return
 * \code this \endcode
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const UndirectedHypergraphInterface<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty,
        HypergraphProperty>* UndirectedHypergraph<VertexTemplate_t,
        Hyperedge_t, VertexProperty, HyperedgeProperty, HypergraphProperty>::
getRootHypergraph() const {
  return this;
}


/* **************************************************************************
 * **************************************************************************
 *                        Protected methods
 * **************************************************************************
 * *************************************************************************/

/**
 * \brief Construct a hyperedge and a HyperedgeProperty object
 *
 * \details
 * Creates a hyperedge object of type Hyperedge_t and hyperedge dependencies are
 * solved, e.g. the hyperedge is added to the hyperedges of its vertices.
 * A HyperedgeProperty object is created via the default constructor. Both, the
 * hyperedge and the HyperedgeProperty are added to the undirected hypergraph
 * and can be accessed via the assigned Id of the hyperedge.
 *
 * \param vertices Unique_ptr to container of pointers to implied vertices.
 * \param attributes Additional parameters that are needed to build a hyperedge
 * of type Hyperedge_t.
 *
 * \return Pointer to const Hyperedge_t.
 *
 * \par Complexity
 * The time complexity depends on the constructors of Hyperedge_t and
 * HyperedgeProperty.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
const Hyperedge_t* UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
buildHyperedgeAndItsDependencies(
        std::unique_ptr<GraphElementContainer<Vertex_t*>> vertices,
        const typename Hyperedge_t::SpecificAttributes& attributes) {
  // A hyperedge x is created and a
  // pointer to this hyperedge is added to the list of hyperedges of the
  // hypergraph. Hyperedge dependencies are solved:
  // 1. Pointer to hyperedge x is added to the hyperedge list of the
  // vertices of x.
  // A hyperedge property object is created using the default ctor.

  // create the hyperedge and add it to the list of hyperedges of the
  // hypergraph
  auto attr_p =
      std::make_unique<typename Hyperedge_t::SpecificAttributes>(attributes);
  auto hyperedge = new Hyperedge_t(++hyperedgeMaxId_, std::move(vertices),
                                   std::move(attr_p));
  hyperedges_->push_back(hyperedge);

  // Solve hyperedge dependencies
  // add hyperedge to hyperedges of the vertices of the hyperedge
  for (auto&& vertex : *(hyperedge->vertices_.get())) {
    vertex->addHyperedge(hyperedge);
  }

  // build hyperedge property and add it to the list of hyperedge properties
  auto hyperedgeProperty = new HyperedgeProperty();
  hyperedgeProperties_->push_back(hyperedgeProperty);
  return hyperedge;
}


/**
 * \brief Remove vertex dependencies
 *
 * \details
 * Vertex dependencies of vertex \em v are removed, that is the vertex \em v is
 * removed from the list of vertices of the hyperedges that contain \em v.
 *
 * \param vertex Const reference to a vertex.
 *
 * \par Complexity
 * Linear in the number of hyperedges of vertex \em v and
 * the respective number of implied vertices in these hyperedges.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
removeVertexDependencies(const Vertex_t& vertex) {
  // remove the vertex from the list of vertices of the vertex's hyperedge list
  for (auto&& edge : *(vertex.hyperedges_.get())) {
    edge->removeVertex(vertex);
  }
}


/**
 * \brief Remove hyperedge dependencies
 *
 * \details
 * This function removes hyperedge dependencies of hyperedge \em e, that is the
 * hyperedge \em e is removed from the container of hyperedges of the implied
 * vertices of hyperedge \em e.
 *
 * \param hyperedge Const reference to a hyperedge.
 *
 * \par Complexity
 * Linear in the number of vertices of hyperedge \em e and
 * the respective number of hyperedges that contain these vertices.
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
removeHyperedgeDependencies(const Hyperedge_t& hyperedge) {
  // remove hyperedge e from the hyperedge list of e's vertices
  for (auto&& vertex : *(hyperedge.vertices_.get())) {
    vertex->removeHyperedge(hyperedge);
  }
}


/**
 * \brief Remove nullptr entries from hyperedge properties container
 *
 * \details
 * Remove nullptr entries from hyperedge properties container. This function
 * changes the size of the hyperedge properties container.
 *
 * \par Complexity
 * Linear
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
void UndirectedHypergraph<VertexTemplate_t, Hyperedge_t,
        VertexProperty, HyperedgeProperty, HypergraphProperty>::
removeNullEntriesFromHyperedgeProperties() {
  // remove nullptrs from underlying vector: std::remove puts all nullptrs at
  // the end of the vector
  auto posFirstNullptr = std::remove(hyperedgeProperties_->data(),
                                     hyperedgeProperties_->data() +
                                     hyperedgeProperties_->size(),
                                     static_cast<HyperedgeProperty*>(NULL));
  // Compute nb of nullptr
  size_t nbNullptr = (hyperedgeProperties_->data() +
                      hyperedgeProperties_->size()) - posFirstNullptr;
  // Resize
  hyperedgeProperties_->resize(hyperedgeProperties_->size() - nbNullptr);
}


/**
 * \brief Return shared_ptr to hyperedge container
 *
 * \details
 * A UndirectedSubHypergraph, a view of a hypergraph, is implemented as a
 * combination of the observer and the decorator pattern. The hyperedge
 * container of the hypergraph is decorated with a particular filter in the
 * UndirectedSubHypergraph.\n
 * This function provides access to the hyperedge container.
 *
 * \return Shared_ptr to the hyperedge container.
 *
 * \par Complexity
 * Constant
 */
template <template <typename> typename VertexTemplate_t, typename Hyperedge_t,
        typename VertexProperty, typename HyperedgeProperty,
        typename HypergraphProperty>
typename UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
    HyperedgeProperty, HypergraphProperty>::HyperedgeContainerPtr
UndirectedHypergraph<VertexTemplate_t, Hyperedge_t, VertexProperty,
    HyperedgeProperty, HypergraphProperty>::
getRootHyperedgeContainerPtr() const {
  return hyperedges_;
}
};  // namespace hglib
