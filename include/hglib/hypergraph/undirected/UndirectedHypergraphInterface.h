// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_UNDIRECTEDHYPERGRAPHINTERFACE_H
#define HGLIB_UNDIRECTEDHYPERGRAPHINTERFACE_H

#include <string>
#include <vector>
#include <cstddef>

#include "hglib/hypergraph/HypergraphInterface.h"
#include "hglib/hyperedge/undirected/ArgumentsToCreateUndirectedHyperedge.h"
#include "hglib/utils/types.h"

namespace hglib {

/**
 * \brief This abstract class defines the interface of an undirected hypergraph
 *
 * \details
 * This interface is currently implemented by UndirectedHypergraph and
 * UndirectedSubHypergraph.
 *
 * @tparam VertexTemplate_t Vertex type
 * @tparam Hyperedge_type Hyperedge type
 * @tparam VertexProperty Vertex property type
 * @tparam HyperedgeProperty Hyperedge property type
 * @tparam HypergraphProperty Hypergraph property type
 */
template <template <typename> typename VertexTemplate_t,
        typename Hyperedge_t,
        typename VertexProperty,
        typename HyperedgeProperty,
        typename HypergraphProperty>
class UndirectedHypergraphInterface : virtual public HypergraphInterface<
        VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
        HypergraphProperty> {
 public:
  /// Alias for the directed vertex type
  using Vertex_t = typename HypergraphInterface<VertexTemplate_t, Hyperedge_t,
          VertexProperty, HyperedgeProperty, HypergraphProperty>::Vertex_t;
  /// Alias for Hyperedge type
  using Hyperedge_type = Hyperedge_t;
  /// Alias for Hyperedge property type
  using HyperedgeProperty_t = HyperedgeProperty;
  /// Alias for conditional range of a container of Vertex_t ptr
  using VertexContainer = typename HypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexContainer;
  /// Alias for conditional range of a container of Hyperedge_t ptr
  using HyperedgeContainer = GraphElementContainer<Hyperedge_t*>;
  /// Alias for iterator over a container of vertices
  using vertex_iterator = typename HypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::vertex_iterator;
  /// Alias for iterator over a container of hyperedges
  using hyperedge_iterator = typename GraphElementContainer<
          Hyperedge_t*>::const_iterator;
  /// \brief Alias for the data container that stores the names of vertices
  /// of a hyperedge
  using VertexNames = std::vector<std::string>;
  /// \brief Alias for the data container that stores the ids of vertices
  /// of a hyperedge
  using VertexIds = std::vector<VertexIdType>;
  /// Alias for unique_ptr of GraphElementContainer<Vertex_t*>
  using VertexContainerPtr = typename HypergraphInterface<VertexTemplate_t,
          Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<VertexProperty*>
  using VertexPropertyContainerPtr = typename HypergraphInterface<
          VertexTemplate_t, Hyperedge_t, VertexProperty, HyperedgeProperty,
          HypergraphProperty>::VertexPropertyContainerPtr;
  /// Alias for unique_ptr of GraphElementContainer<Hyperedge_t*>
  using HyperedgeContainerPtr =
      std::shared_ptr<GraphElementContainer<Hyperedge_t*>>;
  /// Alias for unique_ptr of GraphElementContainer<HyperedgeProperty*>
  using HyperedgePropertyContainerPtr =
      std::shared_ptr<GraphElementContainer<HyperedgeProperty*>>;

  /* **************************************************************************
   * **************************************************************************
   *                    Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Destructor
  virtual ~UndirectedHypergraphInterface() = default;

  /* **************************************************************************
   * **************************************************************************
   *                    General
   * **************************************************************************
   * *************************************************************************/
  /// Check if multi-hyperedges are allowed in this hypergraph
  virtual bool allowMultiHyperedges() const = 0;

  /* **************************************************************************
  * **************************************************************************
  *                        Hyperedge access
  * **************************************************************************
  * *************************************************************************/
 public:
  /// Number of hyperedges in the hypergraph
  virtual size_t nbHyperedges() const = 0;
  /// Size of the hyperedge container
  virtual size_t hyperedgeContainerSize() const = 0;

  /// Add a hyperedge
  virtual const Hyperedge_t* addHyperedge(
          decltype(hglib::NAME),
          const VertexNames& vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes = {}) = 0;

  /// Add a hyperedge
  virtual const Hyperedge_t* addHyperedge(
          const VertexIds& vertices,
          const typename Hyperedge_t::SpecificAttributes& attributes = {}) = 0;

  /// Remove a hyperedge with the given id from the hypergraph
  virtual void removeHyperedge(const HyperedgeIdType& hyperedgeId) = 0;

  /// Search hyperedge list for an hyperedge with the provided id
  virtual const Hyperedge_t* hyperedgeById(
          const HyperedgeIdType & hyperedgeId) const = 0;

  /// Const hyperedge iterator pointing to the begin of the hyperedges
  virtual hyperedge_iterator hyperedgesBegin() const = 0;
  /// Const hyperedge iterator pointing to the end of the hyperedges
  virtual hyperedge_iterator hyperedgesEnd() const = 0;
  /// Begin/End iterator pair of the hyperedges of the hypergraph
  virtual std::pair<hyperedge_iterator,
  hyperedge_iterator> hyperedgesBeginEnd() const = 0;
  /// Return a reference to the container of hyperedges
  virtual const HyperedgeContainer& hyperedges() const = 0;

  // Vertices of a given hyperedge
  /// Return number of vertices in the hyperedge with the given id
  virtual size_t nbImpliedVertices(
          const HyperedgeIdType& hyperedgeId) const = 0;
  /// Check if the hyperedge with the given id has vertices
  virtual bool hasVertices(const HyperedgeIdType& hyperedgeId) const = 0;
  /// Const vertex iterator pointing to the begin of the implied vertices
  virtual vertex_iterator impliedVerticesBegin(
          const HyperedgeIdType& hyperedgeId) const = 0;
  /// Const vertex iterator pointing to the end of the implied vertices
  virtual vertex_iterator impliedVerticesEnd(
          const HyperedgeIdType& hyperedgeId) const = 0;
  /// Pair of vertex iterators pointing to the begin/end of the implied vertices
  virtual std::pair<vertex_iterator, vertex_iterator> impliedVerticesBeginEnd(
          const HyperedgeIdType& hyperedgeId) const = 0;
  /// Get shared_ptr of the container of implied vertices
  virtual std::shared_ptr<
    const GraphElementContainer<Vertex_t*>> impliedVertices(
      const HyperedgeIdType& hyperedgeId) const = 0;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex access
   * **************************************************************************
   * *************************************************************************/
  // Hyperedges that contain a given vertex
  /// Number of hyperedges that contain the vertex with the given Id
  virtual size_t nbHyperedges(const VertexIdType& vertexId) const = 0;
  /// Check if the vertex with the given Id is part of a hyperedge
  virtual bool isContainedInAnyHyperedge(
          const VertexIdType& vertexId) const = 0;
  /// Iterator pointing to the begin of the hyperedges that contain the vertex
  virtual hyperedge_iterator hyperedgesBegin(
          const VertexIdType& vertexId) const = 0;
  /// Iterator pointing to the end of the hyperedges that contain the vertex
  virtual hyperedge_iterator hyperedgesEnd(
          const VertexIdType& vertexId) const = 0;
  /// Pair of iterators pointing to the begin/end of the vertex's hyperedges
  virtual std::pair<hyperedge_iterator, hyperedge_iterator> hyperedgesBeginEnd(
          const VertexIdType& vertexId) const = 0;
  /// Get shared_ptr of container of hyperedges that contain the vertex
  virtual std::shared_ptr<const GraphElementContainer<Hyperedge_t*>>
    hyperedges(const VertexIdType& vertexId) const = 0;

  /// Remove a vertex with the given name from the hypergraph
  virtual void removeVertex(const std::string& vertexName,
                            bool removeImpliedHyperedges = true) = 0;
  /// Remove a vertex with the given Id from the hypergraph
  virtual void removeVertex(const VertexIdType& vertexId,
                            bool removeImpliedHyperedges = true) = 0;

  /* **************************************************************************
   * **************************************************************************
   *                       Vertex/Hyperedge dependent access
   * **************************************************************************
   * *************************************************************************/
  /// Check if the given vertex is part of the given hyperedge
  virtual bool isVertexOfHyperedge(
          const VertexIdType& vertexId,
          const HyperedgeIdType& hyperedgeId) const = 0;

  /* **************************************************************************
  * **************************************************************************
  *                        HyperedgeProperty access
  * **************************************************************************
  * *************************************************************************/
  /// Get properties of the hyperedge with the provided id
  virtual const HyperedgeProperty* getHyperedgeProperties(
          const HyperedgeIdType& hyperedgeId) const = 0;
  /// Get properties of the hyperedge with the provided id
  virtual HyperedgeProperty* getHyperedgeProperties_(
          const HyperedgeIdType& hyperedgeId) const = 0;

  /* **************************************************************************
   * **************************************************************************
   *                  Hypergraph hierarchy/Observer pattern
   * **************************************************************************
   * *************************************************************************/
  /// Get root hypergraph
  virtual const UndirectedHypergraphInterface* getRootHypergraph() const = 0;
  /// Return shared_ptr to hyperedge container
  virtual HyperedgeContainerPtr getRootHyperedgeContainerPtr() const = 0;
};




/* **************************************************************************
 * **************************************************************************
 *               Non-member functions
 * **************************************************************************
 * *************************************************************************/

/**
 * \brief Retrieve graph's hyperedges into a container
 *
 * \details
 * Retrieve hypergraph's hyperedges and insert them into a container through the
 * provided output iterator.
 *
 * \param hypergraph graph from which to retrieve the hyperedges
 * \param output_iter
 * \parblock
 * OutputIterator to the container to fill \n
 * The underlying container's value_type should be `const Hyperedge_t*`
 * \endparblock
 * \param fn (optional) if provided, only those hyperedges for which
 * `fn(hyperedge->id())` evaluates to true will be inserted
 *
 * \par Examples
 * \parblock
 * Simple example
 * \code
 hglib::UndirectedHypergraph<> g;
 for (auto vertexName : vertices) {
   g.addVertex(vertexName);
 }
 for (auto edge : edges) {
   g.addHyperedge(egde);
 }

 std::vector<const hglib::UndirectedHyperedge*> hyperedgeContainer;
 hglib::hyperedges(g, std::back_inserter(hyperedgeContainer));

 std::set<const hglib::UndirectedHyperedge*> s;
 hglib::hyperedges(g, std::inserter(s, s.begin()));
 * \endcode
 *
 * Retrieve hyperedges with a simple constraint
 * \code
 enum class Colour {
    red, blue, green, black
  };
  struct EdgeProperty {
    string name = "Default";
    Colour colour = Colour::green;
  };

  hglib::UndirectedHypergraph<hglib::DirectedVertex,
                              hglib::UndirectedHyperedge,
                              hglib::emptyProperty,
                              EdgeProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first);
  }

  // Get green hyperedges
  std::vector<const hglib::UndirectedHyperedge*> greenHyperedges;
  hglib::hyperedges(g, std::back_inserter(greenHyperedges),
                    [&](hglib::HyperedgeIdType hyperedgeId) -> bool {
                     return g.getHyperedgeProperties(hyperedgeId)->colour ==
                            Colour::green;
                    });
 * \endcode
 * \endparblock
 *
 * \par Complexity
 * Linear most of the time\n
 * O(n * O(\a fn)) if \a fn is provided
 *
 * \relates UndirectedHypergraphInterface
 */
template <typename HGraph, typename OutputIterator>
void hyperedges(const HGraph& hypergraph,
                OutputIterator output_iter,
                std::function<bool(hglib::HyperedgeIdType)>&& fn = nullptr) {
  // Check that output_iter satisfies the OutputIterator requirements
  static_assert(
      std::is_same<
          typename std::iterator_traits<OutputIterator>::iterator_category,
          typename std::output_iterator_tag>::value,
      "provided iterator not an OutputIterator");

  // Insert hyperedges through output_iter
  for (const auto& hyperedge : hypergraph.hyperedges()) {
    // Insert if no fn provided or fn returns true
    if (not fn or fn(hyperedge->id())) {
      output_iter = hyperedge;
    }
  }
}
}  // namespace hglib
#endif  // HGLIB_UNDIRECTEDHYPERGRAPHINTERFACE_H
