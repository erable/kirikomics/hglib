// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_BFHYPERGRAPH_HPP
#define HGLIB_BFHYPERGRAPH_HPP

namespace hglib {

/**
 * \brief Check whether the given hypergraph is a BF-hypergraph.
 *
 * \details
 * A BF-hypergraph is a hypergraph whose hyperarcs are either B- (only one
 * head) or F-hyperarcs (only one tail).
 *
 * \tparam DirectedHypergraph Type of directed hypergraph.
 *
 * \param g Directed hypergraph.
 *
 * \return True if the given hypergraph is a BF-hypergraph, false otherwise.
 *
 * \ingroup algorithmsDirected
 */
template<typename DirectedHypergraph>
bool is_bf_hypergraph(const DirectedHypergraph& g) {
  for (const auto& hyperarc : g.hyperarcs()) {
    if (g.nbTailVertices(hyperarc->id()) > 1 and
            g.nbHeadVertices(hyperarc->id()) > 1) {
      return false;
    }
  }
  return true;
}


/**
 * \brief Transforms a directed hypergraph into a BF-hypergraph.
 *
 * \details
 * Transforms a directed hypergraph into a BF-hypergraph, that is into a
 * hypergraph that contains only Backward- and Forward-hyperarcs.
 * A hyperarc a = (TAILS, HEADS) that has more than one tail and more
 * than one head vertex is transformed in the following way:
 * 1. Add a dummy vertex to the hypergraph.
 * 2. Add a hyperarc a' = (TAILS, dummy vertex) to the hypergraph.
 * 3. Add a hyperarc a'' = (dummy vertex, HEADS) to the hypergraph.
 * 4. Remove hyperarc a from the hypergraph.
 *
 * \tparam DirectedHypergraph_t Type of directed hypergraph.
 *
 * \param g Pointer to directed hypergraph.
 *
 * \ingroup algorithmsDirected
 */
template<typename DirectedHypergraph_t>
void bf_transformation(DirectedHypergraph_t* g) {
  /* First get the hyperarcs that must be transformed.
   * If a hyperarc a must be transformed, we build a dummy vertex
   * with the name "dummy_a->id()". A Backward hyperarc with the tails of a,
   * and with the dummy vertex as single head vertex is added to the given
   * graph. A Forward hyperarc with the dummy vertex as single tail vertex,
   * and the head vertices of a is added to the given hypergraph. The hyperarc
   * a is removed from the network.*/
  // TODO(mw): Discuss if a new graph should be created. Discuss if
  // resetHyperarcIds should be called at the end of the function.

  std::vector<HyperedgeIdType> hyperarcsToTransform;
  for (const auto& hyperarc : g->hyperarcs()) {
    if (g->nbTailVertices(hyperarc->id()) > 1 and
        g->nbHeadVertices(hyperarc->id()) > 1) {
      hyperarcsToTransform.push_back(hyperarc->id());
    }
  }
  if (hyperarcsToTransform.empty()) {
    return;
  }

  // Get the first vertex of the hypergraph to obtain the tuple of arguments
  // that are needed to create a vertex of the specific type of the given
  // hypergraph. The first entry of the tuple is the name of the vertex.
  const typename DirectedHypergraph_t::Vertex_t* aVertex;
  for (const auto& vertex : g->vertices()) {
    aVertex = vertex;
    break;
  }
  auto argsForVertexCreation = g->argumentsToCreateVertex(aVertex->id());

  for (const auto& hyperarcId : hyperarcsToTransform) {
    // 1. Add dummy vertex.
    // 2. Get tuple t to build this hyperarc.
    // 3. Using this tuple t, add two hyperarc: one with tails of t and head
    // as dummy vertex, and another one with dummy vertex as tail and the
    // heads of t.
    // 4. Remove the original hyperarc from the graph.
    std::string dummyVertexName("dummy_" +
                                std::to_string(hyperarcId));
    argsForVertexCreation.setName(dummyVertexName);
    const auto& dummyVertex = g->addVertex(
            argsForVertexCreation.name(),
            argsForVertexCreation.specificAttributes());
    // Add B-DirectedHyperedge
    auto argsForBHyperarc =
        g->hyperarcById(hyperarcId)->argumentsToCreateHyperarc();
    auto tailsAndHeads = argsForBHyperarc.tailAndHeadIds();
    tailsAndHeads.second = {dummyVertex->id()};
    argsForBHyperarc.setTailsAndHeadIds(tailsAndHeads);
    g->addHyperarc(argsForBHyperarc.tailAndHeadIds(),
                   argsForBHyperarc.specificAttributes());
    // Add F-DirectedHyperedge
    auto argsForFHyperarc =
        g->hyperarcById(hyperarcId)->argumentsToCreateHyperarc();
    auto tailsAndHeads2 = argsForFHyperarc.tailAndHeadIds();
    tailsAndHeads2.first = {dummyVertex->id()};
    argsForFHyperarc.setTailsAndHeadIds(tailsAndHeads2);
    g->addHyperarc(argsForFHyperarc.tailAndHeadIds(),
                   argsForFHyperarc.specificAttributes());
    g->removeHyperarc(hyperarcId);
  }
}
}  // namespace hglib

#endif  // HGLIB_BFHYPERGRAPH_HPP
