// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_CLOSURE_HPP
#define HGLIB_CLOSURE_HPP

#include <set>
#include <vector>
#include <algorithm>

namespace hglib {

/**
 * \brief Compute the closure of a set of source vertices in a hypergraph.
 *
 * \details
 * Given a hypergraph \f$G = (V, H)\f$, with vertices \em V and hyperarcs \em H.
 * Let's denote the tails (heads) of a hyperarc \f$h \in H\f$ by \em tails(h)
 * (\em heads(h) ). Then, the set of hyperarcs, denoted by \f$R_{C}\f$, that
 * can be used because all its tail vertices are present in a given set of
 * vertices \em C, is defined as
 * \f$R_{C} = \{r \in H | tails(r) \subseteq C\}\f$.
 * The closure of a set of vertices \em S, denoted by \em Closure(S), are all
 * vertices that are successively produced from \em S. More formally, the
 * \em Closure(S) results from the recursion
 * \f$S_{i+1} = S \cup heads(R_{S_{i}})\f$, starting with \f$S_0 = S\f$ and
 * until \f$S_{i+1} = S\f$.
 *
 * \tparam DirectedHypergraph Type of directed hypergraph.
 *
 * \param g DirectedHypergraph.
 * \param sourceSet Vector of vertex identifiers.
 *
 * \return A set of vertex identifiers (\em Closure(S) ), and a set of hyperarc
 * identifiers (\f$R_{Closure(S)}\f$).
 *
 * \ingroup algorithmsDirected
 */
template<typename DirectedHypergraph>
std::pair<std::set<hglib::VertexIdType>, std::set<hglib::HyperedgeIdType>>
closure(const DirectedHypergraph& g,
        const std::vector<hglib::VertexIdType>& sourceSet) {
  /*
   * Remove first those source vertices that are not declared in the
   * hypergraph.
   * The closure is a set of vertices and is computed iteratively. The set
   * hyperarcsInClosure contains those reactions whose tails are entirely
   * in the closure.
   * In each iteration, we add all reactions to hyperarcsInClosure whose
   * tail vertices are in the closure. In that case, we add also all
   * head vertices of those reactions to the closure. We iterate until no
   * vertex is added to the closure during an iteration.
   */
  std::set<hglib::VertexIdType> verticesInClosure;
  std::set<hglib::HyperedgeIdType> hyperarcsInClosure;

  // add starting set to vertices in closure if they belong to the hypergraph
  for (const auto& vertexId : sourceSet) {
    try {
      g.vertexById(vertexId);
      verticesInClosure.insert(vertexId);
    } catch (const std::out_of_range& e) {
      std::cerr << "The vertex with the id " << vertexId << " from the"
              " starting set of the closure is not int the hypergraph.";
    }
  }

  // the set of source vertices contains all vertices of the hypergraph
  if (verticesInClosure.size() == g.nbVertices()) {
    for (const auto& hyperarc : g.hyperarcs()) {
      hyperarcsInClosure.insert(hyperarc->id());
    }
    return std::make_pair(verticesInClosure, hyperarcsInClosure);
  }

  // compute the closure
  size_t nbVerticesInClosureBeginIteration(verticesInClosure.size());
  do {
    // number of vertices in the closure from last iteration
    nbVerticesInClosureBeginIteration = verticesInClosure.size();
    for (const auto& hyperarc : g.hyperarcs()) {
      const hglib::HyperedgeIdType hyperarcId = hyperarc->id();
      // Check if the hyperarc is already in the closure and if all its tails
      // are in the closure
      if (std::find(hyperarcsInClosure.begin(), hyperarcsInClosure.end(),
                    hyperarcId) == hyperarcsInClosure.end()) {
        bool allTailsInClosure(true);
        auto itTails = g.tailsBegin(hyperarcId);
        auto tailsEnd = g.tailsEnd(hyperarcId);
        for (; itTails != tailsEnd; ++itTails) {
          const auto& tail = *itTails;
          if (std::find(verticesInClosure.begin(), verticesInClosure.end(),
                        tail->id()) == verticesInClosure.end()) {
            allTailsInClosure = false;
            break;
          }
        }
        if (allTailsInClosure) {
          hyperarcsInClosure.insert(hyperarcId);
          auto itHeads = g.headsBegin(hyperarcId);
          auto headsEnd = g.headsEnd(hyperarcId);
          for (; itHeads != headsEnd; ++itHeads) {
            const auto& head = *itHeads;
            verticesInClosure.insert(head->id());
          }
        }
      }
    }
  } while (nbVerticesInClosureBeginIteration != verticesInClosure.size());
  return std::make_pair(verticesInClosure, hyperarcsInClosure);
}
}  // namespace hglib
#endif  // HGLIB_CLOSURE_HPP
