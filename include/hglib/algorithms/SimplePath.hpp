// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_SIMPLEPATH_HPP
#define HGLIB_SIMPLEPATH_HPP

#include <vector>
#include <unordered_set>

#include "hglib/exceptions/NoPathExeption.h"

namespace hglib {

/**
 * \brief Check if there is a simple path between the source and the target.
 *
 * \details
 * Given a hypergraph G=(V, A), a source and a target vertex, a simple
 * path from the source to the target is searched. This function does not
 * verify if all tail vertices of a hyperarc on the path are also head
 * vertices of another hyperarc on the path. Only the head vertices of
 * outgoing hyperarcs are visited (starting from the source) until the target
 * is reached.
 *
 * \tparam DirectedHypergraph Type of directed hypergraph.
 *
 * \param g DirectedHypergraph.
 * \param source Identifier of the source vertex.
 * \param target Identifier of the target vertex.
 *
 * \return True if there is a simple path from the source to the target,
 * false otherwise
 *
 * \par Complexity
 * O(|V| + |A|)
 *
 * \ingroup algorithmsDirected
 */
template<typename DirectedHypergraph>
bool has_path(const DirectedHypergraph& g,
              const hglib::VertexIdType& source,
              const hglib::VertexIdType& target) {
  // Check if source vertex is part of the hypergrapgh
  std::list<hglib::VertexIdType> verticesRemainsToVisit;
  try {
    const auto& vertex = g.vertexById(source);
    if (vertex == nullptr) {
      throw std::invalid_argument("The source vertex with the id "
                                  + std::to_string(source)
                                  + " is not declared in the hypergraph.");
    }
    verticesRemainsToVisit.push_back(source);
  } catch (const std::out_of_range& oor) {
    std::cerr << "The source vertex with the id " << std::to_string(source)
              << " is not declared in the hypergraph." << std::endl;
    throw;
  }
  try {
    const auto& vertex = g.vertexById(target);
    if (vertex == nullptr) {
      throw std::invalid_argument("The target vertex with the id "
                                  + std::to_string(target)
                                  + " is not declared in the hypergraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "The target vertex with the id " << std::to_string(target)
              << " is not declared in the hypergraph." << std::endl;
    throw;
  }


  // Init containers of already visited vertices and hyperarcs
  auto lastVertex = --g.verticesEnd();
  std::vector<bool> alreadyVisitedVertices((*lastVertex)->id() + 1,
                                           false);
  alreadyVisitedVertices[source] = true;

  auto lastHyperarc = --g.hyperarcsEnd();
  std::vector<bool> alreadyVisitedHyperarcs((*lastHyperarc)->id() + 1,
                                            false);

  // Visit from source reachable vertices; stop if target is found
  do {
    auto& nextVertex = verticesRemainsToVisit.front();
    verticesRemainsToVisit.pop_front();
    auto outArcsIt = g.outHyperarcsBegin(nextVertex);
    auto outArcsEnd = g.outHyperarcsEnd(nextVertex);
    for (; outArcsIt != outArcsEnd; ++outArcsIt) {
      const auto& outArcId = (*outArcsIt)->id();
      if (not alreadyVisitedHyperarcs[outArcId]) {
        alreadyVisitedHyperarcs[outArcId] = true;
        auto headsIt = g.headsBegin(outArcId);
        auto headsEnd = g.headsEnd(outArcId);
        for (; headsIt != headsEnd; ++headsIt) {
          const auto& headVertexId = (*headsIt)->id();
          if (headVertexId == target) {
            return true;
          }
          if (not alreadyVisitedVertices[headVertexId]) {
            alreadyVisitedVertices[headVertexId] = true;
            verticesRemainsToVisit.push_back(headVertexId);
          }
        }
      }
    }
  } while (not verticesRemainsToVisit.empty());
  return false;
}


/**
 * \brief Compute the shortest simple path between the source and the target
 *
 * \details
 * Compute the shortest simple path between the source and the target using a
 * default weight of 1 for each hyperarc.
 *
 * \tparam DirectedHypergraph Type of directed hypergraph.
 *
 * \param g DirectedHypergraph.
 * \param source Identifier of the source vertex.
 * \param target Identifier of the target vertex.
 *
 * \return The shortest path and its distance.
 *
 * \throw std::out_of_range if the source or target vertex id are not in the
 * range [0, vertexContainerSize() ).
 * \throw std::invalid_argument if the source or target vertex with the
 * provided id was removed earlier from the hypergraph.
 * \throw Throw an no_path_exception if there is no path between the source
 * and the target.
 *
 * \ingroup algorithmsDirected
 */
template <template <typename> typename VertexTemplate_t,
          typename Hyperarc_type,
          typename VertexProperty,
          typename HyperarcProperty,
          typename HypergraphProperty>
std::tuple<std::unique_ptr<hglib::DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_type, VertexProperty, HyperarcProperty, HypergraphProperty>>,
        float>
shortestPath(DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_type,
        VertexProperty, HyperarcProperty, HypergraphProperty>* g,
             const hglib::VertexIdType& source,
             const hglib::VertexIdType& target) {
  std::unique_ptr<DirectedSubHypergraph<VertexTemplate_t, Hyperarc_type,
          VertexProperty, HyperarcProperty, HypergraphProperty>> subgraph;
  float distance;
  std::tie(subgraph, distance) = shortestPath(g, source, target, [&] (
          const hglib::HyperedgeIdType&) -> float {return 1.0;});
  return std::make_tuple(std::move(subgraph), distance);
}


/**
 * \brief Compute the shortest simple path between the source and the target
 *
 * \details
 * Compute the shortest simple path between the source and the target using the
 * specified weight function.
 *
 * \tparam DirectedHypergraph Type of directed hypergraph.
 *
 * \param g DirectedHypergraph.
 * \param source Identifier of the source vertex.
 * \param target Identifier of the target vertex.
 * \param func Hyperarc weight function.
 *
 * \return The shortest path and its distance.
 *
 * \throw std::out_of_range if the source or target vertex id are not in the
 * range [0, vertexContainerSize() ).
 * \throw std::invalid_argument if the source or target vertex with the
 * provided id was removed earlier from the hypergraph.
 * \throw Throw an no_path_exception if there is no path between the source
 * and the target.
 *
 * \ingroup algorithmsDirected
 */
template<template <typename> typename VertexTemplate_t,
          typename Hyperarc_type,
          typename VertexProperty,
          typename HyperarcProperty,
          typename HypergraphProperty,
          typename Lambda>
std::tuple<std::unique_ptr<hglib::DirectedSubHypergraph<VertexTemplate_t,
        Hyperarc_type, VertexProperty, HyperarcProperty, HypergraphProperty>>,
        float>
shortestPath(DirectedHypergraphInterface<VertexTemplate_t, Hyperarc_type,
        VertexProperty, HyperarcProperty, HypergraphProperty>* g,
             const hglib::VertexIdType& source,
             const hglib::VertexIdType& target,
             Lambda&& func) {
  // Check if source vertex is part of the hypergrapgh
  try {
    const auto& vertex = g->vertexById(source);
    if (vertex == nullptr) {
      throw std::invalid_argument("The source vertex with the id "
                                  + std::to_string(source)
                                  + " is not declared in the hypergraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "The source vertex with the id " << std::to_string(source)
              << " is not declared in the hypergraph." << std::endl;
    throw;
  }
  try {
    const auto& vertex = g->vertexById(target);
    if (vertex == nullptr) {
      throw std::invalid_argument("The target vertex with the id "
                                  + std::to_string(target)
                                  + " is not declared in the hypergraph.");
    }
  } catch (const std::out_of_range& oor) {
    std::cerr << "The target vertex with the id " << std::to_string(target)
              << " is not declared in the hypergraph." << std::endl;
    throw;
  }

  // already visited vertices
  auto lastVertex = --g->verticesEnd();
  std::vector<bool> alreadyVisitedVertices((*lastVertex)->id() + 1,
                                           false);
  // init weight vector
  float infinity = std::numeric_limits<float>::infinity();
  std::vector<float> weight((*lastVertex)->id() + 1, infinity);
  weight[source] = 0;
  // Stores for each vertex v the previous vertex and hyperarc on the path to v
  // for backtracking the path
  std::vector<std::pair<hglib::VertexIdType,
                        hglib::HyperedgeIdType>> previous(
          (*lastVertex)->id() + 1, {-1, -1});

  hglib::VertexIdType minWeightVertexId(source);
  /*
   * exit conditions:
   * 1) minWeightVertexId == target
   * 2) weight[minWeightVertexId] == infinity -> no path from source to target
  */
  do {
    minWeightVertexId = std::min_element(weight.begin(), weight.end()) -
            weight.begin();
    if (weight[minWeightVertexId] == infinity) {
      throw no_path_exception();  // "No path between " + std::to_string(source)
                              // + " and " + std::to_string(target));
    }
    if (minWeightVertexId == target) {
      break;
    }
    alreadyVisitedVertices[minWeightVertexId] = true;
    auto outArcsIt = g->outHyperarcsBegin(minWeightVertexId);
    auto outArcsEnd = g->outHyperarcsEnd(minWeightVertexId);
    for (; outArcsIt != outArcsEnd; ++outArcsIt) {
      hglib::HyperedgeIdType hyperarcId((*outArcsIt)->id());
      auto headsIt = g->headsBegin(hyperarcId);
      auto headsEnd = g->headsEnd(hyperarcId);
      for (; headsIt != headsEnd; ++headsIt) {
        hglib::VertexIdType headId((*headsIt)->id());
        float newWeight = weight[minWeightVertexId] + func(hyperarcId);
        if (not alreadyVisitedVertices[headId] &&
                (newWeight < weight[headId])) {
          previous[headId] = {minWeightVertexId, hyperarcId};
          weight[headId] = newWeight;
        }
      }
    }
    weight[minWeightVertexId] = infinity;
  } while (true);

  // backtrack the path
  std::unordered_set<hglib::VertexIdType> verticesOnPath;
  hglib::VertexIdType currentVertexId = target;
  verticesOnPath.insert(currentVertexId);
  std::unordered_set<hglib::HyperedgeIdType> hyperarcsOnPath;
  while (currentVertexId != source) {
    verticesOnPath.insert(previous[currentVertexId].first);
    hyperarcsOnPath.insert(previous[currentVertexId].second);
    currentVertexId = previous[currentVertexId].first;
  }

  auto ptr = hglib::createDirectedSubHypergraph(g, verticesOnPath,
                                                hyperarcsOnPath);
  return std::make_tuple(std::move(ptr), weight[target]);
}


/**
 * \brief DistanceMatrix
 *
 * \details
 * This class mimics a matrix to store the distance between any pair of vertices
 * in a hypergraph. To have constant access to an entry via the vertex Ids and
 * the fact that vertices may have been removed from the hypergraph (vertex Ids
 * are not consecutive anymore) we store the column/row index for each vertex
 * in a lookup table.
 */
struct DistanceMatrix {
 public:
  /**
   * \brief DistanceMatrix constructor
   *
   * \details
   * Initialise all entries with inf.
   *
   * \param size Size of the matrix.
   * \param dict Lookup table that assign the column/row index for each vertex.
   */
  DistanceMatrix(size_t size,
                 const std::vector<int>& dict) : dict_(dict) {
    std::vector<float> col(size, std::numeric_limits<float>::infinity());
    matrix_ = std::vector<std::vector<float>>(size, col);
  }

  /**
   * \brief Set the distance between the source and target vertex
   *
   * \param source Identifier of a source vertex.
   * \param target Identifier of a target vertex.
   * \param distance Distance between the source and target vertex.
   *
   * \throw out_of_range If either the source or target identifier is not in
   * the range of the matrix dimensions (passing by the indirection of the
   * lookup table).
   */
  void setDistance(const hglib::VertexIdType& source,
                   const hglib::VertexIdType& target,
                   const float& distance) {
    try {
      hglib::VertexIdType i(dict_.at(source));
      hglib::VertexIdType j(dict_.at(target));
      matrix_.at(i).at(j) = distance;
    } catch (const std::out_of_range& oor) {
      std::cerr << "Invalid source (" << std::to_string(source)
                << ") or target (" << std::to_string(target)
                << ") index in DistanceMatrix::setDistance"
                << std::endl;
      throw;
    }
  }

  /**
   * \brief Get the distance between the source and target vertex
   *
   * \param source Identifier of a source vertex.
   * \param target Identifier of a target vertex.
   *
   * \throw out_of_range If either the source or target identifier is not in
   * the range of the matrix dimensions (passing by the indirection of the
   * lookup table).
   */
  float getDistance(const hglib::VertexIdType& source,
                    const hglib::VertexIdType& target) {
    try {
      hglib::VertexIdType i(dict_.at(source));
      hglib::VertexIdType j(dict_.at(target));
      return matrix_.at(i).at(j);
    } catch (const std::out_of_range& oor) {
      std::cerr << "Invalid source (" << std::to_string(source)
                << ") or target (" << std::to_string(target)
                << ") index in DistanceMatrix::getDistance"
                << std::endl;
      throw;
    }
  }

 private:
  /// Matrix
  std::vector<std::vector<float>> matrix_;
  /// Lookup table: Stores the column/row index for each vertex identifier
  std::vector<int> dict_;
};


/**
 * \brief Computes the distance matrix of the hypergraph
 *
 * \details
 * Computes the distance matrix of the hypergraph using a default hyperarc
 * weight function (weight equal to 1). The shortest path for any pair of
 * vertices is computed.
 *
 * \tparam DirectedHypergraph Type of directed hypergraph.
 *
 * \param g Directed hypergraph.
 *
 * \return DistanceMatrix
 *
 * \ingroup algorithmsDirected
 */
template<typename DirectedHypergraph>
DistanceMatrix distanceMatrix(DirectedHypergraph* g) {
  return distanceMatrix(g, [&] (const hglib::HyperedgeIdType&) -> float {
      return 1.0;});
}


/**
 * \brief Computes the distance matrix of the hypergraph
 *
 * \details
 * Computes the distance matrix of the hypergraph using the specified hyperarc
 * weight function. The shortest path for any pair of vertices is computed.
 *
 * \tparam DirectedHypergraph Type of directed hypergraph.
 * \tparam Lambda Lambda hyperarc weight function.
 *
 * \param g Directed hypergraph.
 * \param func Hyperarc weight function.
 *
 * \return DistanceMatrix
 *
 * \ingroup algorithmsDirected
 */
template<typename DirectedHypergraph, typename Lambda>
DistanceMatrix distanceMatrix(DirectedHypergraph* g,
                              Lambda&& func) {
  auto lastVertex = --g->verticesEnd();
  std::vector<int> dict = std::vector<int>((*lastVertex)->id() + 1,
                                           -1);
  int i(0);
  for (const auto& vertex : g->vertices()) {
    dict[vertex->id()] = i++;
  }
  DistanceMatrix matrix = DistanceMatrix(g->nbVertices(), dict);
  for (const auto& source : g->vertices()) {
    for (const auto& target : g->vertices()) {
      try {
        auto shortestPath = hglib::shortestPath(g, source->id(), target->id(),
                                                func);
        matrix.setDistance(source->id(), target->id(),
                           std::get<1>(shortestPath));
      }
      catch (hglib::no_path_exception &e) {
        matrix.setDistance(source->id(), target->id(),
                           std::numeric_limits<float>::infinity());
      }
    }
  }
  return matrix;
}

}  // namespace hglib
#endif  // HGLIB_SIMPLEPATH_HPP
