// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_BACKWARDHYPERGRAPH_HPP
#define HGLIB_BACKWARDHYPERGRAPH_HPP

namespace hglib {

/**
 * \brief Check whether the given hypergraph is a B-hypergraph.
 *
 * \details
 * A B-hypergraph is a hypergraph whose hyperarcs have only one head
 * (B-hyperarc).
 *
 * \tparam DirectedHypergraph Type of directed hypergraph.
 *
 * \param g Directed hypergraph.
 *
 * \return True if the given hypergraph is a B-hypergraph, false otherwise.
 *
 * \par Complexity
 * Linear in the number of hyperarcs.
 *
 * \ingroup algorithmsDirected
 */
template<typename DirectedHypergraph>
bool is_b_hypergraph(const DirectedHypergraph& g) {
  for (const auto& hyperarc : g.hyperarcs()) {
    if (g.nbHeadVertices(hyperarc->id()) > 1) {
      return false;
    }
  }
  return true;
}
}  // namespace hglib
#endif  // HGLIB_BACKWARDHYPERGRAPH_HPP
