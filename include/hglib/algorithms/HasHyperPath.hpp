// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_HASHYPERPATH_HPP
#define HGLIB_HASHYPERPATH_HPP

namespace hglib {

/**
 * \brief Check if there exists a hyperpath from the source to the target
 *
 * \details
 * Check if the target lies in the closure of the source.
 *
 * \tparam DirectedHypergraph Type of directed hypergraph.
 *
 * \param g DirectedHypergraph.
 * \param source source vertex identifier.
 * \param target target vertex identifier.
 *
 * \return True if there exists a hyperpath from the source to the target,
 * false otherwise.
 *
 * \ingroup algorithmsDirected
 */
template<typename DirectedHypergraph>
bool has_hyperpath(const DirectedHypergraph& g,
        const hglib::VertexIdType& source,
        const hglib::VertexIdType& target) {
  std::vector<hglib::VertexIdType> sourceSet = {source};
  std::vector<hglib::VertexIdType> targetSet = {target};
  return has_hyperpath(g, sourceSet, targetSet);
}


/**
 * \brief Check if there exists a hyperpath from a set of sources to the target
 *
 * \details
 * Check if the target lies in the closure of the set of sources.
 *
 * \tparam DirectedHypergraph Type of directed hypergraph.
 *
 * \param g DirectedHypergraph.
 * \param sourceSet set of source vertex identifiers.
 * \param target target vertex identifier.
 *
 * \return True if there exists a hyperpath from the set of sources to the
 * target, false otherwise.
 *
 * \ingroup algorithmsDirected
 */
template<typename DirectedHypergraph>
bool has_hyperpath(const DirectedHypergraph& g,
                  const std::vector<hglib::VertexIdType>& sourceSet,
                  const hglib::VertexIdType& target) {
  std::vector<hglib::VertexIdType> targetSet = {target};
  return has_hyperpath(g, sourceSet, targetSet);
}


/**
 * \brief Check if there exists a hyperpath from a source to a set of targets
 *
 * \details
 * Check if the targets lie in the closure of the source.
 *
 * \tparam DirectedHypergraph Type of directed hypergraph.
 *
 * \param g DirectedHypergraph.
 * \param source source vertex identifier.
 * \param target set of target vertex identifiers.
 *
 * \return True if there exists a hyperpath from the source to the set of
 * targets, false otherwise.
 *
 * \ingroup algorithmsDirected
 */
template<typename DirectedHypergraph>
bool has_hyperpath(const DirectedHypergraph& g,
                  const hglib::VertexIdType& source,
                  const std::vector<hglib::VertexIdType>& targetSet) {
  std::vector<hglib::VertexIdType> sourceSet = {source};
  return has_hyperpath(g, sourceSet, targetSet);
}


/**
 * \brief Check if there exists a hyperpath from the sources to the targets
 *
 * \details
 * Check if the targets lie in the closure of the sources.
 *
 * \tparam DirectedHypergraph Type of directed hypergraph.
 *
 * \param g DirectedHypergraph.
 * \param sourceSet set of source vertex identifiers.
 * \param targetSet set of target vertex identifiers.
 *
 * \return True if there exists a hyperpath from the set of sources to the set
 * of targets, false otherwise.
 *
 * \ingroup algorithmsDirected
 */
template<typename DirectedHypergraph>
bool has_hyperpath(const DirectedHypergraph& g,
                  const std::vector<hglib::VertexIdType>& sourceSet,
                  const std::vector<hglib::VertexIdType>& targetSet) {
  /*
   * Return false, if the target set is empty.
   * Do not consider the source and target vertices that are not declared in
   * the hypergraph.
   * Throw an invalid_argument exception if no target is in the hypergraph.
   * Return true if all targets are part of the source set.
   * Remove all target vertices that are also sources. Compute the closure
   * of the set of sources and stop if all targets are in that closure; return
   * true, false otherwise.
   */
  if (targetSet.empty()) {
    return false;
  }
  std::set<hglib::VertexIdType> verticesInClosure;
  std::set<hglib::HyperedgeIdType> hyperarcsInClosure;

  // add source set to vertices in closure if they belong to the hypergraph
  for (const auto& vertexId : sourceSet) {
    try {
      g.vertexById(vertexId);
      verticesInClosure.insert(vertexId);
    } catch (const std::out_of_range& e) {
      std::cerr << "The vertex with the id " << vertexId << " from the"
              " starting set of the closure is not int the hypergraph.";
    }
  }

  // Check if target vertices exist in the graph
  std::set<hglib::VertexIdType> targetsNotFound;
  for (const auto& vertexId : targetSet) {
    try {
      g.vertexById(vertexId);
      targetsNotFound.insert(vertexId);
    } catch (const std::out_of_range& e) {
      std::cerr << "Target with id " << std::to_string(vertexId) << " is not"
              " declared in the hypergraph.";
    }
  }
  // no vertex of the target set is in the hypergraph
  if (targetsNotFound.empty()) {
    throw std::invalid_argument("No target vertex is declared in the"
                                        " hypergraph.");
  }
  // all targets are part of the source set
  if (std::includes(verticesInClosure.begin(), verticesInClosure.end(),
                    targetSet.begin(), targetSet.end())) {
    return true;
  }

  // remove sources from targets as there is a hyperpath to itself by
  // definition
  for (const auto source : verticesInClosure) {
    targetsNotFound.erase(source);
  }

  // compute the closure and check if the targets are inside
  size_t nbVerticesInClosureBeginIteration(verticesInClosure.size());
  do {
    // number of vertices in the closure from last iteration
    nbVerticesInClosureBeginIteration = verticesInClosure.size();
    for (const auto& hyperarc : g.hyperarcs()) {
      const hglib::HyperedgeIdType hyperarcId = hyperarc->id();
      // Check if the hyperarc is already in the closure and if all its tails
      // are in the closure...
      if (std::find(hyperarcsInClosure.begin(), hyperarcsInClosure.end(),
                    hyperarcId) == hyperarcsInClosure.end()) {
        bool allTailsInClosure(true);
        auto itTails = g.tailsBegin(hyperarcId);
        auto tailsEnd = g.tailsEnd(hyperarcId);
        for (; itTails != tailsEnd; ++itTails) {
          const auto& tail = *itTails;
          if (std::find(verticesInClosure.begin(), verticesInClosure.end(),
                        tail->id()) == verticesInClosure.end()) {
            allTailsInClosure = false;
            break;
          }
        }
        //..., then the hyperarc and all its heads are added to the closure
        // Check if all targets were found
        if (allTailsInClosure) {
          hyperarcsInClosure.insert(hyperarcId);
          auto itHeads = g.headsBegin(hyperarcId);
          auto headsEnd = g.headsEnd(hyperarcId);
          for (; itHeads != headsEnd; ++itHeads) {
            const auto& head = *itHeads;
            targetsNotFound.erase(head->id());
            // all targets were found
            if (targetsNotFound.empty()) {
              return true;
            }
            verticesInClosure.insert(head->id());
          }
        }
      }
    }
  } while (nbVerticesInClosureBeginIteration != verticesInClosure.size());
  return false;
}
}  // namespace hglib
#endif  // HGLIB_HASHYPERPATH_HPP
