
# Append files in this directory to header files list
list(APPEND ${PROJECT_NAME}_HEADER_FILES
  ${CMAKE_CURRENT_LIST_DIR}.h
  ${CMAKE_CURRENT_LIST_DIR}/BFHypergraph.hpp
  ${CMAKE_CURRENT_LIST_DIR}/BHypergraph.hpp
  ${CMAKE_CURRENT_LIST_DIR}/Closure.hpp
  ${CMAKE_CURRENT_LIST_DIR}/Connectivity.hpp
  ${CMAKE_CURRENT_LIST_DIR}/FHypergraph.hpp
  ${CMAKE_CURRENT_LIST_DIR}/HasHyperPath.hpp
  ${CMAKE_CURRENT_LIST_DIR}/SimplePath.hpp
  ${CMAKE_CURRENT_LIST_DIR}/SymmetricImage.hpp)
