// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_HASINSERTMEMBERFUNCTION_H
#define HGLIB_HASINSERTMEMBERFUNCTION_H

#include "SfinaeBase.h"
#include <type_traits>

namespace hglib {
/// @cond

/**
 * Test if a container of pointers has an 'insert' member function.
 * This test is done by checking if the return type of the insert function
 * is equal to Container::iterator.
 * If yes, has_insert_memberFunction::value is true, false otherwise.
 *
 * Note, std::array has no insert method and
 * has_insert_memberFunction<std::array>::value should be equal to false.
 *
 */
template<typename Container, typename Arg>
struct has_insert_member_function : private sfinae_base {
 private:
  static yes test(Container* container,
                  typename std::enable_if<std::is_same<
                          typename Container::iterator,
                          decltype(container->insert(container->begin(),
                                                     std::declval<Arg>()))
                  >::value>::type * = nullptr);
  static no test(...);

 public:
  static const bool value = (sizeof(yes) == sizeof(has_insert_member_function::
  test((typename std::remove_reference<Container>::type*)0)));
};
/// @endcond

}  // namespace hglib
#endif  // HGLIB_HASINSERTMEMBERFUNCTION_H
