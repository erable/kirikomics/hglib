// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_OBSERVER_H
#define HGLIB_OBSERVER_H

#include "ObservableEvent.h"

namespace hglib {

class Observable;

/**
 * \brief Observer class
 *
 * \details
 * This class takes the role of an observer in the observer pattern. For now,
 * the follwing events are observed in the context of hypergraphs:
 *  - a vertex is removed,
 *  - a hyperedge is removed,
 *  - the vertex identifiers are reset,
 *  - the hyperedge identifiers are reset.
 */
class Observer {
 public :
  // ==========================================================================
  //                               Constructors
  // ==========================================================================
  /// Default ctor
  Observer() = default;
  /// Copy ctor
  Observer(const Observer&) = delete;
  /// Move ctor
  Observer(Observer&&) = delete;

  // ==========================================================================
  //                                Destructor
  // ==========================================================================
  /// Destructor
  virtual ~Observer() = default;

  // ==========================================================================
  //                                Operators
  // ==========================================================================
  /// Copy assignment
  Observer& operator=(const Observer& other) = delete;
  /// Move assignment
  Observer& operator=(const Observer&& other) = delete;

  // ==========================================================================
  //                              Public Methods
  // ==========================================================================
  /// This method is called whenever the observed object is changed.
  virtual void Update(const Observable& o, ObservableEvent e, void* arg) = 0;

  // ==========================================================================
  //                                 Getters
  // ==========================================================================

  // ==========================================================================
  //                                 Setters
  // ==========================================================================

 protected :
  // ==========================================================================
  //                            Protected Methods
  // ==========================================================================

  // ==========================================================================
  //                               Attributes
  // ==========================================================================
};
}  // namespace hglib
#endif  // HGLIB_OBSERVER_H
