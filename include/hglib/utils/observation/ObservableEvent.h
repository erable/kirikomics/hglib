// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_OBSERVABLEEVENT_H
#define HGLIB_OBSERVABLEEVENT_H

#include "hglib/utils/types.h"

namespace hglib {

/**
 * \brief Lists observable events
 *
 * \details
 * The following events can be observed in the context of hypergraphs:
 *  - a vertex is removed,
 *  - a hyperedge is removed,
 *  - the vertex identifiers are reset,
 *  - the hyperedge identifiers are reset.
 *
 * \ingroup hglibScope
 */
enum class ObservableEvent {
  /// A vertex is removed
  VERTEX_REMOVED,
  /// A hyperedge is removed
  HYPEREDGE_REMOVED,
  /// The vertex identifiers are reset
  RESET_VERTEX_IDS,
  /// The hyperedge identifiers are reset
  RESET_EDGE_IDS
};


/**
 * \brief Arguments needed to remove a vertex.
 *
 * \details
 * When a vertex is removed from a hypergraph, one can specify whether implied
 * hyperedges are removed, too. The Update() function of the Observer class
 * allows to pass one parameter to specify additional arguments needed to
 * handle a certain event. To handle the event of a removed vertex we need two
 * information:
 *  - the identifier of the removed vertex.
 *  - the flag whether implied hyperedges should be removed.
 *
 * Remove_vertex_args holds both information and can thus be used to handle
 * the event of a removed vertex correctly.
 */
struct Remove_vertex_args {
    /// Iidentifier of the removed vertex
    VertexIdType vertexId_;
    /// Flag whether implied hyperedges should be removed
    bool removeImpliedHyperarcs_;
};
}  // namespace hglib
#endif  // HGLIB_OBSERVABLEEVENT_H
