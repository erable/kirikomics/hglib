// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#ifndef HGLIB_ISDERIVEDFROM_H
#define HGLIB_ISDERIVEDFROM_H

#include <type_traits>
#include "SfinaeBase.h"

namespace hglib {

/**
 * \brief Check if a class is derived from another class.
 *
 * \details
 * Use SFINAE (Substitution Failure Is Not An Error) technique to check whether
 * a class \em D is derived from a class \em P.
 *
 * @tparam Parent Parent class.
 * @tparam Derived Derived class.
 */
template <template <typename...> typename Parent, typename Derived>
struct is_parent_of : public sfinae_base {
    /**
     * \brief Method is called if Derived* can be converted to Parent<Args...>
     *
     * \tparam Args Optional template arguments of the parent class.
     *
     * \return yes
     */
    template <typename... Args>
    static yes& test(Parent<Args...>*);
    /**
     * \brief This method is called otherwise (SFINAE)
     *
     * \return no
     */
    static no& test(...);

    /// Value of the test whether Derived* can be converted to Parent<Args...>
    static bool const value =
            sizeof(test(std::declval<Derived*>())) == sizeof(yes);
};
}  // namespace hglib
#endif  // HGLIB_ISDERIVEDFROM_H
