/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 12.10.17.
//

#include "WhitelistFilter.h"


/// Constructor
template<typename T>
WhitelistFilter<T>::
WhitelistFilter(const std::vector<T>& whitelist) :
        whitelist_(whitelist) {}

/// Constructor
template<typename T>
WhitelistFilter<T>::
WhitelistFilter() : whitelist_() {}


/// Copy constructor
template<typename T>
WhitelistFilter<T>::
WhitelistFilter(const WhitelistFilter<T>& rhs) :
        whitelist_(rhs.whitelist_) {}


/// Assignment operator
template<typename T>
WhitelistFilter<T>& WhitelistFilter<T>::
operator=(const WhitelistFilter<T>& rhs) {
  if (this != &rhs) {
    whitelist_ = rhs.whitelist_;
  }
  return *this;
}


/**
 * Filter function
 *
 * Returns true if the whitelist entry at a given position is a nullptr,
 * or if the given position is greater or equal to the size of the whitelist
 * vector, false otherwise. The position is determined by the Id of the
 * function parameter item (the function T::id() is called).
 *
 * @param item Element to be checked
 * @return true if the whitelist entry at a given position is a nullptr,
 * false otherwise.
 */
template<typename T>
bool WhitelistFilter<T>::
filter_out(const T& item) const {
  if (item == nullptr) {
    return true;
  }
  size_t itemId = item->id();
  if (itemId >= whitelist_.size()) {
    return true;
  }
  return (whitelist_.at(itemId) == nullptr);
}

/// Add item to the whitelist
template<typename T>
void WhitelistFilter<T>::
removeFilteredOutItem(const T& item) {
  // It is a nullptr
  if (item == nullptr) {
    whitelist_.push_back(item);
    return;
  }

  // Add item at position corresponding to its Id
  size_t itemId = item->id();
  if (itemId == whitelist_.size()) {
    whitelist_.push_back(item);
  } else if (itemId > whitelist_.size()) {
    // fill gaps with nullptrs and add item
    size_t nbGaps = itemId - whitelist_.size();
    while (nbGaps-- > 0) {
      whitelist_.push_back(nullptr);
    }
    // Add item
    whitelist_.push_back(item);
  } else {
    whitelist_.operator[](itemId) = item;
  }
}


/// Remove item from the whitelist
template<typename T>
void WhitelistFilter<T>::
addFilterOutItem(const T& item) {
  if (item != nullptr) {
    whitelist_.operator[](item->id()) = nullptr;
  }
}
