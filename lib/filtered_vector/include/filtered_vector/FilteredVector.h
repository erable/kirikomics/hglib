/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 12.10.17.
//

#ifndef FILTERED_VECTOR_FILTERED_VECTOR_H
#define FILTERED_VECTOR_FILTERED_VECTOR_H


#include <type_traits>
#include <memory>
#include <vector>

#include "Filter.h"
#include "FilteredVectorIterator.h"

/**
 * Abstract class to define the interface of a filtered vector.
 *
 * This class provides the interface of the decorator pattern to model a
 * filtered vector that consists of a base vector (filtered_vector_base)
 * decorated by additional filters (filtered_vector_decorator). These filter
 * allow to skip items of the base vector.
 *
 * @tparam T type of items stored in the vector. Has to be a pointer.
 */
template<typename T>
class FilteredVector : public std::enable_shared_from_this<FilteredVector<T>> {
  // Check that the template parameter is a pointer
  static_assert(std::is_pointer<T>::value, "The template parameter is expected"
          " to be a pointer");

 public:
  /// Alias declaration Impl
  using Impl = std::vector<T>;

  /// Alias declaration value_type
  using value_type = typename Impl::value_type;
  /// Alias declaration allocator_type
  using allocator_type = typename Impl::allocator_type;
  /// Alias declaration reference
  using reference = typename Impl::reference;
  /// Alias declaration const_reference
  using const_reference = typename Impl::const_reference;
  /// Alias declaration pointer
  using pointer = typename Impl::pointer;
  /// Alias declaration const_pointer
  using const_pointer = typename Impl::const_pointer;
  /// Alias declaration iterator
  using iterator = FilteredVectorIterator<T, FilteredVector<T>>;
  /// Alias declaration const_iterator
  using const_iterator = FilteredVectorIterator<const T, FilteredVector<T>>;
  /// Alias declaration reverse_iterator
  using reverse_iterator =
  std::reverse_iterator<FilteredVectorIterator<T, FilteredVector<T>>>;
  /// Alias declaration const_reverse_iterator
  using const_reverse_iterator =
  std::reverse_iterator<FilteredVectorIterator<const T,
          FilteredVector<T>>>;
  /// Alias declaration difference_type
  using difference_type =
  typename FilteredVectorIterator<T,
          FilteredVector<T>>::difference_type;
  /// Alias declaration size_type
  using size_type = typename Impl::size_type;

  /* **************************************************************************
   * **************************************************************************
   *                           Constructors/Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Constructor
  explicit FilteredVector(std::shared_ptr<Filter<T>> =
      std::make_shared<Filter<T>>());
  /// Copy constructor
  FilteredVector(const FilteredVector&) = delete;
  /// Destructor
  virtual ~FilteredVector() = default;

  /* **************************************************************************
   * **************************************************************************
   *                           Public methods
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Return iterator to beginning
  virtual iterator begin() noexcept = 0;
  /// Return iterator to beginning
  virtual const_iterator begin() const noexcept = 0;
  /// Return iterator to end
  virtual iterator end() noexcept = 0;
  /// Return iterator to end
  virtual const_iterator end() const noexcept = 0;

  /// Return const_iterator to beginning
  virtual const_iterator cbegin() const noexcept = 0;
  /// Return const_iterator to end
  virtual const_iterator cend() const noexcept = 0;

  /// Return reverse iterator to reverse beginning
  virtual reverse_iterator rbegin() noexcept = 0;
  /// Return reverse iterator to reverse end
  virtual reverse_iterator rend() noexcept = 0;

  /// Return const_reverse_iterator to reverse beginning
  virtual const_reverse_iterator crbegin() noexcept = 0;
  /// Return const_reverse_iterator to reverse end
  virtual const_reverse_iterator crend() noexcept = 0;

  /// Return size
  virtual size_type size() const noexcept = 0;
  /// Return maximum size
  virtual size_type max_size() const noexcept = 0;
  /// Change size
  virtual void resize(size_type n) = 0;
  /// Return size of allocated storage capacity
  virtual size_type capacity() const noexcept = 0;
  /// Test whether the filtered vector is empty
  virtual bool empty() const noexcept = 0;
  /// Requests that the capacity of the filtered vector is at least n
  virtual void reserve(size_type n) = 0;
  /// Non-binding request to the filtered vector to reduce its capacity to fit
  /// its size.
  virtual void shrink_to_fit() = 0;

  /// Returns a reference to the element at position n
  virtual reference operator[] (size_type n) = 0;
  /// Returns a const reference to the element at position n
  virtual const_reference operator[] (size_type n) const = 0;
  /// Returns a reference to the element at position n
  virtual reference at(size_type n) = 0;
  /// Returns a const reference to the element at position n
  virtual const_reference at(size_type n) const = 0;
  /// Returns a reference to the first element
  virtual reference front() = 0;
  /// Returns a const reference to the first element
  virtual const_reference front() const = 0;
  /// Returns a reference to the last element
  virtual reference back() = 0;
  /// Returns a const reference to the last element
  virtual const_reference back() const = 0;
  /// Access data
  virtual const value_type* data() const noexcept = 0;
  /// Access data
  virtual value_type* data() noexcept = 0;

  // Modifiers
  /// Assigns new contents to the vector
  virtual void assign(iterator first, iterator last) = 0;
  /// Assigns new contents to the vector
  virtual void assign(size_type n, const value_type& val) = 0;
  /// Assigns new contents to the vector
  virtual void assign(std::initializer_list<value_type> il) = 0;
  /// Add element at the end of filtered vector container
  virtual void push_back(const value_type& val) = 0;
  /// Delete last element
  virtual void pop_back() = 0;
  /// Clear content
  virtual void clear() noexcept = 0;

  /// Add item to the vector
  void removeFilteredOutItem(const T& item);
  /// Filter item out of the vector
  void addFilteredOutItem(const T& item);
  /// Whether the item is filtered out
  bool filter_out(T item) const;
  /// Return filter
  std::shared_ptr<Filter<T>> filter() const;

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Filter applied when iterating over a vector
  std::shared_ptr<Filter<T>> filter_;
};

#include "FilteredVector.hpp"
#endif  // FILTERED_VECTOR_FILTERED_VECTOR_H
