/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 12.10.17.
//

#ifndef FILTERED_VECTOR_FILTERED_VECTOR_DECORATOR_H
#define FILTERED_VECTOR_FILTERED_VECTOR_DECORATOR_H

#include <memory>

#include "FilteredVector.h"

/**
 * Decorates a filtered_vector by applying a filter on it.
 *
 * The filter allows to skip elements of the decorated filtered_vector
 * when iterating over the latter.
 *
 * @tparam T type of items stored in the vector. Has to be a pointer.
 */
template<typename T>
class FilteredVectorDecorator : public FilteredVector<T> {
 public:
  /// Alias declaration value_type
  using value_type = typename FilteredVector<T>::value_type;
  /// Alias declaration allocator_type
  using allocator_type = typename FilteredVector<T>::allocator_type;
  /// Alias declaration reference
  using reference = typename FilteredVector<T>::reference;
  /// Alias declaration const_reference
  using const_reference = typename FilteredVector<T>::const_reference;
  /// Alias declaration pointer
  using pointer = typename FilteredVector<T>::pointer;
  /// Alias declaration const_pointer
  using const_pointer = typename FilteredVector<T>::const_pointer;
  /// Alias declaration iterator
  using iterator = typename FilteredVector<T>::iterator;
  /// Alias declaration const_iterator
  using const_iterator = typename FilteredVector<T>::const_iterator;
  /// Alias declaration reverse_iterator
  using reverse_iterator = typename FilteredVector<T>::reverse_iterator;
  /// Alias declaration const_reverse_iterator
  using const_reverse_iterator = typename FilteredVector<
          T>::const_reverse_iterator;
  /// Alias declaration difference_type
  using difference_type = typename FilteredVector<T>::difference_type;
  /// Alias declaration size_type
  using size_type = typename FilteredVector<T>::size_type;

  /* **************************************************************************
   * **************************************************************************
   *                           Constructors/Destructor
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Default ctor
  FilteredVectorDecorator() = delete;
  /// Copy constructor
  FilteredVectorDecorator(const FilteredVectorDecorator<T>&) = delete;
  /// Move constructor
  FilteredVectorDecorator(FilteredVectorDecorator<T>&& other);
  /// Destructor
  virtual ~FilteredVectorDecorator() = default;
  /// Copy assignment
  FilteredVectorDecorator<T>&
      operator=(const FilteredVectorDecorator<T>&) = delete;
  /// Move assignment
  FilteredVectorDecorator<T>& operator=(FilteredVectorDecorator<T>&& rhs);
  /// Custom Ctor
  explicit FilteredVectorDecorator(
      std::shared_ptr<FilteredVector<T>> decorated,
      std::shared_ptr<Filter<T>> filter);

  /* **************************************************************************
   * **************************************************************************
   *                           Public methods
   * **************************************************************************
   * *************************************************************************/
 public:
  /// Return iterator to beginning
  iterator begin() noexcept override;
  /// Return iterator to beginning
  const_iterator begin() const noexcept override;
  /// Return iterator to end
  iterator end() noexcept override;
  /// Return iterator to end
  const_iterator end() const noexcept override;

  /// Return const_iterator to beginning
  const_iterator cbegin() const noexcept override;
  /// Return const_iterator to end
  const_iterator cend() const noexcept override;

  /// Return reverse iterator to reverse beginning
  reverse_iterator rbegin() noexcept override;
  /// Return reverse iterator to reverse end
  reverse_iterator rend() noexcept override;

  /// Return const_reverse_iterator to reverse beginning
  const_reverse_iterator crbegin() noexcept override;
  /// Return const_reverse_iterator to reverse end
  const_reverse_iterator crend() noexcept override;

  /// Return size
  size_type size() const noexcept override;
  /// Return maximum size
  size_type max_size() const noexcept override;
  /// Change size
  void resize(size_type n) override;
  /// Return size of allocated storage capacity
  size_type capacity() const noexcept override;
  /// Test whether the filtered vector is empty
  bool empty() const noexcept override;
  /// Requests that the capacity of the filtered vector is at least n
  void reserve(size_type n) override;
  /// Non-binding request to the filtered vector to reduce its capacity to fit
  /// its size.
  void shrink_to_fit() override;

  /// Returns a reference to the element at position n
  reference operator[] (size_type n) override;
  /// Returns a const reference to the element at position n
  const_reference operator[] (size_type n) const override;
  /// Returns a reference to the element at position n
  reference at(size_type n) override;
  /// Returns a const reference to the element at position n
  const_reference at(size_type n) const override;
  /// Returns a reference to the first element
  reference front() override;
  /// Returns a const reference to the first element
  const_reference front() const override;
  /// Returns a reference to the last element
  reference back() override;
  /// Returns a const reference to the last element
  const_reference back() const override;
  /// Access data
  const value_type* data() const noexcept override;
  /// Access data
  value_type* data() noexcept override;

  // Modifiers
  /// Assigns new contents to the vector
  void assign(iterator first, iterator last) override;
  /// Assigns new contents to the vector
  void assign(size_type n, const value_type& val) override;
  /// Assigns new contents to the vector
  void assign(std::initializer_list<value_type> il) override;
  /// Add element at the end of filtered vector container
  void push_back(const value_type& val) override;
  /// Delete last element
  void pop_back() override;
  /// Clear content
  void clear() noexcept override;

  /* **************************************************************************
   * **************************************************************************
   *                              Data members
   * **************************************************************************
   * *************************************************************************/
 protected:
  /// Pointer to decorated filtered_vector
  std::shared_ptr<FilteredVector<T>> decorated_filtered_vector_;
};

#include "FilteredVectorDecorator.hpp"
#endif  // FILTERED_VECTOR_FILTERED_VECTOR_DECORATOR_H
