/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 12.10.17.
//

#include "FilteredVectorBase.h"

/// Constructor
template <typename T>
FilteredVectorBase<T>::
FilteredVectorBase() : FilteredVector<T>(), data_() {}



/// Move constructor
template <typename T>
FilteredVectorBase<T>::
FilteredVectorBase(FilteredVectorBase<T>&& other) :
        data_(std::move(other.data_)) {
  this->filter_ = std::move(other.filter_);
}


/// Initializer_list ctor
template <typename T>
FilteredVectorBase<T>::
FilteredVectorBase(std::initializer_list<value_type> value_list) :
        FilteredVector<T>(), data_(value_list) {}


/// Move assignment
template <typename T>
FilteredVectorBase<T>& FilteredVectorBase<T>::
operator=(FilteredVectorBase<T>&& rhs) {
  if (this != &rhs) {
    // release filter
    delete this->filter_;
    // get filter from rhs
    this->filter_ = rhs.filter_;
    // reset filter of rhs
    rhs.filter_ = nullptr;
    data_ = std::move(rhs.data_);
  }
  return *this;
}


/// Return iterator to beginning
template <typename T>
typename FilteredVectorBase<T>::iterator FilteredVectorBase<T>::
begin() noexcept {
  auto it = iterator(data(), this->shared_from_this());
  // If current element is filtered out, call operator++
  if (data_.size() > 0 && this->filter_out(*it)) {
    ++it;
  }
  return it;
}


/// Return iterator to beginning
template <typename T>
typename FilteredVectorBase<T>::const_iterator FilteredVectorBase<T>::
begin() const noexcept {
  auto it = const_iterator(data(), this->shared_from_this());
  // If current element is filtered out, call operator++
  if (data_.size() > 0 &&  this->filter_out(*it)) {
    ++it;
  }
  return it;
}


/// Return iterator to end
template <typename T>
typename FilteredVectorBase<T>::iterator FilteredVectorBase<T>::
end() noexcept {
  return iterator(data() + size(), this->shared_from_this());
}


/// Return iterator to end
template <typename T>
typename FilteredVectorBase<T>::const_iterator FilteredVectorBase<T>::
end() const noexcept {
  return const_iterator(data() + size(), this->shared_from_this());
}


/// Return const_iterator to beginning
template <typename T>
typename FilteredVectorBase<T>::const_iterator FilteredVectorBase<T>::
cbegin() const noexcept {
  return const_iterator(begin());
}


/// Return const_iterator to end
template <typename T>
typename FilteredVectorBase<T>::const_iterator FilteredVectorBase<T>::
cend() const noexcept {
  return const_iterator(data() + size(), this->shared_from_this());
}


/// Return reverse iterator to reverse beginning
template <typename T>
typename FilteredVectorBase<T>::reverse_iterator FilteredVectorBase<T>::
rbegin() noexcept {
  return reverse_iterator(end());
}


/// Return reverse iterator to reverse end
template <typename T>
typename FilteredVectorBase<T>::reverse_iterator FilteredVectorBase<T>::
rend() noexcept {
  return reverse_iterator(begin());
}


/// Return const_reverse_iterator to reverse beginning
template <typename T>
typename FilteredVectorBase<
        T>::const_reverse_iterator FilteredVectorBase<T>::
crbegin() noexcept {
  return const_reverse_iterator(cend());
}


/// Return const_reverse_iterator to reverse end
template <typename T>
typename FilteredVectorBase<
        T>::const_reverse_iterator FilteredVectorBase<T>::
crend() noexcept {
  return const_reverse_iterator(cbegin());
}



/// Return size
template <typename T>
typename FilteredVectorBase<T>::size_type FilteredVectorBase<T>::
size() const noexcept {
  return data_.size();
}


/// Return maximum size
template <typename T>
typename FilteredVectorBase<T>::size_type FilteredVectorBase<T>::
max_size() const noexcept {
  return data_.max_size();
}

/// Change size
template <typename T>
void FilteredVectorBase<T>::
resize(size_type n) {
  try {
    data_.resize(n);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Return size of allocated storage capacity
template <typename T>
typename FilteredVectorBase<T>::size_type FilteredVectorBase<T>::
capacity() const noexcept {
  return data_.capacity();
}


/// Test whether the filtered vector is empty
template <typename T>
bool FilteredVectorBase<T>::
empty() const noexcept {
  return data_.empty();
}


/// Requests that the capacity of the filtered vector is at least n
template <typename T>
void FilteredVectorBase<T>::
reserve(size_type n) {
  try {
    data_.reserve(n);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Non-binding request to the filtered vector to reduce its capacity to fit
/// its size.
template <typename T>
void FilteredVectorBase<T>::
shrink_to_fit() {
  try {
    data_.shrink_to_fit();
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Returns a reference to the element at position n
template <typename T>
typename FilteredVectorBase<T>::reference FilteredVectorBase<T>::
operator[] (size_type n) {
  return data_[n];
}


/// Returns a const reference to the element at position n
template <typename T>
typename FilteredVectorBase<T>::const_reference FilteredVectorBase<T>::
operator[] (size_type n) const {
  return data_[n];
}


/// Returns a reference to the element at position n
template <typename T>
typename FilteredVectorBase<T>::reference FilteredVectorBase<T>::
at(size_type n) {
  try {
    return data_.at(n);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Returns a const reference to the element at position n
template <typename T>
typename FilteredVectorBase<T>::const_reference FilteredVectorBase<T>::
at(size_type n) const {
  try {
    return data_.at(n);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Returns a reference to the first element
template <typename T>
typename FilteredVectorBase<T>::reference FilteredVectorBase<T>::
front() {
  try {
    return data_.front();
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Returns a const reference to the first element
template <typename T>
typename FilteredVectorBase<T>::const_reference FilteredVectorBase<T>::
front() const {
  try {
    return data_.front();
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Returns a reference to the last element
template <typename T>
typename FilteredVectorBase<T>::reference FilteredVectorBase<T>::
back() {
  try {
    return data_.back();
  } catch (const std::exception& exception) {
    throw;
  }
}

/// Returns a const reference to the last element
template <typename T>
typename FilteredVectorBase<T>::const_reference FilteredVectorBase<T>::
back() const {
  try {
    return data_.back();
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Access data
template <typename T>
const typename FilteredVectorBase<T>::value_type* FilteredVectorBase<T>::
data() const noexcept {
  return data_.data();
}


/// Access data
template <typename T>
typename FilteredVectorBase<T>::value_type* FilteredVectorBase<T>::
data() noexcept {
  return data_.data();
}


// Modifiers
/// Assigns new contents to the vector
// TODO(mw) Make this a non-member function to add a template parameter
// InputIterator. Parameters of this function are then InputIterator first,
// InputIterator last.
template <typename T>
void FilteredVectorBase<T>::
assign(iterator first, iterator last) {
  try {
    data_.assign(first, last);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Assigns new contents to the vector
template <typename T>
void FilteredVectorBase<T>::
assign(size_type n, const value_type& val) {
  try {
    data_.assign(n, val);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Assigns new contents to the vector
template <typename T>
void FilteredVectorBase<T>::
assign(std::initializer_list<value_type> il) {
  try {
    data_.assign(il);
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Add element at the end of filtered vector container
template <typename T>
void FilteredVectorBase<T>::
push_back(const value_type& val) {
  data_.push_back(val);
}

/// Delete last element
template <typename T>
void FilteredVectorBase<T>::
pop_back() {
  try {
    data_.pop_back();
  } catch (const std::exception& exception) {
    throw;
  }
}


/// Clear content
template <typename T>
void FilteredVectorBase<T>::
clear() noexcept {
  data_.clear();
}
