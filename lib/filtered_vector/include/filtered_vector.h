/*Copyright (C) <2016-2017>  David Parsons, Martin Wannagat

This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/
//
// Created by Martin Wannagat on 06.11.17.
//

#include "filtered_vector/Filter.h"
#include "filtered_vector/FilteredVector.h"
#include "filtered_vector/FilteredVectorBase.h"
#include "filtered_vector/FilteredVectorDecorator.h"
#include "filtered_vector/FilteredVectorIterator.h"
#include "filtered_vector/NullptrFilter.h"
#include "filtered_vector/WhitelistFilter.h"

#include "filtered_vector/create_filtered_vector.h"
