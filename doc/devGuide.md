
Developer's guide
=================

The **hglib** library is dedicated for the modeling of hypergraphs.
Its implementation (in C++) is centered on four class hierarchies: Hypergraph, VertexBase (Undirectedvertex/DirectedVertex), UndirectedHyperedgeBase and Hyperarcbase, whereat vertex and hyperedge/arc objects can only be built from Hypergraph class. Properties can be associated to the vertices, hyperedges/arcs, and the
hypergraph, through a respective struct/class provided by the user. This general
approach makes the library applicable in a wide range of problems.

Prerequisites
-------------

* C++ compiler that supports the C++11 standard (e.g. gcc version >= 4.8)
* [Cmake](https://cmake.org) version >= 3.0.2
* [Cpplint](https://pypi.python.org/pypi/cpplint)
* [Doxygen](http://www.stack.nl/~dimitri/doxygen/)
* [Git](https://git-scm.com)

Getting started
---------------

### Clone the git repository

To clone the git repository of the hglib library, type in the command line:

`git clone https://gitlab.inria.fr/kirikomics/hglib.git`

The created directory hglib contains the following files:
* CMakeLists.txt,
* README.md,
* LICENSE,

and subdirectories:
* docs, containing the documentation of the library,
* src, the source code of the library
* test, unit tests.

### Check that you can build the project and run the tests

    cd hglib
    mkdir -p build
    cd build
    cmake ..
    make check

### Create your own branch

To develop a new feature of the library, create a new branch:

`git checkout -b <name_of_your_branch>`

Guidelines
----------

### Follow the Google code style

As several developers contribute to this library we require to follow a
common code style. We opted for the Google code style. To enforce that
all header and source files follow these guidelines, add the path to all
header and source files to the variable SOURCE_FILES in the file
test/CMakeLists.txt. The declaration of this variable can be found
in the section "*check source files against Google code style*" (at around
line 36) and should look like this:

    set(SOURCE_FILES
            ${PROJECT_SOURCE_DIR}/src/hypergraphDirectedHypergraph.hpp
            ...
            <your_header_file>
            <your_source_file>)

### Code documentation with Doxygen

We generate a documentation of the source code via Doxygen. For now, the
documentation takes into account all files in the src directory. If you
want to add a directory, you can edit line 793 in the file docs/Doxyfile.in
which looks like

    INPUT = @PROJECT_SOURCE_DIR@/src

In general we provide a brief description (one line marked by `\\\`) of a
function in the header file. A more detailed description is added to the
source file, *e.g*:

    /* Function declaration in header file */

    /// Add a vertex
    template<typename... Args>
    void addVertex(const std::string& vertexName, Args... args);

    /* Function implementation in source file */

    /**
    * Variadic template function to add vertices.
    *
    * @param vertexName Name of the vertex
    * @param args Additional argument list that is needed to create a vertex.
    * The identifier is excluded from this list as it is attributed from the
    * hypergraph.
    */
    template <template <typename> typename Vertex_t,
              typename Hyperedge_t,
              typename VertexProperty,
              typename EdgeProperty,
              typename HypergraphProperty>
    template<typename... Args>
    void Hypergraph<Vertex_t,
                    Hyperedge_t,
                    VertexProperty,
                    EdgeProperty,
                    HypergraphProperty>::
    addVertex(const std::string& vertexName, Args... args) {
      // implementation goes here
    }

For readability of the code we put the name of a function at the begin of a line.

Check the [Doxygen](http://www.stack.nl/~dimitri/doxygen/) web page for further details about the syntax.

### Unit tests

We use the [Google's C++ test framework](https://github.com/google/googletest)
to test the library code.
For now, we do not yet require to have a 100% code coverage through a tool as
[gcov](https://gcc.gnu.org/onlinedocs/gcc-4.8.1/gcc/Gcov.html), but you are
encouraged to test as much as you can. Try to figure out the extreme cases of
your functions. Your tests should be added to the `hglib/test` directory. To
actually run your unit tests you have to add an entry in the file
`test/CMakeLists.txt`. Suppose your tests are located in the file
`testNewFeature.cpp`, then you have to add `testNewFeature` to the `TESTS`
variable (around line 75). Running then `make check` from the directory `hglib/build`
will execute the tests in testNewFeature.cpp. Have a look at the tests in
testDirectedGraph.cpp to get an idea how writing tests with Google's C++ test
framework works.

### Git

We use [Git](https://git-scm.com) as distributed version control system. Please,
open a new branch when you want to develop a new feature. Push your local
changes only to the remote of your branch. Prioritize to commit small changes of
the code, because it will make it easier to follow the development process.

A comment on the commit message which should of course clearly describe what
exactly was changed: The first line should not exceed 50 characters and should be
followed by a blank line. More details can be written on the subsequent lines
(do not exceed 72 characters per line!).

### Other

Use the [issue tracker](https://gitlab.inria.fr/kirikomics/hglib/issues) to
communicate errors of the code. We will of course discuss these issues in vivo,
but it is easier to keep track of them in the future using this tool. If you solve
an issue, it is good practise to include the issue number or link in the commit message.