# Benchmark for used container (hglib::GraphElementContainer)

The columns in the below table correspond to the following:

  * \# vertices: Number of vertices that are added to the hypergraph at the beginning.
  * sparsity: Percentage of vertices that are removed from the hypergraph.
  * \# hyperarcs: Number of hyperarcs. We first add as many hyperarcs as vertices with two random tail and two random head vertices. A hyperarc may be removed from the hypergraph when all its tail and head vertices were removed.
  * time build hypergraph: Time elapsed to add/remove vertices and hyperarcs.
  * time access all vertices/hyperarcs: Time elapsed to access 10000 times all vertices and all hyperarcs that existed before the removal of vertices/hyperarcs. This means that we may search for items that are not anymore in the data container.

| # vertices | sparsity (in %) | # hyperarcs | time build hypergraph (sec) | time access all vertices/hyperarcs 10000 times (sec)|
|---|---|---|---|---|
| 1000 | 0 | 1000 | 0.153451 | 0.222886
|  | 1 | 961 | 0.151507 | 0.222926
|  | 5 | 822 | 0.157117 | 0.223031
|  | 10 | 656 | 0.165572 | 0.223282
|  | 15 | 536 | 0.168132 | 0.222482
|  | 20 | 401 | 0.174047 | 0.222208
|  | 25 | 340 | 0.178627 | 0.222242
| 2000 | 0 | 2000 | 0.589012 | 0.446675
|  | 1 | 1925 | 0.59504 | 0.446535
|  | 5 | 1640 | 0.623471 | 0.448949
|  | 10 | 1324 | 0.650805 | 0.445146
|  | 15 | 1041 | 0.672131 | 0.451638
|  | 20 | 870 | 0.693177 | 0.448244
|  | 25 | 624 | 0.70798 | 0.450317
| 5000 | 0 | 5000 | 3.66379 | 1.13481
|  | 1 | 4793 | 3.69493 | 1.12264
|  | 5 | 4097 | 3.85486 | 1.13041
|  | 10 | 3266 | 4.03076 | 1.12382
|  | 15 | 2551 | 4.0776 | 1.07388
|  | 20 | 2032 | 4.25465 | 1.18953
|  | 25 | 1594 | 4.43973 | 1.13238
