// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "hglib.h"

#include <iostream>
#include <string>
#include <cstring>
#include <utility>
#include <vector>
#include <list>
#include <deque>
#include <unordered_set>
#include <forward_list>


#include "gtest/gtest.h"

using std::pair;
using std::string;
using std::vector;
using std::cout;
using std::endl;
using hglib::DirectedVertex;
using hglib::DirectedHyperedge;
using hglib::UndirectedHyperedge;
using hglib::NamedDirectedHyperedge;

/**
 * Test fixture
 */
class TestDirectedHyperGraph : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

  vector<string> vertices {"A", "B", "Foo", "Bar", "", "6"};
  vector<pair<pair<vector<string>, vector<string>>, string>> hyperarcs {
          {{{"A"}, {"Foo"}}, "edge1"},
          {{{"A", "B"}, {"Bar", ""}}, "edge2"},
          {{{"6"}, {""}}, "edge3"}
  };
  vector<pair<pair<vector<string>,
  vector<string>>,
  string>> hyperarcsWithUndeclaredVertices {
    {{{"A"}, {"Foo"}}, "edge1"},
    {{{"A", "B"}, {"Bar", ""}}, "edge2"},
    {{{"6"}, {""}}, "edge3"},
    {{{"A"}, {"C"}}, "edge4"},
    {{{"D"}, {"C"}}, "edge5"}
  };
  vector<pair<pair<vector<string>,
  vector<string>>,
  string>> hyperarcsDuplicateEntries {
    {{{"A"}, {"Foo"}}, "edge1"},
    {{{"A2"}, {"Foo2"}}, "edge1"},
    {{{"A", "B"}, {"Bar", ""}}, "edge2"},
    {{{"6"}, {""}}, "edge3"}
  };
  vector<string> verticesDuplicateEntries {"A", "B", "Foo", "Bar", "", "6",
                                           "A"};

  vector<string> vertices2 {"X", "Y", "Z", "U", "V", "A", "Foo"};
  vector<pair<pair<vector<string>,
  vector<string>>,
  string>> hyperarcs2 {
    {{{"7"}, {"X"}}, "edge1"},
    {{{"Z"}, {"X", "A"}}, "edge2"},
    {{{"Y", "V"}, {"Foo"}}, "edge3"},
    {{{"X"}, {"Y"}}, "edge4"},
    {{{"X", "Z"}, {"U", "V"}}, "edge5"},
    {{{"A"}, {"Foo"}}, "edge6"}
  };

 private:
  std::streambuf* storedStreambuf_;
};


TEST_F(TestDirectedHyperGraph, TestCtor) {
  // legal calls of a DirectedHypergraph
  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge> g;
  hglib::DirectedHypergraph<> g1;  // same as above
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g2;


  // Uncommenting the following lines won't compile anymore due to
  // incorrect template parameters
  /* hglib::DirectedHypergraph<DirectedVertex, hglib::UndirectedHyperedge> g4;
  hglib::DirectedHypergraph<hglib::UndirectedVertex,
                            hglib::UndirectedHyperedge> g5;
  hglib::DirectedHypergraph<hglib::UndirectedVertex,
                            hglib::NamedUndirectedHyperedge> g6;*/
}

TEST_F(TestDirectedHyperGraph, Test_CopyCtor) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      std::string label = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      std::string label = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      std::string label;
      Colour colour = Colour::red;
  };

  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
          VertexProperty, HyperarcProperty, HypergraphProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first);
  }

  // remove vertex "6" and associated hyperarc "edge3"
  g.removeVertex("6");

  // change some default properties in hypergraph with unnamed hyperarcs
  g.getVertexProperties_(g.vertexByName("A")->id())->label = "Test";
  hglib::HyperedgeIdType hyperarcId(0);  // hyperarc "edge1"
  g.getHyperarcProperties_(hyperarcId)->label = "TestHyperarc";
  g.getHyperarcProperties_(hyperarcId)->weight = 1.0;
  g.getHypergraphProperties_()->colour = Colour::blue;

  // Copy hypergraph with unnamed hyperarcs
  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
          VertexProperty, HyperarcProperty, HypergraphProperty> gCopy(g);
  // Check
  ASSERT_EQ(g.nbVertices(), gCopy.nbVertices());
  ASSERT_EQ(g.nbHyperarcs(), gCopy.nbHyperarcs());
  // check graph properties
  ASSERT_EQ(gCopy.getHypergraphProperties()->label.compare(""), 0);
  ASSERT_EQ(gCopy.getHypergraphProperties()->colour, Colour::blue);
  // check vertices
  auto vertexIt = g.verticesBegin();
  auto vertexEnd = g.verticesEnd();
  for (; vertexIt != vertexEnd; ++vertexIt) {
    const auto& vertex = *vertexIt;
    const auto& copiedVertex = gCopy.vertexByName(vertex->name());
    ASSERT_NE(copiedVertex, nullptr);
    ASSERT_EQ(vertex->id(), copiedVertex->id());
    const auto& vertexId = vertex->id();
    // compare in-hyperarcs
    ASSERT_EQ(g.inDegree(vertexId), gCopy.inDegree(vertexId));
    auto hyperarcIt = g.inHyperarcsBegin(vertexId);
    auto hyperarcEnd = g.inHyperarcsEnd(vertexId);
    auto hyperarcCopyIt = gCopy.inHyperarcsBegin(vertexId);
    for (; hyperarcIt != hyperarcEnd; ++hyperarcIt) {
      const auto& hyperarc = *hyperarcIt;
      ASSERT_EQ(hyperarc->id(), (*hyperarcCopyIt)->id());
      ++hyperarcCopyIt;
    }
    // compare out-edges
    ASSERT_EQ(g.outDegree(vertexId), gCopy.outDegree(vertexId));
    hyperarcIt = g.outHyperarcsBegin(vertexId);
    hyperarcEnd = g.outHyperarcsEnd(vertexId);
    hyperarcCopyIt = gCopy.outHyperarcsBegin(vertexId);
    for (; hyperarcIt != hyperarcEnd; ++hyperarcIt) {
      const auto& hyperarc = *hyperarcIt;
      ASSERT_EQ(hyperarc->id(), (*hyperarcCopyIt)->id());
      ++hyperarcCopyIt;
    }
    // compare properties
    const auto vertexProperty = g.getVertexProperties(vertexId);
    const auto copiedVertexProperty = gCopy.getVertexProperties(vertexId);
    ASSERT_EQ(vertexProperty->label.compare(copiedVertexProperty->label), 0);
    ASSERT_EQ(vertexProperty->colour, copiedVertexProperty->colour);
  }

  // check hyperarcs
  auto itHyperarcs = g.hyperarcsBegin();
  auto itHyperarcsEnd = g.hyperarcsEnd();
  for (; itHyperarcs != itHyperarcsEnd; ++itHyperarcs) {
    const auto& hyperarc = *itHyperarcs;
    const auto& copiedHyperarc = gCopy.hyperarcById(hyperarc->id());
    ASSERT_NE(copiedHyperarc, nullptr);
    ASSERT_EQ(hyperarc->id(), copiedHyperarc->id());
    const auto& hyperarcId = hyperarc->id();
    // compare tails
    ASSERT_EQ(g.nbTailVertices(hyperarcId), gCopy.nbTailVertices(hyperarcId));
    auto vertexCopyIt = gCopy.tailsBegin(hyperarcId);
    auto it = g.tailsBegin(hyperarcId);
    auto end = g.tailsEnd(hyperarcId);
    for (; it != end; ++it) {
      const auto& tail = *it;
      ASSERT_EQ(tail->id(), (*vertexCopyIt)->id());
      ASSERT_EQ(tail->name().compare((*vertexCopyIt)->name()), 0);
      ++vertexCopyIt;
    }
    // compare heads
    ASSERT_EQ(g.nbHeadVertices(hyperarcId), gCopy.nbHeadVertices(hyperarcId));
    vertexCopyIt = gCopy.headsBegin(hyperarcId);

    it = g.headsBegin(hyperarcId);
    end = g.headsEnd(hyperarcId);
    for (; it != end; ++it) {
      const auto& head = *it;
      ASSERT_EQ(head->id(), (*vertexCopyIt)->id());
      ASSERT_EQ(head->name().compare((*vertexCopyIt)->name()), 0);
      ++vertexCopyIt;
    }

    // compare properties
    const auto hyperarcProperty = g.getHyperarcProperties(hyperarcId);
    const auto copiedHyperarcProperty =
            gCopy.getHyperarcProperties(hyperarcId);
    ASSERT_EQ(hyperarcProperty->label.compare(copiedHyperarcProperty->label),
              0);
    ASSERT_EQ(hyperarcProperty->weight, copiedHyperarcProperty->weight);
  }

  // Add a vertex and hyperarc to gCopy to check their correct id
  const auto& addedvertex = gCopy.addVertex("Test");
  const auto& addedHyperarc = gCopy.addHyperarc(hglib::NAME, {{"A"}, {"Test"}});
  ASSERT_EQ(addedvertex->id(), 6);
  ASSERT_EQ(addedHyperarc->id(), 3);
}

TEST_F(TestDirectedHyperGraph, Test_CopyCtorWithNamedHyperarc) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      std::string label = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      std::string label = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      std::string label;
      Colour colour = Colour::red;
  };

  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge,
          VertexProperty, HyperarcProperty, HypergraphProperty> g;

  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }

  // remove vertex "6" and associated hyperarc "edge3" from both hypergraphs
  g.removeVertex("6");

  // change some default properties in hypergraph with unnamed hyperarcs
  g.getVertexProperties_(g.vertexByName("A")->id())->label = "Test";
  hglib::HyperedgeIdType hyperarcId(0);  // hyperarc "edge1"
  g.getHyperarcProperties_(hyperarcId)->label = "TestHyperarc";
  g.getHyperarcProperties_(hyperarcId)->weight = 1.0;
  g.getHypergraphProperties_()->colour = Colour::blue;

  // Copy hypergraph with unnamed hyperarcs
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge,
          VertexProperty, HyperarcProperty, HypergraphProperty> gCopy(g);
  // Check
  ASSERT_EQ(g.nbVertices(), gCopy.nbVertices());
  ASSERT_EQ(g.nbHyperarcs(), gCopy.nbHyperarcs());
  // check graph properties
  ASSERT_EQ(gCopy.getHypergraphProperties()->label.compare(""), 0);
  ASSERT_EQ(gCopy.getHypergraphProperties()->colour, Colour::blue);
  // check vertices
  for (const auto& vertex : g.vertices()) {
    const auto& copiedVertex = gCopy.vertexByName(vertex->name());
    ASSERT_NE(copiedVertex, nullptr);
    ASSERT_EQ(vertex->id(), copiedVertex->id());
    const auto& vertexId = vertex->id();
    // compare in-hyperarcs
    ASSERT_EQ(g.inDegree(vertexId), gCopy.inDegree(vertexId));
    auto hyperarcCopyIt = gCopy.inHyperarcsBegin(vertexId);
    auto inArcIt = g.inHyperarcsBegin(vertexId);
    auto inArcEnd = g.inHyperarcsEnd(vertexId);
    for (; inArcIt != inArcEnd; ++inArcIt) {
      const auto& hyperarc = *inArcIt;
      ASSERT_EQ(hyperarc->id(), (*hyperarcCopyIt)->id());
      ASSERT_EQ(hyperarc->name().compare((*hyperarcCopyIt)->name()), 0);
      ++hyperarcCopyIt;
    }
    // compare out-edges
    ASSERT_EQ(g.outDegree(vertexId), gCopy.outDegree(vertexId));
    hyperarcCopyIt = gCopy.outHyperarcsBegin(vertexId);
    auto outArcIt = g.outHyperarcsBegin(vertexId);
    auto outArcEnd = g.outHyperarcsEnd(vertexId);
    for (; outArcIt != outArcEnd; ++outArcIt) {
      const auto& hyperarc = *outArcIt;
      ASSERT_EQ(hyperarc->id(), (*hyperarcCopyIt)->id());
      ASSERT_EQ(hyperarc->name().compare((*hyperarcCopyIt)->name()), 0);
      ++hyperarcCopyIt;
    }
    // compare properties
    const auto vertexProperty = g.getVertexProperties(vertexId);
    const auto copiedVertexProperty = gCopy.getVertexProperties(vertexId);
    ASSERT_EQ(vertexProperty->label.compare(copiedVertexProperty->label), 0);
    ASSERT_EQ(vertexProperty->colour, copiedVertexProperty->colour);
  }

  // check hyperarcs
  for (const auto& hyperarc : g.hyperarcs()) {
    const auto& copiedHyperarc = gCopy.hyperarcById(hyperarc->id());
    ASSERT_NE(copiedHyperarc, nullptr);
    ASSERT_EQ(hyperarc->id(), copiedHyperarc->id());
    ASSERT_EQ(hyperarc->name().compare(copiedHyperarc->name()), 0);
    const auto& hyperarcId = hyperarc->id();
    // compare tails
    ASSERT_EQ(g.nbTailVertices(hyperarcId), gCopy.nbTailVertices(hyperarcId));
    auto vertexCopyIt = gCopy.tailsBegin(hyperarcId);
    auto it = g.tailsBegin(hyperarcId);
    auto end = g.tailsEnd(hyperarcId);
    for (; it != end; ++it) {
      const auto& tail = *it;
      ASSERT_EQ(tail->id(), (*vertexCopyIt)->id());
      ASSERT_EQ(tail->name().compare((*vertexCopyIt)->name()), 0);
      ++vertexCopyIt;
    }
    // compare heads
    ASSERT_EQ(g.nbHeadVertices(hyperarcId), gCopy.nbHeadVertices(hyperarcId));
    vertexCopyIt = gCopy.headsBegin(hyperarcId);
    it = g.headsBegin(hyperarcId);
    end = g.headsEnd(hyperarcId);
    for (; it != end; ++it) {
      const auto& head = *it;
      ASSERT_EQ(head->id(), (*vertexCopyIt)->id());
      ASSERT_EQ(head->name().compare((*vertexCopyIt)->name()), 0);
      ++vertexCopyIt;
    }

    // compare properties
    const auto hyperarcProperty = g.getHyperarcProperties(hyperarcId);
    const auto copiedHyperarcProperty =
            gCopy.getHyperarcProperties(hyperarcId);
    ASSERT_EQ(hyperarcProperty->label.compare(copiedHyperarcProperty->label),
              0);
    ASSERT_EQ(hyperarcProperty->weight, copiedHyperarcProperty->weight);
  }
}

TEST_F(TestDirectedHyperGraph, Test_AssignmentOperator) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      string name = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      string name = "Default";
      Colour colour = Colour::blue;
  };

  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge,
          VertexProperty, HyperarcProperty,
          HypergraphProperty> graph, graph2, graph3;

  for (auto vertexName : vertices) {
    graph.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    graph.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }

  for (auto vertexName : vertices2) {
    graph2.addVertex(vertexName);
    graph3.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs2) {
    try {
      graph2.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
      graph3.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
    } catch (const std::invalid_argument& invalid_argument) {}
  }

  graph.getVertexProperties_(graph.vertexByName("A")->id())->name = "Test";
  graph.getHyperarcProperties_(0)->name = "Test";  // "edge1"
  graph.getHypergraphProperties_()->name = "Test";
  graph.getHypergraphProperties_()->colour = Colour::red;
  graph = graph;  // nothing happens in a self assignment
  graph2 = graph3 = graph;
  // check if both graphs are identical
  // check graph properties
  ASSERT_EQ(graph.getHypergraphProperties()->name.compare("Test"), 0);
  ASSERT_EQ(graph.getHypergraphProperties()->colour, Colour::red);
  ASSERT_EQ(graph2.getHypergraphProperties()->name.compare("Test"), 0);
  ASSERT_EQ(graph2.getHypergraphProperties()->colour, Colour::red);
  ASSERT_EQ(graph3.getHypergraphProperties()->name.compare("Test"), 0);
  ASSERT_EQ(graph3.getHypergraphProperties()->colour, Colour::red);
  // check vertices
  auto vertexItGraph2 = graph2.verticesBegin();
  auto vertexItGraph3 = graph3.verticesBegin();
  for (const auto& vertex : graph.vertices()) {
    ASSERT_EQ(vertex->id(), (*vertexItGraph2)->id());
    ASSERT_EQ(vertex->name().compare((*vertexItGraph2)->name()), 0);
    ASSERT_EQ(vertex->id(), (*vertexItGraph3)->id());
    ASSERT_EQ(vertex->name().compare((*vertexItGraph3)->name()), 0);
    // check in-edges
    auto hyperarcItGraph2 = graph2.inHyperarcsBegin(vertex->id());
    auto hyperarcItGraph3 = graph3.inHyperarcsBegin(vertex->id());
    auto inArcsIt = graph.inHyperarcsBegin(vertex->id());
    auto inArcsEnd = graph.inHyperarcsEnd(vertex->id());
    for (; inArcsIt != inArcsEnd; ++inArcsIt) {
      const auto& hyperarc = *inArcsIt;
      ASSERT_EQ(hyperarc->id(), (*hyperarcItGraph2)->id());
      ASSERT_EQ(hyperarc->name().compare((*hyperarcItGraph2)->name()), 0);
      ASSERT_EQ(hyperarc->id(), (*hyperarcItGraph3)->id());
      ASSERT_EQ(hyperarc->name().compare((*hyperarcItGraph3)->name()), 0);
      ++hyperarcItGraph2;
      ++hyperarcItGraph3;
    }
    // check out-edges
    hyperarcItGraph2 = graph2.outHyperarcsBegin(vertex->id());
    hyperarcItGraph3 = graph3.outHyperarcsBegin(vertex->id());
    auto outArcIt = graph.outHyperarcsBegin(vertex->id());
    auto outArcEnd = graph.outHyperarcsEnd(vertex->id());
    for (; outArcIt != outArcEnd; ++outArcIt) {
      const auto& hyperarc = *outArcIt;
      ASSERT_EQ(hyperarc->id(), (*hyperarcItGraph2)->id());
      ASSERT_EQ(hyperarc->name().compare((*hyperarcItGraph2)->name()), 0);
      ASSERT_EQ(hyperarc->id(), (*hyperarcItGraph3)->id());
      ASSERT_EQ(hyperarc->name().compare((*hyperarcItGraph3)->name()), 0);
      ++hyperarcItGraph2;
      ++hyperarcItGraph3;
    }
    // check properties
    auto vertexProperty = graph.getVertexProperties(vertex->id());
    auto vertex2Property = graph2.getVertexProperties((*vertexItGraph2)->id());
    auto vertex3Property = graph3.getVertexProperties((*vertexItGraph3)->id());
    ASSERT_EQ(vertexProperty->name.compare(vertex2Property->name), 0);
    ASSERT_EQ(vertexProperty->name.compare(vertex3Property->name), 0);
    ASSERT_EQ(vertexProperty->colour, vertex2Property->colour);
    ASSERT_EQ(vertexProperty->colour, vertex3Property->colour);

    ++vertexItGraph2;
    ++vertexItGraph3;
  }

  // check hyperarcs
  auto hyperarcItGraph2 = graph2.hyperarcsBegin();
  auto hyperarcItGraph3 = graph3.hyperarcsBegin();
  for (const auto& hyperarc : graph.hyperarcs()) {
    ASSERT_EQ(hyperarc->id(), (*hyperarcItGraph2)->id());
    ASSERT_EQ(hyperarc->name().compare((*hyperarcItGraph2)->name()), 0);
    ASSERT_EQ(hyperarc->id(), (*hyperarcItGraph3)->id());
    ASSERT_EQ(hyperarc->name().compare((*hyperarcItGraph3)->name()), 0);
    // Check tails
    auto tailItGraph2 = graph2.tailsBegin(hyperarc->id());
    auto tailItGraph3 = graph3.tailsBegin(hyperarc->id());
    auto it = graph.tailsBegin(hyperarc->id());
    auto end = graph.tailsEnd(hyperarc->id());
    for (; it != end; ++it) {
      const auto& vertex = *it;
      ASSERT_EQ(vertex->id(), (*tailItGraph2)->id());
      ASSERT_EQ(vertex->name().compare((*tailItGraph2)->name()), 0);
      ASSERT_EQ(vertex->id(), (*tailItGraph3)->id());
      ASSERT_EQ(vertex->name().compare((*tailItGraph3)->name()), 0);
      ++tailItGraph2;
      ++tailItGraph3;
    }
    // Check heads
    auto headItGraph2 = graph2.headsBegin(hyperarc->id());
    auto headItGraph3 = graph3.headsBegin(hyperarc->id());
    it = graph.headsBegin(hyperarc->id());
    end = graph.headsEnd(hyperarc->id());
    for (; it != end; ++it) {
      const auto& vertex = *it;
      ASSERT_EQ(vertex->id(), (*headItGraph2)->id());
      ASSERT_EQ(vertex->name().compare((*headItGraph2)->name()), 0);
      ASSERT_EQ(vertex->id(), (*headItGraph3)->id());
      ASSERT_EQ(vertex->name().compare((*headItGraph3)->name()), 0);
      ++headItGraph2;
      ++headItGraph3;
    }
    // Check properties
    const auto hyperarcProperty = graph.getHyperarcProperties(hyperarc->id());
    const auto hyperarcProperty2 = graph2.getHyperarcProperties(
            (*hyperarcItGraph2)->id());
    const auto hyperarcProperty3 = graph3.getHyperarcProperties(
            (*hyperarcItGraph3)->id());
    ASSERT_EQ(hyperarcProperty->name.compare(hyperarcProperty2->name), 0);
    ASSERT_EQ(hyperarcProperty->name.compare(hyperarcProperty3->name), 0);
    ASSERT_EQ(hyperarcProperty->weight, hyperarcProperty2->weight);
    ASSERT_EQ(hyperarcProperty->weight, hyperarcProperty3->weight);

    ++hyperarcItGraph2;
    ++hyperarcItGraph3;
  }

  // Add a vertex and hyperarc to graph2 to check their correct id
  const auto& addedvertex = graph2.addVertex("Test2");
  const auto& addedHyperarc = graph2.addHyperarc(hglib::NAME,
                                                 {{"A"}, {"Test2"}},
                                                 {"Test_arc"});
  ASSERT_EQ(addedvertex->id(), 6);
  ASSERT_EQ(addedHyperarc->id(), 3);

  // Add a vertex and hyperarc to graph3 to check their correct id
  const auto& addedvertex2 = graph3.addVertex("Test3");
  const auto& addedHyperarc2 = graph3.addHyperarc(hglib::NAME,
                                                  {{"A"}, {"Test3"}},
                                                  {"Test_arc2"});
  ASSERT_EQ(addedvertex2->id(), 6);
  ASSERT_EQ(addedHyperarc2->id(), 3);
}

TEST_F(TestDirectedHyperGraph, Test_addVertex) {
  hglib::DirectedHypergraph<> g;
  size_t nbVertices = 0;
  hglib::VertexIdType vertexCounter = 0;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
    ASSERT_EQ(++nbVertices, g.nbVertices());
    ASSERT_EQ(vertexCounter++, g.vertexByName(vertexName)->id());
  }
  // re-insertion of the same vertex (name) is not possible
  for (auto vertexName : vertices) {
    ASSERT_THROW(g.addVertex(vertexName), std::invalid_argument);
    ASSERT_EQ(nbVertices, g.nbVertices());
  }

  // insert vertices in another graph and check if the vertex ids start by zero
  hglib::DirectedHypergraph<> g2;
  vertexCounter = 0;
  for (auto vertexName : vertices) {
    g2.addVertex(vertexName);
    ASSERT_EQ(vertexCounter++, g2.vertexByName(vertexName)->id());
  }

  // Add vertices without specifying the name
  hglib::DirectedHypergraph<> g3;
  vertexCounter = 0;
  for (auto vertexName : vertices) {
    g3.addVertex();
    std::string expectedName = "dv_" + std::to_string(vertexCounter);
    const auto& vertex = g3.vertexByName(expectedName);
    ASSERT_NE(vertex, nullptr);
    ASSERT_EQ(vertex->name().compare(expectedName), 0);
    ASSERT_EQ(vertex->id(), vertexCounter++);
  }

  // TODO(mw): test with other types of vertices (more parameter than just a
  // name)
  // TODO(mw): test with vertex property (access functions have to be
  // implemented)
}

TEST_F(TestDirectedHyperGraph, Test_addHyperarc) {
  hglib::DirectedHypergraph<> g;  // graph with multi-hyperarcs
  // graph without multi-hyperarcs
  hglib::DirectedHypergraph<> g2(false);
  // graph with multi-hyperarcs
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g3;
  // graph without multi-hyperarcs
  hglib::DirectedHypergraph<DirectedVertex,
          NamedDirectedHyperedge> g4(false);

  // insert vertices in all hypergraphs
  for (const auto& vertexName : vertices) {
    g.addVertex(vertexName);
    g2.addVertex(vertexName);
    g3.addVertex(vertexName);
    g4.addVertex(vertexName);
  }
  // add hyperarcs in all hypergraphs
  size_t nbHyperarcs(0);
  hglib::HyperedgeIdType hyperarcCounter(0);
  for (auto hyperarc : hyperarcs) {
    const auto& arc1 = g.addHyperarc(hglib::NAME, hyperarc.first);
    const auto& arc2 = g2.addHyperarc(hglib::NAME, hyperarc.first);
    const auto& arc3 = g3.addHyperarc(hglib::NAME, hyperarc.first,
                                      hyperarc.second);
    const auto& arc4 = g4.addHyperarc(hglib::NAME, hyperarc.first,
                                      hyperarc.second);
    ++nbHyperarcs;
    ASSERT_EQ(nbHyperarcs, g.nbHyperarcs());
    ASSERT_EQ(nbHyperarcs, g2.nbHyperarcs());
    ASSERT_EQ(nbHyperarcs, g3.nbHyperarcs());
    ASSERT_EQ(nbHyperarcs, g4.nbHyperarcs());
    ASSERT_EQ(hyperarcCounter, arc1->id());
    ASSERT_EQ(hyperarcCounter, arc2->id());
    ASSERT_EQ(hyperarcCounter, arc3->id());
    ASSERT_EQ(hyperarcCounter, arc4->id());
    ++hyperarcCounter;
  }

  // check in/out degree of vertices in hypergraph g
  for (const auto& vertexName : vertices) {
    const auto& vertex = g.vertexByName(vertexName);
    ASSERT_NE(vertex, nullptr);
    switch (vertex->id()) {
      case 0:  // vertex A
        ASSERT_EQ(g.inDegree(vertex->id()), (size_t) 0);
        ASSERT_EQ(g.outDegree(vertex->id()), (size_t) 2);
        break;
      case 1:  // vertex B
        ASSERT_EQ(g.inDegree(vertex->id()), (size_t) 0);
        ASSERT_EQ(g.outDegree(vertex->id()), (size_t) 1);
        break;
      case 2:  // vertex Foo
        ASSERT_EQ(g.inDegree(vertex->id()), (size_t) 1);
        ASSERT_EQ(g.outDegree(vertex->id()), (size_t) 0);
        break;
      case 3:  // vertex Bar
        ASSERT_EQ(g.inDegree(vertex->id()), (size_t) 1);
        ASSERT_EQ(g.outDegree(vertex->id()), (size_t) 0);
        break;
      case 4:  // vertex ''
        ASSERT_EQ(g.inDegree(vertex->id()), (size_t) 2);
        ASSERT_EQ(g.outDegree(vertex->id()), (size_t) 0);
        break;
      case 5:  // vertex 6
        ASSERT_EQ(g.inDegree(vertex->id()), (size_t) 0);
        ASSERT_EQ(g.outDegree(vertex->id()), (size_t) 1);
        break;
    }
  }


  size_t nbCurrentHyperarcs(nbHyperarcs);
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first);
    ASSERT_THROW(g2.addHyperarc(hglib::NAME, hyperarc.first),
                 std::invalid_argument);
    g3.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
    ASSERT_THROW(g4.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second),
                 std::invalid_argument);
    ASSERT_EQ(++nbHyperarcs, g.nbHyperarcs());
    ASSERT_EQ(nbCurrentHyperarcs, g2.nbHyperarcs());
    ASSERT_EQ(nbHyperarcs, g3.nbHyperarcs());
    ASSERT_EQ(nbCurrentHyperarcs, g4.nbHyperarcs());
  }

  // test undeclared vertices in a hyperarc
  hglib::DirectedHypergraph<> g5;
  for (auto vertexName : vertices) {
    g5.addVertex(vertexName);
  }
  // 2 out of 5 hyperarcs have undeclared vertices
  for (auto hyperarc : hyperarcsWithUndeclaredVertices) {
    try {
      g5.addHyperarc(hglib::NAME, hyperarc.first);
    } catch (std::invalid_argument& invalidArgument) {}
  }
  ASSERT_EQ(g5.nbHyperarcs(), (size_t) 3);

  // test undeclared vertex identifiers in a hyperarc
  // Undeclared tail
  try {
    g.addHyperarc({{100}, {1}});
    FAIL() << "Expected std::out_of_range\n";
  } catch (const std::out_of_range& err) {
    std::string expected("Error in DirectedHypergraph::addHyperarc(): Tail"
                             " vertex 100 is not declared in this hypergraph.");
    EXPECT_EQ(err.what(), std::string(expected));
  } catch(...) {
    FAIL() << "Expected std::out_of_range";
  }

  // Undeclared head
  try {
    g.addHyperarc({{1}, {100}});
    FAIL() << "Expected std::out_of_range\n";
  } catch (const std::out_of_range& err) {
    std::string expected("Error in DirectedHypergraph::addHyperarc(): Head"
                             " vertex 100 is not declared in this hypergraph.");
    EXPECT_EQ(err.what(), std::string(expected));
  } catch(...) {
    FAIL() << "Expected std::out_of_range";
  }

  // Remove a vertex
  g.removeVertex(0, false);
  // Undeclared tail because it was removed
  try {
    g.addHyperarc({{0}, {1}});
    FAIL() << "Expected std::invalid_argument\n";
  } catch (const std::invalid_argument& err) {
    std::string expected("Error in DirectedHypergraph::addHyperarc(): Tail"
                             " vertex 0 is not declared in this hypergraph.");
    EXPECT_EQ(err.what(), std::string(expected));
  } catch(...) {
    FAIL() << "Expected std::invalid_argument";
  }

  // Undeclared head because it was removed
  try {
    g.addHyperarc({{1}, {0}});
    FAIL() << "Expected std::invalid_argument\n";
  } catch (const std::invalid_argument& err) {
    std::string expected("Error in DirectedHypergraph::addHyperarc(): Head"
                             " vertex 0 is not declared in this hypergraph.");
    EXPECT_EQ(err.what(), std::string(expected));
  } catch(...) {
    FAIL() << "Expected std::invalid_argument";
  }
}

TEST_F(TestDirectedHyperGraph, Test_vertexByName) {
  hglib::DirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
    ASSERT_EQ(g.vertexByName(vertexName)->name().compare(vertexName), 0);
  }
  ASSERT_EQ(g.vertexByName("UnknownVertex"), nullptr);
}

TEST_F(TestDirectedHyperGraph, Test_vertexById) {
  hglib::DirectedHypergraph<> g;
  hglib::VertexIdType vertexCounter = 0;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
    ASSERT_EQ(g.vertexById(vertexCounter)->id(), vertexCounter);
    ASSERT_EQ(g.vertexByName(vertexName), g.vertexById(vertexCounter++));
  }
  // vertex with given id does not exist -> exception
  ASSERT_THROW(g.vertexById(vertexCounter), std::out_of_range);

  // Check that a nullptr is returned after the removal of a vertex
  vertexCounter = 0;
  for (auto vertexName : vertices) {
    g.removeVertex(vertexCounter);
    ASSERT_EQ(g.vertexById(vertexCounter), nullptr);
    ++vertexCounter;
  }
}

TEST_F(TestDirectedHyperGraph, Test_verticesContainer) {
  hglib::DirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }

  std::vector<const DirectedVertex<DirectedHyperedge>*> verticesContainer;
  hglib::vertices(g, std::back_inserter(verticesContainer));
  auto it = vertices.begin();
  for (auto& vertex : verticesContainer) {
    EXPECT_EQ(vertex->name().compare(*it), 0);
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
    ++it;
  }

  // test with std:list
  // std::back_inserter
  std::list<const DirectedVertex<DirectedHyperedge>*> l;
  hglib::vertices(g, std::back_inserter(l));
  EXPECT_EQ(l.size(), g.nbVertices());
  for (auto& vertex : l) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }
  l.clear();
  // std::inserter
  hglib::vertices(g, std::inserter(l, l.begin()));
  EXPECT_EQ(l.size(), g.nbVertices());
  for (auto& vertex : l) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }

  // test with std::deque
  std::deque<const DirectedVertex<DirectedHyperedge>*> d;
  hglib::vertices(g, std::back_inserter(d));
  EXPECT_EQ(d.size(), g.nbVertices());
  for (auto& vertex : d) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }

  // test with std::set
  std::set<const DirectedVertex<DirectedHyperedge>*> s;
  hglib::vertices(g, std::inserter(s, s.begin()));
  EXPECT_EQ(s.size(), g.nbVertices());
  for (auto& vertex : s) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }
// test with std::multiset
  std::multiset<const DirectedVertex<DirectedHyperedge>*> ms;
  hglib::vertices(g, std::inserter(ms, ms.begin()));
  EXPECT_EQ(ms.size(), g.nbVertices());
  for (auto& vertex : ms) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }

  // test with std::unordered_set
  std::unordered_set<const DirectedVertex<DirectedHyperedge>*> us;
  hglib::vertices(g, std::inserter(us, us.begin()));
  EXPECT_EQ(us.size(), g.nbVertices());
  for (auto& vertex : us) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }

  // test with std::unordered_multiset
  std::unordered_multiset<const DirectedVertex<DirectedHyperedge>*> ums;
  hglib::vertices(g, std::inserter(ums, ums.begin()));
  EXPECT_EQ(ums.size(), g.nbVertices());
  for (auto& vertex : ums) {
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
  }
}


TEST_F(TestDirectedHyperGraph, Test_hyperarcsContainer) {
  hglib::DirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto arc : hyperarcs) {
    g.addHyperarc(hglib::NAME, arc.first);
  }

  std::vector<const DirectedHyperedge*> hyperarcContainer;
  hglib::hyperarcs(g, std::back_inserter(hyperarcContainer));
  ASSERT_EQ(hyperarcs.size(), hyperarcContainer.size());
  ASSERT_EQ(hyperarcs.size(), g.nbHyperarcs());
  for (auto& hyperarc : hyperarcContainer) {
    EXPECT_NE(g.hyperarcById(hyperarc->id()), nullptr);
  }

  // test with std:list
  // std::back_inserter
  std::list<const DirectedHyperedge*> l;
  hglib::hyperarcs(g, std::back_inserter(l));
  EXPECT_EQ(l.size(), g.nbHyperarcs());
  for (auto& hyperarc : l) {
    EXPECT_NE(g.hyperarcById(hyperarc->id()), nullptr);
  }
  l.clear();
  // std::inserter
  hglib::hyperarcs(g, std::inserter(l, l.begin()));
  EXPECT_EQ(l.size(), g.nbHyperarcs());
  for (auto& hyperarc : l) {
    EXPECT_NE(g.hyperarcById(hyperarc->id()), nullptr);
  }

  // test with std::deque
  std::deque<const DirectedHyperedge*> d;
  hglib::hyperarcs(g, std::back_inserter(d));
  EXPECT_EQ(d.size(), g.nbHyperarcs());
  for (auto& hyperarc : d) {
    EXPECT_NE(g.hyperarcById(hyperarc->id()), nullptr);
  }

  // test with std::set
  std::set<const DirectedHyperedge*> s;
  hglib::hyperarcs(g, std::inserter(s, s.begin()));
  EXPECT_EQ(s.size(), g.nbHyperarcs());
  for (auto& hyperarc : s) {
    EXPECT_NE(g.hyperarcById(hyperarc->id()), nullptr);
  }
// test with std::multiset
  std::multiset<const DirectedHyperedge*> ms;
  hglib::hyperarcs(g, std::inserter(ms, ms.begin()));
  EXPECT_EQ(ms.size(), g.nbHyperarcs());
  for (auto& hyperarc : ms) {
    EXPECT_NE(g.hyperarcById(hyperarc->id()), nullptr);
  }

  // test with std::unordered_set
  std::unordered_set<const DirectedHyperedge*> us;
  hglib::hyperarcs(g, std::inserter(us, us.begin()));
  EXPECT_EQ(us.size(), g.nbHyperarcs());
  for (auto& hyperarc : us) {
    EXPECT_NE(g.hyperarcById(hyperarc->id()), nullptr);
  }

  // test with std::unordered_multiset
  std::unordered_multiset<const DirectedHyperedge*> ums;
  hglib::hyperarcs(g, std::inserter(ums, ums.begin()));
  EXPECT_EQ(ums.size(), g.nbHyperarcs());
  for (auto& hyperarc : ums) {
    EXPECT_NE(g.hyperarcById(hyperarc->id()), nullptr);
  }

  // test with std::forward_list
  std::forward_list<const DirectedHyperedge*> fl;
  hglib::hyperarcs(g, std::front_inserter(fl));
  uint flsize = 0;
  for (auto& hyperarc : fl) {
    ++flsize;
    EXPECT_NE(g.hyperarcById(hyperarc->id()), nullptr);
  }
  EXPECT_EQ(flsize, g.nbHyperarcs());

  // The following blocks of code don't compile for different reasons.
  // Each of them exist to showcase specific error messages provided for
  // common user errors

//  // This block should yield a static assertion fail with message:
//  // "provided iterator not an OutputIterator"
//  std::array<DirectedHyperedge*, 3> arr;
//  hglib::hyperarcs(g, arr.begin());
//  EXPECT_EQ(arr.size(), g.nbHyperarcs());
//  for (auto& hyperarc : arr) {
//    EXPECT_NE(g.hyperarcById(hyperarc->id()), nullptr);
//  }

//  // This block should yield a standard error which we consider is readable
//  // enough to not require a specific message.
//  // e.g. "no match for operator= (operand types are iter<cont<xxx>> and yyy)"
//  std::forward_list<const UndirectedHyperedge*> fl2;
//  hglib::hyperarcs(g, std::front_inserter(fl2));
//  EXPECT_EQ(3, g.nbHyperarcs());
//  for (auto& hyperarc : fl2) {
//    EXPECT_NE(g.hyperarcById(hyperarc->id()), nullptr);
//  }

//  // No special treatment for this kind of error
//  // We consider it's deliberate :p
//  std::array<DirectedHyperedge*, 3> arr2;
//  hglib::hyperarcs(g, std::inserter(arr2, arr2.begin()));
//  EXPECT_EQ(arr2.size(), g.nbHyperarcs());
//  for (auto& hyperarc : arr2) {
//    EXPECT_NE(g.hyperarcById(hyperarc->id()), nullptr);
//  }
}

TEST_F(TestDirectedHyperGraph, Test_vertexIterator) {
  hglib::DirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }

  // test verticesBeginEnd (pair of begin/end iter)
  hglib::DirectedHypergraph<>::vertex_iterator it, end;
  std::tie(it, end) = g.verticesBeginEnd();
  ASSERT_EQ(it, g.verticesBegin());
  ASSERT_EQ(end, g.verticesEnd());
  int pos(0);
  for (; it != end; ++it) {
    ASSERT_EQ((*it)->id(), (hglib::VertexIdType) pos);
    ASSERT_STREQ((*it)->name().c_str(), vertices[pos].c_str());
    ++pos;
  }

  // test verticesBegin/verticesEnd
  pos = 0;
  it = g.verticesBegin();
  for (; it != g.verticesEnd(); ++it) {
    ASSERT_EQ((*it)->id(), (hglib::VertexIdType) pos);
    ASSERT_STREQ((*it)->name().c_str(), vertices[pos].c_str());
    ++pos;
  }

  // test ranged based for loop
  pos = 0;
  for (const auto& vertex : g.vertices()) {
    ASSERT_EQ(vertex->id(), (hglib::VertexIdType) pos);
    ASSERT_STREQ(vertex->name().c_str(), vertices[pos].c_str());
    ++pos;
  }

  // test iterators after the removal of vertices
  g.removeVertex(1);
  g.removeVertex(3);
  g.removeVertex(5);

  std::tie(it, end) = g.verticesBeginEnd();
  auto it2 = g.verticesBegin();
  for (const auto& vertex : g.vertices()) {
    ASSERT_EQ(vertex, (*it));
    ASSERT_EQ(vertex, (*it2));
    ++it;
    ++it2;
  }
}

TEST_F(TestDirectedHyperGraph, Test_getVertexProperties) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct EdgeProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
          VertexProperty, EdgeProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (const auto& vertex : g.vertices()) {
    auto vProp = g.getVertexProperties_(vertex->id());
    ASSERT_EQ(vProp->name.compare("Default"), 0);
    ASSERT_EQ(vProp->colour, Colour::green);
    vProp->name = vertex->name() + "_" + std::to_string(vertex->id());
    vProp->colour = Colour::red;
    // Verify that updated properties are stored in the hypergraph
    const auto constVProp = g.getVertexProperties(vertex->id());
    string expected = vertex->name() + "_" + std::to_string(vertex->id());
    ASSERT_EQ(constVProp->name.compare(expected), 0);
    ASSERT_EQ(constVProp->colour, Colour::red);
  }

  hglib::VertexIdType outOfRange = 10;
  ASSERT_THROW(g.getVertexProperties_(outOfRange), std::out_of_range);
  ASSERT_THROW(g.getVertexProperties(outOfRange), std::out_of_range);
  outOfRange = -10;
  ASSERT_THROW(g.getVertexProperties_(outOfRange), std::out_of_range);
  ASSERT_THROW(g.getVertexProperties(outOfRange), std::out_of_range);

  // without VertexProperty (empty struct)
  hglib::DirectedHypergraph<> g2;
  for (auto vertexName : vertices) {
    g2.addVertex(vertexName);
  }

  for (const auto& vertex : g2.vertices()) {
    auto vProp = g2.getVertexProperties(vertex->id());
    EXPECT_EQ(typeid(hglib::emptyProperty), typeid(*vProp));
    EXPECT_NE(typeid(VertexProperty), typeid(*vProp));
  }
}

TEST_F(TestDirectedHyperGraph, Test_verticesWithPropertyContainer) {
  enum Colour {
      red, blue, green, black
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct EdgeProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
          VertexProperty, EdgeProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }

  // Get all (green) vertices
  std::vector<const DirectedVertex<DirectedHyperedge>*> verticesContainer;
  hglib::vertices(g, std::back_inserter(verticesContainer),
                 [&] (hglib::VertexIdType vertexId) -> bool {
                     return g.getVertexProperties(vertexId)->colour ==
                            Colour::green;});
  // All vertices are green by default
  ASSERT_EQ(verticesContainer.size(), vertices.size());
  auto it = vertices.begin();
  for (auto& vertex : verticesContainer) {
    EXPECT_EQ(vertex->name().compare(*it), 0);
    EXPECT_NE(g.vertexByName(vertex->name()), nullptr);
    EXPECT_NE(g.vertexById(vertex->id()), nullptr);
    ++it;
  }
  // set colour of vertex 'A' to red
  g.getVertexProperties_(
          g.vertexByName("A")->id())->colour = Colour::red;
  // set colour of vertex 'B' and 'Foo' to blue
  g.getVertexProperties_(
          g.vertexByName("B")->id())->colour = Colour::blue;
  g.getVertexProperties_(
          g.vertexByName("Foo")->id())->colour = Colour::blue;
  // Get vertices by colour
  for ( int idx = red; idx != black; idx++ ) {
    verticesContainer.clear();  // empty container
    // Get vertices of specified colour
    Colour colour = static_cast<Colour>(idx);
    hglib::vertices(g, std::back_inserter(verticesContainer),
                   [&] (hglib::VertexIdType vertexId) ->
                           bool {
                       return g.getVertexProperties(vertexId)->colour ==
                              colour;});
    switch (colour) {
      case red:
        ASSERT_EQ(verticesContainer.size(), (size_t) 1);
        ASSERT_EQ(verticesContainer[0]->name().compare("A"), 0);
        break;
      case blue:
        ASSERT_EQ(verticesContainer.size(), (size_t) 2);
        ASSERT_EQ(verticesContainer[0]->name().compare("B"), 0);
        ASSERT_EQ(verticesContainer[1]->name().compare("Foo"), 0);
        break;
      case green:
        ASSERT_EQ(verticesContainer.size(), (size_t) 3);
        ASSERT_EQ(verticesContainer[0]->name().compare("Bar"), 0);
        ASSERT_EQ(verticesContainer[1]->name().compare(""), 0);
        ASSERT_EQ(verticesContainer[2]->name().compare("6"), 0);
        break;
      case black:
        ASSERT_TRUE(verticesContainer.empty());
    }
  }
}

TEST_F(TestDirectedHyperGraph, Test_GetHypergraphProperties) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct EdgeProperty {
      std::string name = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      std::string name;
      Colour colour = Colour::red;
  };

  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
          VertexProperty, EdgeProperty, HypergraphProperty> g;

  ASSERT_EQ(g.getHypergraphProperties()->name.compare(""), 0);
  ASSERT_EQ(g.getHypergraphProperties()->colour, Colour::red);
  // change hypergraph properties
  g.getHypergraphProperties_()->name = "Test";
  g.getHypergraphProperties_()->colour = Colour::blue;
  // check changed properties
  ASSERT_EQ(g.getHypergraphProperties()->name.compare("Test"), 0);
  ASSERT_EQ(g.getHypergraphProperties()->colour, Colour::blue);

  // without HypergraphProperty (empty struct)
  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
          VertexProperty, EdgeProperty> g2;
  EXPECT_EQ(typeid(hglib::emptyProperty),
            typeid(*g2.getHypergraphProperties()));
  EXPECT_NE(typeid(HypergraphProperty),
            typeid(*g2.getHypergraphProperties()));
}

TEST_F(TestDirectedHyperGraph, Test_hyperarcById) {
  hglib::DirectedHypergraph<> g;
  // insert vertices
  for (const auto& vertexName : vertices) {
    g.addVertex(vertexName);
  }
  // add hyperarcs
  hglib::HyperedgeIdType hyperarcCounter(0);
  for (auto hyperarc : hyperarcs) {
    const auto& hyperarc1 = g.addHyperarc(hglib::NAME, hyperarc.first);
    const DirectedHyperedge* hyperarc2 = g.hyperarcById(hyperarcCounter);
    ASSERT_EQ(hyperarc1, hyperarc2);
    ++hyperarcCounter;
  }

  // Check that a nullptr is returned after the removal of a hyperarc
  hyperarcCounter = 0;
  for (auto hyperarc : hyperarcs) {
    g.removeHyperarc(hyperarcCounter);
    ASSERT_EQ(g.hyperarcById(hyperarcCounter), nullptr);
    ++hyperarcCounter;
  }

  hglib::HyperedgeIdType outOfRange(10);
  ASSERT_THROW(g.hyperarcById(outOfRange), std::out_of_range);
  outOfRange = -10;
  ASSERT_THROW(g.hyperarcById(outOfRange), std::out_of_range);
}

TEST_F(TestDirectedHyperGraph, Test_hyperarcIterator) {
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }

  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }

  // test hyperarcsBeginEnd (pair of begin/end iter)
  hglib::DirectedHypergraph<DirectedVertex,
          NamedDirectedHyperedge>::hyperarc_iterator it, end;
  std::tie(it, end) = g.hyperarcsBeginEnd();
  ASSERT_EQ(it, g.hyperarcsBegin());
  ASSERT_EQ(end, g.hyperarcsEnd());
  int pos(0);
  for (; it != end; ++it) {
    ASSERT_EQ((*it)->id(), (hglib::HyperedgeIdType) pos);
    ASSERT_STREQ((*it)->name().c_str(), hyperarcs[pos].second.c_str());
    ++pos;
  }

  // test hyperarcsBegin/hyperarcsEnd
  pos = 0;
  auto it2 = g.hyperarcsBegin();
  for (; it2 != g.hyperarcsEnd(); ++it2) {
    ASSERT_EQ((*it2)->id(), (hglib::HyperedgeIdType) pos);
    ASSERT_STREQ((*it2)->name().c_str(), hyperarcs[pos].second.c_str());
    ++pos;
  }

  // test ranged based for loop
  pos = 0;
  for (const auto& hyperarc : g.hyperarcs()) {
    ASSERT_EQ(hyperarc->id(), (hglib::HyperedgeIdType) pos);
    ASSERT_STREQ(hyperarc->name().c_str(), hyperarcs[pos].second.c_str());
    ++pos;
  }

  // Test iterator after the removal of vertices/hyperarcs
  size_t idx(0);
  for (; idx < hyperarcs.size() ; ++idx) {
    g.removeHyperarc(idx);
    // using begin
    it2 = g.hyperarcsBegin();
    // using begin/end-pair
    std::tie(it, end) = g.hyperarcsBeginEnd();
    for (const auto& hyperarc : g.hyperarcs()) {
      ASSERT_EQ((*it2), hyperarc);
      ASSERT_EQ((*it), hyperarc);
      ++it2;
      ++it;
    }
  }
}

TEST_F(TestDirectedHyperGraph, Test_getHyperarcProperties) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge,
          VertexProperty, HyperarcProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }

  for (const auto& hyperarc : g.hyperarcs()) {
    auto hyperarcProp = g.getHyperarcProperties_(hyperarc->id());
    ASSERT_EQ(hyperarcProp->name.compare("Default"), 0);
    ASSERT_EQ(hyperarcProp->weight, 0.0);
    hyperarcProp->name = hyperarc->name() + "_" +
            std::to_string(hyperarc->id());
    hyperarcProp->weight = 1.0;
    // Verify that updated properties are stored in the hypergraph
    const auto constVProp = g.getHyperarcProperties(hyperarc->id());
    string expected = hyperarc->name() + "_" + std::to_string(hyperarc->id());
    ASSERT_EQ(constVProp->name.compare(expected), 0);
    ASSERT_EQ(constVProp->weight, 1.0);
  }

  hglib::HyperedgeIdType outOfRange(10);
  ASSERT_THROW(g.getHyperarcProperties(outOfRange), std::out_of_range);
  ASSERT_THROW(g.getHyperarcProperties_(outOfRange), std::out_of_range);
  outOfRange = -10;
  ASSERT_THROW(g.getHyperarcProperties(outOfRange), std::out_of_range);
  ASSERT_THROW(g.getHyperarcProperties_(outOfRange), std::out_of_range);
  // remove a hyperarc
  g.removeHyperarc(0);
  ASSERT_THROW(g.getHyperarcProperties(0), std::invalid_argument);
  ASSERT_THROW(g.getHyperarcProperties_(0), std::invalid_argument);

  // without HyperarcProperty (empty struct)
  hglib::DirectedHypergraph<> g2;
  for (auto vertexName : vertices) {
    g2.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g2.addHyperarc(hglib::NAME, hyperarc.first);
  }

  for (const auto& hyperarc : g2.hyperarcs()) {
    auto hyperarcProp = g2.getHyperarcProperties(hyperarc->id());
    EXPECT_EQ(typeid(hglib::emptyProperty), typeid(*hyperarcProp));
    EXPECT_NE(typeid(HyperarcProperty), typeid(*hyperarcProp));
  }
}

TEST_F(TestDirectedHyperGraph, Test_hyperarcsWithPropertyContainer) {
  enum Colour {
      red, blue, green, black
  };
  struct VertexProperty {
      string name = "Default";
  };
  struct EdgeProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };

  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge,
          VertexProperty, EdgeProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }

  // Get all (green) vertices
  std::vector<const NamedDirectedHyperedge*> hyperarcsContainer;
  hglib::hyperarcs(g, std::back_inserter(hyperarcsContainer),
                   [&] (hglib::HyperedgeIdType hyperarcId) -> bool {
                       return g.getHyperarcProperties(hyperarcId)->colour ==
                              Colour::green;});
  // All vertices are green by default
  ASSERT_EQ(hyperarcsContainer.size(), g.nbHyperarcs());
  for (auto& hyperarc : hyperarcsContainer) {
    EXPECT_NE(g.hyperarcById(hyperarc->id()), nullptr);
  }
  // set colour of hyperarc 'edge1' to red
  g.getHyperarcProperties_(0)->colour = Colour::red;
  // set colour of hyperarc 'edge2' and 'edge3' to blue
  g.getHyperarcProperties_(1)->colour = Colour::blue;
  g.getHyperarcProperties_(2)->colour = Colour::blue;
  // Get hyperarcs by colour
  for ( int idx = red; idx != black; idx++ ) {
    hyperarcsContainer.clear();  // empty container
    // Get vertices of specified colour
    Colour colour = static_cast<Colour>(idx);
    hglib::hyperarcs(g, std::back_inserter(hyperarcsContainer),
                     [&] (hglib::HyperedgeIdType arcId) ->
                             bool {
                         return g.getHyperarcProperties(arcId)->colour ==
                                colour;});
    switch (colour) {
      case red:
        ASSERT_EQ(hyperarcsContainer.size(), (size_t) 1);
        ASSERT_EQ(hyperarcsContainer[0]->name().compare("edge1"), 0);
        break;
      case blue:
        ASSERT_EQ(hyperarcsContainer.size(), (size_t) 2);
        ASSERT_EQ(hyperarcsContainer[0]->name().compare("edge2"), 0);
        ASSERT_EQ(hyperarcsContainer[1]->name().compare("edge3"), 0);
        break;
      case green:
        ASSERT_TRUE(hyperarcsContainer.empty());
        break;
      case black:
        ASSERT_TRUE(hyperarcsContainer.empty());
        break;
    }
  }
}

TEST_F(TestDirectedHyperGraph, Test_edgeContainsVertex) {
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }

  ASSERT_EQ(hyperarcs.size(), g.nbHyperarcs());
  ASSERT_EQ(vertices.size(), g.nbVertices());

  const auto& hyperarc1 = g.hyperarcById(0);
  const auto& hyperarc2 = g.hyperarcById(1);
  const auto& hyperarc3 = g.hyperarcById(2);
  for (const auto& vertex : g.vertices()) {
    if (vertex->name().compare("A") == 0) {
      ASSERT_TRUE(g.isTailVertexOfHyperarc(vertex->id(), hyperarc1->id()));
      ASSERT_TRUE(g.isVertexOfHyperarc(vertex->id(), hyperarc1->id()));
      ASSERT_TRUE(g.isTailVertexOfHyperarc(vertex->id(), hyperarc2->id()));
      ASSERT_TRUE(g.isVertexOfHyperarc(vertex->id(), hyperarc2->id()));
    } else if (vertex->name().compare("B") == 0) {
      ASSERT_TRUE(g.isTailVertexOfHyperarc(vertex->id(), hyperarc2->id()));
      ASSERT_TRUE(g.isVertexOfHyperarc(vertex->id(), hyperarc2->id()));
    } else if (vertex->name().compare("Foo") == 0) {
      ASSERT_TRUE(g.isHeadVertexOfHyperarc(vertex->id(), hyperarc1->id()));
      ASSERT_TRUE(g.isVertexOfHyperarc(vertex->id(), hyperarc1->id()));
    } else if (vertex->name().compare("Bar") == 0) {
      ASSERT_TRUE(g.isHeadVertexOfHyperarc(vertex->id(), hyperarc2->id()));
      ASSERT_TRUE(g.isVertexOfHyperarc(vertex->id(), hyperarc2->id()));
    } else if (vertex->name().compare("") == 0) {
      ASSERT_TRUE(g.isHeadVertexOfHyperarc(vertex->id(), hyperarc2->id()));
      ASSERT_TRUE(g.isVertexOfHyperarc(vertex->id(), hyperarc2->id()));
      ASSERT_TRUE(g.isHeadVertexOfHyperarc(vertex->id(), hyperarc3->id()));
      ASSERT_TRUE(g.isVertexOfHyperarc(vertex->id(), hyperarc3->id()));
    } else if (vertex->name().compare("6") == 0) {
      ASSERT_TRUE(g.isTailVertexOfHyperarc(vertex->id(), hyperarc3->id()));
      ASSERT_TRUE(g.isVertexOfHyperarc(vertex->id(), hyperarc3->id()));
    }
  }

  for (const auto& edge : g.hyperarcs()) {
    const auto& edgeId = edge->id();
    if (edge->name().compare("edge1") == 0) {
      ASSERT_TRUE(g.isTailVertexOfHyperarc(g.vertexByName("A")->id(), edgeId));
      ASSERT_TRUE(g.isHeadVertexOfHyperarc(g.vertexByName("Foo")->id(),
                                           edgeId));
      ASSERT_FALSE(g.isVertexOfHyperarc(g.vertexByName("B")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperarc(g.vertexByName("Bar")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperarc(g.vertexByName("")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperarc(g.vertexByName("6")->id(), edgeId));
    } else if (edge->name().compare("edge2") == 0) {
      ASSERT_TRUE(g.isTailVertexOfHyperarc(g.vertexByName("A")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperarc(g.vertexByName("Foo")->id(), edgeId));
      ASSERT_TRUE(g.isTailVertexOfHyperarc(g.vertexByName("B")->id(), edgeId));
      ASSERT_TRUE(g.isHeadVertexOfHyperarc(g.vertexByName("Bar")->id(),
                                           edgeId));
      ASSERT_TRUE(g.isHeadVertexOfHyperarc(g.vertexByName("")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperarc(g.vertexByName("6")->id(), edgeId));
    } else if (edge->name().compare("edge3") == 0) {
      ASSERT_FALSE(g.isVertexOfHyperarc(g.vertexByName("A")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperarc(g.vertexByName("Foo")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperarc(g.vertexByName("B")->id(), edgeId));
      ASSERT_FALSE(g.isVertexOfHyperarc(g.vertexByName("Bar")->id(), edgeId));
      ASSERT_TRUE(g.isHeadVertexOfHyperarc(g.vertexByName("")->id(), edgeId));
      ASSERT_TRUE(g.isTailVertexOfHyperarc(g.vertexByName("6")->id(), edgeId));
    }
  }
}

TEST_F(TestDirectedHyperGraph, Test_tailVertices) {
  typedef hglib::DirectedHypergraph<DirectedVertex,
          NamedDirectedHyperedge> hypergraph;
  hypergraph g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  hglib::HyperedgeIdType hyperarcId(0);
  // Check tail iterators per hyperarc
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
    ASSERT_EQ(g.nbTailVertices(hyperarcId), hyperarc.first.first.size());
    ASSERT_TRUE(g.hasTailVertices(hyperarcId));
    // iterator using the begin
    auto itTails = g.tailsBegin(hyperarcId);
    // iterator using begin/end -pair
    hypergraph::vertex_iterator it, end;
    std::tie(it, end) = g.tailsBeginEnd(hyperarcId);
    ASSERT_EQ(it, itTails);
    ASSERT_EQ(end, g.tailsEnd(hyperarcId));
    // iterator of input tail names
    auto itTailNames = hyperarc.first.first.begin();
    // range based for
    auto tailsPtr = g.tails(hyperarcId);
    // Compare name of tails pointed by different iterators
    for (const auto& tail : *tailsPtr) {
      ASSERT_EQ(tail->name().compare(*itTailNames), 0);
      ASSERT_EQ(tail->name().compare((*it)->name()), 0);
      ASSERT_EQ(tail->name().compare((*itTails)->name()), 0);
      ++itTailNames;
      ++itTails;
      ++it;
    }
    ++hyperarcId;
  }

  // test out_of_range exception for given hyperarc id that was never assigned
  // to this hypergraph
  ASSERT_THROW(g.nbTailVertices(hyperarcId), std::out_of_range);
  ASSERT_THROW(g.hasTailVertices(hyperarcId), std::out_of_range);
  ASSERT_THROW(g.tailsBegin(hyperarcId), std::out_of_range);
  ASSERT_THROW(g.tailsEnd(hyperarcId), std::out_of_range);
  ASSERT_THROW(g.tailsBeginEnd(hyperarcId), std::out_of_range);
  ASSERT_THROW(g.tails(hyperarcId), std::out_of_range);

  // test exception after removal of the hyperarc
  hglib::HyperedgeIdType removedHyperarcId(1);
  g.removeHyperarc(removedHyperarcId);
  ASSERT_THROW(g.nbTailVertices(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(g.hasTailVertices(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(g.tailsBegin(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(g.tailsEnd(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(g.tailsBeginEnd(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(g.tails(removedHyperarcId), std::invalid_argument);
}

TEST_F(TestDirectedHyperGraph, Test_headVertices) {
  typedef hglib::DirectedHypergraph<DirectedVertex,
          NamedDirectedHyperedge> hypergraph;
  hypergraph g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  hglib::HyperedgeIdType hyperarcId(0);
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
    ASSERT_EQ(g.nbHeadVertices(hyperarcId), hyperarc.first.second.size());
    ASSERT_TRUE(g.hasHeadVertices(hyperarcId));
    // iterator using the begin
    auto itHeads = g.headsBegin(hyperarcId);
    // iterator using begin/end -pair
    hypergraph::vertex_iterator it, end;
    std::tie(it, end) = g.headsBeginEnd(hyperarcId);
    // iterator of input head names
    auto itHeadNames = hyperarc.first.second.begin();
    // range based for
    auto headsPtr = g.heads(hyperarcId);
    for (const auto& head : *headsPtr) {
      ASSERT_STREQ(head->name().c_str(), (*itHeadNames).c_str());
      ASSERT_STREQ((*itHeads)->name().c_str(), (*itHeadNames).c_str());
      ASSERT_STREQ((*it)->name().c_str(), (*itHeadNames).c_str());
      ++itHeadNames;
      ++itHeads;
      ++it;
    }
    ++hyperarcId;
  }
  // test out_of_range exception for given hyperarc id that was never assigned
  // to this hypergraph
  ASSERT_THROW(g.nbHeadVertices(hyperarcId), std::out_of_range);
  ASSERT_THROW(g.hasHeadVertices(hyperarcId), std::out_of_range);
  ASSERT_THROW(g.headsBegin(hyperarcId), std::out_of_range);
  ASSERT_THROW(g.headsEnd(hyperarcId), std::out_of_range);
  ASSERT_THROW(g.headsBeginEnd(hyperarcId), std::out_of_range);
  ASSERT_THROW(g.heads(hyperarcId), std::out_of_range);

  // test exception after removal of the hyperarc
  hglib::HyperedgeIdType removedHyperarcId(1);
  g.removeHyperarc(removedHyperarcId);
  ASSERT_THROW(g.nbHeadVertices(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(g.hasHeadVertices(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(g.headsBegin(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(g.headsEnd(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(g.headsBeginEnd(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(g.heads(removedHyperarcId), std::invalid_argument);
}

TEST_F(TestDirectedHyperGraph, Test_inAndOutHyperarcs) {
  typedef hglib::DirectedHypergraph<DirectedVertex,
          NamedDirectedHyperedge> hypergraph;
  hypergraph g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }

  for (const auto& vertex : g.vertices()) {
    const auto id = vertex->id();
    std::vector<hglib::HyperedgeIdType> hyperarcIds = {0, 1, 2};
    if (vertex->name().compare("A") == 0) {
      // outgoing hyperarc ids: 0, 1
      ASSERT_TRUE(g.hasOutHyperarcs(id));
      ASSERT_EQ(g.outDegree(id), (size_t) 2);
      hyperarcIds = {0, 1};
      auto itOutarcs = hyperarcIds.begin();
      // iterator using the begin
      auto itHyperarcs = g.outHyperarcsBegin(id);
      // iterator using begin/end -pair
      hypergraph::hyperarc_iterator it, end;
      std::tie(it, end) = g.outHyperarcsBeginEnd(id);
      // range based for
      auto outArcsPtr = g.outHyperarcs(id);
      for (const auto& outHyperarc : *outArcsPtr) {
        ASSERT_EQ((*itOutarcs), outHyperarc->id());
        ASSERT_EQ((*itOutarcs), (*itHyperarcs)->id());
        ASSERT_EQ((*itOutarcs), (*it)->id());
        ++itOutarcs;
        ++itHyperarcs;
        ++it;
      }

      // incoming hyperarc ids: none
      ASSERT_FALSE(g.hasInHyperarcs(id));
      ASSERT_EQ(g.inDegree(id), (size_t) 0);
      ASSERT_TRUE(g.inHyperarcsBegin(id) == g.inHyperarcsEnd(id));
      std::tie(it, end) = g.inHyperarcsBeginEnd(id);
      ASSERT_TRUE(g.inHyperarcsBegin(id) == it);
      ASSERT_TRUE(g.inHyperarcsEnd(id) == end);
    } else if (vertex->name().compare("B") == 0) {
      // outgoing hyperarc ids: 1
      ASSERT_TRUE(g.hasOutHyperarcs(id));
      ASSERT_EQ(g.outDegree(id), (size_t) 1);
      hyperarcIds = {1};
      auto itOutarcs = hyperarcIds.begin();
      // iterator using the begin
      auto itHyperarcs = g.outHyperarcsBegin(id);
      // iterator using begin/end -pair
      hypergraph::hyperarc_iterator it, end;
      std::tie(it, end) = g.outHyperarcsBeginEnd(id);
      // range based for
      auto outArcsPtr = g.outHyperarcs(id);
      for (const auto& outHyperarc : *outArcsPtr) {
        ASSERT_EQ((*itOutarcs), outHyperarc->id());
        ASSERT_EQ((*itOutarcs), outHyperarc->id());
        ASSERT_EQ((*itOutarcs), (*itHyperarcs)->id());
        ASSERT_EQ((*itOutarcs), (*it)->id());
        ++itOutarcs;
        ++itHyperarcs;
        ++it;
      }

      // incoming hyperarc ids: none
      ASSERT_FALSE(g.hasInHyperarcs(id));
      ASSERT_EQ(g.inDegree(id), (size_t) 0);
      ASSERT_EQ(g.inHyperarcsBegin(id), g.inHyperarcsEnd(id));
      std::tie(it, end) = g.inHyperarcsBeginEnd(id);
      ASSERT_TRUE(g.inHyperarcsBegin(id) == it);
      ASSERT_TRUE(g.inHyperarcsEnd(id) == end);
    } else if (vertex->name().compare("Foo") == 0) {
      // outgoing hyperarc ids: none
      ASSERT_FALSE(g.hasOutHyperarcs(id));
      ASSERT_EQ(g.outDegree(id), (size_t) 0);
      ASSERT_EQ(g.outHyperarcsBegin(id), g.outHyperarcsEnd(id));
      // iterator using begin/end -pair
      hypergraph::hyperarc_iterator it, end;
      std::tie(it, end) = g.outHyperarcsBeginEnd(id);
      ASSERT_TRUE(g.outHyperarcsBegin(id) == it);
      ASSERT_TRUE(g.outHyperarcsEnd(id) == end);

      // incoming hyperarc ids: 0
      ASSERT_TRUE(g.hasInHyperarcs(id));
      ASSERT_EQ(g.inDegree(id), (size_t) 1);
      hyperarcIds = {0};
      auto itInarcs = hyperarcIds.begin();
      // iterator using begin
      auto itHyperarcs = g.inHyperarcsBegin(id);
      // iterator using begin/end -pair
      std::tie(it, end) = g.inHyperarcsBeginEnd(id);
      // range absed for
      auto inArcsPtr = g.inHyperarcs(id);
      for (const auto& inHyperarc : *inArcsPtr) {
        ASSERT_EQ((*itInarcs), inHyperarc->id());
        ASSERT_EQ((*itInarcs), (*itHyperarcs)->id());
        ASSERT_EQ((*itInarcs), (*it)->id());
        ++itInarcs;
        ++itHyperarcs;
        ++it;
      }
      ASSERT_EQ(itHyperarcs, g.inHyperarcsEnd(id));
    } else if (vertex->name().compare("Bar") == 0) {
      // outgoing hyperarc ids: none
      ASSERT_FALSE(g.hasOutHyperarcs(id));
      ASSERT_EQ(g.outDegree(id), (size_t) 0);
      ASSERT_EQ(g.outHyperarcsBegin(id), g.outHyperarcsEnd(id));
      // iterator using begin/end -pair
      hypergraph::hyperarc_iterator it, end;
      std::tie(it, end) = g.outHyperarcsBeginEnd(id);
      ASSERT_TRUE(g.outHyperarcsBegin(id) == it);
      ASSERT_TRUE(g.outHyperarcsEnd(id) == end);

      // incoming hyperarc ids: 1
      ASSERT_TRUE(g.hasInHyperarcs(id));
      ASSERT_EQ(g.inDegree(id), (size_t) 1);
      hyperarcIds = {1};
      auto itInarcs = hyperarcIds.begin();
      // iterator using begin
      auto itHyperarcs = g.inHyperarcsBegin(id);
      // iterator using begin/end -pair
      std::tie(it, end) = g.inHyperarcsBeginEnd(id);
      // range absed for
      auto inArcsPtr = g.inHyperarcs(id);
      for (const auto& inHyperarc : *inArcsPtr) {
        ASSERT_EQ((*itInarcs), inHyperarc->id());
        ASSERT_EQ((*itInarcs), (*itHyperarcs)->id());
        ASSERT_EQ((*itInarcs), (*it)->id());
        ++itInarcs;
        ++itHyperarcs;
        ++it;
      }
    } else if (vertex->name().compare("") == 0) {
      // outgoing hyperarc ids: none
      ASSERT_FALSE(g.hasOutHyperarcs(id));
      ASSERT_EQ(g.outDegree(id), (size_t) 0);
      ASSERT_EQ(g.outHyperarcsBegin(id), g.outHyperarcsEnd(id));
      // iterator using begin/end -pair
      hypergraph::hyperarc_iterator it, end;
      std::tie(it, end) = g.outHyperarcsBeginEnd(id);
      ASSERT_TRUE(g.outHyperarcsBegin(id) == it);
      ASSERT_TRUE(g.outHyperarcsEnd(id) == end);

      // incoming hyperarc ids: 1, 2
      ASSERT_TRUE(g.hasInHyperarcs(id));
      ASSERT_EQ(g.inDegree(id), (size_t) 2);
      hyperarcIds = {1, 2};
      auto itInarcs = hyperarcIds.begin();
      // iterator using begin
      auto itHyperarcs = g.inHyperarcsBegin(id);
      // iterator using begin/end -pair
      std::tie(it, end) = g.inHyperarcsBeginEnd(id);
      // range absed for
      auto inArcsPtr = g.inHyperarcs(id);
      for (const auto& inHyperarc : *inArcsPtr) {
        ASSERT_EQ((*itInarcs), inHyperarc->id());
        ASSERT_EQ((*itInarcs), (*itHyperarcs)->id());
        ASSERT_EQ((*itInarcs), (*it)->id());
        ++itInarcs;
        ++itHyperarcs;
        ++it;
      }
    } else if (vertex->name().compare("6") == 0) {
      // outgoing hyperarc ids: 2
      ASSERT_TRUE(g.hasOutHyperarcs(id));
      ASSERT_EQ(g.outDegree(id), (size_t) 1);
      hyperarcIds = {2};
      auto itOutarcs = hyperarcIds.begin();
      // iterator using begin
      auto itHyperarcs = g.outHyperarcsBegin(id);
      // iterator using begin/end -pair
      hypergraph::hyperarc_iterator it, end;
      std::tie(it, end) = g.outHyperarcsBeginEnd(id);
      for (; it != end; ++it) {
        const auto& outHyperarc = *it;
        ASSERT_EQ((*itOutarcs), outHyperarc->id());
        ASSERT_EQ((*itOutarcs), (*itHyperarcs)->id());
        ASSERT_EQ((*itOutarcs), (*it)->id());
        ++itOutarcs;
        ++itHyperarcs;
      }
      // incoming hyperarc ids: none
      ASSERT_FALSE(g.hasInHyperarcs(id));
      ASSERT_EQ(g.inDegree(id), (size_t) 0);
      ASSERT_EQ(g.inHyperarcsBegin(id), g.inHyperarcsEnd(id));
      std::tie(it, end) = g.inHyperarcsBeginEnd(id);
      ASSERT_TRUE(g.inHyperarcsBegin(id) == it);
      ASSERT_TRUE(g.inHyperarcsEnd(id) == end);
    }
  }

  // test exceptions
  std::vector<int> outOfRangeIds = {10, -1};
  for (const auto& outOfRangeId : outOfRangeIds) {
    ASSERT_THROW(g.outDegree(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.hasOutHyperarcs(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.outHyperarcsBegin(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.outHyperarcsEnd(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.outHyperarcsBeginEnd(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.outHyperarcs(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.inDegree(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.hasInHyperarcs(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.inHyperarcsBegin(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.inHyperarcsEnd(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.inHyperarcsBeginEnd(outOfRangeId), std::out_of_range);
    ASSERT_THROW(g.inHyperarcs(outOfRangeId), std::out_of_range);
  }

  // test exception after the removal of a vertex
  hglib::VertexIdType removedVertexId(2);
  g.removeVertex(removedVertexId);
  ASSERT_THROW(g.outDegree(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.hasOutHyperarcs(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.outHyperarcsBegin(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.outHyperarcsEnd(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.outHyperarcsBeginEnd(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.outHyperarcs(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.inDegree(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.hasInHyperarcs(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.inHyperarcsBegin(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.inHyperarcsEnd(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.inHyperarcsBeginEnd(removedVertexId), std::invalid_argument);
  ASSERT_THROW(g.inHyperarcs(removedVertexId), std::invalid_argument);
}

TEST_F(TestDirectedHyperGraph, Test_removeVertex) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      std::string name = "Default";
      double weight = 0.0;
  };


  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge,
          VertexProperty, HyperarcProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }
  size_t nb_v_init = g.nbVertices();
  size_t nb_e_init = g.nbHyperarcs();

  // X does not exist in the hypergraph
  ASSERT_THROW(g.removeVertex("X"), std::invalid_argument);
  ASSERT_EQ(nb_v_init, g.nbVertices());

  auto vertexAId = g.vertexByName("A")->id();
  g.removeVertex("A");
  ASSERT_EQ(nb_v_init - 1, g.nbVertices());
  ASSERT_EQ(nb_e_init - 2, g.nbHyperarcs());
  ASSERT_EQ(g.vertexByName("A"), nullptr);
  ASSERT_FALSE(g.hasInHyperarcs(g.vertexByName("Foo")->id()));
  ASSERT_FALSE(g.hasOutHyperarcs(g.vertexByName("B")->id()));
  ASSERT_FALSE(g.hasInHyperarcs(g.vertexByName("Bar")->id()));
  ASSERT_EQ(g.inDegree(g.vertexByName("")->id()), (size_t) 1);
  // check property entry
  ASSERT_EQ(g.getVertexProperties(vertexAId), nullptr);
  // Check if exception is thrown if we retry to delete the vertex
  ASSERT_THROW(g.removeVertex("A"), std::invalid_argument);

  // remove edge3 in two steps: first remove vertex '6', second remove ''
  // -> edges are only removed if they have neither tails nor heads
  hglib::HyperedgeIdType edge3Id(2);
  auto vertex6Id = g.vertexByName("6")->id();
  g.removeVertex("6", false);
  ASSERT_EQ(g.vertexByName("6"), nullptr);
  ASSERT_NE(g.hyperarcById(edge3Id), nullptr);
  ASSERT_TRUE(g.hasInHyperarcs(g.vertexByName("")->id()));
  ASSERT_FALSE(g.hasOutHyperarcs(g.vertexByName("")->id()));
  ASSERT_FALSE(g.hasInHyperarcs(g.vertexByName("B")->id()));
  ASSERT_FALSE(g.hasOutHyperarcs(g.vertexByName("B")->id()));
  ASSERT_FALSE(g.hasInHyperarcs(g.vertexByName("Foo")->id()));
  ASSERT_FALSE(g.hasOutHyperarcs(g.vertexByName("Foo")->id()));
  ASSERT_FALSE(g.hasInHyperarcs(g.vertexByName("Bar")->id()));
  ASSERT_FALSE(g.hasOutHyperarcs(g.vertexByName("Bar")->id()));
  // check property entry
  ASSERT_EQ(g.getVertexProperties(vertex6Id), nullptr);
  // Check if exception is thrown if we retry to delete the vertex
  ASSERT_THROW(g.removeVertex("6"), std::invalid_argument);

  // remove vertex ''
  g.removeVertex("", false);
  ASSERT_EQ(g.vertexByName(""), nullptr);
  ASSERT_EQ(g.hyperarcById(edge3Id), nullptr);

  // Vertices "B", "Foo", "Bar" still exists in the hypergraph...
  ASSERT_EQ(g.nbVertices(), nb_v_init - 3);
  // ...but they are isolated
  ASSERT_EQ(g.nbHyperarcs(), (size_t) 0);

  // Check that the remaining vertices still have a property entry
  for (const auto& vertex : g.vertices()) {
    const auto& prop = g.getVertexProperties(vertex->id());
    ASSERT_NE(prop, nullptr);
    ASSERT_EQ(prop->name.compare("Default"), 0);
    ASSERT_EQ(prop->colour, Colour::green);
  }
}

TEST_F(TestDirectedHyperGraph, Test_removeHyperarc) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      std::string name = "Default";
      double weight = 0.0;
  };

  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge,
          VertexProperty, HyperarcProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }
  size_t nb_v_init = g.nbVertices();

  for (const auto& hyperarc : g.hyperarcs()) {
    auto hyperarcId = hyperarc->id();
    if (hyperarc->name().compare("edge1") == 0) {
      size_t nb_e_init = g.nbHyperarcs();
      size_t nb_out_init = g.outDegree(g.vertexByName("A")->id());
      size_t nb_in_init = g.inDegree(g.vertexByName("Foo")->id());
      g.removeHyperarc(hyperarcId);
      ASSERT_EQ(g.nbHyperarcs(), nb_e_init - 1);
      ASSERT_EQ(g.outDegree(g.vertexByName("A")->id()), nb_out_init - 1);
      ASSERT_EQ(g.inDegree(g.vertexByName("Foo")->id()), nb_in_init - 1);
    } else if (hyperarc->name().compare("edge2") == 0) {
      size_t nb_e_init = g.nbHyperarcs();
      size_t nb_out_A_init = g.outDegree(g.vertexByName("A")->id());
      size_t nb_out_B_init = g.outDegree(g.vertexByName("B")->id());
      size_t nb_in_Foo_init = g.inDegree(g.vertexByName("Bar")->id());
      size_t nb_in__init = g.inDegree(g.vertexByName("")->id());
      g.removeHyperarc(hyperarcId);
      ASSERT_EQ(g.nbHyperarcs(), nb_e_init - 1);
      ASSERT_EQ(g.outDegree(g.vertexByName("A")->id()), nb_out_A_init - 1);
      ASSERT_EQ(g.outDegree(g.vertexByName("B")->id()), nb_out_B_init - 1);
      ASSERT_EQ(g.inDegree(g.vertexByName("Bar")->id()),
                nb_in_Foo_init - 1);
      ASSERT_EQ(g.inDegree(g.vertexByName("")->id()), nb_in__init - 1);
    } else if (hyperarc->name().compare("edge3") == 0) {
      size_t nb_e_init = g.nbHyperarcs();
      size_t nb_out_init = g.outDegree(g.vertexByName("6")->id());
      size_t nb_in_init = g.inDegree(g.vertexByName("")->id());
      g.removeHyperarc(hyperarcId);
      ASSERT_EQ(g.nbHyperarcs(), nb_e_init - 1);
      ASSERT_EQ(g.outDegree(g.vertexByName("6")->id()), nb_out_init - 1);
      ASSERT_EQ(g.inDegree(g.vertexByName("")->id()), nb_in_init - 1);
    }
    // Verify if respective property was deleted
    ASSERT_THROW(g.getHyperarcProperties(hyperarcId), std::invalid_argument);
  }
  ASSERT_EQ(nb_v_init, g.nbVertices());
  ASSERT_EQ((size_t) 0, g.nbHyperarcs());
  ASSERT_EQ(g.hyperarcsBegin(), g.hyperarcsEnd());
  for (const auto& vertex : g.vertices()) {
    ASSERT_EQ(g.outDegree(vertex->id()), (size_t) 0);
    ASSERT_EQ(g.inDegree(vertex->id()), (size_t) 0);
  }
}

TEST_F(TestDirectedHyperGraph, Test_ResetVertexIds) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge,
          VertexProperty, HyperarcProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }

  // change default property values
  for (const auto& vertex : g.vertices()) {
    auto vProp = g.getVertexProperties_(vertex->id());
    vProp->name = vertex->name() + "_" + std::to_string(vertex->id());
  }
  // reset after no vertex was deleted
  g.resetVertexIds();
  // Verify that nothing has changed
  ASSERT_EQ(g.nbVertices(), vertices.size());
  for (const auto& vertex : g.vertices()) {
    auto vProp = g.getVertexProperties_(vertex->id());
    ASSERT_EQ(vProp->name.compare(
            vertex->name() + "_" + std::to_string(vertex->id())), 0);
  }

  // remove a section of vertices at the begin of the vector
  g.removeVertex("A");  // with Id = 0
  g.removeVertex("B");  // with Id = 1
  g.resetVertexIds();

  // Verify that Ids shifted by 2
  ASSERT_EQ(g.vertexByName("Foo")->id(), (size_t) 0);
  ASSERT_EQ(g.vertexByName("Bar")->id(), (size_t) 1);
  ASSERT_EQ(g.vertexByName("")->id(), (size_t) 2);
  ASSERT_EQ(g.vertexByName("6")->id(), (size_t) 3);

  // remove a section of vertices at the end of the vector
  g.removeVertex("");  // with Id = 2
  g.removeVertex("6");  // with Id = 3
  g.resetVertexIds();
  // Verify the Ids of vertices Foo (0) and Bar (1)
  ASSERT_EQ(g.vertexByName("Foo")->id(), (size_t) 0);
  ASSERT_EQ(g.vertexByName("Bar")->id(), (size_t) 1);

  // Add some new vertices and verify correct Ids
  g.addVertex("U");
  ASSERT_EQ(g.vertexByName("U")->id(), (size_t) 2);
  g.addVertex("X");
  ASSERT_EQ(g.vertexByName("X")->id(), (size_t) 3);
  g.addVertex("Y");
  ASSERT_EQ(g.vertexByName("Y")->id(), (size_t) 4);
  g.addVertex("Z");
  ASSERT_EQ(g.vertexByName("Z")->id(), (size_t) 5);


  // remove a section of vertices in the middle of the vector
  g.removeVertex("Bar");  // with Id = 1
  g.removeVertex("U");  // with Id = 2
  g.removeVertex("Y");  // with Id = 4
  g.resetVertexIds();
  // Verify the Ids of vertices Foo (0), X (1) and Z (2)
  ASSERT_EQ(g.vertexByName("Foo")->id(), (size_t) 0);
  ASSERT_EQ(g.vertexByName("X")->id(), (size_t) 1);
  ASSERT_EQ(g.vertexByName("Z")->id(), (size_t) 2);

  // Verify that vertexMaxId_ was set correctly
  g.addVertex("U");
  ASSERT_EQ(g.vertexByName("U")->id(), (size_t) 3);

  // remove all vertices
  g.removeVertex("Foo");
  g.removeVertex("X");
  g.removeVertex("Z");
  g.removeVertex("U");
  g.resetVertexIds();
  // Verify that vertexMaxId_ was set correctly
  g.addVertex("V");
  ASSERT_EQ(g.vertexByName("V")->id(), (size_t) 0);
}

TEST_F(TestDirectedHyperGraph, Test_ResetHyperarcIds) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge,
          VertexProperty, HyperarcProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }

  // nothing changed if no hyperarc was deleted
  g.resetHyperarcIds();
  ASSERT_EQ(g.hyperarcById(0)->id(), (size_t) 0);  // "edge1"
  ASSERT_EQ(g.hyperarcById(1)->id(), (size_t) 1);  // "edge2"
  ASSERT_EQ(g.hyperarcById(2)->id(), (size_t) 2);  // "edge3"

  // add some hyperarcs
  g.addHyperarc(hglib::NAME, {{"A"}, {"B"}}, {"edge4"});
  g.addHyperarc(hglib::NAME, {{"A"}, {"B"}}, {"edge5"});
  g.addHyperarc(hglib::NAME, {{"A"}, {"B"}}, {"edge6"});
  g.addHyperarc(hglib::NAME, {{"A"}, {"B"}}, {"edge7"});

  // remove hyperarcs at the begin of the vector
  g.removeHyperarc(0);  // "edge1"
  g.removeHyperarc(1);  // "edge2"
  g.resetHyperarcIds();
  // Verify remaining Ids
  ASSERT_EQ(g.hyperarcById(0)->name().compare("edge3"), 0);
  ASSERT_EQ(g.hyperarcById(1)->name().compare("edge4"), 0);
  ASSERT_EQ(g.hyperarcById(2)->name().compare("edge5"), 0);
  ASSERT_EQ(g.hyperarcById(3)->name().compare("edge6"), 0);
  ASSERT_EQ(g.hyperarcById(4)->name().compare("edge7"), 0);
  // Verify hyperarcMaxId_
  g.addHyperarc(hglib::NAME, {{"A"}, {"B"}}, {"edge8"});
  ASSERT_EQ(g.hyperarcById(5)->name().compare("edge8"), 0);

  // remove hyperarcs at the end of the vector
  g.removeHyperarc(4);  // "edge7"
  g.removeHyperarc(5);  // "edge8"
  g.resetHyperarcIds();
  // Verify remaining Ids
  ASSERT_EQ(g.hyperarcById(0)->name().compare("edge3"), 0);
  ASSERT_EQ(g.hyperarcById(1)->name().compare("edge4"), 0);
  ASSERT_EQ(g.hyperarcById(2)->name().compare("edge5"), 0);
  ASSERT_EQ(g.hyperarcById(3)->name().compare("edge6"), 0);
  // Verify hyperarcMaxId_
  g.addHyperarc(hglib::NAME, {{"A"}, {"B"}}, {"edge9"});
  ASSERT_EQ(g.hyperarcById(4)->name().compare("edge9"), 0);
  g.addHyperarc(hglib::NAME, {{"A"}, {"B"}}, {"edge10"});
  ASSERT_EQ(g.hyperarcById(5)->name().compare("edge10"), 0);

  // remove hyperarcs in the middle of the vector
  g.removeHyperarc(1);  // "edge4"
  g.removeHyperarc(2);  // "edge5"
  g.removeHyperarc(4);  // "edge9"
  g.resetHyperarcIds();
  // Verify remaining Ids
  ASSERT_EQ(g.hyperarcById(0)->name().compare("edge3"), 0);
  ASSERT_EQ(g.hyperarcById(1)->name().compare("edge6"), 0);
  ASSERT_EQ(g.hyperarcById(2)->name().compare("edge10"), 0);
  // Verify hyperarcMaxId_
  g.addHyperarc(hglib::NAME, {{"A"}, {"B"}}, {"edge11"});
  ASSERT_EQ(g.hyperarcById(3)->name().compare("edge11"), 0);
}

TEST_F(TestDirectedHyperGraph, Test_ResetIds) {
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }

  g.removeVertex("Bar");
  g.resetIds();

  // check vertex ids
  hglib::VertexIdType vertexId(0);
  for (const auto& vertex : g.vertices()) {
    ASSERT_EQ(vertexId++, vertex->id());
  }
  // check hyperarc ids
  hglib::HyperedgeIdType hyperarcId(0);
  for (const auto& hyperarc : g.hyperarcs()) {
    ASSERT_EQ(hyperarcId++, hyperarc->id());
  }

  // check if vertexMaxId_ and hyperarcMaxId_ were set correctly
  size_t expectedVertexId = g.nbVertices();
  size_t expectedHyperarcId = g.nbHyperarcs();
  // should add two vertices and one hyperarc
  g.addVertex("X");
  g.addVertex("Y");
  g.addHyperarc(hglib::NAME, {{"X"}, {"Y"}}, {"edge4"});
  const auto& newHyperarc = g.hyperarcById(
          (hglib::HyperedgeIdType) expectedHyperarcId);
  ASSERT_NE(newHyperarc, nullptr);
  ASSERT_EQ(newHyperarc->id(), (hglib::HyperedgeIdType) expectedHyperarcId);
  const auto& vertex_X = g.vertexByName("X");
  const auto& vertex_Y = g.vertexByName("Y");
  ASSERT_NE(vertex_X, nullptr);
  ASSERT_NE(vertex_Y, nullptr);
  ASSERT_EQ(vertex_X->id(), (hglib::VertexIdType) expectedVertexId);
  ASSERT_EQ(vertex_Y->id(), (hglib::VertexIdType) expectedVertexId + 1);
}


TEST_F(TestDirectedHyperGraph, Test_print) {
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  // Print isolated vertices
  std::ostringstream output;
  output << g;
  std::string observedString(output.str());
  std::string expectedString = "# Isolated vertices\n\"A\", \"B\", \"Foo\", "
      "\"Bar\", \"\", \"6\"\n# Hyperarcs\n";
  ASSERT_EQ(observedString, expectedString);

  // Add hyperarcs
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }

  // clear output
  output.str("");
  output.clear();
  // Print hypergraph
  output << g;
  observedString = output.str();
  expectedString = "# Isolated vertices\n# Hyperarcs\n"
      "edge1: {\"A\"} -> {\"Foo\"}\n"
      "edge2: {\"A\", \"B\"} -> {\"Bar\", \"\"}\n"
      "edge3: {\"6\"} -> {\"\"}\n";
  ASSERT_EQ(observedString, expectedString);

  // The same with unnamed hyperarcs
  hglib::DirectedHypergraph<> g2;
  for (auto vertexName : vertices) {
    g2.addVertex(vertexName);
  }
  // Add hyperarcs
  for (auto hyperarc : hyperarcs) {
    g2.addHyperarc(hglib::NAME, hyperarc.first);
  }

  // clear output
  output.str("");
  output.clear();
  // Print hypergraph
  output << g2;
  observedString = output.str();
  expectedString = "# Isolated vertices\n# Hyperarcs\n"
      "{\"A\"} -> {\"Foo\"}\n"
      "{\"A\", \"B\"} -> {\"Bar\", \"\"}\n"
      "{\"6\"} -> {\"\"}\n";
  ASSERT_EQ(observedString, expectedString);
}

TEST_F(TestDirectedHyperGraph, Test_argumentsToCreateVertexHyperarc) {
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g;
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g2;
  hglib::DirectedHypergraph<> g3;
  hglib::DirectedHypergraph<> g4;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
    g3.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
    g3.addHyperarc(hglib::NAME, hyperarc.first);
  }

  for (const auto& vertex : g.vertices()) {
    auto args = g.argumentsToCreateVertex(vertex->id());
    const auto vertex2 = g2.addVertex(args.name(), args.specificAttributes());
    ASSERT_EQ(vertex->name().compare(vertex2->name()), 0);
  }

  for (const auto& hyperarc : g.hyperarcs()) {
    auto args = g.hyperarcById(hyperarc->id())->argumentsToCreateHyperarc();
    const auto& hyerparc2 = g2.addHyperarc(args.tailAndHeadIds(),
                                           args.specificAttributes());
    ASSERT_EQ(hyperarc->name().compare(hyerparc2->name()), 0);
    ASSERT_EQ(g.nbTailVertices(hyperarc->id()),
              g2.nbTailVertices(hyerparc2->id()));
    auto it = g.tailsBegin(hyperarc->id());
    auto end = g.tailsEnd(hyperarc->id());
    for (; it != end; ++it) {
      const auto& tail = *it;
      ASSERT_NE(g2.vertexByName(tail->name()), nullptr);
    }
    ASSERT_EQ(g.nbHeadVertices(hyperarc->id()),
              g2.nbHeadVertices(hyerparc2->id()));
    it = g.headsBegin(hyperarc->id());
    end = g.headsEnd(hyperarc->id());
    for (; it != end; ++it) {
      const auto& head = *it;
      ASSERT_NE(g2.vertexByName(head->name()), nullptr);
    }
  }

  for (const auto& vertex : g3.vertices()) {
    auto args = g3.argumentsToCreateVertex(vertex->id());
    const auto vertex4 = g4.addVertex(args.name(), args.specificAttributes());
    ASSERT_EQ(vertex->name().compare(vertex4->name()), 0);
  }

  for (const auto& hyperarc : g3.hyperarcs()) {
    auto args = g3.hyperarcById(hyperarc->id())->argumentsToCreateHyperarc();
    const auto& hyerparc4 = g4.addHyperarc(args.tailAndHeadIds(),
                                           args.specificAttributes());
    ASSERT_EQ(g3.nbTailVertices(hyperarc->id()),
              g4.nbTailVertices(hyerparc4->id()));
    auto it = g3.tailsBegin(hyperarc->id());
    auto end = g3.tailsEnd(hyperarc->id());
    for (; it != end; ++it) {
      const auto& tail = *it;
      ASSERT_NE(g4.vertexByName(tail->name()), nullptr);
    }
    ASSERT_EQ(g3.nbHeadVertices(hyperarc->id()),
              g4.nbHeadVertices(hyerparc4->id()));
    it = g3.headsBegin(hyperarc->id());
    end = g3.headsEnd(hyperarc->id());
    for (; it != end; ++it) {
      const auto& head = *it;
      ASSERT_NE(g4.vertexByName(head->name()), nullptr);
    }
  }
}
