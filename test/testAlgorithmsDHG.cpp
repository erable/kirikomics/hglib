// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include <vector>

#include "hglib.h"
#include "gtest/gtest.h"

using hglib::DirectedVertex;
using hglib::DirectedHyperedge;
using hglib::NamedDirectedHyperedge;

/**
 * Test fixture
 */
class TestAlgorithmDHG : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

 private:
  std::streambuf* storedStreambuf_;
};

TEST_F(TestAlgorithmDHG, Test_closure) {
  hglib::DirectedHypergraph<> g;
  for (int i = 0; i < 8; ++i) {
    g.addVertex();
  }
  // Add hyperarcs
  g.addHyperarc({{0, 1}, {2}});
  g.addHyperarc({{2}, {1}});
  g.addHyperarc({{2}, {3}});
  g.addHyperarc({{2, 3}, {4, 5}});
  g.addHyperarc({{4}, {3}});
  g.addHyperarc({{5}, {6, 7}});
  g.addHyperarc({{6, 7}, {0}});

  // closure from starting set {0}
  auto closure = hglib::closure(g, {0});
  ASSERT_EQ(closure.first.size(), (size_t) 1);
  ASSERT_EQ(closure.second.size(), (size_t) 0);

  // closure from starting set {1}
  closure = hglib::closure(g, {1});
  ASSERT_EQ(closure.first.size(), (size_t) 1);
  ASSERT_EQ(closure.second.size(), (size_t) 0);

  // closure from starting set {0, 1}
  closure = hglib::closure(g, {0, 1});
  ASSERT_EQ(closure.first.size(), (size_t) 8);
  ASSERT_EQ(closure.second.size(), (size_t) 7);

  // closure from starting set {4}
  closure = hglib::closure(g, {4});
  ASSERT_EQ(closure.first.size(), (size_t) 2);
  ASSERT_EQ(closure.second.size(), (size_t) 1);

  // closure from starting set {0, 1, 2, 3, 4, 5, 6, 7}
  closure = hglib::closure(g, {0, 1, 2, 3, 4, 5, 6, 7});
  ASSERT_EQ(closure.first.size(), (size_t) 8);
  ASSERT_EQ(closure.second.size(), (size_t) 7);

  // closure from starting set {}
  closure = hglib::closure(g, {});
  ASSERT_EQ(closure.first.size(), (size_t) 0);
  ASSERT_EQ(closure.second.size(), (size_t) 0);

  // Add a hyperarc that produced vertex 2 from nothing
  g.addHyperarc({{}, {2}});
  // closure from starting set {}
  closure = hglib::closure(g, {});
  ASSERT_EQ(closure.first.size(), (size_t) 8);
  ASSERT_EQ(closure.second.size(), (size_t) 8);
}

TEST_F(TestAlgorithmDHG, Test_has_hyperpath) {
  hglib::DirectedHypergraph<> g;
  for (int i = 0; i < 8; ++i) {
    g.addVertex();
  }
  // Add hyperarcs
  g.addHyperarc({{0, 1}, {2}});
  g.addHyperarc({{2}, {1}});
  g.addHyperarc({{2}, {3}});
  g.addHyperarc({{2, 3}, {4, 5}});
  g.addHyperarc({{4}, {3}});
  g.addHyperarc({{5}, {6, 7}});
  g.addHyperarc({{6, 7}, {0}});

  // Check if there exists a hyperpath from a single source to single target
  ASSERT_TRUE(hglib::has_hyperpath(g, 2, 3));
  ASSERT_EQ(hglib::has_hyperpath(g, 2, 3), hglib::has_hyperpath(g, {2}, 3));
  ASSERT_THROW(hglib::has_hyperpath(g, 2, 1000), std::invalid_argument);
  ASSERT_FALSE(hglib::has_hyperpath(g, 0, 3));
  ASSERT_FALSE(hglib::has_hyperpath(g, 1, 3));

  // Check if there exists a hyperpath from a set of sources to a single target
  std::vector<hglib::VertexIdType> sources = {0, 1};
  ASSERT_TRUE(hglib::has_hyperpath(g, sources, 3));
  sources = {6, 7};
  ASSERT_FALSE(hglib::has_hyperpath(g, sources, 5));
  sources = {2};
  ASSERT_TRUE(hglib::has_hyperpath(g, sources, 0));

  // // Check if there exists a hyperpath from a single source to a set of
  // targets
  std::vector<hglib::VertexIdType> targets = {0, 1, 2, 3, 4, 5, 6, 7};
  ASSERT_TRUE(hglib::has_hyperpath(g, 2, targets));
  targets = {0, 6, 7};
  ASSERT_TRUE(hglib::has_hyperpath(g, 5, targets));
  targets = {0, 6, 7, 1};
  ASSERT_FALSE(hglib::has_hyperpath(g, 5, targets));
  targets = {1000};
  ASSERT_THROW(hglib::has_hyperpath(g, 5, targets),
               std::invalid_argument);

  // Check if there exists a hyperpath from a set of sources to a set of
  // targets
  targets = {0};
  ASSERT_TRUE(hglib::has_hyperpath(g, sources, targets));
  sources = {0, 1};
  targets = {0, 1, 2, 3, 4, 5, 6, 7, 1000};
  ASSERT_TRUE(hglib::has_hyperpath(g, sources, targets));
  sources = {2};
  targets = {1000};
  ASSERT_THROW(hglib::has_hyperpath(g, sources, targets),
               std::invalid_argument);
}

TEST_F(TestAlgorithmDHG, Test_is_b_hypergraph) {
  hglib::DirectedHypergraph<> g;
  for (int i = 0; i < 8; ++i) {
    g.addVertex();
  }
  // Add hyperarcs
  g.addHyperarc({{0, 1}, {2}});
  g.addHyperarc({{4}, {3}});
  g.addHyperarc({{2}, {1}});
  g.addHyperarc({{2}, {3}});
  g.addHyperarc({{6, 7}, {0}});
  ASSERT_TRUE(hglib::is_b_hypergraph(g));
  g.addHyperarc({{2, 3}, {4, 5}});
  ASSERT_FALSE(hglib::is_b_hypergraph(g));
}

TEST_F(TestAlgorithmDHG, Test_is_f_hypergraph) {
  hglib::DirectedHypergraph<> g;
  for (int i = 0; i < 8; ++i) {
    g.addVertex();
  }
  // Add hyperarcs
  g.addHyperarc({{4}, {3}});
  g.addHyperarc({{2}, {1}});
  g.addHyperarc({{2}, {3}});
  g.addHyperarc({{5}, {6, 7}});
  ASSERT_TRUE(hglib::is_f_hypergraph(g));
  g.addHyperarc({{0, 1}, {2}});
  ASSERT_FALSE(hglib::is_f_hypergraph(g));
}

TEST_F(TestAlgorithmDHG, Test_is_bf_hypergraph) {
  hglib::DirectedHypergraph<> g;
  for (int i = 0; i < 8; ++i) {
    g.addVertex();
  }
  // Add hyperarcs
  g.addHyperarc({{4}, {3}});
  g.addHyperarc({{5}, {6, 7}});
  g.addHyperarc({{0, 1}, {2}});
  ASSERT_TRUE(hglib::is_bf_hypergraph(g));
  g.addHyperarc({{3, 4}, {5, 7}});
  ASSERT_FALSE(hglib::is_bf_hypergraph(g));
}

TEST_F(TestAlgorithmDHG, Test_bf_transformation) {
  hglib::DirectedHypergraph<> g;
  for (int i = 0; i < 8; ++i) {
    g.addVertex();
  }
  // Add hyperarcs
  g.addHyperarc({{4}, {3}});
  g.addHyperarc({{5}, {6, 7}});
  g.addHyperarc({{0, 1}, {2}});
  std::vector<hglib::VertexIdType> tails = {3, 4};
  std::vector<hglib::VertexIdType> heads = {5, 7};
  g.addHyperarc({tails, heads});
  ASSERT_FALSE(hglib::is_bf_hypergraph(g));

  size_t nbHyperarcs(g.nbHyperarcs());
  size_t nbVertices(g.nbVertices());
  hglib::VertexIdType expectedDummyVertexId(8);
  hglib::bf_transformation(&g);
  ASSERT_EQ(nbHyperarcs + 1, g.nbHyperarcs());
  ASSERT_EQ(nbVertices + 1, g.nbVertices());
  ASSERT_EQ(g.hyperarcById(3), nullptr);  // was deleted
  ASSERT_EQ(g.nbTailVertices(4), (size_t) 2);
  ASSERT_EQ(g.nbHeadVertices(4), (size_t) 1);
  auto it = tails.begin();
  auto itTails = g.tailsBegin(4);
  auto tailsEnd = g.tailsEnd(4);
  for (; itTails != tailsEnd; ++itTails) {
    const auto& tail = *itTails;
    ASSERT_EQ(tail->id(), *it++);
  }
  auto itHeads = g.headsBegin(4);
  auto headsEnd = g.headsEnd(4);
  for (; itHeads != headsEnd; ++itHeads) {
    const auto& head = *itHeads;
    ASSERT_EQ(head->id(), expectedDummyVertexId);
  }
  ASSERT_EQ(g.nbTailVertices(5), (size_t) 1);
  ASSERT_EQ(g.nbHeadVertices(5), (size_t) 2);
  itTails = g.tailsBegin(5);
  tailsEnd = g.tailsEnd(5);
  for (; itTails != tailsEnd; ++itTails) {
    const auto& tail = *itTails;
    ASSERT_EQ(tail->id(), expectedDummyVertexId);
  }
  it = heads.begin();
  itHeads = g.headsBegin(5);
  headsEnd = g.headsEnd(5);
  for (; itHeads != headsEnd; ++itHeads) {
    const auto& head = *itHeads;
    ASSERT_EQ(head->id(), *it++);
  }
}

TEST_F(TestAlgorithmDHG, Test_symmetricImage) {
  hglib::DirectedHypergraph<> g;
  for (int i = 0; i < 8; ++i) {
    g.addVertex();
  }
  // Add hyperarcs
  g.addHyperarc({{4}, {3}});
  g.addHyperarc({{5}, {6, 7}});
  g.addHyperarc({{0, 1}, {2}});
  g.addHyperarc({{3, 4}, {5, 7}});

  auto symImage = hglib::symmetricImage(g);
  ASSERT_EQ(g.nbVertices(), symImage->nbVertices());
  ASSERT_EQ(g.nbHyperarcs(), symImage->nbHyperarcs());
  for (const auto& vertex : g.vertices()) {
    ASSERT_NE(symImage->vertexById(vertex->id()), nullptr);
  }
  for (const auto& hyperarc : g.hyperarcs()) {
    ASSERT_NE(symImage->hyperarcById(hyperarc->id()), nullptr);
    auto it = g.tailsBegin(hyperarc->id());
    auto end = g.tailsEnd(hyperarc->id());
    for (; it != end; ++it) {
      const auto& tail = *it;
      ASSERT_TRUE(symImage->isHeadVertexOfHyperarc(tail->id(),
                                                   hyperarc->id()));
    }
    it = g.headsBegin(hyperarc->id());
    end = g.headsEnd(hyperarc->id());
    for (; it != end; ++it) {
      const auto& head = *it;
      ASSERT_TRUE(symImage->isTailVertexOfHyperarc(head->id(),
                                                   hyperarc->id()));
    }
  }

  // remove vertices and hyperarcs from the original graph and build again
  // the symmetric image
  g.removeVertex(0, false);
  g.removeVertex(1, false);
  g.removeVertex(3, false);
  g.removeVertex(6, false);
  g.removeVertex(7, false);
  g.removeHyperarc(0);
  g.removeHyperarc(3);
  auto symImage2 = hglib::symmetricImage(g);
  ASSERT_EQ(g.nbVertices(), symImage2->nbVertices());
  ASSERT_EQ(g.nbHyperarcs(), symImage2->nbHyperarcs());
  for (const auto& vertex : g.vertices()) {
    ASSERT_NE(symImage2->vertexById(vertex->id()), nullptr);
  }
  for (const auto& hyperarc : g.hyperarcs()) {
    ASSERT_NE(symImage2->hyperarcById(hyperarc->id()), nullptr);
    auto it = g.tailsBegin(hyperarc->id());
    auto end = g.tailsEnd(hyperarc->id());
    for (; it != end; ++it) {
      const auto& tail = *it;
      ASSERT_TRUE(symImage2->isHeadVertexOfHyperarc(tail->id(),
                                                   hyperarc->id()));
    }
    it = g.headsBegin(hyperarc->id());
    end = g.headsEnd(hyperarc->id());
    for (; it != end; ++it) {
      const auto& head = *it;
      ASSERT_TRUE(symImage2->isTailVertexOfHyperarc(head->id(),
                                                   hyperarc->id()));
    }
  }

  // Check that the inverse image of a f-hypergraph is a b-hypergraph and
  // vice versa
  hglib::DirectedHypergraph<> g2;
  for (int i = 0; i < 8; ++i) {
    g2.addVertex();
  }
  // Add hyperarcs
  g2.addHyperarc({{4}, {3}});
  g2.addHyperarc({{2}, {1}});
  g2.addHyperarc({{2}, {3}});
  g2.addHyperarc({{5}, {6, 7}});
  ASSERT_TRUE(hglib::is_f_hypergraph(g2));
  auto symImage3 = hglib::symmetricImage(g2);
  ASSERT_TRUE(hglib::is_b_hypergraph(*symImage3.get()));

  hglib::DirectedHypergraph<> g3;
  for (int i = 0; i < 8; ++i) {
    g3.addVertex();
  }
  // Add hyperarcs
  g3.addHyperarc({{0, 1}, {2}});
  g3.addHyperarc({{4}, {3}});
  g3.addHyperarc({{2}, {1}});
  g3.addHyperarc({{2}, {3}});
  g3.addHyperarc({{6, 7}, {0}});
  ASSERT_TRUE(hglib::is_b_hypergraph(g3));
  auto symImage4 = hglib::symmetricImage(g3);
  ASSERT_TRUE(hglib::is_f_hypergraph(*symImage4.get()));
}

TEST_F(TestAlgorithmDHG, Test_verticesConnectedToSource) {
  hglib::DirectedHypergraph<> g;
  for (int i = 0; i < 9; ++i) {
    g.addVertex();
  }
  // Add hyperarcs
  g.addHyperarc({{0, 1}, {2}});
  g.addHyperarc({{2}, {1}});
  g.addHyperarc({{2}, {3}});
  g.addHyperarc({{2, 3}, {4, 5}});
  g.addHyperarc({{4}, {3}});
  g.addHyperarc({{5}, {6, 7}});
  g.addHyperarc({{6, 7}, {0}});
  g.addHyperarc({{8}, {0}});

  // from vertex 0
  std::vector<hglib::VertexIdType> expectedResult = {0, 1, 2, 3, 4, 5, 6, 7};
  auto observedResult = hglib::verticesConnectedToSource(g, 0);
  for (const auto& vertex : expectedResult) {
    ASSERT_NE(std::find(observedResult.begin(), observedResult.end(), vertex),
              observedResult.end());
  }

  // from vertex 8
  expectedResult = {0, 1, 2, 3, 4, 5, 6, 7, 8};
  observedResult = hglib::verticesConnectedToSource(g, 8);
  for (const auto& vertex : expectedResult) {
    ASSERT_NE(std::find(observedResult.begin(), observedResult.end(), vertex),
              observedResult.end());
  }
}

TEST_F(TestAlgorithmDHG, Test_has_path) {
  hglib::DirectedHypergraph<> g;
  for (int i = 0; i < 9; ++i) {
    g.addVertex();
  }
  // Add hyperarcs
  g.addHyperarc({{0, 1}, {2}});
  g.addHyperarc({{2}, {1}});
  g.addHyperarc({{2}, {3}});
  g.addHyperarc({{2, 3}, {4, 5}});
  g.addHyperarc({{4}, {3}});
  g.addHyperarc({{5}, {6, 7}});
  g.addHyperarc({{6, 7}, {0}});
  g.addHyperarc({{8}, {0}});

  ASSERT_TRUE(hglib::has_path(g, 0, 2));
  ASSERT_FALSE(hglib::has_path(g, 2, 8));
  ASSERT_TRUE(hglib::has_path(g, 8, 2));

  ASSERT_THROW(hglib::has_path(g, 1000, 2), std::out_of_range);
  ASSERT_THROW(hglib::has_path(g, 2, 1000), std::out_of_range);

  g.removeVertex(2);
  ASSERT_THROW(hglib::has_path(g, 1, 2), std::invalid_argument);
  ASSERT_THROW(hglib::has_path(g, 2, 1), std::invalid_argument);
}

TEST_F(TestAlgorithmDHG, Test_shortestPath) {
  hglib::DirectedHypergraph<> g;
  for (int i = 0; i < 5; ++i) {
    g.addVertex();
  }
  // Add hyperarcs
  g.addHyperarc({{0}, {1}});
  g.addHyperarc({{0}, {2}});
  g.addHyperarc({{1}, {3}});
  g.addHyperarc({{2}, {3}});
  g.addHyperarc({{0}, {4}});
  g.addHyperarc({{4}, {3}});

  // shortest path with default weight 1.0 on each hyperarc
  auto shortestPath = hglib::shortestPath(&g, 0, 3);
  auto subgraph = std::move(std::get<0>(shortestPath));
  float distance = std::get<1>(shortestPath);
  ASSERT_NE(subgraph->vertexById(0), nullptr);
  ASSERT_NE(subgraph->hyperarcById(0), nullptr);
  ASSERT_NE(subgraph->vertexById(1), nullptr);
  ASSERT_NE(subgraph->hyperarcById(2), nullptr);
  ASSERT_NE(subgraph->vertexById(3), nullptr);
  ASSERT_EQ(distance, 2.0);  // distance

  // vertices not on the shortest path
  ASSERT_EQ(subgraph->vertexById(2), nullptr);
  ASSERT_EQ(subgraph->vertexById(4), nullptr);
  // hyperarcs not on the shortest path
  ASSERT_EQ(subgraph->hyperarcById(1), nullptr);
  ASSERT_EQ(subgraph->hyperarcById(3), nullptr);
  ASSERT_EQ(subgraph->hyperarcById(4), nullptr);
  ASSERT_EQ(subgraph->hyperarcById(5), nullptr);


  // remove a vertex on the currently shortest path
  g.removeVertex(1, false);
  ASSERT_EQ(subgraph->vertexById(1), nullptr);
  // shortest path with default weight 1.0 on each hyperarc
  auto shortestPath2 = hglib::shortestPath(&g, 0, 3);
  auto subgraph2 = std::move(std::get<0>(shortestPath2));
  distance = std::get<1>(shortestPath2);

  ASSERT_NE(subgraph2->vertexById(0), nullptr);
  ASSERT_NE(subgraph2->hyperarcById(1), nullptr);
  ASSERT_NE(subgraph2->vertexById(2), nullptr);
  ASSERT_NE(subgraph2->hyperarcById(3), nullptr);
  ASSERT_NE(subgraph2->vertexById(3), nullptr);
  ASSERT_EQ(distance, 2.0);  // distance

  // vertices not on the shortest path
  ASSERT_EQ(subgraph2->vertexById(4), nullptr);
  // hyperarcs not on the shortest path
  ASSERT_EQ(subgraph2->hyperarcById(0), nullptr);
  ASSERT_EQ(subgraph2->hyperarcById(2), nullptr);
  ASSERT_EQ(subgraph2->hyperarcById(4), nullptr);
  ASSERT_EQ(subgraph2->hyperarcById(5), nullptr);

  // test exception handling
  ASSERT_THROW(hglib::shortestPath(&g, 1000, 0), std::out_of_range);

  ASSERT_THROW(hglib::shortestPath(&g, 0, 1000), std::out_of_range);

  ASSERT_THROW(hglib::shortestPath(&g, 3, 0), hglib::no_path_exception);
  // remove vertex
  g.removeVertex(0, false);
  ASSERT_THROW(hglib::shortestPath(&g, 0, 3), std::invalid_argument);
  ASSERT_THROW(hglib::shortestPath(&g, 3, 0), std::invalid_argument);
}

TEST_F(TestAlgorithmDHG, Test_shortestPath_userDefinedWeights) {
  struct HyperarcProperty {
      float weight = 1.0;
  };
  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
          hglib::emptyProperty, HyperarcProperty> g;
  for (int i = 0; i < 5; ++i) {
    g.addVertex();
  }
  // Add hyperarcs
  g.addHyperarc({{0}, {1}});
  g.addHyperarc({{0}, {2}});
  g.addHyperarc({{1}, {3}});
  g.addHyperarc({{2}, {3}});
  g.addHyperarc({{0}, {4}});
  g.addHyperarc({{4}, {3}});

  auto shortestPath =
          hglib::shortestPath(&g, 0, 3,
                              [&] (const hglib::HyperedgeIdType& arcId) ->
                                      float {
                                  return g.getHyperarcProperties(arcId)->
                                          weight;});
  auto subgraph = std::move(std::get<0>(shortestPath));
  float distance = std::get<1>(shortestPath);
  ASSERT_NE(subgraph->vertexById(0), nullptr);
  ASSERT_NE(subgraph->hyperarcById(0), nullptr);
  ASSERT_NE(subgraph->vertexById(1), nullptr);
  ASSERT_NE(subgraph->hyperarcById(2), nullptr);
  ASSERT_NE(subgraph->vertexById(3), nullptr);
  ASSERT_EQ(distance, 2.0);  // distance

  // vertices not on the shortest path
  ASSERT_EQ(subgraph->vertexById(2), nullptr);
  ASSERT_EQ(subgraph->vertexById(4), nullptr);
  // hyperarcs not on the shortest path
  ASSERT_EQ(subgraph->hyperarcById(1), nullptr);
  ASSERT_EQ(subgraph->hyperarcById(3), nullptr);
  ASSERT_EQ(subgraph->hyperarcById(4), nullptr);
  ASSERT_EQ(subgraph->hyperarcById(5), nullptr);


  // change weights of hyperarcs
  for (const auto& hyperarc : g.hyperarcs()) {
    g.getHyperarcProperties_(hyperarc->id())->weight /= (hyperarc->id() + 1);
  }
  auto shortestPath2 = hglib::shortestPath(&g, 0, 3,
                              [&] (const hglib::HyperedgeIdType& arcId) ->
                                      float {
                                  return g.getHyperarcProperties(arcId)->
                                          weight;});
  auto subgraph2 = std::move(std::get<0>(shortestPath2));
  distance = std::get<1>(shortestPath2);
  ASSERT_NE(subgraph2->vertexById(0), nullptr);
  ASSERT_NE(subgraph2->hyperarcById(4), nullptr);
  ASSERT_NE(subgraph2->vertexById(4), nullptr);
  ASSERT_NE(subgraph2->hyperarcById(5), nullptr);
  ASSERT_NE(subgraph2->vertexById(3), nullptr);
  float expectedDistance((1.0/5.0) + (1.0/6.0));
  ASSERT_EQ(distance, expectedDistance);  // distance

  // vertices not on the shortest path
  ASSERT_EQ(subgraph2->vertexById(1), nullptr);
  ASSERT_EQ(subgraph2->vertexById(2), nullptr);
  // hyperarcs not on the shortest path
  ASSERT_EQ(subgraph2->hyperarcById(0), nullptr);
  ASSERT_EQ(subgraph2->hyperarcById(1), nullptr);
  ASSERT_EQ(subgraph2->hyperarcById(2), nullptr);
  ASSERT_EQ(subgraph2->hyperarcById(3), nullptr);

  // test exception handling
  ASSERT_THROW(hglib::shortestPath(&g, 1000, 0,
                                   [&] (const hglib::HyperedgeIdType& arcId) ->
                                           float {
                                       return g.getHyperarcProperties(arcId)->
                                               weight;}),
               std::out_of_range);

  ASSERT_THROW(hglib::shortestPath(&g, 0, 1000,
                                   [&] (const hglib::HyperedgeIdType& arcId) ->
                                           float {
                                       return g.getHyperarcProperties(arcId)->
                                               weight;}),
               std::out_of_range);

  ASSERT_THROW(hglib::shortestPath(&g, 3, 0,
                                   [&] (const hglib::HyperedgeIdType& arcId) ->
                                           float {
                                       return g.getHyperarcProperties(arcId)->
                                               weight;}),
               hglib::no_path_exception);
  // remove vertex
  g.removeVertex(0, false);
  ASSERT_THROW(hglib::shortestPath(&g, 0, 3,
                                   [&] (const hglib::HyperedgeIdType& arcId) ->
                                           float {
                                       return g.getHyperarcProperties(arcId)->
                                               weight;}),
               std::invalid_argument);
  ASSERT_THROW(hglib::shortestPath(&g, 3, 0,
                                   [&] (const hglib::HyperedgeIdType& arcId) ->
                                           float {
                                       return g.getHyperarcProperties(arcId)->
                                               weight;}),
               std::invalid_argument);
}

TEST_F(TestAlgorithmDHG, Test_distanceMatrix) {
  hglib::DirectedHypergraph<> g;
  for (int i = 0; i < 5; ++i) {
    g.addVertex();
  }
  // Add hyperarcs
  g.addHyperarc({{0}, {1}});
  g.addHyperarc({{0}, {2}});
  g.addHyperarc({{1}, {3}});
  g.addHyperarc({{2}, {3}});
  g.addHyperarc({{0}, {4}});
  g.addHyperarc({{4}, {3}});

  auto distanceMatrix = hglib::distanceMatrix(&g);
  // distance to a vertex itself
  for (const auto& vertex : g.vertices()) {
    ASSERT_EQ(distanceMatrix.getDistance(vertex->id(), vertex->id()), 0.0);
  }
  ASSERT_EQ(distanceMatrix.getDistance(0, 1), 1.0);
  ASSERT_EQ(distanceMatrix.getDistance(0, 2), 1.0);
  ASSERT_EQ(distanceMatrix.getDistance(0, 4), 1.0);
  ASSERT_EQ(distanceMatrix.getDistance(1, 3), 1.0);
  ASSERT_EQ(distanceMatrix.getDistance(2, 3), 1.0);
  ASSERT_EQ(distanceMatrix.getDistance(4, 3), 1.0);
  ASSERT_EQ(distanceMatrix.getDistance(0, 3), 2.0);
  // there exists no path between those vertices
  ASSERT_EQ(distanceMatrix.getDistance(1, 0),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(1, 2),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(1, 4),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(2, 0),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(2, 1),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(2, 4),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(3, 0),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(3, 1),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(3, 2),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(3, 4),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(4, 0),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(4, 1),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(4, 2),
            std::numeric_limits<float>::infinity());

  // exception test
  ASSERT_THROW(distanceMatrix.getDistance(1000, 0), std::out_of_range);
  ASSERT_THROW(distanceMatrix.getDistance(0, 1000), std::out_of_range);
  ASSERT_THROW(distanceMatrix.setDistance(1000, 0, 1.0), std::out_of_range);
  ASSERT_THROW(distanceMatrix.setDistance(0, 1000, 1.0), std::out_of_range);
}

TEST_F(TestAlgorithmDHG, Test_distanceMatrix_userDefinedWeights) {
  struct HyperarcProperty {
      float weight = 1.0;
  };
  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
          hglib::emptyProperty, HyperarcProperty> g;
  for (int i = 0; i < 5; ++i) {
    g.addVertex();
  }
  // Add hyperarcs
  g.addHyperarc({{0}, {1}});  // will have weight 1.0
  g.addHyperarc({{0}, {2}});  // will have weight 2.0
  g.addHyperarc({{1}, {3}});  // will have weight 3.0
  g.addHyperarc({{2}, {3}});  // will have weight 4.0
  g.addHyperarc({{0}, {4}});  // will have weight 5.0
  g.addHyperarc({{4}, {3}});  // will have weight 6.0

  for (const auto& arc : g.hyperarcs()) {
    g.getHyperarcProperties_(arc->id())->weight += arc->id();
  }

  auto distanceMatrix = hglib::distanceMatrix(&g, [&] (
          const hglib::HyperedgeIdType& arcId) -> float {
      return g.getHyperarcProperties(arcId)->weight;});

  // distance to a vertex itself
  for (const auto& vertex : g.vertices()) {
    ASSERT_EQ(distanceMatrix.getDistance(vertex->id(), vertex->id()), 0.0);
  }
  ASSERT_EQ(distanceMatrix.getDistance(0, 1), 1.0);
  ASSERT_EQ(distanceMatrix.getDistance(0, 2), 2.0);
  ASSERT_EQ(distanceMatrix.getDistance(0, 4), 5.0);
  ASSERT_EQ(distanceMatrix.getDistance(1, 3), 3.0);
  ASSERT_EQ(distanceMatrix.getDistance(2, 3), 4.0);
  ASSERT_EQ(distanceMatrix.getDistance(4, 3), 6.0);
  ASSERT_EQ(distanceMatrix.getDistance(0, 3), 4.0);
  // there exists no path between those vertices
  ASSERT_EQ(distanceMatrix.getDistance(1, 0),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(1, 2),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(1, 4),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(2, 0),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(2, 1),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(2, 4),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(3, 0),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(3, 1),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(3, 2),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(3, 4),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(4, 0),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(4, 1),
            std::numeric_limits<float>::infinity());
  ASSERT_EQ(distanceMatrix.getDistance(4, 2),
            std::numeric_limits<float>::infinity());
}
