// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "hglib.h"

#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <list>
#include <deque>
#include <unordered_set>

#include "gtest/gtest.h"

using std::pair;
using std::string;
using std::vector;
using hglib::UndirectedVertex;
using hglib::UndirectedHyperedge;
using hglib::NamedUndirectedHyperedge;
using hglib::UndirectedVertex;
using hglib::UndirectedHyperedge;
using hglib::NamedUndirectedHyperedge;

/**
 * Test fixture
 */
class TestUndirectedSubHypergraph : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

  vector<string> vertices {"A", "B", "Foo", "Bar", "", "6"};
  vector<pair<vector<string>, string>> hyperedges {
    {{"A", "Foo"}, "edge1"},
    {{"A", "B", "Bar", ""}, "edge2"},
    {{"6", ""}, "edge3"}
  };
  vector<pair<vector<string>, string>> hyperedgesWithUndeclaredVertices {
    {{"A", "Foo"}, "edge1"},
    {{"A", "B", "Bar", ""}, "edge2"},
    {{"6", ""}, "edge3"},
    {{"A", "C"}, "edge4"},
    {{"D", "C"}, "edge5"}
  };
  vector<pair<vector<string>, string>> hyperedgesDuplicateEntries {
    {{"A", "Foo"}, "edge1"},
    {{"A2", "Foo2"}, "edge1"},
    {{"A", "B", "Bar", ""}, "edge2"},
    {{"6", ""}, "edge3"}
  };
  vector<string> verticesDuplicateEntries {"A", "B", "Foo", "Bar", "", "6",
                                           "A"};

  vector<string> vertices2 {"X", "Y", "Z", "U", "V", "A", "Foo"};
  vector<pair<vector<string>, string>> hyperedges2 {
    {{"7", "X"}, "edge1"},
    {{"Z", "X", "A"}, "edge2"},
    {{"Y", "V", "Foo"}, "edge3"},
    {{"X", "Y"}, "edge4"},
    {{"X", "Z", "U", "V"}, "edge5"},
    {{"A", "Foo"}, "edge6"}
  };

 private:
  std::streambuf* storedStreambuf_;
};


TEST_F(TestUndirectedSubHypergraph, Test_subgraph_Ctor) {
  hglib::UndirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  // Add hyperedges twice (but with different ids)
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first);
  }

  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {0, 1};
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                       whitelistedHyperedges);

  // check that only whitelisted vertices are in the subgraph
  for (const auto& vertex : g.vertices()) {
    if (std::find(whitelistedVertices.begin(), whitelistedVertices.end(),
        vertex->id()) == whitelistedVertices.end()) {
      ASSERT_EQ(subgraph->vertexById(vertex->id()), nullptr);
    } else {
      ASSERT_EQ(subgraph->vertexById(vertex->id()), vertex);
    }
  }
  // check that only whitelisted hyperedges are in the subgraph
  for (const auto& hyperedge : g.hyperedges()) {
    if (std::find(whitelistedHyperedges.begin(), whitelistedHyperedges.end(),
        hyperedge->id()) == whitelistedHyperedges.end()) {
      ASSERT_EQ(subgraph->hyperedgeById(hyperedge->id()), nullptr);
      } else {
      ASSERT_EQ(subgraph->hyperedgeById(hyperedge->id()), hyperedge);
    }
  }

  // sub-sub-hypergraph
  whitelistedVertices = {1};
  whitelistedHyperedges = {1};
  auto subsubgraph = hglib::createUndirectedSubHypergraph(subgraph.get(),
                                                        whitelistedVertices,
                                                        whitelistedHyperedges);
  // check that only whitelisted vertices are in the sub-subgraph
  for (const auto& vertex : g.vertices()) {
    if (std::find(whitelistedVertices.begin(), whitelistedVertices.end(),
            vertex->id()) == whitelistedVertices.end()) {
      ASSERT_EQ(subsubgraph->vertexById(vertex->id()), nullptr);
    } else {
      ASSERT_EQ(subsubgraph->vertexById(vertex->id()), vertex);
    }
  }
  // check that only whitelisted hyperedges are in the sub-subgraph
  for (const auto& hyperedge : g.hyperedges()) {
    if (std::find(whitelistedHyperedges.begin(), whitelistedHyperedges.end(),
            hyperedge->id()) == whitelistedHyperedges.end()) {
      ASSERT_EQ(subsubgraph->hyperedgeById(hyperedge->id()), nullptr);
    } else {
      ASSERT_EQ(subsubgraph->hyperedgeById(hyperedge->id()), hyperedge);
    }
  }

  // Check exceptions
  whitelistedVertices = {1, 2, 3, 1000};
  whitelistedHyperedges = {1, 2, 1000};
  // Vertex with Id 1000 is not in the root hypergraph
  ASSERT_THROW(hglib::createUndirectedSubHypergraph(
      &g, whitelistedVertices, whitelistedHyperedges), std::out_of_range);
  // Hyperedge with Id 1000 is not in the root hypergraph
  whitelistedVertices = {1, 2, 3};
  ASSERT_THROW(hglib::createUndirectedSubHypergraph(
      &g, whitelistedVertices, whitelistedHyperedges), std::out_of_range);
  // Reaction with no vertex in the subgraph
  whitelistedHyperedges = {1};
  ASSERT_THROW(hglib::createUndirectedSubHypergraph(
      &g, {}, whitelistedHyperedges), std::invalid_argument);

  // Remove vertex/hyperarc and check invalid_argument exception
  g.removeVertex(1, false);
  g.removeHyperedge(1);
  whitelistedVertices = {1};
  whitelistedHyperedges = {1};
  ASSERT_THROW(hglib::createUndirectedSubHypergraph(
      &g, whitelistedVertices, whitelistedHyperedges), std::invalid_argument);
  whitelistedVertices = {2, 3};
  ASSERT_THROW(hglib::createUndirectedSubHypergraph(
      &g, whitelistedVertices, whitelistedHyperedges), std::invalid_argument);
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_copyCtor) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      std::string label = "Default";
      Colour colour = Colour::green;
  };
  struct UndirectedHyperedgeProperty {
      std::string label = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      std::string label = "Default";
      Colour colour = Colour::red;
  };
  hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
          VertexProperty, UndirectedHyperedgeProperty, HypergraphProperty> g;
  for (auto vertexName : vertices) {
  g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
  g.addHyperedge(hglib::NAME, hyperedge.first);
  }
  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {0, 1};
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);
  subgraph->removeVertex(1, false);
  // change some default properties in hypergraph with unnamed hyperedges
  subgraph->getVertexProperties_(2)->label = "Test";
  hglib::HyperedgeIdType hyperedgeId(1);  // hyperedge "edge1"
  subgraph->getHyperedgeProperties_(hyperedgeId)->label = "TestHyperedge";
  subgraph->getHyperedgeProperties_(hyperedgeId)->weight = 1.0;
  subgraph->getHypergraphProperties_()->colour = Colour::blue;

  // Make a copy
  auto subgraphCopy = hglib::copyUndirectedSubHypergraph(*subgraph.get());

  // Check
  ASSERT_EQ(subgraph->nbVertices(), subgraphCopy->nbVertices());
  ASSERT_EQ(subgraph->nbHyperedges(), subgraphCopy->nbHyperedges());
  // check graph properties
  ASSERT_EQ(subgraphCopy->getHypergraphProperties()->label.compare("Default"),
  0);
  ASSERT_EQ(subgraphCopy->getHypergraphProperties()->colour, Colour::blue);

  // check vertices
  for (const auto& vertex : subgraph->vertices()) {
    const auto& copiedVertex = subgraphCopy->vertexByName(vertex->name());
    ASSERT_NE(copiedVertex, nullptr);
    ASSERT_EQ(vertex->id(), copiedVertex->id());
    const auto& vertexId = vertex->id();
    // compare hyperedges
    ASSERT_EQ(subgraph->nbHyperedges(vertexId), subgraphCopy->nbHyperedges(
            vertexId));
    auto it = subgraph->hyperedgesBegin(vertexId);
    auto end = subgraph->hyperedgesEnd(vertexId);
    auto hyperedgeCopyIt = subgraphCopy->hyperedgesBegin(vertexId);
    // range based for
    auto hyperedgesPtr = subgraph->hyperedges(vertexId);
    for (const auto& hyperedge : *hyperedgesPtr) {
      ASSERT_EQ(hyperedge->id(), (*hyperedgeCopyIt)->id());
      ++it;
      ++hyperedgeCopyIt;
    }

    // compare properties
    const auto vertexProperty = subgraph->getVertexProperties(vertexId);
    const auto copiedVertexProperty = subgraphCopy->getVertexProperties(
            vertexId);
    ASSERT_EQ(vertexProperty->label.compare(copiedVertexProperty->label), 0);
    ASSERT_EQ(vertexProperty->colour, copiedVertexProperty->colour);
  }

  // check hyperedges
  for (const auto& hyperedge : subgraph->hyperedges()) {
    const auto& copiedUndirectedHyperedge = subgraphCopy->hyperedgeById(
            hyperedge->id());
    ASSERT_NE(copiedUndirectedHyperedge, nullptr);
    ASSERT_EQ(hyperedge->id(), copiedUndirectedHyperedge->id());
    const auto& hyperedgeId = hyperedge->id();
    // compare vertices of hyperedge
    ASSERT_EQ(subgraph->nbImpliedVertices(hyperedgeId),
            subgraphCopy->nbImpliedVertices(hyperedgeId));
    auto it = subgraph->impliedVerticesBegin(hyperedgeId);
    auto end = subgraph->impliedVerticesEnd(hyperedgeId);
    auto vertexCopyIt = subgraphCopy->impliedVerticesBegin(hyperedgeId);
    for (; it != end; ++it) {
      const auto& vertex = *it;
      ASSERT_EQ(vertex->id(), (*vertexCopyIt)->id());
      ASSERT_EQ(vertex->name().compare((*vertexCopyIt)->name()), 0);
      ++vertexCopyIt;
    }

    // compare properties
    const auto hyperedgeProperty = subgraph->getHyperedgeProperties(
            hyperedgeId);
    const auto copiedUndirectedHyperedgeProperty =
            subgraphCopy->getHyperedgeProperties(hyperedgeId);
    ASSERT_EQ(hyperedgeProperty->label.compare(
            copiedUndirectedHyperedgeProperty->label), 0);
    ASSERT_EQ(hyperedgeProperty->weight,
              copiedUndirectedHyperedgeProperty->weight);
  }

  // change subgraph and check that this change is
  // independent from the copied subgraph
  subgraph->getHypergraphProperties_()->colour = Colour::green;
  subgraph->getHypergraphProperties_()->label = "Original subgraph";
  ASSERT_EQ(subgraphCopy->getHypergraphProperties()->colour, Colour::blue);
  ASSERT_EQ(subgraphCopy->getHypergraphProperties()->label.compare("Default"),
            0);

  size_t nbExpectedVertices(subgraphCopy->nbVertices());
  size_t nbExpectedUndirectedHyperedges(subgraphCopy->nbHyperedges());
  subgraph->removeVertex(2, false);
  subgraph->removeHyperedge(1);
  ASSERT_EQ(nbExpectedVertices, subgraphCopy->nbVertices());
  ASSERT_EQ(nbExpectedUndirectedHyperedges, subgraphCopy->nbHyperedges());
}


TEST_F(TestUndirectedSubHypergraph, Test_subgraph_AssignmentOperator) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct UndirectedHyperedgeProperty {
      string name = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      string name = "Default";
      Colour colour = Colour::blue;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge,
          VertexProperty, UndirectedHyperedgeProperty,
          HypergraphProperty> graph;

  for (auto vertexName : vertices) {
    graph.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    graph.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  // Three sub- and sub-sub-graphs
  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3, 5};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {1, 2};
  auto subgraph = hglib::UndirectedSubHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge, VertexProperty,
          UndirectedHyperedgeProperty, HypergraphProperty>(
          &graph, whitelistedVertices, whitelistedHyperedges);
  whitelistedVertices = {1, 2, 3, 5};
  whitelistedHyperedges = {2};
  auto subsubgraph = hglib::UndirectedSubHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge, VertexProperty,
          UndirectedHyperedgeProperty, HypergraphProperty>(
          &subgraph, whitelistedVertices, whitelistedHyperedges);

  // second sub- and sub-sub-graph
  whitelistedVertices = {1, 2, 3, 4, 5};
  whitelistedHyperedges = {2};
  auto subgraph2 = hglib::UndirectedSubHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge, VertexProperty,
          UndirectedHyperedgeProperty, HypergraphProperty>(
          &graph, whitelistedVertices, whitelistedHyperedges);
  whitelistedVertices = {1, 2, 5};
  whitelistedHyperedges = {2};
  auto subsubgraph2 = hglib::UndirectedSubHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge, VertexProperty,
          UndirectedHyperedgeProperty, HypergraphProperty>(
          &subgraph2, whitelistedVertices, whitelistedHyperedges);

  // third sub- and sub-sub-graph
  whitelistedVertices = {0, 3, 5};
  whitelistedHyperedges = {0};
  auto subgraph3 = hglib::UndirectedSubHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge, VertexProperty,
          UndirectedHyperedgeProperty, HypergraphProperty>(
          &graph, whitelistedVertices, whitelistedHyperedges);
  whitelistedVertices = {0, 5};
  whitelistedHyperedges = {0};
  auto subsubgraph3 = hglib::UndirectedSubHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge, VertexProperty,
          UndirectedHyperedgeProperty, HypergraphProperty>(
          &subgraph3, whitelistedVertices, whitelistedHyperedges);

  // Change properties in the first subgraph
  subgraph.getHypergraphProperties_()->name = "Test";
  subgraph.getHypergraphProperties_()->colour = Colour::red;
  subgraph.removeVertex(1, false);
  subgraph.removeHyperedge(1);

  // Assignments
  subgraph = subgraph;  // nothing happens in a self assignment
  // The following assignment should invalidate subsubgraph2 and
  // subsubgraph3
  subgraph2 = subgraph3 = subgraph;
  ASSERT_THROW(subsubgraph2.nbVertices(), std::invalid_argument);
  ASSERT_THROW(subsubgraph3.nbVertices(), std::invalid_argument);
  // check if all sub-hypergraphs are identical
  ASSERT_EQ(subgraph.nbVertices(), subgraph2.nbVertices());
  ASSERT_EQ(subgraph.nbVertices(), subgraph3.nbVertices());
  for (const auto& vertex : subgraph.vertices()) {
    ASSERT_EQ(subgraph2.vertexById(vertex->id()), vertex);
    ASSERT_EQ(subgraph2.vertexById(vertex->id()), vertex);
  }
  ASSERT_EQ(subgraph.nbHyperedges(), subgraph2.nbHyperedges());
  ASSERT_EQ(subgraph.nbHyperedges(), subgraph3.nbHyperedges());
  for (const auto& hyperedge : subgraph.hyperedges()) {
    ASSERT_EQ(subgraph2.hyperedgeById(hyperedge->id()), hyperedge);
    ASSERT_EQ(subgraph2.hyperedgeById(hyperedge->id()), hyperedge);
  }
  // Check that the subsubgraph is not a child of all subgraphs and that
  // all subgraphs are independent from each other
  subgraph2.removeHyperedge(2);
  ASSERT_NE(subgraph.nbHyperedges(), subgraph2.nbHyperedges());
  ASSERT_NE(subgraph3.nbHyperedges(), subgraph2.nbHyperedges());
  // UndirectedHyperedge 2 is still in the subsubgraph as its parent is subgraph
  // and not subgraph2
  ASSERT_NE(subsubgraph.hyperedgeById(2), nullptr);

  hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge,
          VertexProperty, UndirectedHyperedgeProperty,
          HypergraphProperty> graph4;
  // The following assignment invalidates the whole subgraph hierarchy of graph
  graph = graph4;
  ASSERT_THROW(subgraph.nbVertices(), std::invalid_argument);
  ASSERT_THROW(subsubgraph.nbVertices(), std::invalid_argument);
  ASSERT_THROW(subgraph2.nbVertices(), std::invalid_argument);
  ASSERT_THROW(subgraph3.nbVertices(), std::invalid_argument);
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_addVertex) {
  hglib::UndirectedHypergraph<> g;
  size_t nbVertices = 0;
  hglib::VertexIdType vertexCounter = 0;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
    ASSERT_EQ(++nbVertices, g.nbVertices());
    ASSERT_EQ(vertexCounter++, g.vertexByName(vertexName)->id());
  }


  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                       whitelistedHyperedges);
  auto subsubgraph = hglib::createUndirectedSubHypergraph(
      subgraph.get(), whitelistedVertices, whitelistedHyperedges);
  size_t nbVerticesSubgraph(subgraph->nbVertices());
  size_t nbVerticesSubSubgraph(subsubgraph->nbVertices());
  ASSERT_EQ(nbVerticesSubgraph, (size_t) 0);
  ASSERT_EQ(nbVerticesSubSubgraph, (size_t) 0);

  // Add vertices to subgraphs:  "B", "Foo"
  subsubgraph->addVertex("B");
  ASSERT_EQ(++nbVerticesSubgraph, subgraph->nbVertices());
  ASSERT_EQ(++nbVerticesSubSubgraph, subsubgraph->nbVertices());
  subsubgraph->addVertex(2);  // via Id
  ASSERT_EQ(++nbVerticesSubgraph, subgraph->nbVertices());
  ASSERT_EQ(++nbVerticesSubSubgraph, subsubgraph->nbVertices());

  // Add vertex only to first subgraph: "A", "Bar"
  subgraph->addVertex("A");
  ASSERT_EQ(++nbVerticesSubgraph, subgraph->nbVertices());
  ASSERT_EQ(nbVerticesSubSubgraph, subsubgraph->nbVertices());
  subgraph->addVertex(3);
  ASSERT_EQ(++nbVerticesSubgraph, subgraph->nbVertices());
  ASSERT_EQ(nbVerticesSubSubgraph, subsubgraph->nbVertices());

  // Test exceptions
  // without a vertex name; only with specific arguments (empty for
  // DirectedVertex) -> not supported in a sub-hypergraph
  ASSERT_THROW(subgraph->addVertex(), std::invalid_argument);
  ASSERT_THROW(subsubgraph->addVertex(), std::invalid_argument);

  // With unknown name
  ASSERT_THROW(subgraph->addVertex("UnknownVertex"), std::invalid_argument);
  ASSERT_THROW(subsubgraph->addVertex("UnknownVertex"), std::invalid_argument);

  // With unknown vertex Id
  ASSERT_THROW(subgraph->addVertex(100), std::out_of_range);
  ASSERT_THROW(subsubgraph->addVertex(100), std::out_of_range);
  // Remove a vertex in root graph
  hglib::VertexIdType removeVertexId = 4;
  g.removeVertex(removeVertexId, false);
  // Exception when there is a nullptr at the given position
  ASSERT_THROW(subgraph->addVertex(removeVertexId), std::invalid_argument);
  ASSERT_THROW(subsubgraph->addVertex(removeVertexId), std::invalid_argument);


  // Add a new vertex to root hypergraph
  const auto& vertex = g.addVertex("UnknownVertex");
  // Vertex can now be added to the subgraphs
  subsubgraph->addVertex(vertex->id());
  ASSERT_EQ(++nbVerticesSubgraph, subgraph->nbVertices());
  ASSERT_EQ(++nbVerticesSubSubgraph, subsubgraph->nbVertices());
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_addHyperedge) {
  hglib::UndirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first);
  }

  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {
      0, 1, 2, 3, 4, 5};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                       whitelistedHyperedges);
  auto subsubgraph = hglib::createUndirectedSubHypergraph(subgraph.get(),
                                                        whitelistedVertices,
                                                        whitelistedHyperedges);

  size_t nbHyperedgesSubgraph(subgraph->nbHyperedges());
  size_t nbHyperedgesSubSubgraph(subsubgraph->nbHyperedges());
  ASSERT_EQ(nbHyperedgesSubgraph, (size_t) 0);
  ASSERT_EQ(nbHyperedgesSubSubgraph, (size_t) 0);

  // Add hyperedges to subgraphs:  {{"A", "Foo"}}, {{"A", "B", "Bar", ""}}
  subsubgraph->addHyperedge(hglib::NAME, {"A", "Foo"});
  ASSERT_EQ(++nbHyperedgesSubgraph, subgraph->nbHyperedges());
  ASSERT_EQ(++nbHyperedgesSubSubgraph, subsubgraph->nbHyperedges());
  subsubgraph->addHyperedge({0, 1, 3, 4});  // via vertex Ids
  ASSERT_EQ(++nbHyperedgesSubgraph, subgraph->nbHyperedges());
  ASSERT_EQ(++nbHyperedgesSubSubgraph, subsubgraph->nbHyperedges());

  // Add hyperedge only to first subgraph: {{"6"}, {""}}
  subgraph->addHyperedge(hglib::NAME, {"6", ""});
  ASSERT_EQ(++nbHyperedgesSubgraph, subgraph->nbHyperedges());
  ASSERT_EQ(nbHyperedgesSubSubgraph, subsubgraph->nbHyperedges());

  // Test exceptions
  // Unknown hyperedge
  ASSERT_THROW(subgraph->addHyperedge(hglib::NAME, {"A", "6", "Bar", ""}),
               std::invalid_argument);
  ASSERT_THROW(subsubgraph->addHyperedge({0, 5, 3, 4}),
               std::invalid_argument);

  // With unknown hyperedge Id
  ASSERT_THROW(subgraph->addHyperedge(100), std::out_of_range);
  ASSERT_THROW(subsubgraph->addHyperedge(100), std::out_of_range);
  // Remove a hyperedge in root graph
  hglib::HyperedgeIdType removeHyperedgeId = 1;
  g.removeHyperedge(removeHyperedgeId);
  --nbHyperedgesSubgraph;
  --nbHyperedgesSubSubgraph;
  // Exception when there is a nullptr at the given position
  ASSERT_THROW(subgraph->addHyperedge(removeHyperedgeId),
               std::invalid_argument);
  ASSERT_THROW(subsubgraph->addHyperedge(removeHyperedgeId),
               std::invalid_argument);


  // Add a new hyperedge to root hypergraph
  const auto& hyperedge = g.addHyperedge(hglib::NAME, {"Foo", "6", "Bar"});
  ASSERT_NE(hyperedge, nullptr);
  // Hyperedge can now be added to the subgraphs
  subsubgraph->addHyperedge(hyperedge->id());
  ASSERT_EQ(++nbHyperedgesSubgraph, subgraph->nbHyperedges());
  ASSERT_EQ(++nbHyperedgesSubSubgraph, subsubgraph->nbHyperedges());

  // Check that at least one vertex is in the subgraph
  whitelistedVertices = {};
  whitelistedHyperedges = {};
  subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                  whitelistedHyperedges);
  // no vertex is present
  ASSERT_THROW(subgraph->addHyperedge(0), std::invalid_argument);
  subgraph->addVertex(0);
  ASSERT_NO_THROW(subgraph->addHyperedge(0));
  ASSERT_EQ(subgraph->nbHyperedges(), (size_t) 1);
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_vertexByName) {
  hglib::UndirectedHypergraph<> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      whitelistedVertices.insert(vertex->id());
    }
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);
  for (const auto& vertex : g.vertices()) {
    if ((vertex->id() % 2) == 0) {
      ASSERT_EQ(subgraph->vertexByName(vertex->name()), vertex);
    } else {
      ASSERT_EQ(subgraph->vertexByName(vertex->name()), nullptr);
    }
  }
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_vertexById) {
  hglib::UndirectedHypergraph<> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      whitelistedVertices.insert(vertex->id());
    }
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);
  for (const auto& vertex : g.vertices()) {
    if ((vertex->id() % 2) == 0) {
      ASSERT_EQ(subgraph->vertexById(vertex->id()), vertex);
    } else {
      ASSERT_EQ(subgraph->vertexById(vertex->id()), nullptr);
    }
  }
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_verticesContainer) {
  hglib::UndirectedHypergraph<> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      whitelistedVertices.insert(vertex->id());
    }
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);

  std::vector<const UndirectedVertex<UndirectedHyperedge>*> verticesContainer;
  hglib::vertices(*subgraph.get(), std::back_inserter(verticesContainer));
  EXPECT_EQ(verticesContainer.size(), subgraph->nbVertices());
  for (auto& vertex : verticesContainer) {
    EXPECT_EQ(subgraph->vertexById(vertex->id()), vertex);
  }

  // test with std:list
  // std::back_inserter
  std::list<const UndirectedVertex<UndirectedHyperedge>*> l;
  hglib::vertices(*subgraph.get(), std::back_inserter(l));
  EXPECT_EQ(l.size(), subgraph->nbVertices());
  for (auto& vertex : l) {
    EXPECT_EQ(subgraph->vertexById(vertex->id()), vertex);
  }
  l.clear();
  // std::inserter
  hglib::vertices(*subgraph.get(), std::inserter(l, l.begin()));
  EXPECT_EQ(l.size(), subgraph->nbVertices());
  for (auto& vertex : l) {
    EXPECT_EQ(subgraph->vertexById(vertex->id()), vertex);
  }
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_hyperedgesContainer) {
  hglib::UndirectedHypergraph<> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  for (auto edge : hyperedges) {
    const auto& hyperedge = g.addHyperedge(hglib::NAME, edge.first);
    if ((hyperedge->id() % 2) == 0) {
      whitelistedHyperedges.insert(hyperedge->id());
    }
  }

  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);

  std::vector<const UndirectedHyperedge*> hyperedgeContainer;
  hglib::hyperedges(*subgraph.get(), std::back_inserter(hyperedgeContainer));
  ASSERT_EQ(hyperedgeContainer.size(), subgraph->nbHyperedges());
  for (auto& hyperedge : hyperedgeContainer) {
    EXPECT_NE(subgraph->hyperedgeById(hyperedge->id()), nullptr);
  }

  // test with std:list
  // std::back_inserter
  std::list<const UndirectedHyperedge*> l;
  hglib::hyperedges(*subgraph.get(), std::back_inserter(l));
  EXPECT_EQ(l.size(), subgraph->nbHyperedges());
  for (auto& hyperedge : l) {
    EXPECT_NE(subgraph->hyperedgeById(hyperedge->id()), nullptr);
  }
  l.clear();
  // std::inserter
  hglib::hyperedges(*subgraph.get(), std::inserter(l, l.begin()));
  EXPECT_EQ(l.size(), subgraph->nbHyperedges());
  for (auto& hyperedge : l) {
    EXPECT_NE(subgraph->hyperedgeById(hyperedge->id()), nullptr);
  }

  // test with std::deque
  std::deque<const UndirectedHyperedge*> d;
  hglib::hyperedges(*subgraph.get(), std::back_inserter(d));
  EXPECT_EQ(d.size(), subgraph->nbHyperedges());
  for (auto& hyperedge : d) {
    EXPECT_NE(subgraph->hyperedgeById(hyperedge->id()), nullptr);
  }

  // test with std::set
  std::set<const UndirectedHyperedge*> s;
  hglib::hyperedges(*subgraph.get(), std::inserter(s, s.begin()));
  EXPECT_EQ(s.size(), subgraph->nbHyperedges());
  for (auto& hyperedge : s) {
    EXPECT_NE(subgraph->hyperedgeById(hyperedge->id()), nullptr);
  }
  // test with std::multiset
  std::multiset<const UndirectedHyperedge*> ms;
  hglib::hyperedges(*subgraph.get(), std::inserter(ms, ms.begin()));
  EXPECT_EQ(ms.size(), subgraph->nbHyperedges());
  for (auto& hyperedge : ms) {
    EXPECT_NE(subgraph->hyperedgeById(hyperedge->id()), nullptr);
  }

  // test with std::unordered_set
  std::unordered_set<const UndirectedHyperedge*> us;
  hglib::hyperedges(*subgraph.get(), std::inserter(us, us.begin()));
  EXPECT_EQ(us.size(), subgraph->nbHyperedges());
  for (auto& hyperedge : us) {
    EXPECT_NE(subgraph->hyperedgeById(hyperedge->id()), nullptr);
  }

  // test with std::unordered_multiset
  std::unordered_multiset<const UndirectedHyperedge*> ums;
  hglib::hyperedges(*subgraph.get(), std::inserter(ums, ums.begin()));
  EXPECT_EQ(ums.size(), subgraph->nbHyperedges());
  for (auto& hyperedge : ums) {
    EXPECT_NE(subgraph->hyperedgeById(hyperedge->id()), nullptr);
  }
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_vertexIterator) {
  hglib::UndirectedHypergraph<> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      whitelistedVertices.insert(vertex->id());
    }
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperedges);

  // test using begin
  int nbExpectedVertices(3);
  int nbObservedVertices(0);
  auto it = subgraph->verticesBegin();
  for (; it != subgraph->verticesEnd(); ++it) {
    ASSERT_EQ(((*it)->id() % 2), 0);
    ++nbObservedVertices;
  }
  ASSERT_EQ(nbExpectedVertices, nbObservedVertices);

  // test range based for
  nbObservedVertices = 0;
  for (const auto& vertex : subgraph->vertices()) {
    ASSERT_EQ((vertex->id() % 2), 0);
    ++nbObservedVertices;
  }
  ASSERT_EQ(nbExpectedVertices, nbObservedVertices);

  // test begin/end-pair
  nbObservedVertices = 0;
  hglib::UndirectedSubHypergraph<>::vertex_iterator it2, end;
  std::tie(it2, end) = subgraph->verticesBeginEnd();
  for (; it2 != end; ++it2) {
    ASSERT_EQ(((*it2)->id() % 2), 0);
    ++nbObservedVertices;
  }
  ASSERT_EQ(nbExpectedVertices, nbObservedVertices);

  // test iterators after the removal of vertices
  subgraph->removeVertex(0);
  subgraph->removeVertex(4);

  // test using begin
  nbExpectedVertices = 1;
  nbObservedVertices = 0;
  it = subgraph->verticesBegin();
  for (; it != subgraph->verticesEnd(); ++it) {
    ASSERT_EQ(((*it)->id() % 2), 0);
    ++nbObservedVertices;
  }
  ASSERT_EQ(nbExpectedVertices, nbObservedVertices);

  // test range based for loop
  nbObservedVertices = 0;
  for (const auto& vertex : subgraph->vertices()) {
    ASSERT_EQ((vertex->id() % 2), 0);
    ++nbObservedVertices;
  }
  ASSERT_EQ(nbExpectedVertices, nbObservedVertices);

  // test begin/end-pair
  nbObservedVertices = 0;
  std::tie(it2, end) = subgraph->verticesBeginEnd();
  for (; it2 != end; ++it2) {
    ASSERT_EQ(((*it2)->id() % 2), 0);
    ++nbObservedVertices;
  }
  ASSERT_EQ(nbExpectedVertices, nbObservedVertices);
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_getVertexProperties) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct EdgeProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
          VertexProperty, EdgeProperty> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      whitelistedVertices.insert(vertex->id());
    }
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);
  for (const auto& vertex : subgraph->vertices()) {
    auto vProp = subgraph->getVertexProperties_(vertex->id());
    ASSERT_EQ(vProp->name.compare("Default"), 0);
    ASSERT_EQ(vProp->colour, Colour::green);
    vProp->name = vertex->name() + "_" + std::to_string(vertex->id());
    vProp->colour = Colour::red;
  }
  // Verify that updated properties are stored in the hypergraph
  for (const auto& vertex : g.vertices()) {
    const auto constVProp = g.getVertexProperties(vertex->id());
    string expected = "Default";
    ASSERT_EQ(constVProp->name.compare(expected), 0);
    ASSERT_EQ(constVProp->colour, Colour::green);
  }

  hglib::VertexIdType outOfRange = 10;
  ASSERT_THROW(subgraph->getVertexProperties_(outOfRange),
          std::out_of_range);
  ASSERT_THROW(subgraph->getVertexProperties(outOfRange),
          std::out_of_range);
  outOfRange = -10;
  ASSERT_THROW(subgraph->getVertexProperties_(outOfRange),
          std::out_of_range);
  ASSERT_THROW(subgraph->getVertexProperties(outOfRange),
          std::out_of_range);

  // without VertexProperty (empty struct)
  hglib::UndirectedHypergraph<> g2;
  whitelistedVertices.clear();
  for (auto vertexName : vertices) {
    const auto& vertex = g2.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      whitelistedVertices.insert(vertex->id());
    }
  }
  auto subgraph2 = hglib::createUndirectedSubHypergraph(&g2,
                                                        whitelistedVertices,
                                                        whitelistedHyperedges);

  for (const auto& vertex :  subgraph2->vertices()) {
    auto vProp = subgraph2->getVertexProperties(vertex->id());
    EXPECT_EQ(typeid(hglib::emptyProperty), typeid(*vProp));
    EXPECT_NE(typeid(VertexProperty), typeid(*vProp));
  }
}

TEST_F(TestUndirectedSubHypergraph,
       Test_subgraph_verticesWithPropertyContainer) {
  enum Colour {
      red, blue, green, black
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct EdgeProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
          VertexProperty, EdgeProperty> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
     whitelistedVertices.insert(vertex->id());
    }
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);

  // Get all (green) vertices of the subgraph
  std::vector<const UndirectedVertex<UndirectedHyperedge>*> verticesContainer;
  hglib::vertices(*subgraph.get(), std::back_inserter(verticesContainer),
                  [&] (hglib::VertexIdType vertexId) -> bool {
                      return subgraph->getVertexProperties(vertexId)->
                              colour == Colour::green;});
  // All vertices are green by default
  ASSERT_EQ(verticesContainer.size(), subgraph->nbVertices());
  for (auto& vertex : verticesContainer) {
    ASSERT_NE(subgraph->vertexById(vertex->id()), nullptr);
  }

  // Change colour of some vertices
  // set colour of vertex 0 to red
  subgraph->getVertexProperties_(0)->colour = Colour::red;
  // set colour of vertex 2 to blue
  subgraph->getVertexProperties_(2)->colour = Colour::blue;
  // Get vertices by colour
  for ( int idx = red; idx != black; idx++ ) {
    verticesContainer.clear();  // empty container
    // Get vertices of specified colour
    Colour colour = static_cast<Colour>(idx);
    hglib::vertices(*subgraph.get(), std::back_inserter(verticesContainer),
                    [&] (hglib::VertexIdType vertexId) -> bool {
                        return subgraph->getVertexProperties(vertexId)->
                                colour == colour;});
    switch (colour) {
      case red:
        ASSERT_EQ(verticesContainer.size(), (size_t) 1);
        ASSERT_EQ(verticesContainer[0]->id(), 0);
       break;
      case blue:
        ASSERT_EQ(verticesContainer.size(), (size_t) 1);
        ASSERT_EQ(verticesContainer[0]->id(), 2);
        break;
      case green:
        ASSERT_EQ(verticesContainer.size(), (size_t) 1);
        ASSERT_EQ(verticesContainer[0]->id(), 4);
        break;
      case black:
        ASSERT_TRUE(verticesContainer.empty());
      }
  }
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_GetHypergraphProperties) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct EdgeProperty {
      std::string name = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      std::string name = "I am a hypergraph";
      Colour colour = Colour::red;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
          VertexProperty, EdgeProperty, HypergraphProperty> g;
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, {}, {});

  // Check default arguments in subhypergraph
  ASSERT_EQ(subgraph->getHypergraphProperties()->name.compare(
          "I am a hypergraph"), 0);
  ASSERT_EQ(subgraph->getHypergraphProperties()->colour, Colour::red);
  // change subhypergraph properties
  subgraph->getHypergraphProperties_()->name = "I am a sub-hypergraph";
  subgraph->getHypergraphProperties_()->colour = Colour::blue;
  // check changed properties in subgraph but not in parent graph
  ASSERT_EQ(g.getHypergraphProperties()->name.compare("I am a hypergraph"), 0);
  ASSERT_EQ(g.getHypergraphProperties()->colour, Colour::red);
  ASSERT_EQ(subgraph->getHypergraphProperties()->name.compare(
  "I am a sub-hypergraph"), 0);
  ASSERT_EQ(subgraph->getHypergraphProperties()->colour, Colour::blue);

  // without HypergraphProperty (empty struct)
  hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
          VertexProperty, EdgeProperty> g2;
  auto subgraph2 = hglib::createUndirectedSubHypergraph(&g2, {}, {});
  EXPECT_EQ(typeid(hglib::emptyProperty),
  typeid(*g2.getHypergraphProperties()));
  EXPECT_NE(typeid(HypergraphProperty),
  typeid(*g2.getHypergraphProperties()));
  EXPECT_EQ(typeid(hglib::emptyProperty),
  typeid(*subgraph2->getHypergraphProperties()));
  EXPECT_NE(typeid(HypergraphProperty),
  typeid(*subgraph2->getHypergraphProperties()));
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_hyperedgeById) {
  hglib::UndirectedHypergraph<> g;
  // insert vertices
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  // add hyperedges
  for (auto hyperedge : hyperedges) {
    const auto& edge = g.addHyperedge(hglib::NAME, hyperedge.first);
    if ((edge->id() % 2) != 0) {
      whitelistedHyperedges.insert(edge->id());
    }
  }
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);

  for (const auto& hyperedge : g.hyperedges()) {
    if ((hyperedge->id() % 2) != 0) {
      ASSERT_EQ(subgraph->hyperedgeById(hyperedge->id()), hyperedge);
    } else {
      ASSERT_EQ(subgraph->hyperedgeById(hyperedge->id()), nullptr);
    }
  }
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_hyperedgeIterator) {
  hglib::UndirectedHypergraph<> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  // add hyperedges
  for (auto hyperedge : hyperedges) {
    const auto& edge = g.addHyperedge(hglib::NAME, hyperedge.first);
    if ((edge->id() % 2) == 0) {
      whitelistedHyperedges.insert(edge->id());
    }
  }
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperedges);

  // test using begin
  int nbExpectedHyperedges(2);
  int nbObservedHyperedges(0);
  auto it = subgraph->hyperedgesBegin();
  for (; it != subgraph->hyperedgesEnd(); ++it) {
    ASSERT_EQ(((*it)->id() % 2), 0);
    ++nbObservedHyperedges;
  }
  ASSERT_EQ(nbExpectedHyperedges, nbObservedHyperedges);

  // test range based for loop
  nbObservedHyperedges = 0;
  for (const auto& hyperedge : subgraph->hyperedges()) {
    ASSERT_EQ((hyperedge->id() % 2), 0);
    ++nbObservedHyperedges;
  }
  ASSERT_EQ(nbExpectedHyperedges, nbObservedHyperedges);

  // test begin/end-pair
  nbObservedHyperedges = 0;
  hglib::UndirectedSubHypergraph<>::hyperedge_iterator it2, end;
  std::tie(it2, end) = subgraph->hyperedgesBeginEnd();
  for (; it2 != end; ++it2) {
    ASSERT_EQ(((*it2)->id() % 2), 0);
    ++nbObservedHyperedges;
  }
  ASSERT_EQ(nbExpectedHyperedges, nbObservedHyperedges);


  // test iterators after the removal of vertices
  subgraph->removeHyperedge(0);

  // test using begin
  nbExpectedHyperedges = 1;
  nbObservedHyperedges = 0;
  it = subgraph->hyperedgesBegin();
  for (; it != subgraph->hyperedgesEnd(); ++it) {
    ASSERT_EQ(((*it)->id() % 2), 0);
    ++nbObservedHyperedges;
  }
  ASSERT_EQ(nbExpectedHyperedges, nbObservedHyperedges);

  // test range based for loop
  nbObservedHyperedges = 0;
  for (const auto& hyperedge : subgraph->hyperedges()) {
    ASSERT_EQ((hyperedge->id() % 2), 0);
    ++nbObservedHyperedges;
  }
  ASSERT_EQ(nbExpectedHyperedges, nbObservedHyperedges);

  // test begin/end-pair
  nbObservedHyperedges = 0;
  std::tie(it2, end) = subgraph->hyperedgesBeginEnd();
  for (; it2 != end; ++it2) {
    ASSERT_EQ(((*it2)->id() % 2), 0);
    ++nbObservedHyperedges;
  }
  ASSERT_EQ(nbExpectedHyperedges, nbObservedHyperedges);
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_getHyperedgeProperties) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct UndirectedHyperedgeProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge,
          VertexProperty, UndirectedHyperedgeProperty> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  // add hyperedges
  for (auto hyperedge : hyperedges) {
    const auto& edge = g.addHyperedge(hglib::NAME, hyperedge.first,
                                    hyperedge.second);
    if ((edge->id() % 2) != 0) {
      whitelistedHyperedges.insert(edge->id());
    }
  }
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);
  for (const auto& hyperedge : subgraph->hyperedges()) {
    auto edgeProp = subgraph->getHyperedgeProperties_(hyperedge->id());
    ASSERT_EQ(edgeProp->name.compare("Default"), 0);
    ASSERT_EQ(edgeProp->weight, 0.0);
    edgeProp->name = hyperedge->name() + "_" + std::to_string(hyperedge->id());
    edgeProp->weight = 1.0;
  }
  // Verify that updated properties are stored in the hypergraph
  for (const auto& hyperedge : g.hyperedges()) {
    const auto constArcProp = g.getHyperedgeProperties(hyperedge->id());
    string expected = "Default";
    ASSERT_EQ(constArcProp->name.compare(expected), 0);
    ASSERT_EQ(constArcProp->weight, 0.0);
  }

  hglib::HyperedgeIdType outOfRange = 10;
  ASSERT_THROW(subgraph->getHyperedgeProperties_(outOfRange),
          std::out_of_range);
  ASSERT_THROW(subgraph->getHyperedgeProperties(outOfRange),
          std::out_of_range);
  outOfRange = -10;
  ASSERT_THROW(subgraph->getHyperedgeProperties_(outOfRange),
          std::out_of_range);
  ASSERT_THROW(subgraph->getHyperedgeProperties(outOfRange),
          std::out_of_range);

  // without UndirectedHyperedgeProperty (empty struct)
  hglib::UndirectedHypergraph<> g2;
  for (auto vertexName : vertices) {
    g2.addVertex(vertexName);
  }
  for (auto hyperedge : hyperedges) {
    g2.addHyperedge(hglib::NAME, hyperedge.first);
  }
  auto subgraph2 = hglib::createUndirectedSubHypergraph(&g2,
                                                        whitelistedVertices,
                                                        whitelistedHyperedges);

  for (const auto& hyperedge :  subgraph2->hyperedges()) {
    auto edgeProp = subgraph2->getHyperedgeProperties(hyperedge->id());
    EXPECT_EQ(typeid(hglib::emptyProperty), typeid(*edgeProp));
    EXPECT_NE(typeid(UndirectedHyperedgeProperty), typeid(*edgeProp));
  }
}

TEST_F(TestUndirectedSubHypergraph,
       Test_subgraph_hyperedgesWithPropertyContainer) {
  enum Colour {
      red, blue, green, black
  };
  struct VertexProperty {
      string name = "Default";
  };
  struct EdgeProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, NamedUndirectedHyperedge,
          VertexProperty, EdgeProperty> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  for (auto edge : hyperedges) {
    const auto& hyperedge = g.addHyperedge(hglib::NAME, edge.first,
                                           edge.second);
    if ((hyperedge->id() % 2) == 0) {
      whitelistedHyperedges.insert(hyperedge->id());
    }
  }

  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);

  // Get all (green) vertices
  std::vector<const NamedUndirectedHyperedge*> hyperedgesContainer;
  hglib::hyperedges(*subgraph.get(), std::back_inserter(hyperedgesContainer),
                    [&] (hglib::HyperedgeIdType hyperedgeId) -> bool {
                        return subgraph->getHyperedgeProperties(hyperedgeId)->
                                colour == Colour::green;});
  // All vertices are green by default
  ASSERT_EQ(hyperedgesContainer.size(), subgraph->nbHyperedges());
  for (auto& hyperedge : hyperedgesContainer) {
    EXPECT_NE(subgraph->hyperedgeById(hyperedge->id()), nullptr);
  }
  // set colour of hyperedge 'edge1' to red
  subgraph->getHyperedgeProperties_(0)->colour = Colour::red;
  // set colour of hyperedge 'edge3' to blue
  subgraph->getHyperedgeProperties_(2)->colour = Colour::blue;
  // Get hyperedges by colour
  for ( int idx = red; idx != black; idx++ ) {
    hyperedgesContainer.clear();  // empty container
    // Get vertices of specified colour
    Colour colour = static_cast<Colour>(idx);
    hglib::hyperedges(*subgraph.get(), std::back_inserter(hyperedgesContainer),
                      [&] (hglib::HyperedgeIdType edgeId) -> bool {
                          return subgraph->getHyperedgeProperties(edgeId)->
                                  colour == colour;});
    switch (colour) {
      case red:
        ASSERT_EQ(hyperedgesContainer.size(), (size_t) 1);
        ASSERT_EQ(hyperedgesContainer[0]->name().compare("edge1"), 0);
        break;
      case blue:
        ASSERT_EQ(hyperedgesContainer.size(), (size_t) 1);
        ASSERT_EQ(hyperedgesContainer[0]->name().compare("edge3"), 0);
        break;
      case green:
        ASSERT_TRUE(hyperedgesContainer.empty());
      break;
      case black:
        ASSERT_TRUE(hyperedgesContainer.empty());
      break;
    }
  }
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_hyperedgeContainsVertex) {
  hglib::UndirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  // add hyperedges
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first);
  }
  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {0, 2};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {0};
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);
  ASSERT_TRUE(subgraph->isVertexOfHyperedge(0, 0));
  ASSERT_TRUE(subgraph->isVertexOfHyperedge(2, 0));

  whitelistedVertices = {0};
  subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                whitelistedHyperedges);
  ASSERT_TRUE(subgraph->isVertexOfHyperedge(0, 0));

  whitelistedVertices = {2};
  subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                whitelistedHyperedges);
  ASSERT_THROW(subgraph->isVertexOfHyperedge(0, 0), std::invalid_argument);
  ASSERT_TRUE(subgraph->isVertexOfHyperedge(2, 0));
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_Vertices_of_hyperedge) {
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges;
  for (auto hyperedge : hyperedges) {
    const auto &edge = g.addHyperedge(hglib::NAME, hyperedge.first,
                                    hyperedge.second);
    whitelistedHyperedges.insert(edge->id());
  }

  // subgraph eq parent graph
  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);

  typedef hglib::UndirectedSubHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> subHypergraph_t;
  subHypergraph_t::vertex_iterator it, end;
  hglib::HyperedgeIdType hyperedgeId(0);
  for (auto hyperedge : hyperedges) {
    ASSERT_EQ(subgraph->nbImpliedVertices(hyperedgeId),
            hyperedge.first.size());
    ASSERT_TRUE(subgraph->hasVertices(hyperedgeId));
    // using begin
    auto it2 = subgraph->impliedVerticesBegin(hyperedgeId);
    // using begin/end-pair
    std::tie(it, end) = subgraph->impliedVerticesBeginEnd(hyperedgeId);
    // vertex names iterator
    auto itVertexNames = hyperedge.first.begin();
    // range based for
    auto impliedVerticesPtr = subgraph->impliedVertices(hyperedgeId);
    for (const auto& vertex : *impliedVerticesPtr) {
      ASSERT_EQ(vertex->name().compare(*itVertexNames), 0);
      ASSERT_EQ((*it)->name().compare(*itVertexNames), 0);
      ASSERT_EQ((*it2)->name().compare(*itVertexNames), 0);
      ++itVertexNames;
      ++it;
      ++it2;
    }
    ++hyperedgeId;
  }

  // test exception for given hyperedge id that was never assigned
  // to this hypergraph
  ASSERT_THROW(subgraph->nbImpliedVertices(hyperedgeId), std::out_of_range);
  ASSERT_THROW(subgraph->hasVertices(hyperedgeId), std::out_of_range);
  ASSERT_THROW(subgraph->impliedVerticesBegin(hyperedgeId), std::out_of_range);
  ASSERT_THROW(subgraph->impliedVerticesEnd(hyperedgeId), std::out_of_range);
  ASSERT_THROW(subgraph->impliedVerticesBeginEnd(hyperedgeId),
               std::out_of_range);
  ASSERT_THROW(subgraph->impliedVertices(hyperedgeId), std::out_of_range);

  // test exception after removal of a hyperedge
  hglib::HyperedgeIdType removedUndirectedHyperedgeId(1);
  subgraph->removeHyperedge(removedUndirectedHyperedgeId);
  ASSERT_THROW(subgraph->nbImpliedVertices(removedUndirectedHyperedgeId),
          std::invalid_argument);
  ASSERT_THROW(subgraph->hasVertices(removedUndirectedHyperedgeId),
          std::invalid_argument);
  ASSERT_THROW(subgraph->impliedVerticesBegin(removedUndirectedHyperedgeId),
               std::invalid_argument);
  ASSERT_THROW(subgraph->impliedVerticesEnd(removedUndirectedHyperedgeId),
               std::invalid_argument);
  ASSERT_THROW(subgraph->impliedVerticesBeginEnd(removedUndirectedHyperedgeId),
               std::invalid_argument);
  ASSERT_THROW(subgraph->impliedVertices(removedUndirectedHyperedgeId),
               std::invalid_argument);

  // re-init subgraph
  subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                whitelistedHyperedges);

  // remove one vertex vertex after the other and check tails
  subgraph->removeVertex(0, false);  // "A"
  // "edge1"
  ASSERT_NE(subgraph->impliedVerticesBegin(0),
            subgraph->impliedVerticesEnd(0));
  // using begin/end-pair
  std::tie(it, end) = subgraph->impliedVerticesBeginEnd(0);
  ASSERT_NE(it, end);
  ASSERT_EQ(it, subgraph->impliedVerticesBegin(0));
  ASSERT_EQ(end, subgraph->impliedVerticesEnd(0));
  ASSERT_TRUE(subgraph->hasVertices(0));
  ASSERT_EQ(subgraph->nbImpliedVertices(0), (size_t) 1);

  // "edge2"
  ASSERT_TRUE(subgraph->hasVertices(1));
  ASSERT_EQ(subgraph->nbImpliedVertices(1), (size_t) 3);
  subgraph->removeVertex(1, false);  // "B"
  ASSERT_NE(subgraph->impliedVerticesBegin(1),
            subgraph->impliedVerticesEnd(1));  // "edge2"
  // using begin/end-pair
  std::tie(it, end) = subgraph->impliedVerticesBeginEnd(1);
  ASSERT_EQ(it, subgraph->impliedVerticesBegin(1));
  ASSERT_EQ(end, subgraph->impliedVerticesEnd(1));
  ASSERT_TRUE(subgraph->hasVertices(1));
  ASSERT_EQ(subgraph->nbImpliedVertices(1), (size_t) 2);

  // Remove another vertex
  subgraph->removeVertex(5, false);  // "6"
  ASSERT_NE(subgraph->impliedVerticesBegin(2),
            subgraph->impliedVerticesEnd(2));  // "edge3"
  // using begin/end-pair
  std::tie(it, end) = subgraph->impliedVerticesBeginEnd(2);
  ASSERT_EQ(it, subgraph->impliedVerticesBegin(2));
  ASSERT_EQ(end, subgraph->impliedVerticesEnd(2));
  ASSERT_TRUE(subgraph->hasVertices(2));
  ASSERT_EQ(subgraph->nbImpliedVertices(2), (size_t) 1);

  // Remove another vertex
  subgraph->removeVertex(4, false);  // ""
  // "edge3" is removed as vertices "" and "6" are removed
  ASSERT_THROW(subgraph->impliedVerticesBegin(2), std::invalid_argument);
  ASSERT_THROW(subgraph->impliedVerticesEnd(2), std::invalid_argument);
  ASSERT_THROW(subgraph->impliedVerticesBeginEnd(2), std::invalid_argument);
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_hyperedges_of_vertex) {
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges;
  for (auto hyperedge : hyperedges) {
    const auto& edge = g.addHyperedge(hglib::NAME, hyperedge.first,
                                    hyperedge.second);
    if ((edge->id() % 2) == 0) {
      whitelistedHyperedges.insert(edge->id());
    }
  }

  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);

  typedef hglib::UndirectedSubHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> subHypergraph_t;
  subHypergraph_t::hyperedge_iterator it, end;
  for (const auto& vertex : subgraph->vertices()) {
    const auto vertexId = vertex->id();
    if (vertex->name().compare("A") == 0 or
            vertex->name().compare("6") == 0 or
            vertex->name().compare("Foo") == 0 or
            vertex->name().compare("") == 0) {
      // associated hyperedges
      ASSERT_EQ(subgraph->nbHyperedges(vertexId), (size_t) 1);
      ASSERT_TRUE(subgraph->isContainedInAnyHyperedge(vertexId));
      ASSERT_NE(subgraph->hyperedgesBegin(vertexId),
                subgraph->hyperedgesEnd(vertexId));
      int nbExpectedHyperedges(1);
      int nbObservedHyperedges(0);
      // using begin
      auto it2 = subgraph->hyperedgesBegin(vertexId);
      // using begin/end-pair
      std::tie(it, end) = subgraph->hyperedgesBeginEnd(vertexId);
      nbObservedHyperedges = 0;
      // range based for
      auto hyperedgesPtr = subgraph->hyperedges(vertexId);
      for (const auto& hyperedge : *hyperedgesPtr) {
        ASSERT_EQ((hyperedge->id() % 2), 0);
        ASSERT_EQ(((*it)->id() % 2), 0);
        ASSERT_EQ(((*it2)->id() % 2), 0);
        ++nbObservedHyperedges;
        ++it;
        ++it2;
      }
      ASSERT_EQ(nbExpectedHyperedges, nbObservedHyperedges);
    } else if (vertex->name().compare("B") == 0 or
      vertex->name().compare("Bar") == 0) {
      // hyperedges
      ASSERT_EQ(subgraph->nbHyperedges(vertexId), (size_t) 0);
      ASSERT_FALSE(subgraph->isContainedInAnyHyperedge(vertexId));
      ASSERT_EQ(subgraph->hyperedgesBegin(vertexId),
                subgraph->hyperedgesEnd(vertexId));

      int nbExpectedHyperedges(0);
      int nbObservedHyperedges(0);
      // using begin
      auto it2 = subgraph->hyperedgesBegin(vertexId);
      // using begin/end-pair
      std::tie(it, end) = subgraph->hyperedgesBeginEnd(vertexId);
      ASSERT_EQ(it, end);
      // range based for
      auto hyperedgesPtr = subgraph->hyperedges(vertexId);
      for (const auto& hyperedge : *hyperedgesPtr) {
        ASSERT_EQ((hyperedge->id() % 2), 0);
        ASSERT_EQ(((*it)->id() % 2), 0);
        ASSERT_EQ(((*it2)->id() % 2), 0);
        ++nbObservedHyperedges;
        ++it;
        ++it2;
      }
      ASSERT_EQ(nbExpectedHyperedges, nbObservedHyperedges);
    }
  }

  // delete hyperedges from the subgraph
  subgraph->removeHyperedge(0);
  subgraph->removeHyperedge(2);
  for (const auto& vertex : subgraph->vertices()) {
    const auto vertexId = vertex->id();
    // hyperedges
    ASSERT_EQ(subgraph->nbHyperedges(vertexId), (size_t) 0);
    ASSERT_FALSE(subgraph->isContainedInAnyHyperedge(vertexId));
    ASSERT_EQ(subgraph->hyperedgesBegin(vertexId),
              subgraph->hyperedgesEnd(vertexId));
    // using begin
    auto it2 = subgraph->hyperedgesBegin(vertexId);
    // using begin/end-pair
    std::tie(it, end) = subgraph->hyperedgesBeginEnd(vertexId);
    ASSERT_EQ(it, end);
    int nbExpectedHyperedges(0);
    int nbObservedHyperedges(0);
    // range based for
    auto hyperedgesPtr = subgraph->hyperedges(vertexId);
    for (const auto& hyperedge : *hyperedgesPtr) {
      ASSERT_EQ((hyperedge->id() % 2), 0);
      ASSERT_EQ(((*it)->id() % 2), 0);
      ASSERT_EQ(((*it2)->id() % 2), 0);
      ++nbObservedHyperedges;
      ++it;
      ++it2;
    }
    ASSERT_EQ(nbExpectedHyperedges, nbObservedHyperedges);
  }
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_removeVertex) {
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges;
  for (auto hyperedge : hyperedges) {
    const auto& arc = g.addHyperedge(hglib::NAME, hyperedge.first,
                                    hyperedge.second);
    whitelistedHyperedges.insert(arc->id());
  }

  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);

  subgraph->removeVertex("A", false);
  ASSERT_NE(g.vertexByName("A"), nullptr);  // still exists in parent
  ASSERT_TRUE(subgraph->hasVertices(0));  // "edge1"
  ASSERT_EQ(subgraph->nbImpliedVertices(0), (size_t) 1);
  ASSERT_TRUE(subgraph->hasVertices(1));  // "edge2"
  ASSERT_EQ(subgraph->nbImpliedVertices(1), (size_t) 3);

  // will remove hyperedges of vertex "" from the sub-hypergraph
  // only "edge1" remains
  subgraph->removeVertex("");
  ASSERT_NE(g.vertexByName(""), nullptr);  // still exists in parent
  ASSERT_EQ(subgraph->nbHyperedges(), (size_t) 1);
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_removeHyperedge) {
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges;
  for (auto hyperedge : hyperedges) {
    const auto& arc = g.addHyperedge(hglib::NAME, hyperedge.first,
                                    hyperedge.second);
    whitelistedHyperedges.insert(arc->id());
  }

  auto subgraph = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperedges);

  hglib::HyperedgeIdType idx(0);
  size_t nbExpectedUndirectedHyperedges(3);
  for (auto hyperedge : hyperedges) {
    subgraph->removeHyperedge(idx);
    ASSERT_NE(g.hyperedgeById(idx), nullptr);  // still exists in parent
    ASSERT_EQ(subgraph->nbHyperedges(), --nbExpectedUndirectedHyperedges);
    ++idx;
  }
  // all hyperedges are removed only from the subgraph
  for (const auto& vertex : subgraph->vertices()) {
    ASSERT_FALSE(subgraph->isContainedInAnyHyperedge(vertex->id()));
  }
}

TEST_F(TestUndirectedSubHypergraph, Test_getRootHypergraph) {
  hglib::UndirectedHypergraph<> g, g2;
  ASSERT_EQ(g.getRootHypergraph(), &g);
  ASSERT_EQ(g2.getRootHypergraph(), &g2);

  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  auto subgraph = hglib::UndirectedSubHypergraph <>(&g, whitelistedVertices,
                                                 whitelistedHyperedges);
  auto subsubgraph = hglib::UndirectedSubHypergraph <>(&subgraph,
                                                    whitelistedVertices,
                                                    whitelistedHyperedges);
  ASSERT_EQ(subgraph.getRootHypergraph(), &g);
  ASSERT_EQ(subsubgraph.getRootHypergraph(), &g);

  auto subgraph2 = hglib::UndirectedSubHypergraph <>(&g2, whitelistedVertices,
                                                  whitelistedHyperedges);
  ASSERT_EQ(subgraph2.getRootHypergraph(), &g2);
  subgraph2 = subgraph;
  ASSERT_EQ(subgraph2.getRootHypergraph(), &g);
  ASSERT_EQ(subgraph.getRootHypergraph(), &g);
  ASSERT_EQ(subsubgraph.getRootHypergraph(), &g);

  auto subgraph3 = hglib::UndirectedSubHypergraph <>(&g2, whitelistedVertices,
                                                  whitelistedHyperedges);
  ASSERT_EQ(subgraph3.getRootHypergraph(), &g2);

  g = g2;
  ASSERT_EQ(subgraph3.getRootHypergraph(), &g2);
  ASSERT_THROW(subgraph2.getRootHypergraph(), std::invalid_argument);
  ASSERT_THROW(subgraph.getRootHypergraph(), std::invalid_argument);
  ASSERT_THROW(subsubgraph.getRootHypergraph(), std::invalid_argument);
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_resetVertexIds) {
  hglib::UndirectedHypergraph<> g;
  for (hglib::VertexIdType id = 0; id < 5; ++id) {
    g.addVertex();
  }
  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {2, 3};
  auto subgraph = hglib::UndirectedSubHypergraph <>(&g,
                                                    whitelistedVertices, {});
  whitelistedVertices = {3};
  auto subsubgraph = hglib::UndirectedSubHypergraph <>(&subgraph,
                                                    whitelistedVertices, {});
  whitelistedVertices = {1};
  auto subgraph2 = hglib::UndirectedSubHypergraph <>(&g,
                                                     whitelistedVertices, {});

  g.removeVertex(0, false);
  g.removeVertex(4, false);
  g.resetVertexIds();

  for (const auto& vertex : g.vertices()) {
    const auto vertexId = vertex->id();
    switch (vertexId) {
      case 0: {
        ASSERT_EQ(subgraph.vertexById(vertexId), nullptr);
        ASSERT_EQ(subsubgraph.vertexById(vertexId), nullptr);
        ASSERT_EQ(subgraph2.vertexById(vertexId), vertex);
        break;
      }
      case 1: {
        ASSERT_EQ(subgraph.vertexById(vertexId), vertex);
        ASSERT_EQ(subsubgraph.vertexById(vertexId), nullptr);
        ASSERT_EQ(subgraph2.vertexById(vertexId), nullptr);
        break;
      }
      case 2: {
        ASSERT_EQ(subgraph.vertexById(vertexId), vertex);
        ASSERT_EQ(subsubgraph.vertexById(vertexId), vertex);
        ASSERT_EQ(subgraph2.vertexById(vertexId), nullptr);
        break;
      }
    }
  }
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_resetHyperedgeIds) {
  hglib::UndirectedHypergraph<> g;
  // dummy vertices and hyperedges
  for (hglib::VertexIdType id = 0; id < 2; ++id) {
    g.addVertex();
  }
  for (hglib::HyperedgeIdType id = 0; id < 5; ++id) {
    g.addHyperedge({{0, 1}});
  }
  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {0, 1};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {2, 3};
  auto subgraph = hglib::UndirectedSubHypergraph <>(&g, whitelistedVertices,
                                                 whitelistedHyperedges);
  whitelistedHyperedges = {3};
  auto subsubgraph = hglib::UndirectedSubHypergraph <>(&subgraph,
                                                    whitelistedVertices,
                                                    whitelistedHyperedges);
  whitelistedHyperedges = {1};
  auto subgraph2 = hglib::UndirectedSubHypergraph <>(&g, whitelistedVertices,
                                                  whitelistedHyperedges);

  g.removeHyperedge(0);
  g.removeHyperedge(4);
  g.resetHyperedgeIds();

  for (const auto& hyperedge : g.hyperedges()) {
    const auto hyperedgeId = hyperedge->id();
    switch (hyperedgeId) {
      case 0: {
        ASSERT_EQ(subgraph.hyperedgeById(hyperedgeId), nullptr);
        ASSERT_EQ(subsubgraph.hyperedgeById(hyperedgeId), nullptr);
        ASSERT_EQ(subgraph2.hyperedgeById(hyperedgeId), hyperedge);
        break;
      }
      case 1: {
        ASSERT_EQ(subgraph.hyperedgeById(hyperedgeId), hyperedge);
        ASSERT_EQ(subsubgraph.hyperedgeById(hyperedgeId), nullptr);
        ASSERT_EQ(subgraph2.hyperedgeById(hyperedgeId), nullptr);
        break;
      }
      case 2: {
        ASSERT_EQ(subgraph.hyperedgeById(hyperedgeId), hyperedge);
        ASSERT_EQ(subsubgraph.hyperedgeById(hyperedgeId), hyperedge);
        ASSERT_EQ(subgraph2.hyperedgeById(hyperedgeId), nullptr);
        break;
      }
    }
  }
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_vertexProperties) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      std::string label = "Default";
      Colour colour = Colour::green;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
      VertexProperty> g;
  // Change the colour of every second vertex
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      g.getVertexProperties_(vertex->id())->colour = Colour::red;
    }
  }
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first);
  }

  // Create (empty) sub-hypergraphs
  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  auto sg = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                 whitelistedHyperedges);

  auto ssg = hglib::createUndirectedSubHypergraph(sg.get(), whitelistedVertices,
                                                  whitelistedHyperedges);

  // Add vertices to subgraph(s)
  for (const auto& vertex : g.vertices()) {
    if ((vertex->id() % 2) == 0) {
      sg->addVertex(vertex->id());
    } else if ((vertex->id() % 3) == 0) {
      ssg->addVertex(vertex->id());
    }
  }

  // Set all vertex colour in the root graph to blue
  for (const auto& vertex : g.vertices()) {
    g.getVertexProperties_(vertex->id())->colour = Colour::blue;
  }

  // Check colour in subgraphs
  for (const auto& vertex : sg->vertices()) {
    if ((vertex->id() % 2) == 0) {
      ASSERT_EQ(sg->getVertexProperties(vertex->id())->colour, Colour::red);
    } else {
      ASSERT_EQ(sg->getVertexProperties(vertex->id())->colour, Colour::green);
    }
  }
  for (const auto& vertex : ssg->vertices()) {
    if ((vertex->id() % 2) == 0) {
      ASSERT_EQ(ssg->getVertexProperties(vertex->id())->colour, Colour::red);
    } else {
      ASSERT_EQ(ssg->getVertexProperties(vertex->id())->colour, Colour::green);
    }
  }

  // Add other vertices to root graph
  const auto& vertex1 = g.addVertex("Vertex1");
  g.getVertexProperties_(vertex1->id())->colour = Colour::blue;
  const auto& lastVertex = g.addVertex("Vertex2");
  // Add the second one to both subgraphs
  sg->addVertex(lastVertex->id());
  // ... and check properties
  // property is copied from parent
  ASSERT_EQ(sg->getVertexProperties(lastVertex->id())->colour, Colour::green);
  // Set to red in subgraph
  sg->getVertexProperties_(lastVertex->id())->colour = Colour::red;
  ASSERT_EQ(sg->getVertexProperties(lastVertex->id())->colour, Colour::red);
  // Changed only in subgraph
  ASSERT_EQ(g.getVertexProperties(lastVertex->id())->colour, Colour::green);
  // Copy property from subgraph
  ssg->addVertex(lastVertex->id());
  ASSERT_EQ(ssg->getVertexProperties(lastVertex->id())->colour, Colour::red);

  // Test exceptions
  ASSERT_THROW(sg->getVertexProperties(1), std::invalid_argument);
  ASSERT_THROW(sg->getVertexProperties_(1), std::invalid_argument);
  ASSERT_THROW(ssg->getVertexProperties(1), std::invalid_argument);
  ASSERT_THROW(ssg->getVertexProperties_(1), std::invalid_argument);

  ASSERT_THROW(sg->getVertexProperties(100), std::out_of_range);
  ASSERT_THROW(sg->getVertexProperties_(100), std::out_of_range);
  ASSERT_THROW(ssg->getVertexProperties(100), std::out_of_range);
  ASSERT_THROW(ssg->getVertexProperties_(100), std::out_of_range);

  // remove vertices
  ASSERT_NO_THROW(sg->getVertexProperties(3));
  ASSERT_NO_THROW(sg->getVertexProperties_(3));
  ASSERT_NO_THROW(ssg->getVertexProperties(3));
  ASSERT_NO_THROW(ssg->getVertexProperties_(3));
  g.removeVertex(3, false);
  ASSERT_THROW(sg->getVertexProperties(3), std::invalid_argument);
  ASSERT_THROW(sg->getVertexProperties_(3), std::invalid_argument);
  ASSERT_THROW(ssg->getVertexProperties(3), std::invalid_argument);
  ASSERT_THROW(ssg->getVertexProperties_(3), std::invalid_argument);

  // reset vertex ids
  g.removeVertex(0, false);
  // change colours of vertices in subgraphs
  sg->getVertexProperties_(2)->colour = Colour::red;
  sg->getVertexProperties_(4)->colour = Colour::blue;
  sg->getVertexProperties_(7)->colour = Colour::green;
  ssg->getVertexProperties_(7)->colour = Colour::red;
  g.resetVertexIds();
  // Check vertex colours in subgraphs
  int idx = 0;
  for (const auto& vertex : sg->vertices()) {
    // red = 0, blue = 1, green = 2
    ASSERT_EQ(sg->getVertexProperties(vertex->id())->colour, idx);
    ++idx;
  }
  // Vertex with Id=7 became ID=5
  ASSERT_EQ(ssg->getVertexProperties(5)->colour, Colour::red);
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_hyperedgeProperties) {
  enum Colour {red, blue, green};
  struct HyperedgeProperty {
      Colour colour = Colour::green;
  };

  hglib::UndirectedHypergraph<UndirectedVertex, UndirectedHyperedge,
      hglib::emptyProperty, HyperedgeProperty> g;
  for (hglib::VertexIdType id = 0; id < 2; ++id) {
    g.addVertex();
  }
  for (hglib::HyperedgeIdType id = 0; id < 6; ++id) {
    const auto& edge = g.addHyperedge({{0, 1}});
    // Change colour of every second hyperedge
    if ((edge->id() % 2) == 0) {
      g.getHyperedgeProperties_(edge->id())->colour = Colour::red;
    }
  }

  // Create (empty) sub-hypergraphs
  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {0, 1};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {};
  auto sg = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                 whitelistedHyperedges);

  auto ssg = hglib::createUndirectedSubHypergraph(sg.get(), whitelistedVertices,
                                                  whitelistedHyperedges);

  // Add hyperedges to subgraph(s)
  for (const auto& edge : g.hyperedges()) {
    if ((edge->id() % 2) == 0) {
      sg->addHyperedge(edge->id());
    } else if ((edge->id() % 3) == 0) {
      ssg->addHyperedge(edge->id());
    }
  }

  // Set all hyperedge colour in the root graph to blue
  for (const auto& edge : g.hyperedges()) {
    g.getHyperedgeProperties_(edge->id())->colour = Colour::blue;
  }

  // Check colour in subgraphs
  for (const auto& edge : sg->hyperedges()) {
    if ((edge->id() % 2) == 0) {
      ASSERT_EQ(sg->getHyperedgeProperties(edge->id())->colour, Colour::red);
    } else {
      ASSERT_EQ(sg->getHyperedgeProperties(edge->id())->colour, Colour::green);
    }
  }
  for (const auto& edge : ssg->hyperedges()) {
    if ((edge->id() % 2) == 0) {
      ASSERT_EQ(ssg->getHyperedgeProperties(edge->id())->colour, Colour::red);
    } else {
      ASSERT_EQ(ssg->getHyperedgeProperties(edge->id())->colour,
                Colour::green);
    }
  }

  // Add other hyperedges to root graph
  const auto& edge1 = g.addHyperedge({{0, 1}});
  g.getHyperedgeProperties_(edge1->id())->colour = Colour::blue;
  const auto& lastHyperedge = g.addHyperedge({{0, 1}});
  // Add the second one to both subgraphs
  sg->addHyperedge(lastHyperedge->id());
  // ... and check properties
  // property is copied from parent
  ASSERT_EQ(sg->getHyperedgeProperties(lastHyperedge->id())->colour,
            Colour::green);
  // Set to red in subgraph
  sg->getHyperedgeProperties_(lastHyperedge->id())->colour = Colour::red;
  ASSERT_EQ(sg->getHyperedgeProperties(lastHyperedge->id())->colour,
            Colour::red);
  // Changed only in subgraph
  ASSERT_EQ(g.getHyperedgeProperties(lastHyperedge->id())->colour,
            Colour::green);
  // Copy property from subgraph
  ssg->addHyperedge(lastHyperedge->id());
  ASSERT_EQ(ssg->getHyperedgeProperties(lastHyperedge->id())->colour,
            Colour::red);

  // Test exceptions
  ASSERT_THROW(sg->getHyperedgeProperties(1), std::invalid_argument);
  ASSERT_THROW(sg->getHyperedgeProperties_(1), std::invalid_argument);
  ASSERT_THROW(ssg->getHyperedgeProperties(1), std::invalid_argument);
  ASSERT_THROW(ssg->getHyperedgeProperties_(1), std::invalid_argument);

  ASSERT_THROW(sg->getHyperedgeProperties(100), std::out_of_range);
  ASSERT_THROW(sg->getHyperedgeProperties_(100), std::out_of_range);
  ASSERT_THROW(ssg->getHyperedgeProperties(100), std::out_of_range);
  ASSERT_THROW(ssg->getHyperedgeProperties_(100), std::out_of_range);

  // remove hyperedges
  ASSERT_NO_THROW(sg->getHyperedgeProperties(3));
  ASSERT_NO_THROW(sg->getHyperedgeProperties_(3));
  ASSERT_NO_THROW(ssg->getHyperedgeProperties(3));
  ASSERT_NO_THROW(ssg->getHyperedgeProperties_(3));
  g.removeHyperedge(3);
  ASSERT_THROW(sg->getHyperedgeProperties(3), std::invalid_argument);
  ASSERT_THROW(sg->getHyperedgeProperties_(3), std::invalid_argument);
  ASSERT_THROW(ssg->getHyperedgeProperties(3), std::invalid_argument);
  ASSERT_THROW(ssg->getHyperedgeProperties_(3), std::invalid_argument);

  // reset hyperedge ids
  g.removeHyperedge(0);
  // change colours of hyperedges in subgraphs
  sg->getHyperedgeProperties_(2)->colour = Colour::red;
  sg->getHyperedgeProperties_(4)->colour = Colour::blue;
  sg->getHyperedgeProperties_(7)->colour = Colour::green;
  ssg->getHyperedgeProperties_(7)->colour = Colour::red;
  g.resetHyperedgeIds();
  // Check hyperedge colours in subgraphs
  int idx = 0;
  for (const auto& edge : sg->hyperedges()) {
    // red = 0, blue = 1, green = 2
    ASSERT_EQ(sg->getHyperedgeProperties(edge->id())->colour, idx);
    ++idx;
  }
  // Hyperedge with Id=7 became ID=5
  ASSERT_EQ(ssg->getHyperedgeProperties(5)->colour, Colour::red);
}

TEST_F(TestUndirectedSubHypergraph, Test_subgraph_print) {
// TODO(mw): Print only those tails/heads of a hyperedge in a subgraph
// that are visible in the subgraph
}


TEST_F(TestUndirectedSubHypergraph, Test_observer) {
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }

  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {0, 1, 2, 3, 5};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {0, 1, 2};
  auto subgraph = hglib::UndirectedSubHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge>(
          &g, whitelistedVertices, whitelistedHyperedges);
  auto subgraph2 = hglib::UndirectedSubHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge>(
          &g, whitelistedVertices, whitelistedHyperedges);
  whitelistedVertices = {0, 1, 2};
  whitelistedHyperedges = {0, 1};
  auto subsubgraph = hglib::UndirectedSubHypergraph <UndirectedVertex,
          NamedUndirectedHyperedge>(
          &subgraph, whitelistedVertices, whitelistedHyperedges);
  auto subsubgraph2 = hglib::UndirectedSubHypergraph <UndirectedVertex,
          NamedUndirectedHyperedge>(
          &subgraph2, whitelistedVertices, whitelistedHyperedges);

  size_t nbVertices(g.nbVertices());
  size_t nbHyperedges(g.nbHyperedges());
  size_t nbVerticesSubgraph(subgraph.nbVertices());
  size_t nbHyperedgesSubgraph(subgraph.nbHyperedges());
  size_t nbVerticesSubgraph2(subgraph2.nbVertices());
  size_t nbHyperedgesSubgraph2(subgraph2.nbHyperedges());
  size_t nbVerticesSubSubgraph(subsubgraph.nbVertices());
  size_t nbHyperedgesSubSubgraph(subsubgraph.nbHyperedges());
  size_t nbVerticesSubSubgraph2(subsubgraph2.nbVertices());
  size_t nbHyperedgesSubSubgraph2(subsubgraph2.nbHyperedges());

  // Nothing happens to the (grand-)parent graphs on removal of a
  // vertex/hyperedge from a graph
  subsubgraph.removeVertex(0, false);
  subsubgraph.removeHyperedge(0);
  ASSERT_EQ(nbVertices, g.nbVertices());
  ASSERT_EQ(nbHyperedges, g.nbHyperedges());
  ASSERT_EQ(nbVerticesSubgraph, subgraph.nbVertices());
  ASSERT_EQ(nbHyperedgesSubgraph, subgraph.nbHyperedges());
  ASSERT_EQ(nbVerticesSubgraph2, subgraph2.nbVertices());
  ASSERT_EQ(nbHyperedgesSubgraph2, subgraph2.nbHyperedges());
  ASSERT_EQ(--nbVerticesSubSubgraph, subsubgraph.nbVertices());
  ASSERT_EQ(--nbHyperedgesSubSubgraph, subsubgraph.nbHyperedges());
  ASSERT_EQ(nbVerticesSubSubgraph2, subsubgraph2.nbVertices());
  ASSERT_EQ(nbHyperedgesSubSubgraph2, subsubgraph2.nbHyperedges());

  // Changes are delegated recursively to observers
  g.removeVertex(1, false);
  g.removeHyperedge(1);
  ASSERT_EQ(--nbVertices, g.nbVertices());
  ASSERT_EQ(--nbHyperedges, g.nbHyperedges());
  ASSERT_EQ(--nbVerticesSubgraph, subgraph.nbVertices());
  ASSERT_EQ(--nbHyperedgesSubgraph, subgraph.nbHyperedges());
  ASSERT_EQ(--nbVerticesSubgraph2, subgraph2.nbVertices());
  ASSERT_EQ(--nbHyperedgesSubgraph2, subgraph2.nbHyperedges());
  ASSERT_EQ(--nbVerticesSubSubgraph, subsubgraph.nbVertices());
  ASSERT_EQ(--nbHyperedgesSubSubgraph, subsubgraph.nbHyperedges());
  ASSERT_EQ(--nbVerticesSubSubgraph2, subsubgraph2.nbVertices());
  ASSERT_EQ(--nbHyperedgesSubSubgraph2, subsubgraph2.nbHyperedges());

  // Check removal of vertex with removeImpliedUndirectedHyperedges == true
  hglib::UndirectedHypergraph<UndirectedVertex,
          NamedUndirectedHyperedge> g2;
  for (auto vertexName : vertices) {
    g2.addVertex(vertexName);
  }

  for (auto hyperedge : hyperedges) {
    g2.addHyperedge(hglib::NAME, hyperedge.first, hyperedge.second);
  }

  whitelistedVertices = {0, 1, 2, 3, 5};
  whitelistedHyperedges = {0, 1, 2};
  subgraph = hglib::UndirectedSubHypergraph <UndirectedVertex,
          NamedUndirectedHyperedge>(
          &g2, whitelistedVertices, whitelistedHyperedges);
  nbVertices = g2.nbVertices();
  nbHyperedges = g2.nbHyperedges();
  nbVerticesSubgraph = subgraph.nbVertices();
  nbHyperedgesSubgraph = subgraph.nbHyperedges();
  g2.removeVertex(0);
  ASSERT_EQ(--nbVertices, g2.nbVertices());
  ASSERT_EQ(nbHyperedges - 2, g2.nbHyperedges());
  ASSERT_EQ(--nbVerticesSubgraph, subgraph.nbVertices());
  ASSERT_EQ(nbHyperedgesSubgraph - 2, subgraph.nbHyperedges());
}

TEST_F(TestUndirectedSubHypergraph, Test_getAncestors) {
  hglib::UndirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  // Add hyperarcs twice (but with different ids)
  for (auto hyperedge : hyperedges) {
    g.addHyperedge(hglib::NAME, hyperedge.first);
  }

  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperedges = {0, 1};
  auto sg = hglib::createUndirectedSubHypergraph(&g, whitelistedVertices,
                                                 whitelistedHyperedges);

  whitelistedVertices = {1, 2};
  whitelistedHyperedges = {1};
  auto ssg = hglib::createUndirectedSubHypergraph(sg.get(), whitelistedVertices,
                                                  whitelistedHyperedges);
  whitelistedVertices = {1};
  whitelistedHyperedges = {1};
  auto sssg = hglib::createUndirectedSubHypergraph(ssg.get(),
                                                   whitelistedVertices,
                                                   whitelistedHyperedges);

  std::vector<hglib::HypergraphInterface<UndirectedVertex, UndirectedHyperedge,
      hglib::emptyProperty, hglib::emptyProperty,
      hglib::emptyProperty>*> hierarchy = {ssg.get(), sg.get(), &g};

  std::vector<hglib::HypergraphInterface<UndirectedVertex, UndirectedHyperedge,
      hglib::emptyProperty, hglib::emptyProperty,
      hglib::emptyProperty>*> ancestors;
  g.getAncestors(&ancestors);
  ASSERT_EQ(ancestors.size(), (size_t) 0);
  sg->getAncestors(&ancestors);
  ASSERT_EQ(ancestors.size(), (size_t) 1);
  auto it = hierarchy.cbegin() + 2;
  for (const auto& ancestor : ancestors) {
    ASSERT_EQ(ancestor, *it);
    ++it;
  }
  ancestors.clear();
  ASSERT_EQ(ancestors.size(), (size_t) 0);
  ssg->getAncestors(&ancestors);
  ASSERT_EQ(ancestors.size(), (size_t) 2);
  it = hierarchy.cbegin() + 1;
  for (const auto& ancestor : ancestors) {
    ASSERT_EQ(ancestor, *it);
    ++it;
  }
  ancestors.clear();
  ASSERT_EQ(ancestors.size(), (size_t) 0);
  sssg->getAncestors(&ancestors);
  ASSERT_EQ(ancestors.size(), (size_t) 3);
  it = hierarchy.cbegin();
  for (const auto& ancestor : ancestors) {
    ASSERT_EQ(ancestor, *it);
    ++it;
  }
  ancestors.clear();
  ASSERT_EQ(ancestors.size(), (size_t) 0);
}
