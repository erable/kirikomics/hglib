// ****************************************************************************
//
//                          hglib - HyperGraph Library
//
// ****************************************************************************
//
// Copyright (C) 2018 Arnaud Mary, David Parsons, Martin Wannagat
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

#include "hglib.h"

#include <iostream>
#include <string>
#include <cstring>
#include <utility>
#include <vector>
#include <list>
#include <deque>
#include <unordered_set>


#include "gtest/gtest.h"

using std::pair;
using std::string;
using std::vector;
using hglib::DirectedVertex;
using hglib::DirectedHyperedge;
using hglib::NamedDirectedHyperedge;

/**
 * Test fixture
 */
class TestDirectedSubHyperGraph : public testing::Test {
 protected:
  virtual void SetUp() {
    storedStreambuf_ = std::cerr.rdbuf();
    std::cerr.rdbuf(nullptr);
  }

  virtual void TearDown() {
    std::cerr.rdbuf(storedStreambuf_);
  }

  vector<string> vertices {"A", "B", "Foo", "Bar", "", "6"};
  vector<pair<pair<vector<string>, vector<string>>, string>> hyperarcs {
          {{{"A"}, {"Foo"}}, "edge1"},
          {{{"A", "B"}, {"Bar", ""}}, "edge2"},
          {{{"6"}, {""}}, "edge3"}
  };

 private:
  std::streambuf* storedStreambuf_;
};

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_Ctor) {
  hglib::DirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  // Add hyperarcs twice (but with different ids)
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first);
  }

  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {0, 1};
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);

  // check that only whitelisted vertices are in the subgraph
  for (const auto& vertex : g.vertices()) {
    if (std::find(whitelistedVertices.begin(), whitelistedVertices.end(),
                  vertex->id()) == whitelistedVertices.end()) {
      ASSERT_EQ(subgraph->vertexById(vertex->id()), nullptr);
    } else {
      ASSERT_EQ(subgraph->vertexById(vertex->id()), vertex);
    }
  }
  // check that only whitelisted hyperarcs are in the subgraph
  for (const auto& hyperarc : g.hyperarcs()) {
    if (std::find(whitelistedHyperarcs.begin(), whitelistedHyperarcs.end(),
                  hyperarc->id()) == whitelistedHyperarcs.end()) {
      ASSERT_EQ(subgraph->hyperarcById(hyperarc->id()), nullptr);
    } else {
      ASSERT_EQ(subgraph->hyperarcById(hyperarc->id()), hyperarc);
    }
  }

  // sub-sub-hypergraph
  whitelistedVertices = {1};
  whitelistedHyperarcs = {1};
  auto subsubgraph = hglib::createDirectedSubHypergraph(subgraph.get(),
                                                       whitelistedVertices,
                                                       whitelistedHyperarcs);
  // check that only whitelisted vertices are in the sub-subgraph
  for (const auto& vertex : g.vertices()) {
    if (std::find(whitelistedVertices.begin(), whitelistedVertices.end(),
                  vertex->id()) == whitelistedVertices.end()) {
      ASSERT_EQ(subsubgraph->vertexById(vertex->id()), nullptr);
    } else {
      ASSERT_EQ(subsubgraph->vertexById(vertex->id()), vertex);
    }
  }
  // check that only whitelisted hyperarcs are in the sub-subgraph
  for (const auto& hyperarc : g.hyperarcs()) {
    if (std::find(whitelistedHyperarcs.begin(), whitelistedHyperarcs.end(),
                  hyperarc->id()) == whitelistedHyperarcs.end()) {
      ASSERT_EQ(subsubgraph->hyperarcById(hyperarc->id()), nullptr);
    } else {
      ASSERT_EQ(subsubgraph->hyperarcById(hyperarc->id()), hyperarc);
    }
  }

  // Check exceptions
  whitelistedVertices = {1, 2, 3, 1000};
  whitelistedHyperarcs = {1, 2, 1000};
  // Vertex with Id 1000 is not in the root hypergraph
  ASSERT_THROW(hglib::createDirectedSubHypergraph(
      &g, whitelistedVertices, whitelistedHyperarcs), std::out_of_range);
  // Hyperarc with Id 1000 is not in the root hypergraph
  whitelistedVertices = {1, 2, 3};
  ASSERT_THROW(hglib::createDirectedSubHypergraph(
      &g, whitelistedVertices, whitelistedHyperarcs), std::out_of_range);
  // Reaction with neither tail nor head
  whitelistedHyperarcs = {1};
  ASSERT_THROW(hglib::createDirectedSubHypergraph(
      &g, {}, whitelistedHyperarcs), std::invalid_argument);

  // Remove vertex/hyperarc and check invalid_argument exception
  g.removeVertex(1, false);
  g.removeHyperarc(1);
  whitelistedVertices = {1};
  whitelistedHyperarcs = {1};
  ASSERT_THROW(hglib::createDirectedSubHypergraph(
      &g, whitelistedVertices, whitelistedHyperarcs), std::invalid_argument);
  whitelistedVertices = {2, 3};
  ASSERT_THROW(hglib::createDirectedSubHypergraph(
      &g, whitelistedVertices, whitelistedHyperarcs), std::invalid_argument);
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_copyCtor) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      std::string label = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      std::string label = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      std::string label = "Default";
      Colour colour = Colour::red;
  };

  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
          VertexProperty, HyperarcProperty, HypergraphProperty> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first);
  }

  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {0, 1};
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);
  subgraph->removeVertex(1, false);
  // change some default properties in hypergraph with unnamed hyperarcs
  subgraph->getVertexProperties_(2)->label = "Test";
  hglib::HyperedgeIdType hyperarcId(1);  // hyperarc "edge1"
  subgraph->getHyperarcProperties_(hyperarcId)->label = "TestHyperarc";
  subgraph->getHyperarcProperties_(hyperarcId)->weight = 1.0;
  subgraph->getHypergraphProperties_()->colour = Colour::blue;

  // Make a copy
  auto subgraphCopy = hglib::copyDirectedSubHypergraph(*subgraph.get());

  // Check
  ASSERT_EQ(subgraph->nbVertices(), subgraphCopy->nbVertices());
  ASSERT_EQ(subgraph->nbHyperarcs(), subgraphCopy->nbHyperarcs());
  // check graph properties
  ASSERT_EQ(subgraphCopy->getHypergraphProperties()->label.compare("Default"),
            0);
  ASSERT_EQ(subgraphCopy->getHypergraphProperties()->colour, Colour::blue);

  // check vertices
  for (const auto& vertex : subgraph->vertices()) {
    const auto& copiedVertex = subgraphCopy->vertexByName(vertex->name());
    ASSERT_NE(copiedVertex, nullptr);
    ASSERT_EQ(vertex->id(), copiedVertex->id());
    const auto& vertexId = vertex->id();
    // compare in-hyperarcs
    ASSERT_EQ(subgraph->inDegree(vertexId), subgraphCopy->inDegree(vertexId));
    auto hyperarcCopyIt = subgraphCopy->inHyperarcsBegin(vertexId);
    auto itArcs = subgraph->inHyperarcsBegin(vertexId);
    auto itArcsEnd = subgraph->inHyperarcsEnd(vertexId);
    for (; itArcs != itArcsEnd; ++itArcs) {
      const auto& hyperarc = *itArcs;
      ASSERT_EQ(hyperarc->id(), (*hyperarcCopyIt)->id());
      ++hyperarcCopyIt;
    }
    // compare out-edges
    ASSERT_EQ(subgraph->outDegree(vertexId), subgraphCopy->outDegree(vertexId));
    hyperarcCopyIt = subgraphCopy->outHyperarcsBegin(vertexId);
    itArcs = subgraph->outHyperarcsBegin(vertexId);
    itArcsEnd = subgraph->outHyperarcsEnd(vertexId);
    for (; itArcs != itArcsEnd; ++itArcs) {
      const auto& hyperarc = *itArcs;
      ASSERT_EQ(hyperarc->id(), (*hyperarcCopyIt)->id());
      ++hyperarcCopyIt;
    }
    // compare properties
    const auto vertexProperty = subgraph->getVertexProperties(vertexId);
    const auto copiedVertexProperty = subgraphCopy->getVertexProperties(
            vertexId);
    ASSERT_EQ(vertexProperty->label.compare(copiedVertexProperty->label), 0);
    ASSERT_EQ(vertexProperty->colour, copiedVertexProperty->colour);
  }

  // check hyperarcs
  for (const auto& hyperarc : subgraph->hyperarcs()) {
    const auto& copiedHyperarc = subgraphCopy->hyperarcById(hyperarc->id());
    ASSERT_NE(copiedHyperarc, nullptr);
    ASSERT_EQ(hyperarc->id(), copiedHyperarc->id());
    const auto& hyperarcId = hyperarc->id();
    // compare tails
    ASSERT_EQ(subgraph->nbTailVertices(hyperarcId),
              subgraphCopy->nbTailVertices(hyperarcId));
    auto vertexCopyIt = subgraphCopy->tailsBegin(hyperarcId);
    auto it = subgraph->tailsBegin(hyperarcId);
    auto end = subgraph->tailsEnd(hyperarcId);
    for (; it != end; ++it) {
      const auto& tail = *it;
      ASSERT_EQ(tail->id(), (*vertexCopyIt)->id());
      ASSERT_EQ(tail->name().compare((*vertexCopyIt)->name()), 0);
      ++vertexCopyIt;
    }
    // compare heads
    ASSERT_EQ(subgraph->nbHeadVertices(hyperarcId),
              subgraphCopy->nbHeadVertices(hyperarcId));
    vertexCopyIt = subgraphCopy->headsBegin(hyperarcId);
    it = subgraph->headsBegin(hyperarcId);
    end = subgraph->headsEnd(hyperarcId);
    for (; it != end; ++it) {
      const auto& head = *it;
      ASSERT_EQ(head->id(), (*vertexCopyIt)->id());
      ASSERT_EQ(head->name().compare((*vertexCopyIt)->name()), 0);
      ++vertexCopyIt;
    }

    // compare properties
    const auto hyperarcProperty = subgraph->getHyperarcProperties(hyperarcId);
    const auto copiedHyperarcProperty =
            subgraphCopy->getHyperarcProperties(hyperarcId);
    ASSERT_EQ(hyperarcProperty->label.compare(copiedHyperarcProperty->label),
              0);
    ASSERT_EQ(hyperarcProperty->weight, copiedHyperarcProperty->weight);
  }

  // change subgraph and check that this change is
  // independent from the copied subgraph
  subgraph->getHypergraphProperties_()->colour = Colour::green;
  subgraph->getHypergraphProperties_()->label = "Original subgraph";
  ASSERT_EQ(subgraphCopy->getHypergraphProperties()->colour, Colour::blue);
  ASSERT_EQ(subgraphCopy->getHypergraphProperties()->label.compare("Default"),
            0);

  size_t nbExpectedVertices(subgraphCopy->nbVertices());
  size_t nbExpectedHyperarcs(subgraphCopy->nbHyperarcs());
  subgraph->removeVertex(2, false);
  subgraph->removeHyperarc(1);
  ASSERT_EQ(nbExpectedVertices, subgraphCopy->nbVertices());
  ASSERT_EQ(nbExpectedHyperarcs, subgraphCopy->nbHyperarcs());
}


TEST_F(TestDirectedSubHyperGraph, Test_subgraph_AssignmentOperator) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      string name = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      string name = "Default";
      Colour colour = Colour::blue;
  };

  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge,
          VertexProperty, HyperarcProperty, HypergraphProperty> graph;

  for (auto vertexName : vertices) {
    graph.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    graph.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }

  // Three sub- and sub-sub-graphs
  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3, 5};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {1, 2};
  auto subgraph = hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge, VertexProperty, HyperarcProperty,
          HypergraphProperty>(&graph,
                              whitelistedVertices,
                              whitelistedHyperarcs);
  whitelistedVertices = {1, 2, 3, 5};
  whitelistedHyperarcs = {2};
  auto subsubgraph = hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge, VertexProperty, HyperarcProperty,
          HypergraphProperty>(&subgraph,
                              whitelistedVertices,
                              whitelistedHyperarcs);

  // second sub- and sub-sub-graph
  whitelistedVertices = {1, 2, 3, 4, 5};
  whitelistedHyperarcs = {2};
  auto subgraph2 = hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge, VertexProperty, HyperarcProperty,
          HypergraphProperty>(&graph,
                              whitelistedVertices,
                              whitelistedHyperarcs);
  whitelistedVertices = {1, 2, 5};
  whitelistedHyperarcs = {2};
  auto subsubgraph2 = hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge, VertexProperty, HyperarcProperty,
          HypergraphProperty>(&subgraph2, whitelistedVertices,
                              whitelistedHyperarcs);

  // third sub- and sub-sub-graph
  whitelistedVertices = {0, 3, 5};
  whitelistedHyperarcs = {0};
  auto subgraph3 = hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge, VertexProperty, HyperarcProperty,
          HypergraphProperty>(&graph,
                              whitelistedVertices,
                              whitelistedHyperarcs);
  whitelistedVertices = {0, 5};
  whitelistedHyperarcs = {0};
  auto subsubgraph3 = hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge, VertexProperty, HyperarcProperty,
          HypergraphProperty>(&subgraph3, whitelistedVertices,
                              whitelistedHyperarcs);

  // Change properties in the first subgraph
  subgraph.getHypergraphProperties_()->name = "Test";
  subgraph.getHypergraphProperties_()->colour = Colour::red;
  subgraph.removeVertex(1, false);
  subgraph.removeHyperarc(1);

  // Assignments
  subgraph = subgraph;  // nothing happens in a self assignment
  // The following assignment should invalidate subsubgraph2 and
  // subsubgraph3
  subgraph2 = subgraph3 = subgraph;
  ASSERT_THROW(subsubgraph2.nbVertices(), std::invalid_argument);
  ASSERT_THROW(subsubgraph3.nbVertices(), std::invalid_argument);
  // check if all sub-hypergraphs are identical
  ASSERT_EQ(subgraph.nbVertices(), subgraph2.nbVertices());
  ASSERT_EQ(subgraph.nbVertices(), subgraph3.nbVertices());
  for (const auto& vertex : subgraph.vertices()) {
    ASSERT_EQ(subgraph2.vertexById(vertex->id()), vertex);
    ASSERT_EQ(subgraph2.vertexById(vertex->id()), vertex);
  }
  ASSERT_EQ(subgraph.nbHyperarcs(), subgraph2.nbHyperarcs());
  ASSERT_EQ(subgraph.nbHyperarcs(), subgraph3.nbHyperarcs());
  for (const auto& hyperarc : subgraph.hyperarcs()) {
    ASSERT_EQ(subgraph2.hyperarcById(hyperarc->id()), hyperarc);
    ASSERT_EQ(subgraph2.hyperarcById(hyperarc->id()), hyperarc);
  }
  // Check that the subsubgraph is not a child of all subgraphs and that
  // all subgraphs are independent from each other
  subgraph2.removeHyperarc(2);
  ASSERT_NE(subgraph.nbHyperarcs(), subgraph2.nbHyperarcs());
  ASSERT_NE(subgraph3.nbHyperarcs(), subgraph2.nbHyperarcs());
  // DirectedHyperedge 2 is still in the subsubgraph as its parent is subgraph
  // and not subgraph2
  ASSERT_NE(subsubgraph.hyperarcById(2), nullptr);

  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge,
          VertexProperty, HyperarcProperty, HypergraphProperty> graph4;
  // The following assignment invalidates the whole subgraph hierarchy of graph
  graph = graph4;
  ASSERT_THROW(subgraph.nbVertices(), std::invalid_argument);
  ASSERT_THROW(subsubgraph.nbVertices(), std::invalid_argument);
  ASSERT_THROW(subgraph2.nbVertices(), std::invalid_argument);
  ASSERT_THROW(subgraph3.nbVertices(), std::invalid_argument);
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_addVertex) {
  hglib::DirectedHypergraph<> g;
  size_t nbVertices = 0;
  hglib::VertexIdType vertexCounter = 0;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
    ASSERT_EQ(++nbVertices, g.nbVertices());
    ASSERT_EQ(vertexCounter++, g.vertexByName(vertexName)->id());
  }


  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);
  auto subsubgraph = hglib::createDirectedSubHypergraph(subgraph.get(),
                                                       whitelistedVertices,
                                                       whitelistedHyperarcs);
  size_t nbVerticesSubgraph(subgraph->nbVertices());
  size_t nbVerticesSubSubgraph(subsubgraph->nbVertices());
  ASSERT_EQ(nbVerticesSubgraph, (size_t) 0);
  ASSERT_EQ(nbVerticesSubSubgraph, (size_t) 0);

  // Add vertices to subgraphs:  "B", "Foo"
  subsubgraph->addVertex("B");
  ASSERT_EQ(++nbVerticesSubgraph, subgraph->nbVertices());
  ASSERT_EQ(++nbVerticesSubSubgraph, subsubgraph->nbVertices());
  subsubgraph->addVertex(2);  // via Id
  ASSERT_EQ(++nbVerticesSubgraph, subgraph->nbVertices());
  ASSERT_EQ(++nbVerticesSubSubgraph, subsubgraph->nbVertices());

  // Add vertex only to first subgraph: "A", "Bar"
  subgraph->addVertex("A");
  ASSERT_EQ(++nbVerticesSubgraph, subgraph->nbVertices());
  ASSERT_EQ(nbVerticesSubSubgraph, subsubgraph->nbVertices());
  subgraph->addVertex(3);
  ASSERT_EQ(++nbVerticesSubgraph, subgraph->nbVertices());
  ASSERT_EQ(nbVerticesSubSubgraph, subsubgraph->nbVertices());

  // Test exceptions
  // without a vertex name; only with specific arguments (empty for
  // DirectedVertex) -> not supported in a sub-hypergraph
  ASSERT_THROW(subgraph->addVertex(), std::invalid_argument);
  ASSERT_THROW(subsubgraph->addVertex(), std::invalid_argument);

  // With unknown name
  ASSERT_THROW(subgraph->addVertex("UnknownVertex"), std::invalid_argument);
  ASSERT_THROW(subsubgraph->addVertex("UnknownVertex"), std::invalid_argument);

  // With unknown vertex Id
  ASSERT_THROW(subgraph->addVertex(100), std::out_of_range);
  ASSERT_THROW(subsubgraph->addVertex(100), std::out_of_range);
  // Remove a vertex in root graph
  hglib::VertexIdType removeVertexId = 4;
  g.removeVertex(removeVertexId, false);
  // Exception when there is a nullptr at the given position
  ASSERT_THROW(subgraph->addVertex(removeVertexId), std::invalid_argument);
  ASSERT_THROW(subsubgraph->addVertex(removeVertexId), std::invalid_argument);


  // Add a new vertex to root hypergraph
  const auto& vertex = g.addVertex("UnknownVertex");
  // Vertex can now be added to the subgraphs
  subsubgraph->addVertex(vertex->id());
  ASSERT_EQ(++nbVerticesSubgraph, subgraph->nbVertices());
  ASSERT_EQ(++nbVerticesSubSubgraph, subsubgraph->nbVertices());
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_addHyperarc) {
  hglib::DirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first);
  }

  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {
          0, 1, 2, 3, 4, 5};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);
  whitelistedHyperarcs = {};
  auto subsubgraph = hglib::createDirectedSubHypergraph(subgraph.get(),
                                                       whitelistedVertices,
                                                       whitelistedHyperarcs);

  size_t nbHyperarcsSubgraph(subgraph->nbHyperarcs());
  size_t nbHyperarcsSubSubgraph(subsubgraph->nbHyperarcs());
  ASSERT_EQ(nbHyperarcsSubgraph, (size_t) 0);
  ASSERT_EQ(nbHyperarcsSubSubgraph, (size_t) 0);

  // Add hyperacs to subgraphs:  {{"A"}, {"Foo"}}, {{"A", "B"}, {"Bar", ""}}
  subsubgraph->addHyperarc(hglib::NAME, {{"A"}, {"Foo"}});
  ASSERT_EQ(++nbHyperarcsSubgraph, subgraph->nbHyperarcs());
  ASSERT_EQ(++nbHyperarcsSubSubgraph, subsubgraph->nbHyperarcs());
  subsubgraph->addHyperarc({{0, 1}, {3, 4}});  // via vertex Ids
  ASSERT_EQ(++nbHyperarcsSubgraph, subgraph->nbHyperarcs());
  ASSERT_EQ(++nbHyperarcsSubSubgraph, subsubgraph->nbHyperarcs());

  // Add hyperarc only to first subgraph: {{"6"}, {""}}
  subgraph->addHyperarc(hglib::NAME, {{"6"}, {""}});
  ASSERT_EQ(++nbHyperarcsSubgraph, subgraph->nbHyperarcs());
  ASSERT_EQ(nbHyperarcsSubSubgraph, subsubgraph->nbHyperarcs());

  // Test exceptions
  // Unknown hyperarc
  ASSERT_THROW(subgraph->addHyperarc(hglib::NAME, {{"A", "6"}, {"Bar", ""}}),
               std::invalid_argument);
  ASSERT_THROW(subsubgraph->addHyperarc({{0, 5}, {3, 4}}),
               std::invalid_argument);

  // With unknown hyperarc Id
  ASSERT_THROW(subgraph->addHyperarc(100), std::out_of_range);
  ASSERT_THROW(subsubgraph->addHyperarc(100), std::out_of_range);
  // Remove a hyperarc in root graph
  hglib::HyperedgeIdType removeHyperarcId = 1;
  g.removeHyperarc(removeHyperarcId);
  --nbHyperarcsSubgraph;
  --nbHyperarcsSubSubgraph;
  // Exception when there is a nullptr at the given position
  ASSERT_THROW(subgraph->addHyperarc(removeHyperarcId), std::invalid_argument);
  ASSERT_THROW(subsubgraph->addHyperarc(removeHyperarcId),
               std::invalid_argument);


  // Add a new hyperarc to root hypergraph
  const auto& hyperarc = g.addHyperarc(hglib::NAME, {{"Foo"}, {"6", "Bar"}});
  ASSERT_NE(hyperarc, nullptr);
  // Hyperarc can now be added to the subgraphs
  subsubgraph->addHyperarc(hyperarc->id());
  ASSERT_EQ(++nbHyperarcsSubgraph, subgraph->nbHyperarcs());
  ASSERT_EQ(++nbHyperarcsSubSubgraph, subsubgraph->nbHyperarcs());

  // Check that at least one tail or head vertex is in the subgraph
  whitelistedVertices = {};
  whitelistedHyperarcs = {};
  subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperarcs);
  // neither tail nor head is present
  ASSERT_THROW(subgraph->addHyperarc(0), std::invalid_argument);
  subgraph->addVertex(0);
  ASSERT_NO_THROW(subgraph->addHyperarc(0));
  ASSERT_EQ(subgraph->nbHyperarcs(), (size_t) 1);
  // remove vertex 0 and hyperarc
  subgraph->removeVertex(0, true);
  // try to re-insert fails
  ASSERT_THROW(subgraph->addHyperarc(0), std::invalid_argument);
  // Add head vertex first
  subgraph->addVertex(2);
  ASSERT_NO_THROW(subgraph->addHyperarc(0));
  ASSERT_EQ(subgraph->nbHyperarcs(), (size_t) 1);
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_vertexByName) {
  hglib::DirectedHypergraph<> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      whitelistedVertices.insert(vertex->id());
    }
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);
  for (const auto& vertex : g.vertices()) {
    if ((vertex->id() % 2) == 0) {
      ASSERT_EQ(subgraph->vertexByName(vertex->name()), vertex);
    } else {
      ASSERT_EQ(subgraph->vertexByName(vertex->name()), nullptr);
    }
  }
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_vertexById) {
  hglib::DirectedHypergraph<> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      whitelistedVertices.insert(vertex->id());
    }
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);
  for (const auto& vertex : g.vertices()) {
    if ((vertex->id() % 2) == 0) {
      ASSERT_EQ(subgraph->vertexById(vertex->id()), vertex);
    } else {
      ASSERT_EQ(subgraph->vertexById(vertex->id()), nullptr);
    }
  }
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_verticesContainer) {
  hglib::DirectedHypergraph<> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      whitelistedVertices.insert(vertex->id());
    }
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);

  std::vector<const DirectedVertex<DirectedHyperedge>*> verticesContainer;
  hglib::vertices(*subgraph.get(), std::back_inserter(verticesContainer));
  EXPECT_EQ(verticesContainer.size(), subgraph->nbVertices());
  for (auto& vertex : verticesContainer) {
    EXPECT_EQ(subgraph->vertexById(vertex->id()), vertex);
  }

  // test with std:list
  // std::back_inserter
  std::list<const DirectedVertex<DirectedHyperedge>*> l;
  hglib::vertices(*subgraph.get(), std::back_inserter(l));
  EXPECT_EQ(l.size(), subgraph->nbVertices());
  for (auto& vertex : l) {
    EXPECT_EQ(subgraph->vertexById(vertex->id()), vertex);
  }
  l.clear();
  // std::inserter
  hglib::vertices(*subgraph.get(), std::inserter(l, l.begin()));
  EXPECT_EQ(l.size(), subgraph->nbVertices());
  for (auto& vertex : l) {
    EXPECT_EQ(subgraph->vertexById(vertex->id()), vertex);
  }
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_hyperarcsContainer) {
  hglib::DirectedHypergraph<> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  for (auto arc : hyperarcs) {
    const auto& hyperarc = g.addHyperarc(hglib::NAME, arc.first);
    if ((hyperarc->id() % 2) == 0) {
      whitelistedHyperarcs.insert(hyperarc->id());
    }
  }

  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperarcs);

  std::vector<const DirectedHyperedge*> hyperarcContainer;
  hglib::hyperarcs(*subgraph.get(), std::back_inserter(hyperarcContainer));
  ASSERT_EQ(hyperarcContainer.size(), subgraph->nbHyperarcs());
  for (auto& hyperarc : hyperarcContainer) {
    EXPECT_NE(subgraph->hyperarcById(hyperarc->id()), nullptr);
  }

  // test with std:list
  // std::back_inserter
  std::list<const DirectedHyperedge*> l;
  hglib::hyperarcs(*subgraph.get(), std::back_inserter(l));
  EXPECT_EQ(l.size(), subgraph->nbHyperarcs());
  for (auto& hyperarc : l) {
    EXPECT_NE(subgraph->hyperarcById(hyperarc->id()), nullptr);
  }
  l.clear();
  // std::inserter
  hglib::hyperarcs(*subgraph.get(), std::inserter(l, l.begin()));
  EXPECT_EQ(l.size(), subgraph->nbHyperarcs());
  for (auto& hyperarc : l) {
    EXPECT_NE(subgraph->hyperarcById(hyperarc->id()), nullptr);
  }

  // test with std::deque
  std::deque<const DirectedHyperedge*> d;
  hglib::hyperarcs(*subgraph.get(), std::back_inserter(d));
  EXPECT_EQ(d.size(), subgraph->nbHyperarcs());
  for (auto& hyperarc : d) {
    EXPECT_NE(subgraph->hyperarcById(hyperarc->id()), nullptr);
  }

  // test with std::set
  std::set<const DirectedHyperedge*> s;
  hglib::hyperarcs(*subgraph.get(), std::inserter(s, s.begin()));
  EXPECT_EQ(s.size(), subgraph->nbHyperarcs());
  for (auto& hyperarc : s) {
    EXPECT_NE(subgraph->hyperarcById(hyperarc->id()), nullptr);
  }
  // test with std::multiset
  std::multiset<const DirectedHyperedge*> ms;
  hglib::hyperarcs(*subgraph.get(), std::inserter(ms, ms.begin()));
  EXPECT_EQ(ms.size(), subgraph->nbHyperarcs());
  for (auto& hyperarc : ms) {
    EXPECT_NE(subgraph->hyperarcById(hyperarc->id()), nullptr);
  }

  // test with std::unordered_set
  std::unordered_set<const DirectedHyperedge*> us;
  hglib::hyperarcs(*subgraph.get(), std::inserter(us, us.begin()));
  EXPECT_EQ(us.size(), subgraph->nbHyperarcs());
  for (auto& hyperarc : us) {
    EXPECT_NE(subgraph->hyperarcById(hyperarc->id()), nullptr);
  }

  // test with std::unordered_multiset
  std::unordered_multiset<const DirectedHyperedge*> ums;
  hglib::hyperarcs(*subgraph.get(), std::inserter(ums, ums.begin()));
  EXPECT_EQ(ums.size(), subgraph->nbHyperarcs());
  for (auto& hyperarc : ums) {
    EXPECT_NE(subgraph->hyperarcById(hyperarc->id()), nullptr);
  }
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_vertexIterator) {
  hglib::DirectedHypergraph<> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      whitelistedVertices.insert(vertex->id());
    }
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);

  // test using begin
  int nbExpectedVertices(3);
  int nbObservedVertices(0);
  auto it = subgraph->verticesBegin();
  for (; it != subgraph->verticesEnd(); ++it) {
    ASSERT_EQ(((*it)->id() % 2), 0);
    ++nbObservedVertices;
  }
  ASSERT_EQ(nbExpectedVertices, nbObservedVertices);

  // test range based for
  nbObservedVertices = 0;
  for (const auto& vertex : subgraph->vertices()) {
    ASSERT_EQ((vertex->id() % 2), 0);
    ++nbObservedVertices;
  }
  ASSERT_EQ(nbExpectedVertices, nbObservedVertices);

  // test begin/end-pair
  nbObservedVertices = 0;
  hglib::DirectedSubHypergraph<>::vertex_iterator it2, end;
  std::tie(it2, end) = subgraph->verticesBeginEnd();
  for (; it2 != end; ++it2) {
    ASSERT_EQ(((*it2)->id() % 2), 0);
    ++nbObservedVertices;
  }
  ASSERT_EQ(nbExpectedVertices, nbObservedVertices);

  // test iterators after the removal of vertices
  subgraph->removeVertex(0);
  subgraph->removeVertex(4);

  // test using begin
  nbExpectedVertices = 1;
  nbObservedVertices = 0;
  it = subgraph->verticesBegin();
  for (; it != subgraph->verticesEnd(); ++it) {
    ASSERT_EQ(((*it)->id() % 2), 0);
    ++nbObservedVertices;
  }
  ASSERT_EQ(nbExpectedVertices, nbObservedVertices);

  // test range based for loop
  nbObservedVertices = 0;
  for (const auto& vertex : subgraph->vertices()) {
    ASSERT_EQ((vertex->id() % 2), 0);
    ++nbObservedVertices;
  }
  ASSERT_EQ(nbExpectedVertices, nbObservedVertices);

  // test begin/end-pair
  nbObservedVertices = 0;
  std::tie(it2, end) = subgraph->verticesBeginEnd();
  for (; it2 != end; ++it2) {
    ASSERT_EQ(((*it2)->id() % 2), 0);
    ++nbObservedVertices;
  }
  ASSERT_EQ(nbExpectedVertices, nbObservedVertices);
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_getVertexProperties) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct EdgeProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
          VertexProperty, EdgeProperty> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      whitelistedVertices.insert(vertex->id());
    }
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);
  for (const auto& vertex : subgraph->vertices()) {
    auto vProp = subgraph->getVertexProperties_(vertex->id());
    ASSERT_EQ(vProp->name.compare("Default"), 0);
    ASSERT_EQ(vProp->colour, Colour::green);
    vProp->name = vertex->name() + "_" + std::to_string(vertex->id());
    vProp->colour = Colour::red;
  }
  // Verify that updated properties in sub-hypergraph are not delegated to the
  // root hypergraph
  for (const auto& vertex : g.vertices()) {
    const auto constVProp = g.getVertexProperties(vertex->id());
    string expected = "Default";
    ASSERT_EQ(constVProp->name.compare(expected), 0);
    ASSERT_EQ(constVProp->colour, Colour::green);
  }

  hglib::VertexIdType outOfRange = 10;
  ASSERT_THROW(subgraph->getVertexProperties_(outOfRange),
               std::out_of_range);
  ASSERT_THROW(subgraph->getVertexProperties(outOfRange),
               std::out_of_range);
  outOfRange = -10;
  ASSERT_THROW(subgraph->getVertexProperties_(outOfRange),
               std::out_of_range);
  ASSERT_THROW(subgraph->getVertexProperties(outOfRange),
               std::out_of_range);

  // without VertexProperty (empty struct)
  hglib::DirectedHypergraph<> g2;
  whitelistedVertices.clear();
  for (auto vertexName : vertices) {
    const auto& vertex = g2.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      whitelistedVertices.insert(vertex->id());
    }
  }
  auto subgraph2 = hglib::createDirectedSubHypergraph(&g2, whitelistedVertices,
                                             whitelistedHyperarcs);

  for (const auto& vertex :  subgraph2->vertices()) {
    auto vProp = subgraph2->getVertexProperties(vertex->id());
    EXPECT_EQ(typeid(hglib::emptyProperty), typeid(*vProp));
    EXPECT_NE(typeid(VertexProperty), typeid(*vProp));
  }
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_verticesWithPropertyContainer) {
  enum Colour {
      red, blue, green, black
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct EdgeProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
          VertexProperty, EdgeProperty> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      whitelistedVertices.insert(vertex->id());
    }
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);

  // Get all (green) vertices of the subgraph
  std::vector<const DirectedVertex<DirectedHyperedge>*> verticesContainer;
  hglib::vertices(*subgraph.get(), std::back_inserter(verticesContainer),
             [&] (hglib::VertexIdType vertexId) -> bool {
                 return subgraph->getVertexProperties(vertexId)->colour ==
                        Colour::green;});
  // All vertices are green by default
  ASSERT_EQ(verticesContainer.size(), subgraph->nbVertices());
  for (auto& vertex : verticesContainer) {
    ASSERT_NE(subgraph->vertexById(vertex->id()), nullptr);
  }

  // Change colour of some vertices
  // set colour of vertex 0 to red
  subgraph->getVertexProperties_(0)->colour = Colour::red;
  // set colour of vertex 2 to blue
  subgraph->getVertexProperties_(2)->colour = Colour::blue;
  // Get vertices by colour
  for ( int idx = red; idx != black; idx++ ) {
    verticesContainer.clear();  // empty container
    // Get vertices of specified colour
    Colour colour = static_cast<Colour>(idx);
    hglib::vertices(*subgraph.get(), std::back_inserter(verticesContainer),
               [&] (hglib::VertexIdType vertexId) ->
                       bool {
                   return subgraph->getVertexProperties(vertexId)->colour ==
                          colour;});
    switch (colour) {
      case red:
        ASSERT_EQ(verticesContainer.size(), (size_t) 1);
        ASSERT_EQ(verticesContainer[0]->id(), 0);
        break;
      case blue:
        ASSERT_EQ(verticesContainer.size(), (size_t) 1);
        ASSERT_EQ(verticesContainer[0]->id(), 2);
        break;
      case green:
        ASSERT_EQ(verticesContainer.size(), (size_t) 1);
        ASSERT_EQ(verticesContainer[0]->id(), 4);
        break;
      case black:
        ASSERT_TRUE(verticesContainer.empty());
    }
  }
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_GetHypergraphProperties) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      std::string name = "Default";
      Colour colour = Colour::green;
  };
  struct EdgeProperty {
      std::string name = "Default";
      double weight = 0.0;
  };
  struct HypergraphProperty {
      std::string name = "I am a hypergraph";
      Colour colour = Colour::red;
  };

  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
          VertexProperty, EdgeProperty, HypergraphProperty> g;
  auto subgraph = hglib::createDirectedSubHypergraph(&g, {}, {});

  // Check default arguments in subhypergraph
  ASSERT_EQ(subgraph->getHypergraphProperties()->name.compare(
          "I am a hypergraph"), 0);
  ASSERT_EQ(subgraph->getHypergraphProperties()->colour, Colour::red);
  // change subhypergraph properties
  subgraph->getHypergraphProperties_()->name = "I am a sub-hypergraph";
  subgraph->getHypergraphProperties_()->colour = Colour::blue;
  // check changed properties in subgraph but not in parent graph
  ASSERT_EQ(g.getHypergraphProperties()->name.compare(
          "I am a hypergraph"), 0);
  ASSERT_EQ(g.getHypergraphProperties()->colour, Colour::red);
  ASSERT_EQ(subgraph->getHypergraphProperties()->name.compare(
          "I am a sub-hypergraph"), 0);
  ASSERT_EQ(subgraph->getHypergraphProperties()->colour, Colour::blue);

  // without HypergraphProperty (empty struct)
  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
          VertexProperty, EdgeProperty> g2;
  auto subgraph2 = hglib::createDirectedSubHypergraph(&g2, {}, {});
  EXPECT_EQ(typeid(hglib::emptyProperty),
            typeid(*g2.getHypergraphProperties()));
  EXPECT_NE(typeid(HypergraphProperty),
            typeid(*g2.getHypergraphProperties()));
  EXPECT_EQ(typeid(hglib::emptyProperty),
            typeid(*subgraph2->getHypergraphProperties()));
  EXPECT_NE(typeid(HypergraphProperty),
            typeid(*subgraph2->getHypergraphProperties()));
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_hyperarcById) {
  hglib::DirectedHypergraph<> g;
  // insert vertices
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  // add hyperarcs
  for (auto hyperarc : hyperarcs) {
    const auto& arc = g.addHyperarc(hglib::NAME, hyperarc.first);
    if ((arc->id() % 2) != 0) {
      whitelistedHyperarcs.insert(arc->id());
    }
  }
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);

  for (const auto& hyperarc : g.hyperarcs()) {
    if ((hyperarc->id() % 2) != 0) {
      ASSERT_EQ(subgraph->hyperarcById(hyperarc->id()), hyperarc);
    } else {
      ASSERT_EQ(subgraph->hyperarcById(hyperarc->id()), nullptr);
    }
  }
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_hyperarcIterator) {
  hglib::DirectedHypergraph<> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  // add hyperarcs
  for (auto hyperarc : hyperarcs) {
    const auto& arc = g.addHyperarc(hglib::NAME, hyperarc.first);
    if ((arc->id() % 2) == 0) {
      whitelistedHyperarcs.insert(arc->id());
    }
  }
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);

  // test using begin
  int nbExpectedHyperarcs(2);
  int nbObservedHyperarcs(0);
  auto it = subgraph->hyperarcsBegin();
  for (; it != subgraph->hyperarcsEnd(); ++it) {
    ASSERT_EQ(((*it)->id() % 2), 0);
    ++nbObservedHyperarcs;
  }
  ASSERT_EQ(nbExpectedHyperarcs, nbObservedHyperarcs);

  // test range based for loop
  nbObservedHyperarcs = 0;
  for (const auto& hyperarc : subgraph->hyperarcs()) {
    ASSERT_EQ((hyperarc->id() % 2), 0);
    ++nbObservedHyperarcs;
  }
  ASSERT_EQ(nbExpectedHyperarcs, nbObservedHyperarcs);

  // test begin/end-pair
  nbObservedHyperarcs = 0;
  hglib::DirectedSubHypergraph<>::hyperarc_iterator it2, end;
  std::tie(it2, end) = subgraph->hyperarcsBeginEnd();
  for (; it2 != end; ++it2) {
    ASSERT_EQ(((*it2)->id() % 2), 0);
    ++nbObservedHyperarcs;
  }
  ASSERT_EQ(nbExpectedHyperarcs, nbObservedHyperarcs);


  // test iterators after the removal of vertices
  subgraph->removeHyperarc(0);

  // test using begin
  nbExpectedHyperarcs = 1;
  nbObservedHyperarcs = 0;
  it = subgraph->hyperarcsBegin();
  for (; it != subgraph->hyperarcsEnd(); ++it) {
    ASSERT_EQ(((*it)->id() % 2), 0);
    ++nbObservedHyperarcs;
  }
  ASSERT_EQ(nbExpectedHyperarcs, nbObservedHyperarcs);

  // test range based for loop
  nbObservedHyperarcs = 0;
  for (const auto& hyperarc : subgraph->hyperarcs()) {
    ASSERT_EQ((hyperarc->id() % 2), 0);
    ++nbObservedHyperarcs;
  }
  ASSERT_EQ(nbExpectedHyperarcs, nbObservedHyperarcs);

  // test begin/end-pair
  nbObservedHyperarcs = 0;
  std::tie(it2, end) = subgraph->hyperarcsBeginEnd();
  for (; it2 != end; ++it2) {
    ASSERT_EQ(((*it2)->id() % 2), 0);
    ++nbObservedHyperarcs;
  }
  ASSERT_EQ(nbExpectedHyperarcs, nbObservedHyperarcs);
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_getHyperarcProperties) {
  enum Colour {
      red, blue, green
  };
  struct VertexProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };
  struct HyperarcProperty {
      string name = "Default";
      double weight = 0.0;
  };

  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge,
          VertexProperty, HyperarcProperty> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  // add hyperarcs
  for (auto hyperarc : hyperarcs) {
    const auto& arc = g.addHyperarc(hglib::NAME, hyperarc.first,
                                    hyperarc.second);
    if ((arc->id() % 2) != 0) {
      whitelistedHyperarcs.insert(arc->id());
    }
  }
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);
  for (const auto& hyperarc : subgraph->hyperarcs()) {
    auto arcProp = subgraph->getHyperarcProperties_(hyperarc->id());
    ASSERT_EQ(arcProp->name.compare("Default"), 0);
    ASSERT_EQ(arcProp->weight, 0.0);
    arcProp->name = hyperarc->name() + "_" + std::to_string(hyperarc->id());
    arcProp->weight = 1.0;
  }
  // Verify that updated properties in sub-hypergraph are not delegated to the
  // root hypergraph
  for (const auto& hyperarc : g.hyperarcs()) {
    const auto constArcProp = g.getHyperarcProperties(hyperarc->id());
    string expected = "Default";
    ASSERT_EQ(constArcProp->name.compare(expected), 0);
    ASSERT_EQ(constArcProp->weight, 0.0);
  }

  hglib::HyperedgeIdType outOfRange = 10;
  ASSERT_THROW(subgraph->getHyperarcProperties_(outOfRange),
               std::out_of_range);
  ASSERT_THROW(subgraph->getHyperarcProperties(outOfRange),
               std::out_of_range);
  outOfRange = -10;
  ASSERT_THROW(subgraph->getHyperarcProperties_(outOfRange),
               std::out_of_range);
  ASSERT_THROW(subgraph->getHyperarcProperties(outOfRange),
               std::out_of_range);

  // without HyperarcProperty (empty struct)
  hglib::DirectedHypergraph<> g2;
  for (auto vertexName : vertices) {
    g2.addVertex(vertexName);
  }
  for (auto hyperarc : hyperarcs) {
    g2.addHyperarc(hglib::NAME, hyperarc.first);
  }
  auto subgraph2 = hglib::createDirectedSubHypergraph(&g2, whitelistedVertices,
                                             whitelistedHyperarcs);

  for (const auto& hyperarc :  subgraph2->hyperarcs()) {
    auto arcProp = subgraph2->getHyperarcProperties(hyperarc->id());
    EXPECT_EQ(typeid(hglib::emptyProperty), typeid(*arcProp));
    EXPECT_NE(typeid(HyperarcProperty), typeid(*arcProp));
  }
}

TEST_F(TestDirectedSubHyperGraph,
       Test_subgraph_hyperarcsWithPropertyContainer) {
  enum Colour {
      red, blue, green, black
  };
  struct VertexProperty {
      string name = "Default";
  };
  struct EdgeProperty {
      string name = "Default";
      Colour colour = Colour::green;
  };

  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge,
          VertexProperty, EdgeProperty> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  for (auto arc : hyperarcs) {
    const auto& hyperarc = g.addHyperarc(hglib::NAME, arc.first, arc.second);
    if ((hyperarc->id() % 2) == 0) {
      whitelistedHyperarcs.insert(hyperarc->id());
    }
  }

  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperarcs);

  // Get all (green) vertices
  std::vector<const NamedDirectedHyperedge*> hyperarcsContainer;
  hglib::hyperarcs(*subgraph.get(), std::back_inserter(hyperarcsContainer),
                   [&] (hglib::HyperedgeIdType hyperarcId) -> bool {
                       return subgraph->getHyperarcProperties(
                               hyperarcId)->colour == Colour::green;});
  // All vertices are green by default
  ASSERT_EQ(hyperarcsContainer.size(), subgraph->nbHyperarcs());
  for (auto& hyperarc : hyperarcsContainer) {
    EXPECT_NE(subgraph->hyperarcById(hyperarc->id()), nullptr);
  }
  // set colour of hyperarc 'edge1' to red
  subgraph->getHyperarcProperties_(0)->colour = Colour::red;
  // set colour of hyperarc 'edge3' to blue
  subgraph->getHyperarcProperties_(2)->colour = Colour::blue;
  // Get hyperarcs by colour
  for ( int idx = red; idx != black; idx++ ) {
    hyperarcsContainer.clear();  // empty container
    // Get vertices of specified colour
    Colour colour = static_cast<Colour>(idx);
    hglib::hyperarcs(*subgraph.get(), std::back_inserter(hyperarcsContainer),
                     [&] (hglib::HyperedgeIdType arcId) ->
                             bool {
                         return subgraph->getHyperarcProperties(
                                 arcId)->colour == colour;});
    switch (colour) {
      case red:
        ASSERT_EQ(hyperarcsContainer.size(), (size_t) 1);
        ASSERT_EQ(hyperarcsContainer[0]->name().compare("edge1"), 0);
        break;
      case blue:
        ASSERT_EQ(hyperarcsContainer.size(), (size_t) 1);
        ASSERT_EQ(hyperarcsContainer[0]->name().compare("edge3"), 0);
        break;
      case green:
        ASSERT_TRUE(hyperarcsContainer.empty());
        break;
      case black:
        ASSERT_TRUE(hyperarcsContainer.empty());
        break;
    }
  }
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_hyperarcContainsVertex) {
  hglib::DirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }

  // add hyperarcs
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first);
  }
  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {0, 2};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {0};
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);
  ASSERT_TRUE(subgraph->isVertexOfHyperarc(0, 0));
  ASSERT_TRUE(subgraph->isTailVertexOfHyperarc(0, 0));
  ASSERT_TRUE(subgraph->isVertexOfHyperarc(2, 0));
  ASSERT_TRUE(subgraph->isHeadVertexOfHyperarc(2, 0));

  whitelistedVertices = {0};
  subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                       whitelistedHyperarcs);
  ASSERT_TRUE(subgraph->isVertexOfHyperarc(0, 0));
  ASSERT_TRUE(subgraph->isTailVertexOfHyperarc(0, 0));
  ASSERT_THROW(subgraph->isVertexOfHyperarc(2, 0), std::invalid_argument);
  ASSERT_THROW(subgraph->isHeadVertexOfHyperarc(2, 0), std::invalid_argument);

  whitelistedVertices = {2};
  subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                       whitelistedHyperarcs);
  ASSERT_THROW(subgraph->isVertexOfHyperarc(0, 0), std::invalid_argument);
  ASSERT_THROW(subgraph->isTailVertexOfHyperarc(0, 0), std::invalid_argument);
  ASSERT_TRUE(subgraph->isVertexOfHyperarc(2, 0));
  ASSERT_TRUE(subgraph->isHeadVertexOfHyperarc(2, 0));
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_tailVertices) {
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs;
  for (auto hyperarc : hyperarcs) {
    const auto &arc = g.addHyperarc(hglib::NAME, hyperarc.first,
                                    hyperarc.second);
    whitelistedHyperarcs.insert(arc->id());
  }

  // subgraph eq parent graph
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                       whitelistedHyperarcs);

  hglib::HyperedgeIdType hyperarcId(0);
  for (auto hyperarc : hyperarcs) {
    ASSERT_EQ(subgraph->nbTailVertices(hyperarcId),
              hyperarc.first.first.size());
    ASSERT_TRUE(subgraph->hasTailVertices(hyperarcId));
    // using begin
    auto itTails = subgraph->tailsBegin(hyperarcId);
    // using begin/end-pair
    typedef hglib::DirectedSubHypergraph<DirectedVertex,
            NamedDirectedHyperedge> subHypergraph;
    subHypergraph::vertex_iterator it, end;
    std::tie(it, end) = subgraph->tailsBeginEnd(hyperarcId);
    // range based for
    auto tailsPtr = subgraph->tails(hyperarcId);
    auto itTailNames = hyperarc.first.first.begin();
    for (const auto& tail : *tailsPtr) {
      ASSERT_STREQ(tail->name().c_str(), (*itTailNames).c_str());
      ASSERT_EQ((*itTails)->name().compare(*itTailNames), 0);
      ASSERT_EQ((*it)->name().compare(*itTailNames), 0);
      ++itTailNames;
      ++itTails;
      ++it;
    }
    ++hyperarcId;
  }

  // test exception for given hyperarc id that was never assigned
  // to this hypergraph
  ASSERT_THROW(subgraph->nbTailVertices(hyperarcId), std::out_of_range);
  ASSERT_THROW(subgraph->hasTailVertices(hyperarcId), std::out_of_range);
  ASSERT_THROW(subgraph->tailsBegin(hyperarcId), std::out_of_range);
  ASSERT_THROW(subgraph->tailsEnd(hyperarcId), std::out_of_range);
  ASSERT_THROW(subgraph->tailsBeginEnd(hyperarcId), std::out_of_range);
  ASSERT_THROW(subgraph->tails(hyperarcId), std::out_of_range);

  // test exception after removal of a hyperarc
  hglib::HyperedgeIdType removedHyperarcId(1);
  subgraph->removeHyperarc(removedHyperarcId);
  ASSERT_THROW(subgraph->nbTailVertices(removedHyperarcId),
               std::invalid_argument);
  ASSERT_THROW(subgraph->hasTailVertices(removedHyperarcId),
               std::invalid_argument);
  ASSERT_THROW(subgraph->tailsBegin(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(subgraph->tailsEnd(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(subgraph->tailsBeginEnd(removedHyperarcId),
               std::invalid_argument);
  ASSERT_THROW(subgraph->tails(removedHyperarcId), std::invalid_argument);

  // re-init subgraph
  // add hyperarc without tails
  const auto& noTailArc = g.addHyperarc(hglib::NAME, {{}, {"A"}},
                                        {"NoTailArc"});
  whitelistedHyperarcs.insert(noTailArc->id());
  subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                       whitelistedHyperarcs);

  // Check hyperarc "NoTailArc"
  ASSERT_EQ(subgraph->tailsBegin(noTailArc->id()),
            subgraph->tailsEnd(noTailArc->id()));
  ASSERT_EQ(subgraph->nbTailVertices(noTailArc->id()), (size_t) 0);
  ASSERT_FALSE(subgraph->hasTailVertices(noTailArc->id()));
  typedef hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge> subHypergraph_t;
  subHypergraph_t::vertex_iterator it, end;
  std::tie(it, end) = subgraph->tailsBeginEnd(noTailArc->id());
  ASSERT_EQ(it, end);

  // remove one tail vertex after the other and check tails
  subgraph->removeVertex(0, false);  // "A"
  ASSERT_EQ(subgraph->tailsBegin(0), subgraph->tailsEnd(0));  // "edge1"
  std::tie(it, end) = subgraph->tailsBeginEnd(0);
  ASSERT_EQ(it, end);
  ASSERT_EQ(it, subgraph->tailsBegin(0));
  ASSERT_EQ(end, subgraph->tailsEnd(0));
  ASSERT_FALSE(subgraph->hasTailVertices(0));
  ASSERT_EQ(subgraph->nbTailVertices(0), (size_t) 0);

  ASSERT_TRUE(subgraph->hasTailVertices(1));  // "edge2"
  ASSERT_EQ(subgraph->nbTailVertices(1), (size_t) 1);
  subgraph->removeVertex(1, false);  // "B"
  ASSERT_EQ(subgraph->tailsBegin(1), subgraph->tailsEnd(1));  // "edge2"
  std::tie(it, end) = subgraph->tailsBeginEnd(1);
  ASSERT_EQ(it, end);
  ASSERT_EQ(it, subgraph->tailsBegin(1));
  ASSERT_EQ(end, subgraph->tailsEnd(1));
  ASSERT_FALSE(subgraph->hasTailVertices(1));
  ASSERT_EQ(subgraph->nbTailVertices(1), (size_t) 0);

  // Remove another vertex
  subgraph->removeVertex(5, false);  // "6"
  ASSERT_EQ(subgraph->tailsBegin(2), subgraph->tailsEnd(2));  // "edge3"
  std::tie(it, end) = subgraph->tailsBeginEnd(2);
  ASSERT_EQ(it, end);
  ASSERT_EQ(it, subgraph->tailsBegin(2));
  ASSERT_EQ(end, subgraph->tailsEnd(2));
  ASSERT_FALSE(subgraph->hasTailVertices(2));
  ASSERT_EQ(subgraph->nbTailVertices(2), (size_t) 0);
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_headVertices) {
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs;
  for (auto hyperarc : hyperarcs) {
    const auto &arc = g.addHyperarc(hglib::NAME, hyperarc.first,
                                    hyperarc.second);
    whitelistedHyperarcs.insert(arc->id());
  }

  // subgraph eq parent graph
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);

  typedef hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge> subHypergraph_t;
  hglib::HyperedgeIdType hyperarcId(0);
  for (auto hyperarc : hyperarcs) {
    ASSERT_EQ(subgraph->nbHeadVertices(hyperarcId),
              hyperarc.first.second.size());
    ASSERT_TRUE(subgraph->hasHeadVertices(hyperarcId));
    // using begin
    auto itHeads = subgraph->headsBegin(hyperarcId);
    // using begin/end-pair
    subHypergraph_t::vertex_iterator it, end;
    std::tie(it, end) = subgraph->headsBeginEnd(hyperarcId);
    auto itHeadNames = hyperarc.first.second.begin();
    // range based for
    auto headsPtr = subgraph->heads(hyperarcId);
    for (const auto& head : *headsPtr) {
      ASSERT_EQ(head->name().compare(*itHeadNames), 0);
      ASSERT_EQ((*itHeads)->name().compare(*itHeadNames), 0);
      ASSERT_EQ((*it)->name().compare(*itHeadNames), 0);
      ++itHeadNames;
      ++itHeads;
      ++it;
    }
    ++hyperarcId;
  }

  // test exception for given hyperarc id that was never assigned
  // to this hypergraph
  ASSERT_THROW(subgraph->nbHeadVertices(hyperarcId), std::out_of_range);
  ASSERT_THROW(subgraph->hasHeadVertices(hyperarcId), std::out_of_range);
  ASSERT_THROW(subgraph->headsBegin(hyperarcId), std::out_of_range);
  ASSERT_THROW(subgraph->headsEnd(hyperarcId), std::out_of_range);
  ASSERT_THROW(subgraph->headsBeginEnd(hyperarcId), std::out_of_range);
  ASSERT_THROW(subgraph->heads(hyperarcId), std::out_of_range);

  // test exception after removal of a hyperarc
  hglib::HyperedgeIdType removedHyperarcId(1);
  subgraph->removeHyperarc(removedHyperarcId);
  ASSERT_THROW(subgraph->nbHeadVertices(removedHyperarcId),
               std::invalid_argument);
  ASSERT_THROW(subgraph->hasHeadVertices(removedHyperarcId),
               std::invalid_argument);
  ASSERT_THROW(subgraph->headsBegin(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(subgraph->headsEnd(removedHyperarcId), std::invalid_argument);
  ASSERT_THROW(subgraph->headsBeginEnd(removedHyperarcId),
               std::invalid_argument);
  ASSERT_THROW(subgraph->heads(removedHyperarcId), std::invalid_argument);

  // re-init subgraph
  // add hyperarc without heads
  const auto& noHeadArc = g.addHyperarc(hglib::NAME, {{"A"}, {}},
                                        {"NoHeadArc"});
  whitelistedHyperarcs.insert(noHeadArc->id());
  subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                       whitelistedHyperarcs);

  // Check hyperarc "NoHeadArc"
  ASSERT_EQ(subgraph->headsBegin(noHeadArc->id()),
            subgraph->headsEnd(noHeadArc->id()));
  ASSERT_EQ(subgraph->nbHeadVertices(noHeadArc->id()), (size_t) 0);
  ASSERT_FALSE(subgraph->hasHeadVertices(noHeadArc->id()));
  subHypergraph_t::vertex_iterator it, end;
  std::tie(it, end) = subgraph->headsBeginEnd(noHeadArc->id());
  ASSERT_EQ(it, end);

  // remove one tail vertex after the other and check heads
  subgraph->removeVertex(2, false);  // "Foo"
  ASSERT_EQ(subgraph->headsBegin(0), subgraph->headsEnd(0));  // "edge1"
  // using begin/end-pair
  std::tie(it, end) = subgraph->headsBeginEnd(0);
  ASSERT_EQ(it, end);
  ASSERT_EQ(it, subgraph->headsBegin(0));
  ASSERT_EQ(end, subgraph->headsEnd(0));
  ASSERT_FALSE(subgraph->hasHeadVertices(0));
  ASSERT_EQ(subgraph->nbHeadVertices(0), (size_t) 0);

  subgraph->removeVertex(3, false);  // "Bar"
  ASSERT_NE(subgraph->headsBegin(1), subgraph->headsEnd(1));  // "edge2"
  std::tie(it, end) = subgraph->headsBeginEnd(1);
  ASSERT_NE(it, end);
  ASSERT_EQ(it, subgraph->headsBegin(1));
  ASSERT_EQ(end, subgraph->headsEnd(1));
  ASSERT_TRUE(subgraph->hasHeadVertices(1));
  ASSERT_EQ(subgraph->nbHeadVertices(1), (size_t) 1);

  subgraph->removeVertex(4, false);  // ""
  ASSERT_EQ(subgraph->headsBegin(1), subgraph->headsEnd(1));  // "edge2"
  std::tie(it, end) = subgraph->headsBeginEnd(1);
  ASSERT_EQ(it, end);
  ASSERT_EQ(it, subgraph->headsBegin(1));
  ASSERT_EQ(end, subgraph->headsEnd(1));
  ASSERT_FALSE(subgraph->hasHeadVertices(1));
  ASSERT_EQ(subgraph->nbHeadVertices(1), (size_t) 0);
  ASSERT_EQ(subgraph->headsBegin(2), subgraph->headsEnd(2));  // "edge3"
  std::tie(it, end) = subgraph->headsBeginEnd(2);
  ASSERT_EQ(it, end);
  ASSERT_EQ(it, subgraph->headsBegin(2));
  ASSERT_EQ(end, subgraph->headsEnd(2));
  ASSERT_FALSE(subgraph->hasHeadVertices(2));
  ASSERT_EQ(subgraph->nbHeadVertices(2), (size_t) 0);
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_inAndOutHyperarcs) {
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs;
  for (auto hyperarc : hyperarcs) {
    const auto& arc = g.addHyperarc(hglib::NAME, hyperarc.first,
                                    hyperarc.second);
    if ((arc->id() % 2) == 0) {
      whitelistedHyperarcs.insert(arc->id());
    }
  }

  typedef hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge> subHypergraph_t;
  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);

  for (const auto& vertex : subgraph->vertices()) {
    const auto vertexId = vertex->id();
    if (vertex->name().compare("A") == 0 or
            vertex->name().compare("6") == 0) {
      // in-arcs
      ASSERT_EQ(subgraph->inDegree(vertexId), (size_t) 0);
      ASSERT_FALSE(subgraph->hasInHyperarcs(vertexId));
      ASSERT_EQ(subgraph->inHyperarcsBegin(vertexId),
                subgraph->inHyperarcsEnd(vertexId));
      // using begin/end-pair
      subHypergraph_t::hyperarc_iterator it, end;
      std::tie(it, end) = subgraph->inHyperarcsBeginEnd(vertexId);
      // using begin
      auto it2 = subgraph->inHyperarcsBegin(vertexId);
      int nbExpectedInArcs(0);
      int nbObservedInArcs(0);
      auto inArcsIt = subgraph->inHyperarcsBegin(vertexId);
      auto inArcsEnd = subgraph->inHyperarcsEnd(vertexId);
      for (; inArcsIt != inArcsEnd; ++inArcsIt) {
        const auto& arc = *inArcsIt;
        ASSERT_EQ((arc->id() % 2), 0);
        ASSERT_EQ(((*it)->id() % 2), 0);
        ASSERT_EQ(((*it2)->id() % 2), 0);
        ++nbObservedInArcs;
        ++it;
        ++it2;
      }
      ASSERT_EQ(nbExpectedInArcs, nbObservedInArcs);

      // out-arcs
      ASSERT_EQ(subgraph->outDegree(vertexId), (size_t) 1);
      ASSERT_TRUE(subgraph->hasOutHyperarcs(vertexId));
      // using begin/end-pair
      std::tie(it, end) = subgraph->outHyperarcsBeginEnd(vertexId);
      // using begin
      it2 = subgraph->outHyperarcsBegin(vertexId);
      int nbExpectedOutArcs(1);
      int nbObservedOutArcs(0);
      auto outArcIt = subgraph->outHyperarcsBegin(vertexId);
      auto outArcEnd = subgraph->outHyperarcsEnd(vertexId);
      // range based for
      auto outArcsPtr = subgraph->outHyperarcs(vertexId);
      for (const auto& arc : *outArcsPtr) {
        ASSERT_EQ((arc->id() % 2), 0);
        ASSERT_EQ(((*it)->id() % 2), 0);
        ASSERT_EQ(((*it2)->id() % 2), 0);
        ++nbObservedOutArcs;
        ++it;
        ++it2;
        ++outArcIt;
      }
      ASSERT_EQ(nbExpectedOutArcs, nbObservedOutArcs);
    } else if (vertex->name().compare("Foo") == 0 or
            vertex->name().compare("") == 0) {
      // in-arcs
      ASSERT_EQ(subgraph->inDegree(vertexId), (size_t) 1);
      ASSERT_TRUE(subgraph->hasInHyperarcs(vertexId));
      ASSERT_NE(subgraph->inHyperarcsBegin(vertexId),
                subgraph->inHyperarcsEnd(vertexId));
      // using begin/end-pair
      subHypergraph_t::hyperarc_iterator it, end;
      std::tie(it, end) = subgraph->inHyperarcsBeginEnd(vertexId);
      // using begin
      auto it2 = subgraph->inHyperarcsBegin(vertexId);
      int nbExpectedInArcs(1);
      int nbObservedInArcs(0);
      auto inArcsIt = subgraph->inHyperarcsBegin(vertexId);
      auto inArcsEnd = subgraph->inHyperarcsEnd(vertexId);
      // range based for
      auto inArcsPts = subgraph->inHyperarcs(vertexId);
      for (const auto& arc : *inArcsPts) {
        ASSERT_EQ((arc->id() % 2), 0);
        ASSERT_EQ(((*it)->id() % 2), 0);
        ASSERT_EQ(((*it2)->id() % 2), 0);
        ++nbObservedInArcs;
        ++it;
        ++it2;
        ++inArcsIt;
      }
      ASSERT_EQ(nbExpectedInArcs, nbObservedInArcs);

      // out-arcs
      ASSERT_EQ(subgraph->outDegree(vertexId), (size_t) 0);
      ASSERT_FALSE(subgraph->hasOutHyperarcs(vertexId));
      // using begin/end-pair
      std::tie(it, end) = subgraph->outHyperarcsBeginEnd(vertexId);
      // using begin
      it2 = subgraph->outHyperarcsBegin(vertexId);
      int nbExpectedOutArcs(0);
      int nbObservedOutArcs(0);
      auto outArcIt = subgraph->outHyperarcsBegin(vertexId);
      auto outArcEnd = subgraph->outHyperarcsEnd(vertexId);
      for (; outArcIt != outArcEnd; ++outArcIt) {
        const auto& arc = *outArcIt;
        ASSERT_EQ((arc->id() % 2), 0);
        ASSERT_EQ(((*it)->id() % 2), 0);
        ASSERT_EQ(((*it2)->id() % 2), 0);
        ++nbObservedOutArcs;
        ++it;
        ++it2;
      }
      ASSERT_EQ(nbExpectedOutArcs, nbObservedOutArcs);
    }
  }

  // delete hyperarcs from the subgraph
  subgraph->removeHyperarc(0);
  subgraph->removeHyperarc(2);
  for (const auto& vertex : subgraph->vertices()) {
    const auto vertexId = vertex->id();
    // in-arcs
    ASSERT_EQ(subgraph->inDegree(vertexId), (size_t) 0);
    ASSERT_FALSE(subgraph->hasInHyperarcs(vertexId));
    ASSERT_EQ(subgraph->inHyperarcsBegin(vertexId),
              subgraph->inHyperarcsEnd(vertexId));
    // using begin/end-pair
    subHypergraph_t::hyperarc_iterator it, end;
    std::tie(it, end) = subgraph->inHyperarcsBeginEnd(vertexId);
    // using begin
    auto it2 = subgraph->inHyperarcsBegin(vertexId);
    int nbExpectedInArcs(0);
    int nbObservedInArcs(0);
    auto inArcsIt = subgraph->inHyperarcsBegin(vertexId);
    auto inArcsEnd = subgraph->inHyperarcsEnd(vertexId);
    for (; inArcsIt != inArcsEnd; ++inArcsIt) {
      const auto& arc = *inArcsIt;
      ASSERT_EQ((arc->id() % 2), 0);
      ASSERT_EQ(((*it)->id() % 2), 0);
      ASSERT_EQ(((*it2)->id() % 2), 0);
      ++nbObservedInArcs;
      ++it;
      ++it2;
    }
    ASSERT_EQ(nbExpectedInArcs, nbObservedInArcs);

    // out-arcs
    ASSERT_EQ(subgraph->outDegree(vertexId), (size_t) 0);
    ASSERT_FALSE(subgraph->hasOutHyperarcs(vertexId));
    int nbExpectedOutArcs(0);
    int nbObservedOutArcs(0);
    // using begin/end-pair
    std::tie(it, end) = subgraph->outHyperarcsBeginEnd(vertexId);
    // using begin
    it2 = subgraph->outHyperarcsBegin(vertexId);
    auto outArcIt = subgraph->outHyperarcsBegin(vertexId);
    auto outArcEnd = subgraph->outHyperarcsEnd(vertexId);
    for (; outArcIt != outArcEnd; ++outArcIt) {
      const auto& arc = *outArcIt;
      ASSERT_EQ((arc->id() % 2), 0);
      ASSERT_EQ(((*it)->id() % 2), 0);
      ASSERT_EQ(((*it2)->id() % 2), 0);
      ++nbObservedOutArcs;
      ++it;
      ++it2;
    }
    ASSERT_EQ(nbExpectedOutArcs, nbObservedOutArcs);
  }
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_removeVertex) {
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs;
  for (auto hyperarc : hyperarcs) {
    const auto& arc = g.addHyperarc(hglib::NAME, hyperarc.first,
                                    hyperarc.second);
    whitelistedHyperarcs.insert(arc->id());
  }

  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);

  subgraph->removeVertex("A", false);
  ASSERT_NE(g.vertexByName("A"), nullptr);  // still exists in parent
  ASSERT_FALSE(subgraph->hasTailVertices(0));  // "edge1"
  ASSERT_TRUE(subgraph->hasTailVertices(1));  // "edge2"
  ASSERT_EQ(subgraph->nbTailVertices(1), (size_t) 1);

  // will remove in/out-hyperarcs of vertex "" from the sub-hypergraph
  // only "edge1" remains
  subgraph->removeVertex("");
  ASSERT_NE(g.vertexByName(""), nullptr);  // still exists in parent
  ASSERT_EQ(subgraph->nbHyperarcs(), (size_t) 1);

  // Check that if all tails and heads of a reaction are removed, then the
  // reaction is also removed.
  whitelistedVertices = {0};
  whitelistedHyperarcs = {0};
  subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                                     whitelistedHyperarcs);
  // remove only tail vertex
  subgraph->removeVertex(0, false);
  ASSERT_EQ(subgraph->nbHyperarcs(), (size_t) 0);

  // The same test but with the head vertex
  whitelistedVertices = {2};
  whitelistedHyperarcs = {0};
  subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                                whitelistedHyperarcs);
  subgraph->removeVertex(2, false);
  ASSERT_EQ(subgraph->nbHyperarcs(), (size_t) 0);
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_removeHyperarc) {
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g;
  std::unordered_set<hglib::VertexIdType> whitelistedVertices;
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    whitelistedVertices.insert(vertex->id());
  }
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs;
  for (auto hyperarc : hyperarcs) {
    const auto& arc = g.addHyperarc(hglib::NAME, hyperarc.first,
                                    hyperarc.second);
    whitelistedHyperarcs.insert(arc->id());
  }

  auto subgraph = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                            whitelistedHyperarcs);

  hglib::HyperedgeIdType idx(0);
  size_t nbExpectedHyperarcs(3);
  for (auto hyperarc : hyperarcs) {
    subgraph->removeHyperarc(idx);
    ASSERT_NE(g.hyperarcById(idx), nullptr);  // still exists in parent
    ASSERT_EQ(subgraph->nbHyperarcs(), --nbExpectedHyperarcs);
    ++idx;
  }
  // all hyperarcs are removed only from the subgraph
  for (const auto& vertex : subgraph->vertices()) {
    ASSERT_FALSE(subgraph->hasInHyperarcs(vertex->id()));
    ASSERT_FALSE(subgraph->hasOutHyperarcs(vertex->id()));
    if (vertex->name().compare("Foo") == 0 or
            vertex->name().compare("Bar") == 0 or
            vertex->name().compare("") == 0) {
      ASSERT_TRUE(g.hasInHyperarcs(vertex->id()));
    } else {  // as from the beginning
      ASSERT_FALSE(g.hasInHyperarcs(vertex->id()));
    }
    if (vertex->name().compare("A") == 0 or
        vertex->name().compare("B") == 0 or
            vertex->name().compare("6") == 0) {
      ASSERT_TRUE(g.hasOutHyperarcs(vertex->id()));
    } else {  // as from the beginning
      ASSERT_FALSE(g.hasOutHyperarcs(vertex->id()));
    }
  }
}

TEST_F(TestDirectedSubHyperGraph, Test_getRootHypergraph) {
  hglib::DirectedHypergraph<> g, g2;
  ASSERT_EQ(g.getRootHypergraph(), &g);
  ASSERT_EQ(g2.getRootHypergraph(), &g2);

  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  auto subgraph = hglib::DirectedSubHypergraph<>(&g, whitelistedVertices,
                                                 whitelistedHyperarcs);
  auto subsubgraph = hglib::DirectedSubHypergraph<>(&subgraph,
                                                    whitelistedVertices,
                                                    whitelistedHyperarcs);
  ASSERT_EQ(subgraph.getRootHypergraph(), &g);
  ASSERT_EQ(subsubgraph.getRootHypergraph(), &g);

  auto subgraph2 = hglib::DirectedSubHypergraph<>(&g2, whitelistedVertices,
                                                  whitelistedHyperarcs);
  ASSERT_EQ(subgraph2.getRootHypergraph(), &g2);
  subgraph2 = subgraph;
  ASSERT_EQ(subgraph2.getRootHypergraph(), &g);
  ASSERT_EQ(subgraph.getRootHypergraph(), &g);
  ASSERT_EQ(subsubgraph.getRootHypergraph(), &g);

  auto subgraph3 = hglib::DirectedSubHypergraph<>(&g2, whitelistedVertices,
                                                  whitelistedHyperarcs);
  ASSERT_EQ(subgraph3.getRootHypergraph(), &g2);

  g = g2;
  ASSERT_EQ(subgraph3.getRootHypergraph(), &g2);
  ASSERT_THROW(subgraph2.getRootHypergraph(), std::invalid_argument);
  ASSERT_THROW(subgraph.getRootHypergraph(), std::invalid_argument);
  ASSERT_THROW(subsubgraph.getRootHypergraph(), std::invalid_argument);
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_resetVertexIds) {
  hglib::DirectedHypergraph<> g;
  for (hglib::VertexIdType id = 0; id < 5; ++id) {
    g.addVertex();
  }
  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {2, 3};
  auto subgraph = hglib::DirectedSubHypergraph<>(&g, whitelistedVertices, {});
  whitelistedVertices = {3};
  auto subsubgraph = hglib::DirectedSubHypergraph<>(&subgraph,
                                                    whitelistedVertices, {});
  whitelistedVertices = {1};
  auto subgraph2 = hglib::DirectedSubHypergraph<>(&g, whitelistedVertices, {});

  g.removeVertex(0, false);
  g.removeVertex(4, false);
  g.resetVertexIds();

  for (const auto& vertex : g.vertices()) {
    const auto vertexId = vertex->id();
    switch (vertexId) {
      case 0: {
        ASSERT_EQ(subgraph.vertexById(vertexId), nullptr);
        ASSERT_EQ(subsubgraph.vertexById(vertexId), nullptr);
        ASSERT_EQ(subgraph2.vertexById(vertexId), vertex);
        break;
      }
      case 1: {
        ASSERT_EQ(subgraph.vertexById(vertexId), vertex);
        ASSERT_EQ(subsubgraph.vertexById(vertexId), nullptr);
        ASSERT_EQ(subgraph2.vertexById(vertexId), nullptr);
        break;
      }
      case 2: {
        ASSERT_EQ(subgraph.vertexById(vertexId), vertex);
        ASSERT_EQ(subsubgraph.vertexById(vertexId), vertex);
        ASSERT_EQ(subgraph2.vertexById(vertexId), nullptr);
        break;
      }
    }
  }
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_resetHyperarcIds) {
  hglib::DirectedHypergraph<> g;
  // dummy vertices and hyperarcs
  for (hglib::VertexIdType id = 0; id < 2; ++id) {
    g.addVertex();
  }
  for (hglib::HyperedgeIdType id = 0; id < 5; ++id) {
    g.addHyperarc({{0}, {1}});
  }
  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {0, 1};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {2, 3};
  auto subgraph = hglib::DirectedSubHypergraph<>(&g, whitelistedVertices,
                                                 whitelistedHyperarcs);
  whitelistedHyperarcs = {3};
  auto subsubgraph = hglib::DirectedSubHypergraph<>(&subgraph,
                                                    whitelistedVertices,
                                                    whitelistedHyperarcs);
  whitelistedHyperarcs = {1};
  auto subgraph2 = hglib::DirectedSubHypergraph<>(&g, whitelistedVertices,
                                                  whitelistedHyperarcs);

  g.removeHyperarc(0);
  g.removeHyperarc(4);
  g.resetHyperarcIds();

  for (const auto& hyperarc : g.hyperarcs()) {
    const auto hyperarcId = hyperarc->id();
    switch (hyperarcId) {
      case 0: {
        ASSERT_EQ(subgraph.hyperarcById(hyperarcId), nullptr);
        ASSERT_EQ(subsubgraph.hyperarcById(hyperarcId), nullptr);
        ASSERT_EQ(subgraph2.hyperarcById(hyperarcId), hyperarc);
        break;
      }
      case 1: {
        ASSERT_EQ(subgraph.hyperarcById(hyperarcId), hyperarc);
        ASSERT_EQ(subsubgraph.hyperarcById(hyperarcId), nullptr);
        ASSERT_EQ(subgraph2.hyperarcById(hyperarcId), nullptr);
        break;
      }
      case 2: {
        ASSERT_EQ(subgraph.hyperarcById(hyperarcId), hyperarc);
        ASSERT_EQ(subsubgraph.hyperarcById(hyperarcId), hyperarc);
        ASSERT_EQ(subgraph2.hyperarcById(hyperarcId), nullptr);
        break;
      }
    }
  }
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_vertexProperties) {
  enum Colour {red, blue, green};
  struct VertexProperty {
      std::string label = "Default";
      Colour colour = Colour::green;
  };

  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
      VertexProperty> g;
  // Change the colour of every second vertex
  for (auto vertexName : vertices) {
    const auto& vertex = g.addVertex(vertexName);
    if ((vertex->id() % 2) == 0) {
      g.getVertexProperties_(vertex->id())->colour = Colour::red;
    }
  }
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first);
  }

  // Create (empty) sub-hypergraphs
  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  auto sg = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                               whitelistedHyperarcs);

  auto ssg = hglib::createDirectedSubHypergraph(sg.get(), whitelistedVertices,
                                                whitelistedHyperarcs);

  // Add vertices to subgraph(s)
  for (const auto& vertex : g.vertices()) {
    if ((vertex->id() % 2) == 0) {
      sg->addVertex(vertex->id());
    } else if ((vertex->id() % 3) == 0) {
      ssg->addVertex(vertex->id());
    }
  }

  // Set all vertex colour in the root graph to blue
  for (const auto& vertex : g.vertices()) {
    g.getVertexProperties_(vertex->id())->colour = Colour::blue;
  }

  // Check colour in subgraphs
  for (const auto& vertex : sg->vertices()) {
    if ((vertex->id() % 2) == 0) {
      ASSERT_EQ(sg->getVertexProperties(vertex->id())->colour, Colour::red);
    } else {
      ASSERT_EQ(sg->getVertexProperties(vertex->id())->colour, Colour::green);
    }
  }
  for (const auto& vertex : ssg->vertices()) {
    if ((vertex->id() % 2) == 0) {
      ASSERT_EQ(ssg->getVertexProperties(vertex->id())->colour, Colour::red);
    } else {
      ASSERT_EQ(ssg->getVertexProperties(vertex->id())->colour, Colour::green);
    }
  }

  // Add other vertices to root graph
  const auto& vertex1 = g.addVertex("Vertex1");
  g.getVertexProperties_(vertex1->id())->colour = Colour::blue;
  const auto& lastVertex = g.addVertex("Vertex2");
  // Add the second one to both subgraphs
  sg->addVertex(lastVertex->id());
  // ... and check properties
  // property is copied from parent
  ASSERT_EQ(sg->getVertexProperties(lastVertex->id())->colour, Colour::green);
  // Set to red in subgraph
  sg->getVertexProperties_(lastVertex->id())->colour = Colour::red;
  ASSERT_EQ(sg->getVertexProperties(lastVertex->id())->colour, Colour::red);
  // Changed only in subgraph
  ASSERT_EQ(g.getVertexProperties(lastVertex->id())->colour, Colour::green);
  // Copy property from subgraph
  ssg->addVertex(lastVertex->id());
  ASSERT_EQ(ssg->getVertexProperties(lastVertex->id())->colour, Colour::red);

  // Test exceptions
  ASSERT_THROW(sg->getVertexProperties(1), std::invalid_argument);
  ASSERT_THROW(sg->getVertexProperties_(1), std::invalid_argument);
  ASSERT_THROW(ssg->getVertexProperties(1), std::invalid_argument);
  ASSERT_THROW(ssg->getVertexProperties_(1), std::invalid_argument);

  ASSERT_THROW(sg->getVertexProperties(100), std::out_of_range);
  ASSERT_THROW(sg->getVertexProperties_(100), std::out_of_range);
  ASSERT_THROW(ssg->getVertexProperties(100), std::out_of_range);
  ASSERT_THROW(ssg->getVertexProperties_(100), std::out_of_range);

  // remove vertices
  ASSERT_NO_THROW(sg->getVertexProperties(3));
  ASSERT_NO_THROW(sg->getVertexProperties_(3));
  ASSERT_NO_THROW(ssg->getVertexProperties(3));
  ASSERT_NO_THROW(ssg->getVertexProperties_(3));
  g.removeVertex(3, false);
  ASSERT_THROW(sg->getVertexProperties(3), std::invalid_argument);
  ASSERT_THROW(sg->getVertexProperties_(3), std::invalid_argument);
  ASSERT_THROW(ssg->getVertexProperties(3), std::invalid_argument);
  ASSERT_THROW(ssg->getVertexProperties_(3), std::invalid_argument);

  // reset vertex ids
  g.removeVertex(0, false);
  // change colours of vertices in subgraphs
  sg->getVertexProperties_(2)->colour = Colour::red;
  sg->getVertexProperties_(4)->colour = Colour::blue;
  sg->getVertexProperties_(7)->colour = Colour::green;
  ssg->getVertexProperties_(7)->colour = Colour::red;
  g.resetVertexIds();
  // Check vertex colours in subgraphs
  int idx = 0;
  for (const auto& vertex : sg->vertices()) {
    // red = 0, blue = 1, green = 2
    ASSERT_EQ(sg->getVertexProperties(vertex->id())->colour, idx);
    ++idx;
  }
  // Vertex with Id=7 became ID=5
  ASSERT_EQ(ssg->getVertexProperties(5)->colour, Colour::red);
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_hyperarcProperties) {
  enum Colour {red, blue, green};
  struct HyperarcProperty {
      std::string label = "Default";
      Colour colour = Colour::green;
  };

  hglib::DirectedHypergraph<DirectedVertex, DirectedHyperedge,
      hglib::emptyProperty, HyperarcProperty> g;
  for (hglib::VertexIdType id = 0; id < 2; ++id) {
    g.addVertex();
  }
  for (hglib::HyperedgeIdType id = 0; id < 6; ++id) {
    const auto& arc = g.addHyperarc({{0}, {1}});
    // Change colour of every second hyperarc
    if ((arc->id() % 2) == 0) {
      g.getHyperarcProperties_(arc->id())->colour = Colour::red;
    }
  }

  // Create (empty) sub-hypergraphs
  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {0, 1};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {};
  auto sg = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                               whitelistedHyperarcs);

  auto ssg = hglib::createDirectedSubHypergraph(sg.get(), whitelistedVertices,
                                                whitelistedHyperarcs);

  // Add hyperarcs to subgraph(s)
  for (const auto& arc : g.hyperarcs()) {
    if ((arc->id() % 2) == 0) {
      sg->addHyperarc(arc->id());
    } else if ((arc->id() % 3) == 0) {
      ssg->addHyperarc(arc->id());
    }
  }

  // Set all hyperarc colour in the root graph to blue
  for (const auto& arc : g.hyperarcs()) {
    g.getHyperarcProperties_(arc->id())->colour = Colour::blue;
  }

  // Check colour in subgraphs
  for (const auto& arc : sg->hyperarcs()) {
    if ((arc->id() % 2) == 0) {
      ASSERT_EQ(sg->getHyperarcProperties(arc->id())->colour, Colour::red);
    } else {
      ASSERT_EQ(sg->getHyperarcProperties(arc->id())->colour, Colour::green);
    }
  }
  for (const auto& arc : ssg->hyperarcs()) {
    if ((arc->id() % 2) == 0) {
      ASSERT_EQ(ssg->getHyperarcProperties(arc->id())->colour, Colour::red);
    } else {
      ASSERT_EQ(ssg->getHyperarcProperties(arc->id())->colour,
                Colour::green);
    }
  }

  // Add other hyperarcs to root graph
  const auto& arc1 = g.addHyperarc({{0}, {1}});
  g.getHyperarcProperties_(arc1->id())->colour = Colour::blue;
  const auto& lastHyperarc = g.addHyperarc({{0}, {1}});
  // Add the second one to both subgraphs
  sg->addHyperarc(lastHyperarc->id());
  // ... and check properties
  // property is copied from parent
  ASSERT_EQ(sg->getHyperarcProperties(lastHyperarc->id())->colour,
            Colour::green);
  // Set to red in subgraph
  sg->getHyperarcProperties_(lastHyperarc->id())->colour = Colour::red;
  ASSERT_EQ(sg->getHyperarcProperties(lastHyperarc->id())->colour, Colour::red);
  // Changed only in subgraph
  ASSERT_EQ(g.getHyperarcProperties(lastHyperarc->id())->colour, Colour::green);
  // Copy property from subgraph
  ssg->addHyperarc(lastHyperarc->id());
  ASSERT_EQ(ssg->getHyperarcProperties(lastHyperarc->id())->colour,
            Colour::red);

  // Test exceptions
  ASSERT_THROW(sg->getHyperarcProperties(1), std::invalid_argument);
  ASSERT_THROW(sg->getHyperarcProperties_(1), std::invalid_argument);
  ASSERT_THROW(ssg->getHyperarcProperties(1), std::invalid_argument);
  ASSERT_THROW(ssg->getHyperarcProperties_(1), std::invalid_argument);

  ASSERT_THROW(sg->getHyperarcProperties(100), std::out_of_range);
  ASSERT_THROW(sg->getHyperarcProperties_(100), std::out_of_range);
  ASSERT_THROW(ssg->getHyperarcProperties(100), std::out_of_range);
  ASSERT_THROW(ssg->getHyperarcProperties_(100), std::out_of_range);

  // remove vertices
  ASSERT_NO_THROW(sg->getHyperarcProperties(3));
  ASSERT_NO_THROW(sg->getHyperarcProperties_(3));
  ASSERT_NO_THROW(ssg->getHyperarcProperties(3));
  ASSERT_NO_THROW(ssg->getHyperarcProperties_(3));
  g.removeHyperarc(3);
  ASSERT_THROW(sg->getHyperarcProperties(3), std::invalid_argument);
  ASSERT_THROW(sg->getHyperarcProperties_(3), std::invalid_argument);
  ASSERT_THROW(ssg->getHyperarcProperties(3), std::invalid_argument);
  ASSERT_THROW(ssg->getHyperarcProperties_(3), std::invalid_argument);

  // reset hyperarc ids
  g.removeHyperarc(0);
  // change colours of hyperarcs in subgraphs
  sg->getHyperarcProperties_(2)->colour = Colour::red;
  sg->getHyperarcProperties_(4)->colour = Colour::blue;
  sg->getHyperarcProperties_(7)->colour = Colour::green;
  ssg->getHyperarcProperties_(7)->colour = Colour::red;
  g.resetHyperarcIds();
  // Check hyperarc colours in subgraphs
  int idx = 0;
  for (const auto& arc : sg->hyperarcs()) {
    // red = 0, blue = 1, green = 2
    ASSERT_EQ(sg->getHyperarcProperties(arc->id())->colour, idx);
    ++idx;
  }
  // Hyperarc with Id=7 became ID=5
  ASSERT_EQ(ssg->getHyperarcProperties(5)->colour, Colour::red);
}

TEST_F(TestDirectedSubHyperGraph, Test_subgraph_print) {
  // TODO(mw): Print only those tails/heads of a hyperarc in a subgraph
  // that are visible in the subgraph
}


TEST_F(TestDirectedSubHyperGraph, Test_observer) {
  hglib::DirectedHypergraph<DirectedVertex, NamedDirectedHyperedge> g, g2;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
    g2.addVertex(vertexName);
  }

  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
    g2.addHyperarc(hglib::NAME, hyperarc.first, hyperarc.second);
  }

  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {0, 1, 2, 3, 5};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {0, 1, 2};
  auto subgraph = hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge>(&g, whitelistedVertices,
                                  whitelistedHyperarcs);
  auto subgraph2 = hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge>(&g, whitelistedVertices,
                                  whitelistedHyperarcs);
  whitelistedVertices = {0, 1, 2};
  whitelistedHyperarcs = {0, 1};
  auto subsubgraph = hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge>(&subgraph, whitelistedVertices,
                                  whitelistedHyperarcs);
  auto subsubgraph2 = hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge>(&subgraph2, whitelistedVertices,
                         whitelistedHyperarcs);

  size_t nbVertices(g.nbVertices());
  size_t nbHyperarcs(g.nbHyperarcs());
  size_t nbVerticesSubgraph(subgraph.nbVertices());
  size_t nbHyperarcsSubgraph(subgraph.nbHyperarcs());
  size_t nbVerticesSubgraph2(subgraph2.nbVertices());
  size_t nbHyperarcsSubgraph2(subgraph2.nbHyperarcs());
  size_t nbVerticesSubSubgraph(subsubgraph.nbVertices());
  size_t nbHyperarcsSubSubgraph(subsubgraph.nbHyperarcs());
  size_t nbVerticesSubSubgraph2(subsubgraph2.nbVertices());
  size_t nbHyperarcsSubSubgraph2(subsubgraph2.nbHyperarcs());

  // Nothing happens to the (grand-)parent graphs on removal of a
  // vertex/hyperarc from a graph
  subsubgraph.removeVertex(0, false);
  subsubgraph.removeHyperarc(0);
  ASSERT_EQ(nbVertices, g.nbVertices());
  ASSERT_EQ(nbHyperarcs, g.nbHyperarcs());
  ASSERT_EQ(nbVerticesSubgraph, subgraph.nbVertices());
  ASSERT_EQ(nbHyperarcsSubgraph, subgraph.nbHyperarcs());
  ASSERT_EQ(nbVerticesSubgraph2, subgraph2.nbVertices());
  ASSERT_EQ(nbHyperarcsSubgraph2, subgraph2.nbHyperarcs());
  ASSERT_EQ(--nbVerticesSubSubgraph, subsubgraph.nbVertices());
  ASSERT_EQ(--nbHyperarcsSubSubgraph, subsubgraph.nbHyperarcs());
  ASSERT_EQ(nbVerticesSubSubgraph2, subsubgraph2.nbVertices());
  ASSERT_EQ(nbHyperarcsSubSubgraph2, subsubgraph2.nbHyperarcs());

  // Changes are delegated recursively to observers
  g.removeVertex(1, false);
  g.removeHyperarc(1);
  ASSERT_EQ(--nbVertices, g.nbVertices());
  ASSERT_EQ(--nbHyperarcs, g.nbHyperarcs());
  ASSERT_EQ(--nbVerticesSubgraph, subgraph.nbVertices());
  ASSERT_EQ(--nbHyperarcsSubgraph, subgraph.nbHyperarcs());
  ASSERT_EQ(--nbVerticesSubgraph2, subgraph2.nbVertices());
  ASSERT_EQ(--nbHyperarcsSubgraph2, subgraph2.nbHyperarcs());
  ASSERT_EQ(--nbVerticesSubSubgraph, subsubgraph.nbVertices());
  ASSERT_EQ(--nbHyperarcsSubSubgraph, subsubgraph.nbHyperarcs());
  ASSERT_EQ(--nbVerticesSubSubgraph2, subsubgraph2.nbVertices());
  ASSERT_EQ(--nbHyperarcsSubSubgraph2, subsubgraph2.nbHyperarcs());

  // Make subgraph a sub-hypergraph of g2
  whitelistedVertices = {0, 1, 2, 3, 5};
  whitelistedHyperarcs = {0, 1, 2};
  subgraph = hglib::DirectedSubHypergraph<DirectedVertex,
          NamedDirectedHyperedge>(&g2, whitelistedVertices,
                         whitelistedHyperarcs);
  nbVertices = g2.nbVertices();
  nbHyperarcs = g2.nbHyperarcs();
  nbVerticesSubgraph = subgraph.nbVertices();
  nbHyperarcsSubgraph = subgraph.nbHyperarcs();
  g2.removeVertex(0);
  ASSERT_EQ(--nbVertices, g2.nbVertices());
  ASSERT_EQ(nbHyperarcs - 2, g2.nbHyperarcs());
  ASSERT_EQ(--nbVerticesSubgraph, subgraph.nbVertices());
  ASSERT_EQ(nbHyperarcsSubgraph - 2, subgraph.nbHyperarcs());
}

TEST_F(TestDirectedSubHyperGraph, Test_getAncestors) {
  hglib::DirectedHypergraph<> g;
  for (auto vertexName : vertices) {
    g.addVertex(vertexName);
  }
  // Add hyperarcs twice (but with different ids)
  for (auto hyperarc : hyperarcs) {
    g.addHyperarc(hglib::NAME, hyperarc.first);
  }

  std::unordered_set<hglib::VertexIdType> whitelistedVertices = {1, 2, 3};
  std::unordered_set<hglib::HyperedgeIdType> whitelistedHyperarcs = {0, 1};
  auto sg = hglib::createDirectedSubHypergraph(&g, whitelistedVertices,
                                               whitelistedHyperarcs);

  whitelistedVertices = {1, 2};
  whitelistedHyperarcs = {1};
  auto ssg = hglib::createDirectedSubHypergraph(sg.get(), whitelistedVertices,
                                                 whitelistedHyperarcs);
  whitelistedVertices = {1};
  whitelistedHyperarcs = {1};
  auto sssg = hglib::createDirectedSubHypergraph(ssg.get(),
                                                 whitelistedVertices,
                                                 whitelistedHyperarcs);

  std::vector<hglib::HypergraphInterface<DirectedVertex, DirectedHyperedge,
      hglib::emptyProperty, hglib::emptyProperty,
      hglib::emptyProperty>*> hierarchy = {ssg.get(), sg.get(), &g};

  std::vector<hglib::HypergraphInterface<DirectedVertex, DirectedHyperedge,
      hglib::emptyProperty, hglib::emptyProperty,
      hglib::emptyProperty>*> ancestors;
  g.getAncestors(&ancestors);
  ASSERT_EQ(ancestors.size(), (size_t) 0);
  sg->getAncestors(&ancestors);
  ASSERT_EQ(ancestors.size(), (size_t) 1);
  auto it = hierarchy.cbegin() + 2;
  for (const auto& ancestor : ancestors) {
    ASSERT_EQ(ancestor, *it);
    ++it;
  }
  ancestors.clear();
  ASSERT_EQ(ancestors.size(), (size_t) 0);
  ssg->getAncestors(&ancestors);
  ASSERT_EQ(ancestors.size(), (size_t) 2);
  it = hierarchy.cbegin() + 1;
  for (const auto& ancestor : ancestors) {
    ASSERT_EQ(ancestor, *it);
    ++it;
  }
  ancestors.clear();
  ASSERT_EQ(ancestors.size(), (size_t) 0);
  sssg->getAncestors(&ancestors);
  ASSERT_EQ(ancestors.size(), (size_t) 3);
  it = hierarchy.cbegin();
  for (const auto& ancestor : ancestors) {
    ASSERT_EQ(ancestor, *it);
    ++it;
  }
  ancestors.clear();
  ASSERT_EQ(ancestors.size(), (size_t) 0);
}
