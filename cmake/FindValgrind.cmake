#
# - Try to find Valgrind
#
# Once done this will define
#
#   VALGRIND_FOUND - system has Valgrind
#   VALGRIND_PROGRAM, the valgrind executable.
#
find_program(VALGRIND_PROGRAM
             NAMES valgrind
             PATHS /usr/bin
                   /usr/local/bin
                   /builds/bin)

include(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Valgrind DEFAULT_MSG VALGRIND_PROGRAM)
